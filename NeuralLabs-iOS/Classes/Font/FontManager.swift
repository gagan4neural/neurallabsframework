//
//  FontManager.swift
//  NeuralLabs
//
//  Created by Gagan on 23/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

fileprivate let iconFontFileName = "Material-Design-Iconic-Font"
fileprivate let helveticaNeueFontName = "Helvetica Neue"

open class FontManager {
    
    public static let shared = FontManager();
    
    private var appFonts : [UIFont?] = [UIFont?]();
    lazy var systemFonts: [String] = UIFont.familyNames;

    public func iconFont () -> UIFont?{
        if let font = self.getFontFor(size: nlUtils.shared.iconFontSize(), family: iconFontFileName) {
            return font;
        }
        return nil;
    }
    
    public func iconFont (size : CGFloat) -> UIFont? {
        if let font = self.getFontFor(size: size, family: iconFontFileName) {
            return font;
        }
        return nil;
    }
    
    public func helveticaNeueFont (size : CGFloat) -> UIFont? {
        if let font = self.getFontFor(size: size, family: helveticaNeueFontName) {
            return font;
        }
        return nil;
    }

    
    private func getFontFor (size:CGFloat, family:String) -> UIFont?{
        for font:UIFont? in appFonts {
            if ((size == font?.pointSize) && (font?.fontName == family)){
                return font;
            }
        }
        
        let font : UIFont? = UIFont(name: family, size: size);
        appFonts.append(font);

        return font;
    }
    
    public func badgeFont() -> UIFont? {
        let font: UIFont? = helveticaNeueThin((nlUtils.shared.isIpad() ? 20 : 10))
        return font
    }
    
    public func helveticaNeue(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue"
        let font = UIFont(name: fontName, size: size)
        return font
    }
    
    public func helveticaNeueThin(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue-Thin"
        let font = UIFont(name: fontName, size: size)
        return font
    }

    public func helveticaNeueLight(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue-Light"
        let font = UIFont(name: fontName, size: size)
        return font
    }
    
    public func helveticaNeueMedium(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue-Medium"
        let font = UIFont(name: fontName, size: size)
        return font
    }
    
    public func helveticaNeueItalic(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue-Italic"
        
        var font = UIFont(name: fontName, size: size)
        if font == nil {
            font = UIFont(name: ".HelveticaNeue-ItalicM3", size: size)
        }
        return font
    }
    
    public func helveticaNeueBold(_ size: CGFloat) -> UIFont? {
        let fontName = "HelveticaNeue-Bold"
        let font = UIFont(name: fontName, size: size)
        return font
    }

}
