//
//  NeuralTheming.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 19/05/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


class NeuralTheming {
    
    public static let shared : NeuralTheming = NeuralTheming();
    static let kApplicationTheming = "ApplicationBackground"
    static let kApplicationBackgroundImage = "ApplicationBackgroundImage"
    static let kApplicationBackgroundColor = "ApplicationBackgroundColor"

    var backgroundImagePath: String? {
        get{
            return self.getApplicationBackgroundImage();
        }set{
            self.setApplicationBackgroundImage(path: newValue);
        }
    }
    
    var backgroundImage: UIImage? {
        get{
            if let path = self.backgroundImagePath {
                let image = UIImage(contentsOfFile: path);
                if (image != nil){
                    return image;
                }
            }
            let imageName = BundleManager.applicationImageName();
            if let dirPath = nlUtils.shared.applicationSupportDirectory(){
                let filePath = dirPath.appendPath(component: imageName);
                if (nlUtils.shared.write(image: UIImage(named: imageName)!, atPath: filePath)){
                    self.backgroundImagePath = filePath;
                }
            }
            return UIImage(named: imageName);
        }
    }
    
    func getApplicationBackgroundImage () -> String? {
        // returns the path for application background images.
        let theming = NeuralDefaults.getSettings(NeuralTheming.kApplicationTheming) as? [AnyHashable:Any];
        if let tDict = theming {
            let background = tDict[NeuralTheming.kApplicationBackgroundImage] as? String
            return background;
        }
        return nil;
    }
    
    func setApplicationBackgroundImage(path: String?){
        
        var theming = NeuralDefaults.getSettings(NeuralTheming.kApplicationTheming) as? [AnyHashable:Any];
        if (theming == nil){
            theming = [:];
        }
        theming![NeuralTheming.kApplicationBackgroundImage] = path;
        NeuralDefaults.setSettings(NeuralTheming.kApplicationTheming, value: theming!);
    }
}
