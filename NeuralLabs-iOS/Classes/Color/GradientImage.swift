//
//  File.swift
//  ColorUtils
//
//  Created by Gagan on 14/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

enum GradientType {
    case Linear_Vertical
    case Linear_Horizontal
    case Radial
}

class GradientImage {
    
    struct GradientImageSize {
        let width : Double;
        let height : Double;
        
        func isEqual (size : GradientImageSize) -> Bool{
            return (self.width == size.width) ? (self.height == size.height) : false;
        }
    }
    
    let gradImgSize : GradientImageSize;
    var gradImage : UIImage? = nil;
    let gradType : GradientType;
    var gradColors : [UIColor] = [UIColor]();
    
    init (size:CGSize, image:UIImage, type:GradientType) {
        gradImgSize = GradientImageSize(width: Double(size.width), height: Double(size.height));
        gradImage = image;
        gradType = type;
    }

    init (size:CGSize, colors:[UIColor], type:GradientType) {
        gradImgSize = GradientImageSize(width: Double(size.width), height: Double(size.height));
        gradColors.insert(contentsOf: colors, at: 0);
        gradType = type;
    }
    
    convenience init (size:CGSize, colors:[UIColor], type:GradientType, drawGradient:Bool) {
        self.init(size: size, colors: colors, type: type);
        if (drawGradient == true) {
            self.drawGradient();
        }
    }

    func isEqual (obj : GradientImage) -> Bool{
        return obj.gradImgSize.isEqual(size: self.gradImgSize) ? (obj.gradType == self.gradType) : false;
    }
    
    func drawGradient () {
        
        let scale = UIScreen.main.scale;
        let imgSize = CGSize(width: Double(scale)*self.gradImgSize.width, height: Double(scale)*self.gradImgSize.height);
        var redGrad:[Double] = [Double]();
        var greenGrad:[Double] = [Double]();
        var blueGrad:[Double] = [Double]();
        var alphaGrad:[Double] = [Double]();
        var allColors:[CGFloat] = [CGFloat]();
        var locations:[CGFloat] = [CGFloat]();
        
        UIGraphicsBeginImageContext(imgSize);
        let context: CGContext? = UIGraphicsGetCurrentContext();
        
        for color in self.gradColors {
            
            var red : CGFloat = 0
            var green : CGFloat = 0
            var blue : CGFloat = 0
            var alpha : CGFloat = 0
            
            color.getRed(&red, green: &green, blue: &blue, alpha: &alpha);

            redGrad.append(Double(red));
            greenGrad.append(Double(green));
            blueGrad.append(Double(blue));
            alphaGrad.append(Double(alpha));
            
            allColors.append(red);
            allColors.append(green);
            allColors.append(blue);
        }

        let colorSpace : CGColorSpace = CGColorSpaceCreateDeviceRGB()
        let num_locations : size_t = self.gradColors.count;
        
        for i in 0..<num_locations {
            locations.append(CGFloat(i)/CGFloat(num_locations-1))
        }

        let gradient : CGGradient? = CGGradient(colorSpace: colorSpace,
                                                colorComponents: &allColors,
                                                locations: &locations,
                                                count: num_locations)
        let startPoint = CGPoint(x: imgSize.width * 0.5, y: 0)
        let endPoint = CGPoint(x: imgSize.width * 0.5, y: imgSize.height)

        context!.saveGState();
        
        context!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: .drawsBeforeStartLocation);
        context!.strokePath();
        context!.restoreGState();

        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.gradImage = image;
    }
}
