//
//  ColorUtil.swift
//  ColorUtils
//
//  Created by Gagan on 09/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

fileprivate let kDefault : String = "Default"
fileprivate let kTextColor : String = "TextColor"
fileprivate let kHighlighted : String = "Highlighted"
fileprivate let kBorder : String = "Border"
fileprivate let kDoneButton : String = "DoneButton"
fileprivate let kCancelButton : String = "CancelButton"
fileprivate let kWarning_Normal : String = "Warning.Normal"
fileprivate let kWarning_Highlighted : String = "Warning.Highlighted"
fileprivate let kWarning_Selected : String = "Warning.Selected"
fileprivate let kSegmentedBar_Normal : String = "SegmentedBar.Normal"
fileprivate let kSegmentedBar_Highlighted : String = "SegmentedBar.Highlighted"
fileprivate let kSegmentedBar_Selected : String = "SegmentedBar.Selected"
fileprivate let kBG_Grad_Color_1 : String = "BG-Grad-Color-1"
fileprivate let kBG_Grad_Color_2 : String = "BG-Grad-Color-2"
fileprivate let kShadow_Title : String = "Shadow.Title"
fileprivate let kShadow_ListCell : String = "Shadow.ListCell"
fileprivate let kShadowRadius_Title : String = "Shadow-Radius.Title"
fileprivate let kShadowRadius_ListCell : String = "Shadow-Radius.ListCell"
fileprivate let kTitleBarColor : String = "TitleBarColor"
fileprivate let kRemoveAdsBG : String = "RemoveAdsBG"
fileprivate let kRemoveAdsBG_Highlighted : String = "RemoveAdsBG-Highlighted"
fileprivate let kThumbNail : String = "ThumbNail"
fileprivate let kBadge : String = "Badge"
fileprivate let kSeperator : String = "Seperator"
fileprivate let kSlider_Background : String = "Slider.Background"
fileprivate let kSliderThumb_Normal : String = "Slider.Thumb.Normal"
fileprivate let kSliderThumb_Highlighted : String = "Slider.Thumb.Highlighted"
fileprivate let kSliderThumb_Selected : String = "Slider.Thumb.Selected"

fileprivate let kBackground_Default : String = "Background.Default"
fileprivate let kBackground_ListCell : String = "Background.ListCell"
fileprivate let kBackground_GridCell : String = "Background.GridCell"
fileprivate let kBackground_LockButton : String = "Background.LockButton"

fileprivate let kFeature_BackgroundColor : String = "Features.BackgroundColor"
fileprivate let kFeature_ShadowColor : String = "Features.Shadow.Color"
fileprivate let kFeature_ShadowRadius : String = "Features.Shadow.Radius"
fileprivate let kFeature_TextColor : String = "Features.TextColor"

fileprivate let kBundle_Application_Theme = "Application Theme"



enum ColorMode{
    case Normal;
    case Highlighted;
    case Selected;
}

public enum BackgroundColorType{
    case Default;
    case ListCell;
    case GridCell;
    case LockButton;
}

public enum ShadowType{
    case Title;
    case ListCell;
}

public enum FeatureType{
    case TextColor;
    case Background;
    case ShadowColor;
}

public enum MediaPlayerTheme: String {
    case VolumeLevel = "MediaPlayerTheme.VolumeLevel";
    case BrightnessLevel = "MediaPlayerTheme.BrightnessLevel";
    case VolBrtKnob = "MediaPlayerTheme.VolBrtKnob";
}

public class ColorUtils {
    
    private static var shared : ColorUtils? = nil;
    public static func instance () -> ColorUtils{
        if (ColorUtils.shared == nil){
            ColorUtils.shared = ColorUtils();
        }
        return (ColorUtils.shared)!;
    }
    
    init() {
        themeDictionary = NSMutableDictionary();
        self.updateTheme();
    }
    
    private let gradManager : GradientManager = GradientManager();
    private var themeDictionary : NSMutableDictionary;
    private var themeFileName : String {
        get{
            var retVal : String = "CT0";
            
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist"){
                if let nsDict = NSDictionary(contentsOfFile: path){
                    retVal = nsDict.value(forKey: kBundle_Application_Theme) as! String;
                }
            }
            return retVal;
        }
    }
    private let themeFileType : String = "plist"
    private let nilColor : UIColor = UIColor.white;
    
    func updateTheme () {
        let thisBundle = Bundle(identifier: "com.neural.NeuralLabs-iOS");
        if let path = thisBundle?.path(forResource: self.themeFileName, ofType: self.themeFileType) {
            self.themeDictionary = NSMutableDictionary(contentsOfFile: path) ?? NSMutableDictionary();
        }
    }
    
    
    
    //MARK: Colors
    private func colorForKey (key : String) -> UIColor {
        let hexStr:String? = self.themeDictionary[key] as? String
        if (hexStr != nil){
            return hexStr!.rgbaColor();
        }
        return self.nilColor;
    }
    private func colorForKeyPath (keyPath : String) -> UIColor {
        
        let hexStr:String? = self.themeDictionary.value(forKeyPath: keyPath) as? String;
        if (hexStr != nil){
            return hexStr!.rgbaColor();
        }
        return self.nilColor;
    }
    public var defaultColor : UIColor {
        get{
            return self.colorForKey(key: kDefault);
        }
    }
    public var textColor : UIColor {
        get{
            return self.colorForKey(key: kTextColor);
        }
    }
    public var highlightedColor : UIColor {
        get{
            return self.colorForKey(key: kHighlighted);
        }
    }
    public var borderColor : UIColor {
        get{
            return self.colorForKey(key: kBorder);
        }
    }
    public var doneButtonColor : UIColor {
        get{
            return self.colorForKey(key: kDoneButton);
        }
    }
    public var cancelButtonColor : UIColor {
        get{
            return self.colorForKey(key: kCancelButton);
        }
    }
    func warningColor (mode:ColorMode) -> UIColor {
        switch mode {
        case .Normal:
            return self.colorForKeyPath(keyPath: kWarning_Normal);
        case .Highlighted:
            return self.colorForKeyPath(keyPath: kWarning_Highlighted);
        case .Selected:
            return self.colorForKeyPath(keyPath: kWarning_Selected);
        }
    }
    func segmentedBarColor (mode:ColorMode) -> UIColor {
        switch mode {
        case .Normal:
            return self.colorForKeyPath(keyPath: kSegmentedBar_Normal);
        case .Highlighted:
            return self.colorForKeyPath(keyPath: kSegmentedBar_Highlighted);
        case .Selected:
            return self.colorForKeyPath(keyPath: kSegmentedBar_Selected);
        }
    }
    func sliderThumb (mode:ColorMode) -> UIColor {
        switch mode {
        case .Normal:
            return self.colorForKeyPath(keyPath: kSliderThumb_Normal);
        case .Highlighted:
            return self.colorForKeyPath(keyPath: kSliderThumb_Highlighted);
        case .Selected:
            return self.colorForKeyPath(keyPath: kSliderThumb_Selected);
        }
    }
    
    public func backgroundColor (type:BackgroundColorType) -> UIColor{
        switch type {
        case .Default:
            return self.colorForKeyPath(keyPath: kBackground_Default);
        case .ListCell:
            return self.colorForKeyPath(keyPath: kBackground_ListCell);
        case .GridCell:
            return self.colorForKeyPath(keyPath: kBackground_GridCell);
        case .LockButton:
            return self.colorForKeyPath(keyPath: kBackground_LockButton);
        }
    }

    public func featureColor (type:FeatureType) -> UIColor{
        switch type {
        case .Background:
            return self.colorForKeyPath(keyPath: kFeature_BackgroundColor);
        case .ShadowColor:
            return self.colorForKeyPath(keyPath: kFeature_ShadowColor);
        case .TextColor:
            return self.colorForKeyPath(keyPath: kFeature_TextColor);
        }
    }

    public func featureShadowRadius () -> CGFloat{
        let key:String? = kFeature_ShadowRadius;
        if let str:NSNumber = self.themeDictionary[key!] as? NSNumber{
            return CGFloat(str.floatValue);
        }
        return 1;
    }
    
    private var sliderBackground : UIColor {
        get{
            return self.colorForKey(key: kSlider_Background);
        }
    }
    
    private var bgGradColor1 : UIColor {
        get{
            return self.colorForKey(key: kBG_Grad_Color_1);
        }
    }
    private var bgGradColor2 : UIColor {
        get{
            return self.colorForKey(key: kBG_Grad_Color_2);
        }
    }
    public func shadowColor (_ type: ShadowType) -> UIColor{
        switch type {
        case .Title:
            return self.colorForKeyPath(keyPath: kShadow_Title);
        case .ListCell:
            return self.colorForKeyPath(keyPath: kShadow_ListCell);
        }
    }
    public func shadowRadius (_ type: ShadowType) -> CGFloat{
        var key:String? = nil;
        switch type {
        case .Title:
            key = kShadowRadius_Title;
            break;
        case .ListCell:
            key = kShadowRadius_ListCell;
            break;
        }
        
        if let str:NSNumber = self.themeDictionary[key!] as? NSNumber{
            return CGFloat(str.floatValue);
        }
        return 1;
    }
    public func mediaPlayerTheme(_ type:MediaPlayerTheme) -> UIColor {
        return self.colorForKeyPath(keyPath: type.rawValue)
    }
    public var titleBarColor : UIColor {
        get{
            return self.colorForKey(key: kTitleBarColor);
        }
    }
    public var removeAdsBG : UIColor {
        get{
            return self.colorForKey(key: kRemoveAdsBG);
        }
    }
    public var removeAdsBGHighlighted : UIColor {
        get{
            return self.colorForKey(key: kRemoveAdsBG_Highlighted);
        }
    }
    public var thumbNail : UIColor {
        get{
            return self.colorForKey(key: kThumbNail);
        }
    }
    public var badge : UIColor {
        get{
            return self.colorForKey(key: kBadge);
        }
    }
    public var seperator : UIColor {
        get{
            return self.colorForKey(key: kSeperator);
        }
    }

    
    
    //MARK: Gradients
    open func bgGradientImage (size : CGSize) -> UIImage? {
        let grad1 : UIColor = self.bgGradColor1
        let grad2 : UIColor = self.bgGradColor2
        return self.gradientImage(size: size, colors: [grad1,grad2], gradType: .Linear_Vertical);
    }
    
    private func isGradientParamsCorrect (size:CGSize, colors:[UIColor], gradType:GradientType) -> Bool{
        return GradientManager.doesGradParamsCorrect (size:size, colors:colors);
    }
    
    private func gradientImage (size : CGSize, colors : [UIColor], gradType:GradientType) -> UIImage? {
        
        if (self.isGradientParamsCorrect(size: size,
                                         colors: colors,
                                         gradType: gradType) == false) {
            // Illegal params supplied
            return nil;
        }
        
        return self.gradManager.imageFor(size: size, colors: colors, gradType: gradType);
    }
}
