//
//  GradientManager.swift
//  ColorUtils
//
//  Created by Gagan on 14/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class GradientManager {

    private var gradImages:[GradientImage] = [GradientImage]();
    
    static func doesGradParamsCorrect (size:CGSize, colors:[UIColor]) -> Bool {
        if ((size.width <= 0) || (size.height <= 0)) {
            // cannot process with zero size
            return false;
        }
        else if (colors.count <= 1) {
            // cannot process no colors or with single color
            return false;
        }
        return true;
    }
    
    func doesGradExist (size:CGSize, colors:[UIColor], gradType:GradientType) -> Bool {
        return ((self.gradImageFor(size: size, colors: colors, gradType: gradType)) != nil) ? true : false;
    }
    
    func gradImageFor(size:CGSize, colors:[UIColor], gradType:GradientType) -> GradientImage? {
        
        let cmpGrad = GradientImage(size: size, colors: colors, type: gradType);
        
        for grad in self.gradImages {
            if (grad.isEqual(obj: cmpGrad)) {
                return grad;
            }
        }
        return nil;
    }
    
    func imageFor(size:CGSize, colors:[UIColor], gradType:GradientType) -> UIImage? {
        
        if let image = self.gradImageFor(size: size, colors: colors, gradType: gradType)?.gradImage {
            return image;
        }
        
        let gradObj = GradientImage(size: size, colors: colors, type: gradType, drawGradient: true);
        if (gradObj.gradImage != nil){
            self.gradImages.append(gradObj);
            return gradObj.gradImage;
        }
        
        return nil;
    }
    
}
