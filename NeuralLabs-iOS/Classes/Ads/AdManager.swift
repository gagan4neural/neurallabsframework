//
//  AdManager.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

let defaultInterstetialFrequency: Int = 1;
let defaultVideoFrequency: Int = 1;

enum AdProviderType {
    case NoAds;
    case AdMob;
    case StartApp;
    case Unity;
    case Flurry;
    case Facebook;
}

enum AdType {
    case Banner;
    case Interstetial;
    case Video;
    case InterVideo; // Toggle Interstetial and Video Ads
}

@objc protocol AdServiceProtocol {
    @objc optional func adViewReceiveBannerAds(view : UIView, viewController: UIViewController);
    @objc optional func adViewReceiveInterstetialAds(view : UIView, viewController: UIViewController);
    @objc optional func adViewErrorInReceivingAds(view: UIView, error: Error);
    @objc optional func adViewDismissInterstetialAdForViewController(viewController:UIViewController);
}

open class AdManager {

    // Firebase Settings
    struct Firebase {
        
        var dictionary : Dictionary<AnyHashable, String> = Dictionary<AnyHashable, String>();
        var interstetialFrequency : Int{
            get{
                return FirebaseManager.shared.getInterstetialFrequency();
            }
        }
        var videoFrequency : Int{
            get{
                return FirebaseManager.shared.getVideoFrequency();
            }
        }
        var adsAppId: String? {
            get{
                return FirebaseManager.shared.getAdsAppId();
            }
        }
        var bannerId: String? {
            get{
                return FirebaseManager.shared.getAdId(.Banner);
            }
        }
        var interstetialId: String? {
            get{
                return FirebaseManager.shared.getAdId(.Interstetial);
            }
        }
        var videoId: String? {
            get{
                return FirebaseManager.shared.getAdId(.Video);
            }
        }
        var interVideo: String? {
            get{
                return FirebaseManager.shared.getAdId(.InterVideo);
            }
        }
        var adProviderType: AdProviderType {
            get{
                return FirebaseManager.shared.adProviderType();
            }
        }
        var isFreeApp:Bool {
            get{
                return FirebaseManager.shared.isFreeApp();
            }
        }
        var removeAdId: String?{
            get{
                return FirebaseManager.shared.getRemoveAdId();
            }
        }
    }
    
    static public let shared : AdManager = AdManager();
    
    init() {
        self.listenNotifications();
    }
    
    deinit {
        self.removeNotifications();
    }
    
    let firebase : Firebase = Firebase();
    var isRemoveAdsPurchased : Bool {
        get{
            var retVal = false;
            if (self.firebase.removeAdId == nil){
                return retVal;
            }
            
            if let id = self.firebase.removeAdId {
                retVal = InAppManager.shared.isProductPurchased(id);
            }
            return retVal;
        }
    }
    private var isRemovedAdPurchaseInProgress : Bool = false;
    var byPassFirebaseSettingForRemoveAdsStrip : Bool = false;

    private var bannerContainers: [String: BannerAdView] = [:]; // a storage for all banner ads.
    var interstetialVideoToggler = false
    var interstetialFrequencyCounter: Int = 0
    var videoFrequencyCounter: Int = 0
    private var interstatialAdView: AdView? = nil;
    
    internal var interstetialAdWillPresent : InterstetialAdWillPresentBlock? = nil;
    internal var interstetialAdDidPresent : InterstetialAdDidPresentBlock? = nil;
    internal var interstetialAdDismiss : InterstetialAdDismissBlock? = nil;
    internal var interstetialAdFailed : InterstetialAdFailedBlock? = nil;
    internal var bannerAdsAvaliable : BannerAdsAvaliableBlock? = nil;
    internal var bannerAdsFailed : BannerAdsFailedBlock? = nil;
    internal var removeAdsPurchased : RemoveAdsPurchase? = nil;
    
    
    
    
    func toggleInterstetialVideoAd() {
        if interstetialVideoToggler {
            showInterstetialAd()
        } else {
            showVideoAd()
        }
        interstetialVideoToggler = !interstetialVideoToggler
    }
    
    func showInterstetialAd() {

        if !self.canShowAds(){
            self.removeAds()
            return
        }

        interstetialFrequencyCounter -= 1
        if interstetialFrequencyCounter >= 0 {
            return
        }
        interstetialFrequencyCounter = firebase.interstetialFrequency;
        self.showAds(type: .Interstetial)
    }

    func showVideoAd() {

        if !self.canShowAds(){
            self.removeAds()
            return
        }

        videoFrequencyCounter -= 1
        if videoFrequencyCounter >= 0 {
            return
        }
        videoFrequencyCounter = firebase.videoFrequency
        self.showAds(type: .Video)
    }
   

    public func update(bannerView :BannerAdView) {
        
        if !self.canShowAds(){
            return;
        }
        
        var showRemoveAdsStrip = bannerView.showRemoveAdsBanner;
        if (byPassFirebaseSettingForRemoveAdsStrip){
            // force hide the remove ads stip
            showRemoveAdsStrip = false;
        }
        
        var height: CGFloat = bannerAdDefaultHeight;
        if (showRemoveAdsStrip){
            //          if (nlUtils.shared.applicationNavigationController()?.isPortrait() ?? false){
            height = (2 * bannerAdDefaultHeight);
            //            }
        }
        
        if let parentView = bannerView.superview{
            let bannerAdViewRect = CGRect(x: 0, y: parentView.frame.size.height - height, width: parentView.frame.size.width, height: height)
            bannerView.frame = bannerAdViewRect;
            bannerView.showRemoveAdsBanner = showRemoveAdsStrip;
            bannerView.layer.zPosition = 100
            bannerView.layoutSubviews()
        }
    }
    
    public func showBannerAd(view: UIView){
        let id : String = view.pointerIdentifier();
        let _ = self.showBannerAd(withId: id, parentView: view, withRemoveAdsStrip: true);
    }
    
    public func removeBannerAd(view: UIView){
        let id : String = view.pointerIdentifier();
        if let bannerView = self.bannerContainers[id]{
            bannerView.removeFromSuperview();
            self.bannerContainers.removeValue(forKey: id);
        }
    }
    
    public func bannerHeight(forView: UIView) -> CGFloat {
        let id : String = forView.pointerIdentifier();
        if let bannerView = self.bannerContainers[id]{
            return bannerView.frame.height;
        }
        return bannerAdDefaultHeight * 2;
    }
    
    public func showBannerAd(withId: String, parentView: UIView, withRemoveAdsStrip: Bool) -> NeuralBaseView?
    {
        if !self.canShowAds(){
            Logger.shared.adServiceLog("Cannot show banner ads.");
            self.removeAds()
            return nil
        }
        
        if let bannerView = self.bannerContainers[withId] {
            self.update(bannerView: bannerView);
            return bannerView;
        }
        if let bannerView = self.getBanner(withRemoveAdsStrip: withRemoveAdsStrip) {
            self.bannerContainers[withId] = bannerView;
            parentView.addSubview(bannerView);
            self.update(bannerView: bannerView);
            return bannerView;
        }
        Logger.shared.adServiceLog("Failed to create Banner ads instance.");
        return nil;
    }
    
    private func getBanner(withRemoveAdsStrip: Bool) -> BannerAdView?{
        let bannerAdView = BannerAdView(banner: firebase.bannerId,
                                        inter: firebase.interstetialId,
                                        video: firebase.videoId,
                                        appId: firebase.adsAppId);
        bannerAdView?.showRemoveAdsBanner = withRemoveAdsStrip && (firebase.removeAdId != nil)
        bannerAdView?.autoresizingMask = [.flexibleWidth, .flexibleTopMargin];
        return bannerAdView;
    }

    
    private func canShowAds () -> Bool{
        // A single point function to take decision that ads can be shown or not.
        
        if ((firebase.adsAppId == nil) || (firebase.adsAppId?.count ?? 0 <= 0)){
            Logger.shared.adServiceLog(#function, "Cannot show Ads : ", "Firebase has not initialized till now.")
            return false;
        }
        
        if firebase.adProviderType == .NoAds{
            Logger.shared.adServiceLog(#function, "Cannot show Ads : ", "No Ad Provider Set from Firebase")
            return false;
        }
        
        if firebase.isFreeApp == false {
            Logger.shared.adServiceLog(#function, "Cannot show Ads : ", "This is paid app")
            return false;
        }
        
        if self.isRemoveAdsPurchased == true {
            Logger.shared.adServiceLog(#function, "Cannot show Ads : ", "Remvoe Ads was purchased")
            return false;
        }
        
        if self.isRemovedAdPurchaseInProgress == true {
            Logger.shared.adServiceLog(#function, "Cannot show Ads : ", "A Remove Ads purchase is in progress")
            return false;
        }
        
        return true;
    }

    func removeAdsFromApplication() {
        
        if self.isRemoveAdsPurchased {
            return
        }
        
        self.startProcessing()
        self.isRemovedAdPurchaseInProgress = true;
        weak var weakSelf = self
        
        if let prodId = firebase.removeAdId {
            
            InAppManager.shared.purchaseProduct(prodId) { (sMsg, eMsg) in
                weakSelf?.stopProcessing();
                weakSelf?.isRemovedAdPurchaseInProgress = false;
                if let m = sMsg {
                    Logger.shared.log(#function, "Successfully Purchased Remove Ads : ", m);
                    weakSelf?.removeAds()
                    
                    NotificationController.noti_RemoveAdsPurchased();
                    if (weakSelf!.removeAdsPurchased != nil){
                        weakSelf!.removeAdsPurchased!(true);
                    }
                }
                if let m = eMsg {
                    Logger.shared.log(#function, "Failed to purchased Remove Ads : ", m);
                    if (weakSelf!.removeAdsPurchased != nil){
                        weakSelf!.removeAdsPurchased!(false);
                    }
                }
            }
        }
    }
    
    func removeAds() {
        let _ = self.bannerContainers.map {
                //remove banner view from superview
                $0.value.removeFromSuperview()
            };
        //clean the container.
        self.bannerContainers = [:]
    }
    
    private func startProcessing () {
        let _ = self.bannerContainers.map {
            //remove banner view from superview
            $0.value.btnRemoveAd.startProcessing();
        };
    }

    private func stopProcessing () {
        let _ = self.bannerContainers.map {
            //remove banner view from superview
            $0.value.btnRemoveAd.stopProcessing();
        };
    }

    private func showAds (type: AdType)
    {
        if (self.interstatialAdView == nil){
            self.interstatialAdView = AdView(banner: firebase.bannerId, inter: firebase.interstetialId, video: firebase.videoId, appId: firebase.adsAppId);
            self.interstatialAdView!.loadAdView();
        }
        
        if (type == .Interstetial) || (type == .Video){
            self.interstatialAdView?.showAd(provider: firebase.adProviderType, type: type);
        }else if (type == .Banner){
            let _ = self.bannerContainers.map {
                //remove banner view from superview
                $0.value.showAd(provider: firebase.adProviderType, type: type);
            };
        }
    }
}


//MARK: AdView listners
extension (AdManager){
    private func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(bannerAdsReceived(_:)), name: nlBannerAdsReceived, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(bannerAdsFailed(_:)), name: nlBannerAdsFailed, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(interstetialAdsFailed(_:)), name: nlInterstetialAdsFailed, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(interstetialAdsReceived(_:)), name: nlInterstetialAdsReceived, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(interstetialAdsDidDismiss(_:)), name: nlInterstetialAdsDidDismiss, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(interstetialAdsWillDismiss(_:)), name: nlInterstetialAdsWillDismiss, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPurchaseRemoveAds(_:)), name: nlPurchaseRemoveAds, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onDeviceOrientationChange(_:)), name: nlDeviceOrientationChanged, object: nil);
    }
    
    private func removeNotifications () {
        NotificationCenter.default.removeObserver(self, name: nlBannerAdsReceived, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlBannerAdsFailed, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlInterstetialAdsFailed, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlInterstetialAdsReceived, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlInterstetialAdsDidDismiss, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlInterstetialAdsWillDismiss, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlPurchaseRemoveAds, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlDeviceOrientationChanged, object: nil);
    }
    
    @objc internal func bannerAdsReceived (_ notification:Notification?) {
        weak var weakSelf = self;
        if (bannerAdsAvaliable != nil){
            DispatchQueue.main.async {
                weakSelf?.bannerAdsAvaliable?();
            }
        }
    }
    @objc internal func bannerAdsFailed (_ notification:Notification?) {
        weak var weakSelf = self;
        if (bannerAdsFailed != nil){
            DispatchQueue.main.async {
                weakSelf?.bannerAdsFailed?();
            }
        }
    }
    @objc internal func interstetialAdsFailed (_ notification:Notification?) {
        weak var weakSelf = self;
        if (interstetialAdFailed != nil){
            DispatchQueue.main.async {
                weakSelf?.interstetialAdFailed?();
            }
        }
    }
    @objc internal func interstetialAdsReceived (_ notification:Notification?) {
    }
    @objc internal func interstetialAdsDidDismiss (_ notification:Notification?) {
        weak var weakSelf = self;
        if (interstetialAdDidPresent != nil){
            DispatchQueue.main.async {
                weakSelf?.interstetialAdDidPresent?();
            }
        }
    }
    @objc internal func interstetialAdsWillDismiss (_ notification:Notification?) {
        weak var weakSelf = self;
        if (interstetialAdWillPresent != nil){
            DispatchQueue.main.async {
                weakSelf?.interstetialAdWillPresent?();
            }
        }
    }
}

//MARK: Navigation Listeners
extension (AdManager){
    
    @objc func onPurchaseRemoveAds(_ notification: Notification) {
        self.removeAdsFromApplication();
    }

    @objc func onDeviceOrientationChange(_ notification: Notification) {
        let _ = self.bannerContainers.map {
            self.update(bannerView: $0.value);
        };
    }
}
