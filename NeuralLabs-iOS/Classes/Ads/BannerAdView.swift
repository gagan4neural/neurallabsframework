//
//  BannerAdView.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 07/04/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

public class BannerAdView: AdView {
    
    static let defaultFrame: CGRect = CGRect(x: 0, y: 0, width: bannerAdDefaultWidth, height: bannerAdDefaultHeight);
    
    private var _btnRemoveAd: RemoveAdButton? = nil;
    var btnRemoveAd: RemoveAdButton {
        get{
            if (_btnRemoveAd == nil) {
                _btnRemoveAd = RemoveAdButton.instance();
                addSubview(_btnRemoveAd!)
            }
            _btnRemoveAd?.isHidden = !self.showRemoveAdsBanner;
            return _btnRemoveAd!;
        }
    }
    
    var showRemoveAdsBanner = false;
    
    override func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationChanged), name: nlDeviceOrientationChanged, object: nil)
    }
    
    @objc func deviceOrientationChanged (notification : NSNotification) {
        self.layoutSubviews();
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews();
        
        self.layoutPortrait();
        //        if (nlUtils.shared.applicationNavigationController()?.isLandscape() == true){
        //            self.layoutPortrait();
        //        }else if (nlUtils.shared.applicationNavigationController()?.isPortrait() == true){
        //            self.layoutLandscape();
        //        }
    }
    
    func layoutPortrait () {
        var rectBtnRemoveAds: CGRect = self.btnRemoveAd.frame;
        var rectAdView: CGRect = self.adView.frame;
        
        let width = self.bounds.size.width;
        let height = self.showRemoveAdsBanner ? (self.bounds.size.height * 0.5) : self.bounds.size.height;
        var y: CGFloat = 0.0;
        
        if (self.btnRemoveAd.isHidden == false){
            rectBtnRemoveAds.size.width = width;
            rectBtnRemoveAds.size.height = height;
            rectBtnRemoveAds.origin.x = 0;
            rectBtnRemoveAds.origin.y = y;
            y = y + rectBtnRemoveAds.origin.y;
        }

        rectAdView.size.width = width;
        rectAdView.size.height = height;
        rectAdView.origin.x = 0;
        rectAdView.origin.y = y + rectBtnRemoveAds.size.height;
        
        self.btnRemoveAd.frame = rectBtnRemoveAds;
        self.adView.frame = rectAdView;
    }
    
    func layoutLandscape () {
        var rectBtnRemoveAds: CGRect = self.btnRemoveAd.frame;
        var rectAdView: CGRect = self.adView.frame;
        
        let width = self.showRemoveAdsBanner ? (self.bounds.size.width * 0.9) : self.bounds.size.width;
        let height = self.bounds.size.height;
        
        rectAdView.size.width = width;
        rectAdView.size.height = height;
        rectAdView.origin.x = 0;
        rectAdView.origin.y = 0;
        
        rectBtnRemoveAds.size.width = self.bounds.size.width - width;
        rectBtnRemoveAds.size.height = height;
        rectBtnRemoveAds.origin.x = rectAdView.origin.x + rectAdView.size.width;
        rectBtnRemoveAds.origin.y = 0;
        
        self.btnRemoveAd.frame = rectBtnRemoveAds;
        self.adView.frame = rectAdView;
    }
    
}
