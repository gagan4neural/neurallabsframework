//
//  AdView.swift
//  NeuralLabs
//
//  Created by Gagan on 26/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

let bannerAdDefaultHeight : CGFloat = 60
let bannerAdDefaultWidth : CGFloat = 320

fileprivate let animationDuration : Double = nlUtils.shared.defaultLayoutAnimationDuration;

public class AdView: NeuralBaseView {
    
    convenience init?(banner:String?, inter:String?, video:String?, appId:String?) {
        
        if ((appId == nil) || (appId?.count ?? 0 <= 0)){
            return nil;
        }
        
        self.init(frame: BannerAdView.defaultFrame);
        self.bannerId = banner;
        self.interstetialId = inter;
        self.videoId = video;
        self.appId = appId;
        
        self.backgroundColor = .clear;
        self.listenNotifications()
        
        self.backgroundColor = .white
    }
    
    var bannerId: String? = nil
    var interstetialId: String? = nil
    var videoId: String? = nil
    var appId: String? = nil

    internal var adMobView : AdMobView? = nil;
//    private var startAppView : StartAppView? = nil;
//    private var flurryView : FlurryView? = nil;
//    private var unityView : UnityView? = nil;
//    private var facebookView : FacebookView? = nil;
    private var _adView : UIView?;
    internal var adView : UIView {
        get{
            if (_adView == nil)
            {
                switch FirebaseManager.shared.adProviderType(){
                case .AdMob:
                    adMobView = AdMobView(frame: self.bounds,
                                          banner: self.bannerId ?? "",
                                          inter: self.interstetialId ?? "",
                                          video: self.videoId ?? "",
                                          appId: self.appId ?? "")
                    _adView = adMobView;
                    break;
                default:
                    adMobView = AdMobView(frame: self.bounds,
                                          banner: self.bannerId ?? "",
                                          inter: self.interstetialId ?? "",
                                          video: self.videoId ?? "",
                                          appId: self.appId ?? "")
                    _adView = adMobView;
                    break;
                }
                _adView!.backgroundColor = UIColor.clear
                addSubview(_adView!)
            }
            return _adView!;
        }
    };

    // this extra loading is for interstetial ads.
    func loadAdView() {
        let _ = self.adView;
    }
    
    
    func showAd (provider: AdProviderType, type:AdType) {
        
        switch provider {
        case .AdMob:
            switch type {
            case .Interstetial:
                self.adMobView?.showInterstetialAd();
                break;
            case .Video:
                self.adMobView?.showVideoAd();
                break;
            default:
                break;
            }
        default:
            break;
        }
    }
    
    func listenNotifications () {
        // BannerAdView child class is overriding this.
    }
}

