//
//  AdMobView.swift
//  NeuralLabs
//
//  Created by Gagan on 23/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdMobView: UIView, GADBannerViewDelegate, GADInterstitialDelegate {

    var bannerId : String = "";
    var interstetialId : String = "";
    var videoId : String = "";
    var isInterstetialRequestPending : Bool = false;
    
    convenience  init (frame:CGRect, banner:String, inter:String, video:String, appId: String) {
        self.init(frame: frame);
        self.bannerId = banner;
        self.interstetialId = inter;
        self.videoId = video;
        self.backgroundColor = .clear;
        GADMobileAds.sharedInstance().start(completionHandler: nil);
//        GADMobileAds.configure(withApplicationID: appId)
    }

    private var _gadBanner : GADBannerView? = nil;
    var gadBanner : GADBannerView?  {
        get {
            if (_gadBanner == nil){
                _gadBanner = GADBannerView(frame: self.bounds);
                _gadBanner?.adUnitID = self.bannerId;
                _gadBanner?.delegate = self;
                _gadBanner?.backgroundColor = UIColor.clear;
                self.addSubview(_gadBanner!);
            }
            _gadBanner?.rootViewController = nlUtils.shared.topViewController()
            _gadBanner?.load(self.gadRequest);
            return _gadBanner;
        }
    }
    var gadInterstitial : GADInterstitial {
        get{
            let gadInter = GADInterstitial(adUnitID: self.interstetialId);
            gadInter.delegate = self;
            gadInterstetialsRequested.append(gadInter);
            return gadInter;
        }
    }
    var gadVideo : GADInterstitial {
        get{
            let gadVideo = GADInterstitial(adUnitID: self.videoId);
            gadVideo.delegate = self;
            gadVideoRequested.append(gadVideo);
            return gadVideo;
        }
    }
    private var _gadRequest : GADRequest? = nil;
    var gadRequest : GADRequest? {
        get{
            if (_gadRequest == nil){
                _gadRequest = GADRequest();
                _gadRequest?.testDevices = self.testDevices();
            }
            return _gadRequest;
        }
    }

    private var _dfpBanner : DFPBannerView? = nil;
    var dfpBanner : DFPBannerView? {
        get{
            if (_dfpBanner == nil){
                _dfpBanner = DFPBannerView(frame: self.bounds);
                _dfpBanner?.adUnitID = self.bannerId;
                _dfpBanner?.delegate = self;
                _dfpBanner?.backgroundColor = UIColor.clear;
            }
            _dfpBanner?.rootViewController = nlUtils.shared.topViewController()
            _dfpBanner?.load(self.dfpRequest);
            return _dfpBanner;
        }
    }
    var dfpInterstitial : DFPInterstitial? = nil
    var dfpVideo : DFPInterstitial? = nil
    var dfpRequest : DFPRequest? = nil

    var gadInterstetialsRequested : [GADInterstitial] = [GADInterstitial]();
    var gadVideoRequested : [GADInterstitial] = [GADInterstitial]();

    override func layoutSubviews() {
        super.layoutSubviews();
        self.gadBanner?.frame = self.bounds;
    }
    
    func testDevices () -> [String] {
        var retArr = [String]();
        retArr.append(kGADSimulatorID as! String);
        retArr.append("8ac0335fc225e6780337a9f5c6170e8c");
        retArr.append("9a4cf2444649ec5fe574edc698cebdb7");
        
        return retArr;
    }
    
    // MARK: Interstetial Ads
    func showInterstetialAd () {
        var removeInterstitial : GADInterstitial? = nil;
        for inter in self.gadInterstetialsRequested{
            if (inter.isReady){
                inter.present(fromRootViewController:nlUtils.shared.topViewController()!)
                removeInterstitial = inter;
                break;
            }
        }
        
        self.gadInterstetialsRequested.removeAll { (interstetial) -> Bool in
            if (interstetial == removeInterstitial){
                return true;
            }
            return false;
        }
        
        if (!isInterstetialRequestPending)
        {
            isInterstetialRequestPending = true;
            self.gadInterstitial.load(self.gadRequest);
        }
    }
    
    func showVideoAd () {
        var removeVideo : GADInterstitial? = nil;
        for video in self.gadVideoRequested{
            if (video.isReady){
                video.present(fromRootViewController:nlUtils.shared.topViewController()!)
                removeVideo = video;
                break;
            }
        }
        
        self.gadVideoRequested.removeAll { (video) -> Bool in
            if (video == removeVideo){
                return true;
            }
            return false;
        }
        
        if (!isInterstetialRequestPending)
        {
            isInterstetialRequestPending = true;
            self.gadVideo.load(self.gadRequest);
        }
    }
    
    
    // MARK: AD Delegate
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        NotificationController.noti_bannerAdsReceived();
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        Logger.shared.log("Failed to receive Banner Ads : ", error.localizedDescription, #function)
        NotificationController.noti_bannerAdsFailed();
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        if let topVC = nlUtils.shared.topViewController(){
            ad.present(fromRootViewController: topVC);
        }
        NotificationController.noti_interstetialAdsReceived();
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.isInterstetialRequestPending = false;
        NotificationController.noti_interstetialAdsDidDismiss();
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        NotificationController.noti_interstetialAdsDidDismiss();
    }
    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        Logger.shared.log("Failed to receive Interstetial Ads : ", error.localizedDescription, #function)
        self.isInterstetialRequestPending = false;
        NotificationController.noti_interstetialAdsFailed();
    }
}
