//
//  NibManager.swift
//  AVPlayer
//
//  Created by Gagan on 15/09/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import Foundation
import UIKit

public class BundleManager {
    
    static public let shared : BundleManager = BundleManager();

    public static func loadNib <T>(name:String, owner: Any?, options: [UINib.OptionsKey : Any]?) -> T? {
    
        if let nibs = Bundle.main.loadNibNamed(name, owner: owner, options: options) {

            if let classNib = nibs.first as? T {
                return classNib;
            }
        }
        return nil;
    }
    
    public static func viewControllerFromStoryBoard (name: String) -> UIViewController?{
        let storyBoard = UIStoryboard.init(name: name, bundle: Bundle.main);
        return storyBoard.instantiateInitialViewController();
    }
    
    public static func applicationBackgroundFromBundle () -> UIImage? {
        return UIImage(named: self.valueForKeyInInfo(key: "Application Background") as! String);
    }
    
    public static func applicationName () -> String {
        return self.valueForKeyInInfo(key: "CFBundleName") as! String
    }
    
    public static func applicationImageName() -> String {
        return self.valueForKeyInInfo(key: "Application Background") as! String
    }

    private static func valueForKeyInInfo (key : String) -> Any? {
        
        if let nsDict = Bundle.main.infoDictionary{
            if let value = nsDict[key] {
                return value;
            }
        }
        return nil;
    }
    
    func appID() -> NSNumber? {
        let retVal = FirebaseManager.shared.appId()
        return retVal
    }
    
    func bundleDisplayName() -> String? {
        return FirebaseManager.shared.appName()
    }

    var bundleIdentifier: String? {
        get{
            let retVal = Bundle.main.bundleIdentifier
            return retVal
        }
    }
    
    public func frameworkBundle() -> Bundle? {
        return Bundle(identifier: "com.neural.NeuralLabs-iOS");
    }
    
    func appVersion() -> String? {
        let retVal = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        return retVal
    }
    
    func myAppsLink() -> String? {
        let retVal = FirebaseManager.shared.vendorUrl()
        return retVal
    }

}
