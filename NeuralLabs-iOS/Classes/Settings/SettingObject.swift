//
//  SettingObject.swift
//  NeuralLabs
//
//  Created by Gagan on 06/11/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

open class SettingObject <DATA_TYPE: Any> {
    
    private let key: String;
    required public init (key: String){
        self.key = key;
    }
    
    func getValue() -> DATA_TYPE?{
        return NeuralDefaults.getSettings(self.key) as? DATA_TYPE;
    }
    
    func setValue(value: DATA_TYPE){
        NeuralDefaults.setSettings(self.key, value: value);
    }
}
