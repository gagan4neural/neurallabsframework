//
//  NotificationController.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

let nlApplicationDidEnterBackground = Notification.Name("ApplicationDidEnterBackground");
let nlApplicationWillResignActive = Notification.Name("ApplicationWillResignActive");

let nlDeviceUnknown = Notification.Name("nlDeviceChangedToUnknown");
let nlDevicePortrait = Notification.Name("nlDeviceChangedToPortrait");
let nlDevicePortraitUpSideDown = Notification.Name("nlDeviceChangedToPortraitUpSideDown");
let nlDeviceLandscapeLeft = Notification.Name("nlDeviceChangedToLandscapeLeft");
let nlDeviceLandscapeRight = Notification.Name("nlDeviceChangedToLandscapeRight");
let nlDeviceFaceUp = Notification.Name("nlDeviceChangedToFaceUp");
let nlDeviceFaceDown = Notification.Name("nlDeviceChangedToFaceDown");
let nlDeviceOrientationChanged = Notification.Name("nlDeviceOrientationChanged");
let nlProximityStateDidChange = Notification.Name("nlProximityStateDidChange");
let nlBatteryStateDidChange = Notification.Name("nlBatteryStateDidChange");
let nlBatteryLevelDidChange = Notification.Name("nlBatteryLevelDidChange");

let nlPurchaseRemoveAds = Notification.Name("PurchaseRemoveAds")
let nlRemoveAdsPurchased = Notification.Name("RemoveAdsPurchased")

let nlBannerAdsReceived = Notification.Name("BannerAdsReceived")
let nlBannerAdsFailed = Notification.Name("BannerAdsFailed")
let nlInterstetialAdsReceived = Notification.Name("InterstetialAdsReceived")
let nlInterstetialAdsFailed = Notification.Name("InterstetialAdsFailed")
let nlInterstetialAdsDidDismiss = Notification.Name("InterstetialAdsDidDismiss")
let nlInterstetialAdsWillDismiss = Notification.Name("InterstetialAdsWillDismiss")

let nlFirebaseApplicationUpdated = Notification.Name("FirebaseApplicationUpdated")
let nlFirebaseVendorUpdated = Notification.Name("FirebaseVendorUpdated")

let nlVideoCollectionsInitialized = Notification.Name("VideoCollectionsInitialized")
let nlImageCollectionsInitialized = Notification.Name("ImageCollectionsInitialized")
let nlAudioCollectionsInitialized = Notification.Name("AudioCollectionsInitialized")

let nlSystemVolumeChanged = Notification.Name("SystemVolumeChanged")



open class NotificationController {
    
    public static func noti_ApplicationDidEnterBackground () {
        NotificationCenter.default.post(name: nlApplicationDidEnterBackground, object: nil);
    }

    public static func noti_ApplicationWillResignActive () {
        NotificationCenter.default.post(name: nlApplicationWillResignActive, object: nil);
    }

    public static func noti_DeviceOrientationDidChange () {
        NotificationCenter.default.post(name: nlDeviceOrientationChanged, object: nil);
        
        switch UIDevice.current.orientation {
        case .portrait:
            NotificationCenter.default.post(name: nlDevicePortrait, object: nil);
            break;
        case .portraitUpsideDown:
            NotificationCenter.default.post(name: nlDevicePortraitUpSideDown, object: nil);
            break;
        case .landscapeLeft:
            NotificationCenter.default.post(name: nlDeviceLandscapeLeft, object: nil);
            break;
        case .landscapeRight:
            NotificationCenter.default.post(name: nlDeviceLandscapeRight, object: nil);
            break;
        case .faceUp:
            NotificationCenter.default.post(name: nlDeviceFaceUp, object: nil);
            break;
        case .faceDown:
            NotificationCenter.default.post(name: nlDeviceFaceDown, object: nil);
            break;
        default:
            break;
        }
    }
    
    static func noti_BatteryLevelDidChange () {
        NotificationCenter.default.post(name: nlBatteryLevelDidChange, object: nil);
    }
    
    static func noti_BatteryStateDidChange () {
        NotificationCenter.default.post(name: nlBatteryStateDidChange, object: nil);
    }
    
    static func noti_ProximityStateChange () {
        NotificationCenter.default.post(name: nlProximityStateDidChange, object: nil);
    }
    
    static func noti_PurchaseRemoveAds () {
        NotificationCenter.default.post(name: nlPurchaseRemoveAds, object: nil);
    }

    public static func noti_RemoveAdsPurchased () {
        NotificationCenter.default.post(name: nlRemoveAdsPurchased, object: nil);
    }

    static func noti_bannerAdsReceived () {
        NotificationCenter.default.post(name: nlBannerAdsReceived, object: nil);
    }

    static func noti_bannerAdsFailed () {
        NotificationCenter.default.post(name: nlBannerAdsFailed, object: nil);
    }
    static func noti_interstetialAdsFailed () {
        NotificationCenter.default.post(name: nlInterstetialAdsFailed, object: nil);
    }
    static func noti_interstetialAdsReceived () {
        NotificationCenter.default.post(name: nlInterstetialAdsReceived, object: nil);
    }
    static func noti_interstetialAdsDidDismiss () {
        NotificationCenter.default.post(name: nlInterstetialAdsDidDismiss, object: nil);
    }
    static func noti_interstetialAdsWillDismiss () {
        NotificationCenter.default.post(name: nlInterstetialAdsWillDismiss, object: nil);
    }

    static func noti_firebaseApplicationUpdated () {
        NotificationCenter.default.post(name: nlFirebaseApplicationUpdated, object: nil);
    }
    static func noti_firebaseVendorUpdated () {
        NotificationCenter.default.post(name: nlFirebaseVendorUpdated, object: nil);
    }
    
    
    static func noti_videoCollectionsInitialized () {
        NotificationCenter.default.post(name: nlVideoCollectionsInitialized, object: nil);
    }
    static func noti_imageCollectionsInitialized () {
        NotificationCenter.default.post(name: nlImageCollectionsInitialized, object: nil);
    }
    static func noti_audioCollectionsInitialized () {
        NotificationCenter.default.post(name: nlAudioCollectionsInitialized, object: nil);
    }


    static func noti_systemVolumeChanged () {
        NotificationCenter.default.post(name: nlSystemVolumeChanged, object: nil);
    }
}
