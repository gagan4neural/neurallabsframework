//
//  NeuralChatViewController.swift
//  NeuralLabs-iOS
//
//  Created by Gagandeep Madan on 11/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


open class NeuralChatViewController : NeuralBaseViewController {

    open override func viewDidLoad() {
        super.viewDidLoad();
        FireChatManager.shared.initialize(withFireChatDelegate: self);
        FireChatManager.shared.loadCompanyDatabase();
    }
 
    open func fetchChatsFor(phoneNumber: String?, email: String?) {
        FireChatManager.shared.fetchChatsFor(phoneNumber: phoneNumber, email: email);
    }
}







//MARK: Handles all the chatting protocols
extension NeuralChatViewController: FireChatProtocol{
    
    func fireChat_Collection() -> String {
        // This class is the base for testing and Neural Labs.
        // while implementing for other applications this method is overriden with the other collections creates are firestore/firebase database.
        return "root_neural_chat";
    }
    
    func fireChat_companyInfoDone(_ isSuccess: Bool, errorString: String) {
        if (isSuccess == false) {
            Logger.shared.log("FireChat error \(errorString)");
        }else{
            Logger.shared.log("Successfully loaded company chat");
        }
    }
}

