//
//  FireChatManager.swift
//  NeuralLabs-iOS
//
//  Created by Gagandeep Madan on 11/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseCore
import FirebaseAuth
import FirebaseInvites
import FirebaseStorage
import FirebaseFirestore
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseFunctions
import FirebaseMessaging
import FirebasePerformance


class FireChatManager {
    
    static let shared = FireChatManager();
    private var delegate: FireChatProtocol? = nil;
    private let fireQ: DispatchQueue = DispatchQueue(label: "FireChatQueue", qos: .userInitiated);
    
    private let configuration: FirebaseConfiguration = FirebaseConfiguration(appName: "Neural-Chatting");
    private var firCollection : CollectionReference?;
    private var firDocReferences: [DocumentReference] = [];
    
    
    
    func initialize(withFireChatDelegate delegate:FireChatProtocol){
        self.delegate = delegate;
        // initialisation requires main thread
        self.initializeConfiguration();
        self.initializeDatabase();
    }
    
    func fetchChatsFor(phoneNumber: String?, email: String?) {
        if let ph = phoneNumber {
            self.loadUserWith(phoneNumber: ph);
        }
    }
}


//MARK: Testing extension
extension FireChatManager {
    func printAllDocs() {
        self.log(message: "----->> Printing all document reference. <<-----")
        for ref in self.firDocReferences {
            ref.getDocument {[unowned self] (documentSnapShot, error) in
                if (error != nil) {
                    self.log(message: error?.localizedDescription ?? "Error in fetching document \(ref.documentID)");
                }else{
                    self.log(message: "Fetched document \(documentSnapShot.debugDescription)");
                    if let data = documentSnapShot?.data() {
                        for (key, value) in data {
                            print("{Key: \(key)} , {value: \(value)}");
                        }
                    }
                }
            }
        }
    }
}


//MARK: Initialisation Extensions
private extension FireChatManager {
    
    func log(message: String) {
        if let d = self.delegate {
            d.fireChat(error: "Firebase Chatting: \(message)")
        }
    }
    
    func initializeConfiguration() {
        self.configuration.CLIENT_ID = "834018078592-m6g9etv115mra9vc7tvhqacdevfh5d29.apps.googleusercontent.com";
        self.configuration.REVERSED_CLIENT_ID = "com.googleusercontent.apps.834018078592-m6g9etv115mra9vc7tvhqacdevfh5d29";
        self.configuration.API_KEY = "AIzaSyDdqQDnOwdKct_oPyTZ2HB-i-R6U8Ttars";
        self.configuration.GCM_SENDER_ID = "834018078592";
        self.configuration.PLIST_VERSION = "1";
        self.configuration.BUNDLE_ID = "com.neural.neural-chatting"
        self.configuration.PROJECT_ID = "neural-chatting";
        self.configuration.STORAGE_BUCKET = "neural-chatting.appspot.com";
        self.configuration.IS_APPINVITE_ENABLED = true;
        self.configuration.IS_GCM_ENABLED = true;
        self.configuration.IS_SIGNIN_ENABLED = true;
        self.configuration.GOOGLE_APP_ID = "1:834018078592:ios:068faf68d30b4b745c1b65";
        self.configuration.DATABASE_URL = "https://neural-chatting.firebaseio.com";
    }
    
    func initializeDatabase()
    {
        guard let del = self.delegate else{
            Logger.shared.firebaseLog("No delegate set");
            return;
        }
        
        var firOptions: FirebaseOptions?;
        do {
            try firOptions = self.configuration.firebaseOptions();
        }catch{
            //TODO: Needs to check that how to throw custom class Error
            Logger.shared.assertionLogs("Missing firebase configurations.");
        }
        
        guard let options = firOptions else {
            Logger.shared.assertionLogs("Missing firebase configurations.");
            return;
        }
        
        FirebaseApp.configure(name: self.configuration.appName, options: options);
        guard let firApp = FirebaseApp.app(name: self.configuration.appName) else {
            Logger.shared.assertionLogs("Failed to create firebase app.");
            return;
        }

        let coll = del.fireChat_Collection();
        self.firCollection = Firestore.firestore(app: firApp).collection(coll);
        
        if (self.firCollection == nil) {
            Logger.shared.assertionLogs("Failed to initialize firestore collection.");
            return;
        }
    }
}



//MARK: Company Data from firestore
extension FireChatManager {

    func loadCompanyDatabase() {
        guard let del = self.delegate else{
            Logger.shared.firebaseLog("No delegate set");
            return;
        }
        guard let fColl = self.firCollection else {
            self.log(message: "No collections found");
            return;
        }
        
        let docRef = fColl.document(FireChatModel_Collection.kCompany);
        docRef.getDocument { (documentSnapshot, error) in
            if (error != nil){
                let msg = error?.localizedDescription ?? "Failed to get company document";
                self.log(message: msg);
                del.fireChat_companyInfoDone(false, errorString: msg);
            }else{
                if let data = documentSnapshot?.data() {
                    FireChatModel_Collection.loadCompany(document: data);
                }else{
                    del.fireChat_companyInfoDone(false, errorString: "No data received in company info");
                }
            }
        }
    }
}



//MARK: User Data from Firestore
extension FireChatManager {

    func loadUserWith(phoneNumber: String){
        guard let del = self.delegate else{
            Logger.shared.firebaseLog("No delegate set");
            return;
        }
        guard let fColl = self.firCollection else {
            self.log(message: "No collections found");
            return;
        }

//        let strUrl = "https://us-central1-neural-chatting.cloudfunctions.net/findUser";
//        let req = NetworkRequest.requestForURL(strUrl) { (response) in
//            if (response.error != nil){
//                print("Response error \(response.error?.localizedDescription)")
//            }
//            print("Response \(response.dictData)");
//        }
//        req.paramaters = ["email":"user1@gmail.com", "phone":"1111111111"];
//        req.requestType = .POST;
//        NetworkManager.shared.executeNetworkRequest(req);
//
//        return;
        
        let fn = Functions.functions(app: fColl.firestore.app);
        let callable = fn.httpsCallable("findUser");

        let p = NSMutableDictionary()
        p.setValue("user1@gmail.com", forKey: "email");
        p.setValue("1111111111", forKey: "phone");

        callable.call(p) { (httpsCallable, error) in
            if (error != nil){
                print("Error in getting result from function : findUser \(error?.localizedDescription)");
            }
            if let d = httpsCallable?.data {
                print("Received Data \(d)")
            }
        }
        
        
        return;
        
        let docRef = fColl.document(FireChatModel_Collection.kUsers);
        docRef.getDocument { (documentSnapshot, error) in
            if (error != nil){
                let msg = error?.localizedDescription ?? "Failed to get user document";
                self.log(message: msg);
                del.fireChat_companyInfoDone(false, errorString: msg);
            }else{
                if let data = documentSnapshot?.data() {
                    for (key, value) in data {
                        print("{key:\(key)}, {value: \(value)}")
                    }
                }else{
                    del.fireChat_companyInfoDone(false, errorString: "No data received in company info");
                }
            }
        }
    }
    
}
