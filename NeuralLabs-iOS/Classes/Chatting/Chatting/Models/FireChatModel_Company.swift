//
//  FireChatModel_Company.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 12/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


class FireChatModel_Company {
    
    static let kApplication_Name = "application_name";
    static let kName = "name";
    static let kId = "id";

    var application_name: String?;
    var id: String?;
    var name: String?;
    
    func fromDictionary(_ dict: [String:Any]) {
        if let v = dict[FireChatModel_Company.kApplication_Name] as? String {
            self.application_name = v;
        }

        if let v = dict[FireChatModel_Company.kName] as? String {
            self.name = v;
        }

        if let v = dict[FireChatModel_Company.kId] as? String {
            self.id = v;
        }
    }
}
