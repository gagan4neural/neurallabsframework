//
//  FireChatModel_Collection.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 12/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


class FireChatModel_Collection {
    
    static let kCompany: String = "company"
    static let kUsers: String = "users"
    static let kConversations: String = "conversations"

    static let shared = FireChatModel_Collection();
    
    let company: FireChatModel_Company = FireChatModel_Company();
    
    static func loadCompany(document: [String: Any]) {
        FireChatModel_Collection.shared.company.fromDictionary(document);
    }
}
