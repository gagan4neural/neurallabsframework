//
//  FireChatModel_User.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 12/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation

class FireChatModel_User_BasicInfo {
    
    static let kEmail = "email";
    static let kName = "name";
    static let kAge = "age";
    static let kPhone = "phone";
    static let kSex = "sex";
    
    var email: String?
    var name: String?
    var phone: String?
    var sex: String?
    var age: Int?

    func fromDictionary(_ dict: [String:Any]) {
        if let v = dict[FireChatModel_User_BasicInfo.kEmail] as? String {
            self.email = v;
        }
        if let v = dict[FireChatModel_User_BasicInfo.kName] as? String {
            self.name = v;
        }
        if let v = dict[FireChatModel_User_BasicInfo.kAge] as? Int {
            self.age = v;
        }
        if let v = dict[FireChatModel_User_BasicInfo.kPhone] as? String {
            self.phone = v;
        }
        if let v = dict[FireChatModel_User_BasicInfo.kSex] as? String {
            self.sex = v;
        }
    }
}

class FireChatModel_User_ImageInfo {
    
    static let kProfilePic = "profile_pic";
    var profile_pic: String?;

    func fromDictionary(_ dict: [String:Any]) {
        if let v = dict[FireChatModel_User_ImageInfo.kProfilePic] as? String {
            self.profile_pic = v;
        }
    }
}

class FireChatModel_User_UsageInfo {
    
    static let kFirstLogin = "first_login";
    static let kLastSeen = "last_seen";
    var first_login: String?;
    var last_seen: String?;
    
    
    func fromDictionary(_ dict: [String:Any]) {
        if let v = dict[FireChatModel_User_UsageInfo.kFirstLogin] as? String {
            self.first_login = v;
        }
        if let v = dict[FireChatModel_User_UsageInfo.kLastSeen] as? String {
            self.last_seen = v;
        }
    }
}

class FireChatModel_User {
    
    let basicInfo = FireChatModel_User_BasicInfo();
    let imageInfo = FireChatModel_User_ImageInfo();
    let usageInfo = FireChatModel_User_UsageInfo();

}
