//
//  FirebaseChatProtocol.swift
//  NeuralLabs-iOS
//
//  Created by Gagandeep Madan on 11/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation

protocol FireChatProtocol {
    
    // Return the collection for chat. This is root level. 2 different appliations will have different collections.
    func fireChat_Collection() -> String;
    func fireChat(error: String);
    
    func fireChat_companyInfoDone(_ isSuccess: Bool, errorString: String);
}


extension FireChatProtocol {
    func fireChat(error: String){
        Logger.shared.firebaseLog(error);
    }
}
