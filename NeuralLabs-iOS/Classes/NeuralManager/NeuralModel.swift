//
//  NeuralModel.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

protocol NeuralModelProtocol {
    func getDictionary() -> [AnyHashable: Any];
    func fromDictionary(dict:[AnyHashable: Any]);
}
