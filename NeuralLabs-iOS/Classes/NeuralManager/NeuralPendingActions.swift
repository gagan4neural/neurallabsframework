//
//  NeuralPendingActions.swift
//  NeuralLabs
//
//  Created by Gagan on 25/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class NeuralPendingActions {
    struct PendingActions{
        
        var pendingAction:String?;
        var pendingSelector:Selector?;
        
        init (_ action:String){
            self.pendingAction = action;
        }
        
        init (_ selector:Selector){
            self.pendingSelector = selector;
        }
    }
    
    static let shared = NeuralPendingActions();
    private var pendingActions:[PendingActions] = [];
    
    static func addStringAction(_ string:String){
        let action = PendingActions(string);
        NeuralPendingActions.shared.pendingActions.append(action);
    }
    static func addSelectorAction(_ selector:Selector){
        let action = PendingActions(selector);
        NeuralPendingActions.shared.pendingActions.append(action);
    }

    static func containsStringAction(_ string:String) -> Bool{
        let objts = NeuralPendingActions.shared.pendingActions.filter { (pendingAction) -> Bool in
            return pendingAction.pendingAction == string;
        }
        return (objts.count>0)
    }
    static func containsSelectorAction(_ selector:Selector) -> Bool{
        let objts = NeuralPendingActions.shared.pendingActions.filter { (pendingAction) -> Bool in
            return pendingAction.pendingSelector == selector;
        }
        return (objts.count>0)
    }

    static func removeStringAction(_ string:String) -> Bool{
        if (NeuralPendingActions.containsStringAction(string)){
            NeuralPendingActions.shared.pendingActions.removeAll { (pendingAction) -> Bool in
                return pendingAction.pendingAction == string;
            }
            return true;
        }
        return false;
    }
    static func removeSelectorAction(_ selector:Selector) -> Bool{
        if (NeuralPendingActions.containsSelectorAction(selector)){
            NeuralPendingActions.shared.pendingActions.removeAll { (pendingAction) -> Bool in
                return pendingAction.pendingSelector == selector;
            }
            return true;
        }
        return false;
    }
}
