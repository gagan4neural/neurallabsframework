//
//  NeuralLabManager.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

open class NeuralLabManager {
    
    public static let shared : NeuralLabManager = NeuralLabManager();
    private var notificationController : NotificationController = NotificationController();
    
    var isFirebaseApplicationInitialized = false;
    var isFirebaseVendorInitialized = false;
    public lazy var isFirebaseInitialized = (self.isFirebaseApplicationInitialized || self.isFirebaseVendorInitialized);
    var applicationInitialized : completion? = nil;
    var vendorInitialized : completion? = nil;

    public func initialize () {
        UIDevice.current.beginGeneratingDeviceOrientationNotifications();
        self.addListeners();
        NetworkManager.shared.initialize();
        FirebaseManager.shared.initialize();
    }
    
    public func initializeAssets (){
        AssetManager.shared.initialize();
    }
    
    public func deInitialize() {
        // Safe shutdown of all services
        self.removeListeners();
        NetworkManager.shared.deInitialize();
    }
    
    public func addListeners () {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onDeviceOrientationChange(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBatteryLevelDidChange(_:)), name: UIDevice.batteryLevelDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBatteryStateDidChange(_:)), name: UIDevice.batteryStateDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onProximityStateDidChange(_:)), name: UIDevice.proximityStateDidChangeNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.onFirebaseApplicationInitialized(_:)), name: nlFirebaseApplicationUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onFirebaseVendorInitialized(_:)), name: nlFirebaseVendorUpdated, object: nil)
    }
 
    public func removeListeners() {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIDevice.batteryLevelDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIDevice.batteryStateDidChangeNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIDevice.proximityStateDidChangeNotification, object: nil)

        NotificationCenter.default.removeObserver(self, name: nlFirebaseApplicationUpdated, object: nil)
        NotificationCenter.default.removeObserver(self, name: nlFirebaseVendorUpdated, object: nil)
    }

    @objc public func onFirebaseApplicationInitialized(_ sender: Notification?) {
        if (isFirebaseApplicationInitialized == false){
            InAppManager.shared.initialize()
        }
        isFirebaseApplicationInitialized = true;
        
        weak var weakSelf = self;
        if (applicationInitialized != nil){
            DispatchQueue.main.async {
                weakSelf?.applicationInitialized?(weakSelf?.isFirebaseApplicationInitialized ?? false);
            }
        }
    }

    @objc public func onFirebaseVendorInitialized(_ sender: Notification?) {
        isFirebaseVendorInitialized = true;
        
        weak var weakSelf = self;
        if (vendorInitialized != nil){
            DispatchQueue.main.async {
                weakSelf?.vendorInitialized?(weakSelf?.isFirebaseVendorInitialized ?? false);
            }
        }
    }

    
    // Ads
    static func showAd(_ type: AdType, view: UIView) -> Bool
    {
        switch type {
            case .Banner:
                AdManager.shared.showBannerAd(view: view)
                break;
            case .Interstetial:
                AdManager.shared.showInterstetialAd()
                break;
            case .Video:
                AdManager.shared.showVideoAd()
                break;
            case .InterVideo:
                AdManager.shared.toggleInterstetialVideoAd()
                break;
        }
        return true;
    }
    
    static func getAdId (type : AdType) -> String?{
        return FirebaseManager.shared.getAdId(type);
    }
    
    public static func removeAdsFromApplication () {
        AdManager.shared.removeAdsFromApplication();
    }
    
    public static func canShowBannerAds() -> Bool {
        var retVal = true
        
        if !NeuralLabManager.isFreeApp {
            retVal = false
        } else if FirebaseManager.shared.getAdId(.Banner) != nil {
            if AdManager.shared.isRemoveAdsPurchased {
                retVal = false
            }
        }
        return retVal
    }
    
    public static  func bannerHeight(forView: UIView) -> CGFloat{
        if (NeuralLabManager.canShowBannerAds() == false){
            return 0;
        }
        return AdManager.shared.bannerHeight(forView: forView);
    }

    public static func showBannerAd(withId: String, parentView: UIView, withRemoveAdsStrip: Bool) -> NeuralBaseView?
    {
        if (NeuralLabManager.canShowBannerAds() == false){
            return nil;
        }
        return AdManager.shared.showBannerAd(withId: withId, parentView: parentView, withRemoveAdsStrip: withRemoveAdsStrip);
    }

    public static func purchaseProduct (_ id:String, completion:@escaping InAppPurchaseCompleteBlock) {
        InAppManager.shared.purchaseProduct(id, completion: completion);
    }
    
    public static func restoreInApps (_ completion:@escaping InAppPurchaseRestoreBlock) {
        InAppManager.shared.restoreTransactions(completion: completion);
    }

    public static func hideRemoveAdsPurchaseStrip () {
        AdManager.shared.byPassFirebaseSettingForRemoveAdsStrip = true;
    }
    
    public static func launchFacebook (){
        FirebaseManager.shared.launchFacebookPage();
    }

    public static func launchVendor (){
        FirebaseManager.shared.launchVendorURL();
    }
    
    public static func bundleDisplayName() -> String{
        return BundleManager.shared.bundleDisplayName() ?? BundleManager.applicationName();
    }
    
    public static let isFreeApp:Bool = FirebaseManager.shared.isFreeApp();
    public static let appName:String = FirebaseManager.shared.appName() ?? BundleManager.applicationName();
    public static let storeName:String? = FirebaseManager.shared.storeName();
    public static let removeAdId:String? = FirebaseManager.shared.getRemoveAdId();
    public static let isInAppsReady:Bool = InAppManager.shared.inAppInitialized;
    public static let allInApps:[InAppObject] = InAppManager.shared.inAppObjects;
//    public static let isRemoveAdsPurchased = AdManager.shared.isRemoveAdsPurchased;
    
    public static func isRemoveAdsPurchased() -> Bool {
        return AdManager.shared.isRemoveAdsPurchased;
    }
    
    public static func canShowActiveInAppsToUser () -> Bool {
        return (FirebaseManager.shared.getAllInAppIds().count > 0);
    }
}


//MARK: Assets
extension (NeuralLabManager){
    
    static public func getAudioCollection(_ callback:MediaCollectionAccessCallback?) {
        AssetManager.shared.accessAudioCollections(callback);
    }
    
    static public func getVideoCollection(_ callback:MediaCollectionAccessCallback?) {
        AssetManager.shared.accessVideoCollections(callback);
    }
    
    static public func getImageCollection(_ callback:MediaCollectionAccessCallback?) {
        AssetManager.shared.accessImageCollections(callback);
    }
}


//MARK: Pending Actions
extension (NeuralLabManager){
    static public func addPendingAction (_ string:String?, selector:Selector?){
        if (string != nil){
            NeuralPendingActions.addStringAction(string!);
        }else if (selector != nil){
            NeuralPendingActions.addSelectorAction(selector!)
        }
    }
    static public func removePendingAction (_ string:String?, selector:Selector?) -> Bool{
        if (string != nil){
            return NeuralPendingActions.removeStringAction(string!);
        }else if (selector != nil){
            return NeuralPendingActions.removeSelectorAction(selector!)
        }
        return false;
    }
    static public func containPendingAction (_ string:String?, selector:Selector?) -> Bool{
        if (string != nil){
            return NeuralPendingActions.containsStringAction(string!);
        }else if (selector != nil){
            return NeuralPendingActions.containsSelectorAction(selector!)
        }
        return false;
    }
}


//MARK: Device Info
extension (NeuralLabManager){
    @objc func onDeviceOrientationChange(_ notification:Notification){
        NotificationController.noti_DeviceOrientationDidChange();
    }
    @objc func onBatteryLevelDidChange(_ notification:Notification){
        NotificationController.noti_BatteryLevelDidChange()
    }
    @objc func onBatteryStateDidChange(_ notification:Notification){
        NotificationController.noti_BatteryStateDidChange()
    }
    @objc func onProximityStateDidChange(_ notification:Notification){
        NotificationController.noti_ProximityStateChange();
    }
}



//MARK: NeuralTheming
extension (NeuralLabManager) {
    public func getBackgroundImage() -> UIImage?{
        return NeuralTheming.shared.backgroundImage;
    }
    
    public func setBackgroundImage(path: String) {
        NeuralTheming.shared.backgroundImagePath = path;
    }
}
