//
//  FbAdsModel.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

let kAdProvider_AdMob = "AdMob"
let kAdProvider_StartApp = "StartApp"

class FbAdsModel: NeuralModelProtocol {
    
    static let kFbBanner : String = "Banner"
    static let kFbInterstetial : String = "Interstitial"
    static let kFbInterstetialFrequency : String = "InterstitialFrequency"
    static let kFbVideo : String = "Video"
    static let kFbVideoFrequency : String = "VideoFrequency"
    static let kFbPriority : String = "Priority"
    static let kFbAppId : String = "AppId"

    var banner = ""
    var interstetial = ""
    var video = ""
    var appId = ""
    var interstitialFrequency: NSNumber?
    var videoFrequency: NSNumber?
    var priority: NSNumber?

    //MARK: NeuralModelProtocol
    func getDictionary() -> [AnyHashable: Any] {
        var dict = [AnyHashable : Any]()
        dict[FbAdsModel.kFbBanner] = banner;
        dict[FbAdsModel.kFbInterstetial] = interstetial;
        dict[FbAdsModel.kFbInterstetialFrequency] = interstitialFrequency;
        dict[FbAdsModel.kFbVideo] = video;
        dict[FbAdsModel.kFbVideoFrequency] = videoFrequency;
        dict[FbAdsModel.kFbPriority] = priority;
        dict[FbAdsModel.kFbAppId] = appId;
        return dict
    }
    
    func fromDictionary (dict:[AnyHashable: Any]){
        if let val = dict[FbAdsModel.kFbBanner] {
            banner = val as! String;
        }
        if let val = dict[FbAdsModel.kFbInterstetial] {
            interstetial = val as! String;
        }
        if let val = dict[FbAdsModel.kFbVideo] {
            video = val as! String;
        }
        if let val = dict[FbAdsModel.kFbAppId] {
            appId = val as! String;
        }
        if let val = dict[FbAdsModel.kFbInterstetialFrequency] {
            interstitialFrequency = val as? NSNumber;
        }
        if let val = dict[FbAdsModel.kFbVideoFrequency] {
            videoFrequency = val as? NSNumber;
        }
        if let val = dict[FbAdsModel.kFbVideoFrequency] {
            videoFrequency = val as? NSNumber;
        }
        if let val = dict[FbAdsModel.kFbPriority] {
            priority = val as? NSNumber;
        }
    }
}

protocol FbAdsModelProtocol {
    func fbAdsProviderName () -> String;
    func fbAdsProviderType () -> AdProviderType;
    func getPriority() -> Int;
}

class FbAdMobModel:FbAdsModel, FbModelProtocol,FbAdsModelProtocol {
    
    typealias RET_TYPE = FbAdMobModel

    //MARK: FbModelProtocol
    static func instance(dict: [AnyHashable: Any]) -> FbAdMobModel {
        let model = FbAdMobModel();
        model.fromDictionary(dict:dict);
        return model;
    }

    //MARK: FbAdsModelProtocol
    func fbAdsProviderName () -> String{
        return kAdProvider_AdMob;
    }
    func fbAdsProviderType () -> AdProviderType{
        return .AdMob;
    }
    func getPriority() -> Int{
        return self.priority?.intValue ?? 1;
    }
}

class FbStartAppModel:FbAdsModel, FbModelProtocol,FbAdsModelProtocol {
    
    typealias RET_TYPE = FbStartAppModel

    //MARK: FbModelProtocol
    static func instance(dict: [AnyHashable: Any]) -> FbStartAppModel {
        let model = FbStartAppModel();
        model.fromDictionary(dict:dict);
        return model;
    }

    //MARK: FbAdsModelProtocol
    func fbAdsProviderName () -> String{
        return kAdProvider_StartApp;
    }
    func fbAdsProviderType () -> AdProviderType{
        return .StartApp;
    }
    func getPriority() -> Int{
        return self.priority?.intValue ?? 1;
    }
}
