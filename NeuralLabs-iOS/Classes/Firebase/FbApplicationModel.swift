//
//  FbApplicationModel.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class FbApplicationModel : NeuralModelProtocol, FbModelProtocol {
    typealias RET_TYPE = FbApplicationModel

    static let key : String = "applications"

    static let kFbAppName : String = "appName"
    static let kFbAppleId : String = "appleId"
    static let kFbIsFreeApp : String = "isFreeApp"
    static let kFbStoreName : String = "storeName"
    static let kFbInApps : String = FbInAppPurchaseModel.key;
    static let kFbAdMob : String = kAdProvider_AdMob;
    static let kFbStartApp : String = kAdProvider_StartApp;

    var appName: String?
    var appleId: NSNumber?
    var isFreeApp: NSNumber?
    var storeName: String?
    var startApp: FbStartAppModel?
    var adMob: FbAdMobModel?
    var inApps: [FbInAppPurchaseModel] = [FbInAppPurchaseModel]()

    //MARK: FbModelProtocol
    static func instance(dict: [AnyHashable : Any]) -> FbApplicationModel {
        let model = FbApplicationModel();
        model.fromDictionary(dict:dict);
        return model;
    }
    
    //MARK: NeuralModelProtocol
    func getDictionary() -> [AnyHashable: Any] {
        var dict = [AnyHashable : Any]()
        
        dict[FbApplicationModel.kFbAppName] = appName;
        dict[FbApplicationModel.kFbAppleId] = appleId;
        dict[FbApplicationModel.kFbIsFreeApp] = isFreeApp;
        dict[FbApplicationModel.kFbStoreName] = storeName;

        var inAppsDict:[[AnyHashable : Any]] = [[AnyHashable : Any]]()
        for inApp:FbInAppPurchaseModel in self.inApps {
            inAppsDict.append(inApp.getDictionary());
        }
        dict[FbApplicationModel.kFbInApps] = inAppsDict;
        dict[FbApplicationModel.kFbAdMob] = adMob?.getDictionary();
        dict[FbApplicationModel.kFbStartApp] = startApp?.getDictionary();

        return dict
    }
    
    func fromDictionary(dict: [AnyHashable: Any]) {
        if let val = dict[FbApplicationModel.kFbAppName] {
            appName = val as? String;
        }
        if let val = dict[FbApplicationModel.kFbAppleId] {
            appleId = val as? NSNumber;
        }
        if let val = dict[FbApplicationModel.kFbIsFreeApp] {
            isFreeApp = val as? NSNumber;
        }
        if let val = dict[FbApplicationModel.kFbStoreName] {
            storeName = val as? String;
        }
        if let val = dict[FbApplicationModel.kFbStartApp] {
            startApp = FbStartAppModel.instance(dict: (val as? [AnyHashable:Any])!);
        }
        if let val = dict[FbApplicationModel.kFbAdMob] {
            adMob = FbAdMobModel.instance(dict: (val as? [AnyHashable:Any])!);
        }
        if let val = dict[FbApplicationModel.kFbInApps] {
            self.inApps.removeAll();
            for dInApp in val as! [[AnyHashable: Any]]{
                self.inApps.append(FbInAppPurchaseModel.instance(dict: dInApp));
            }
        }
    }

    
    // AdKeys
    func adProviderType() -> AdProviderType {
        
        var priorityModel : [FbAdsModelProtocol] = [FbAdsModelProtocol]();
        if let m = self.adMob {
            priorityModel.append(m);
        }
        if let m = self.startApp {
            priorityModel.append(m);
        }

        if (priorityModel.count > 0){
            priorityModel.sort { (model1, model2) -> Bool in
                return model1.getPriority() <  model2.getPriority();
            }
            
            return priorityModel[0].fbAdsProviderType();
        }
        
        return .NoAds;
    }
    
    func getBannerId () -> String? {
        return self.bannerId(provider: self.adProviderType());
    }
    
    func getInterstetialId() -> String? {
        return self.interstetialId(provider: self.adProviderType());
    }

    func getVideoId() -> String? {
        return self.videoId(provider: self.adProviderType());
    }
    
    func getInterstetialFrequency() -> Int {
        return self.interstetialFrequency(provider: self.adProviderType());
    }

    func getVideoFrequency() -> Int {
        return self.videoFrequency(provider: self.adProviderType());
    }

    func getAdsAppId() -> String {
        return self.adsAppId(provider: self.adProviderType())
    }

    func getAllInAppIds () -> Set<String> {
        return self.allInAppIds();
    }
    
    func isInAppActive (inApp : String) -> Bool {
        if let fbInApp = self.inApps.first(where: {$0.id == inApp}){
            return fbInApp.id.toBool() ?? false;
        }
        return false;
    }
    
    func getRemoveAdId() -> String? {
        return self.removeAdId();
    }

    private func bannerId (provider : AdProviderType) -> String?{
        
        var retVal : String? = nil;
        switch provider {
        case .AdMob:
            retVal = self.adMob?.banner;
            if (retVal == nil){
                retVal = self.bannerId(provider: .StartApp)
            }
            break;
        case .StartApp:
            retVal = self.startApp?.banner;
            break;
        default:
            break;
        }
        return retVal;
    }

    private func interstetialId (provider : AdProviderType) -> String?{
        var retVal : String? = nil;
        switch provider {
        case .AdMob:
            retVal = self.adMob?.interstetial;
            if (retVal == nil){
                retVal = self.interstetialId(provider: .StartApp)
            }
            break;
        case .StartApp:
            retVal = self.startApp?.interstetial;
            break;
        default:
            break;
        }
        return retVal;
    }

    private func videoId (provider : AdProviderType) -> String?{
        var retVal : String? = nil;
        switch provider {
        case .AdMob:
            retVal = self.adMob?.video;
            if (retVal == nil){
                retVal = self.videoId(provider: .StartApp)
            }
            break;
        case .StartApp:
            retVal = self.startApp?.video;
            break;
        default:
            break;
        }
        return retVal;
    }
    
    private func interstetialFrequency (provider : AdProviderType) -> Int {
        var retVal : Int = 1;
        switch provider {
        case .AdMob:
            retVal = self.adMob?.interstitialFrequency?.intValue ?? defaultInterstetialFrequency;
            break;
        case .StartApp:
            retVal = self.startApp?.interstitialFrequency?.intValue ?? defaultInterstetialFrequency;
            break;
        default:
            break;
        }
        return retVal;
    }
    
    private func videoFrequency (provider : AdProviderType) -> Int {
        var retVal : Int = 1;
        switch provider {
        case .AdMob:
            retVal = self.adMob?.videoFrequency?.intValue ?? defaultVideoFrequency;
            break;
        case .StartApp:
            retVal = self.startApp?.videoFrequency?.intValue ?? defaultVideoFrequency;
            break;
        default:
            break;
        }
        return retVal;
    }

    private func adsAppId (provider : AdProviderType) -> String {
        var retVal : String = ""
        switch provider {
        case .AdMob:
            retVal = self.adMob?.appId ?? "";
            break;
        case .StartApp:
            retVal = self.startApp?.appId ?? "";
            break;
        default:
            break;
        }
        return retVal;
    }

    private func allInAppIds () -> Set<String>{
        // return active inapps from firebase
        let inApps:[String] = self.inApps.filter { (inAppModel) -> Bool in
            return inAppModel.isActive?.boolValue ?? false;
            }.map({$0.id})
        let retVal :Set<String> = Set<String>(inApps);
        return retVal;
    }
    
    private func removeAdId() -> String? {
        return self.inApps.first(where: {$0.name == FbInAppPurchaseModel.kUDRemoveAdsValue})?.id
    }
}
