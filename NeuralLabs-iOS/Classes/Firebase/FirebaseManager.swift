//
//  FirebaseController.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseCore
import FirebaseAuth
import FirebaseInvites
import FirebaseStorage
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseFunctions
import FirebaseMessaging
import FirebasePerformance


protocol FbModelProtocol {
    associatedtype RET_TYPE;
    static func instance (dict: [AnyHashable: Any]) -> RET_TYPE;
}


class FirebaseManager {
    
    static let shared : FirebaseManager = FirebaseManager();
    
    private var applicationDBRef: DatabaseReference?
    private var vendorDBRef: DatabaseReference?
    private var applicationModel: FbApplicationModel? = nil;
    private var vendorModel: FbVendorModel? = nil;
    
    func initialize () {
        Logger.shared.firebaseLog("Version", "\(String(describing: DatabaseReference.version))")
        
        FirebaseApp.configure();
        
        self.loadFromuserDefaults();
        self.loadFromFirebase();
    }
    
    func loadFromuserDefaults () {
        if let application : [AnyHashable:Any] = NeuralDefaults.getFirebase(FbApplicationModel.key) as? [AnyHashable : Any]
        {
            self.applicationModel = FbApplicationModel.instance(dict: application);
            NotificationController.noti_firebaseApplicationUpdated();
        }

        if let vendor : [AnyHashable:Any] = NeuralDefaults.getFirebase(FbVendorModel.key) as? [AnyHashable : Any]
        {
            self.vendorModel = FbVendorModel.instance(dict: vendor);
            NotificationController.noti_firebaseVendorUpdated();
        }
    }
    
    func loadFromFirebase () {
        self.loadApplicationRefrence()
        self.loadVendorRefrence()
    }

    func loadVendorRefrence () {
        
        let path : String = FbVendorModel.key
        vendorDBRef = Database.database().reference().child(path);
        weak var weakSelf: FirebaseManager? = self
        
        vendorDBRef?.runTransactionBlock({ (currentData:MutableData) -> FirebaseDatabase.TransactionResult in
            
            if let data = currentData.value as? [AnyHashable : Any]{
                let obj = FbVendorModel.instance(dict: data)
                let dict = obj.getDictionary()
                Logger.shared.firebaseTagLog("Vendor Directory", dictMsgs: dict);
                
                if (dict.count > 0) {
                    weakSelf?.vendorModel = obj
                    NeuralDefaults.setFirebase(FbVendorModel.key, value: dict);
                    NotificationController.noti_firebaseVendorUpdated();
                }
            }

            return FirebaseDatabase.TransactionResult.success(withValue: currentData)
        })
    }

    func loadApplicationRefrence () {
        
        let path : String = "\(FbApplicationModel.key)/\(firebaseBundleId())"
        applicationDBRef = Database.database().reference().child(path);
        weak var weakSelf: FirebaseManager? = self
        
        applicationDBRef?.runTransactionBlock({ currentData in
            
            if let data = currentData.value as? [AnyHashable : Any]{
                let obj = FbApplicationModel.instance(dict: data)
                let dict = obj.getDictionary()
                Logger.shared.firebaseTagLog("Application Directory", dictMsgs: dict);
                
                if (dict.count > 0) {
                    weakSelf?.applicationModel = obj
                    NeuralDefaults.setFirebase(FbApplicationModel.key, value: dict);
                    NotificationController.noti_firebaseApplicationUpdated();
                }
            }else{
                // probalbly we have hit the wrong/misconfigured URL.
                NotificationController.noti_firebaseApplicationUpdated();
            }
            return FirebaseDatabase.TransactionResult.success(withValue: currentData)
        })
    }
    
    func firebaseBundleId() -> String {
        var bundle : String? = BundleManager.shared.bundleIdentifier;
        bundle = bundle?.replacingOccurrences(of: ".", with: "-")
        return bundle!
    }
}



extension (FirebaseManager) {
    // This extension will provide some quick external support without traversal
    
    func isFreeApp() -> Bool {
        if let val = applicationModel?.isFreeApp{
            return val.boolValue
        }
        return false;
    }
    
    func appId() -> NSNumber? {
        return applicationModel?.appleId;
    }
    
    func appName() -> String? {
        return applicationModel?.appName;
    }
    
    func storeName() -> String? {
        return applicationModel?.storeName;
    }
    
    func vendorUrl() -> String? {
        return vendorModel?.url;
    }
    
    func launchFacebookPage() {
        
        let fbUrl = vendorModel?.facebookpage
        if fbUrl == nil {
            Logger.shared.firebaseLog("Cannot open facebook page with nil url", #function);
            return
        }
        let url = URL(string: fbUrl!)
        if url == nil {
            Logger.shared.firebaseLog("Error in parsing facebook url", url?.absoluteString ?? "", #function);
            return
        }
        
        url?.open(completionCallback: { (done) in
            if !done {
                Logger.shared.firebaseLog("Error in launching facebook url", url?.absoluteString ?? "", #function);
            }
        })
    }

    func launchVendorURL() {
        
        let fbUrl = vendorModel?.url
        if fbUrl == nil {
            Logger.shared.firebaseLog("Cannot open vendor page with nil url", #function);
            return
        }
        let url = URL(string: fbUrl!)
        if url == nil {
            Logger.shared.firebaseLog("Error in parsing vendor url", url?.absoluteString ?? "", #function);
            return
        }
        
        url?.open(completionCallback: { (done) in
            if !done {
                Logger.shared.firebaseLog("Error in launching vendor url", url?.absoluteString ?? "", #function);
            }
        })
    }
}


//MARK: Ads extension
extension (FirebaseManager){
    
    func adProviderType () -> AdProviderType {
        if let app = self.applicationModel {
            return app.adProviderType();
        }
        return .NoAds;
    }
    
    func getAdId(_ type: AdType) -> String? {
        
        var retVal:String? = nil;
        
        switch type {
        case .Banner:
            if let id = applicationModel?.getBannerId(){
                retVal = id;
            }
            break;
        case .Interstetial:
            if let id = applicationModel?.getInterstetialId(){
                retVal = id;
            }
            break;
        case .Video:
            if let id = applicationModel?.getVideoId(){
                retVal = id;
            }
            break;
        default:
            break
        }
        return retVal
    }
    
    func getInterstetialFrequency() -> Int {
        return self.applicationModel?.getInterstetialFrequency() ?? defaultInterstetialFrequency;
    }
    
    func getVideoFrequency() -> Int {
        return self.applicationModel?.getVideoFrequency() ?? defaultVideoFrequency;
    }
    
    func getAdsAppId() -> String {
        return self.applicationModel?.getAdsAppId() ?? "";
    }
}


//InApp Extension
extension (FirebaseManager){
    
    func inApps () -> [FbInAppPurchaseModel]? {
        return self.applicationModel?.inApps;
    }

    func isInAppActive (_ inApp : String) -> Bool {
        return self.applicationModel?.isInAppActive(inApp:inApp) ?? false;
    }
    
    func getAllInAppIds () -> Set<String> {
        return self.applicationModel?.getAllInAppIds() ?? Set<String>();
    }
    
    func getRemoveAdId () -> String? {
        return self.applicationModel?.getRemoveAdId();
    }
}
