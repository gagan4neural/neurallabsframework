//
//  FirebaseConfiguration.swift
//  NeuralLabs-iOS
//
//  Created by Gagandeep Madan on 12/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import FirebaseCore

internal struct FirebaseConfigurationError : Error {
    let message: String;
}


internal class FirebaseConfiguration {
    
    //MARK: FirebaseConfigurations
    var CLIENT_ID: String?;
    var REVERSED_CLIENT_ID: String?;
    var API_KEY: String?;
    var GCM_SENDER_ID: String?;
    var PLIST_VERSION: String?;
    var BUNDLE_ID: String?;
    var PROJECT_ID: String?;
    var STORAGE_BUCKET: String?;
    var IS_ADS_ENABLED: Bool = false;
    var IS_ANALYTICS_ENABLED: Bool = false;
    var IS_APPINVITE_ENABLED: Bool = false;
    var IS_GCM_ENABLED: Bool = false;
    var IS_SIGNIN_ENABLED: Bool = false;
    var GOOGLE_APP_ID: String?;
    var DATABASE_URL: String?;
    
    //MARK: Custom Configurations
    var appName: String; // this is firebase app name not the iOS app name
    
    
    
    //MARK: Initialisation methods
    
    init(appName: String) {
        self.appName = appName;
    }
    
    
    func firebaseOptions() throws -> FirebaseOptions? {
        
        guard let googleAppId = self.GOOGLE_APP_ID else{
            throw FirebaseConfigurationError(message: "Google app ID not found in configuration.");
        }

        guard let gcmId = self.GCM_SENDER_ID else{
            throw FirebaseConfigurationError(message: "GCM ID not found in configuration.");
        }

        let retVal = FirebaseOptions(googleAppID: googleAppId, gcmSenderID: gcmId);
        
        retVal.clientID = self.CLIENT_ID;
        retVal.apiKey = self.API_KEY;
        
        if let id = self.BUNDLE_ID {
            retVal.bundleID = id;
        }else{
            throw FirebaseConfigurationError(message: "Bundle ID not found in configuration.");
        }
        retVal.projectID = self.PROJECT_ID;
        retVal.storageBucket = self.STORAGE_BUCKET;
        if let id = self.GOOGLE_APP_ID {
            retVal.googleAppID = id;
        }else{
        }
        retVal.databaseURL = self.DATABASE_URL;
        
        return retVal;
    }
}
