//
//  FbInAppPurchaseModel.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class FbInAppPurchaseModel : NeuralModelProtocol, FbModelProtocol {
    
    typealias RET_TYPE = FbInAppPurchaseModel

    static let key : String = "inApps"

    static let kFbId : String = "id"
    static let kFbisActive : String = "isActive"
    static let kFbName : String = "name"
    static let kFbPrice : String = "price"

    static let kUDRemoveAdsValue : String = "RemoveAds"
    static let kUDInAppsPurchased : String = "InAppsPurchased"

    var id = ""
    var isActive:NSNumber? = nil;
    var name = ""
    var price:NSNumber? = nil;
    

    //MARK: FbModelProtocol
    static func instance(dict: [AnyHashable: Any]) -> FbInAppPurchaseModel {
        let model = FbInAppPurchaseModel();
        model.fromDictionary(dict:dict);
        return model;
    }

    //MARK: NeuralModelProtocol
    func getDictionary() -> [AnyHashable : Any] {
        var dict = [AnyHashable : Any]()
        dict[FbInAppPurchaseModel.kFbId] = id;
        dict[FbInAppPurchaseModel.kFbisActive] = isActive;
        dict[FbInAppPurchaseModel.kFbName] = name;
        dict[FbInAppPurchaseModel.kFbPrice] = price;
        return dict
    }
    
    func fromDictionary(dict: [AnyHashable : Any]) {
        if let val = dict[FbInAppPurchaseModel.kFbId] {
            id = val as! String;
        }
        if let val = dict[FbInAppPurchaseModel.kFbisActive] {
            isActive = val as? NSNumber;
        }
        if let val = dict[FbInAppPurchaseModel.kFbName] {
            name = val as! String;
        }
        if let val = dict[FbInAppPurchaseModel.kFbPrice] {
            price = val as? NSNumber;
        }
    }
}
