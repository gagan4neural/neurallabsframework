//
//  FbVendorModel.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class FbVendorModel: NeuralModelProtocol, FbModelProtocol {
    
    static let key : String = "vendor"

    static let kFbId : String = "id"
    static let kFbURL : String = "url"
    static let kFbFacebookPage : String = "facebookpage"

    var id = ""
    var url = ""
    var facebookpage = ""
    
    //MARK: FbModelProtocol
    static func instance(dict: [AnyHashable: Any]) -> FbVendorModel {
        let model = FbVendorModel();
        model.fromDictionary(dict:dict);
        return model;
    }
    
    //MARK: NeuralModelProtocol
    func getDictionary() -> [AnyHashable : Any] {
        var dict = [AnyHashable : Any]()
        dict[FbVendorModel.kFbId] = id;
        dict[FbVendorModel.kFbURL] = url;
        dict[FbVendorModel.kFbFacebookPage] = facebookpage;
        return dict
    }
    
    func fromDictionary(dict: [AnyHashable : Any]) {
        if let val = dict[FbVendorModel.kFbId] {
            id = val as! String;
        }
        if let val = dict[FbVendorModel.kFbURL] {
            url = val as! String;
        }
        if let val = dict[FbVendorModel.kFbFacebookPage] {
            facebookpage = val as! String;
        }
    }
}
