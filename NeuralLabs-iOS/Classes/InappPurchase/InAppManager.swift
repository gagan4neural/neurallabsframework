//
//  InAppManager.swift
//  NeuralLabs
//
//  Created by Gagan on 01/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import StoreKit

class InAppManager {
    
    static var shared : InAppManager = InAppManager();
    var inAppInitialized : Bool = false;
    
    var fbInApps: [FbInAppPurchaseModel]? {
        get{
            return FirebaseManager.shared.inApps()
        }
    }
    var inAppObjects: [InAppObject] = [InAppObject]();

    func initialize() {
        
        self.setupIAP();

        weak var weakSelf = self;
        let pids : Set<String> = FirebaseManager.shared.getAllInAppIds();
        
        self.reteriveProductsInfo(pids) { (successInApps, errInApps) in
            
            Logger.shared.inAppLog("Successfully Initialized : ", String(successInApps.count), "Products");
            Logger.shared.inAppLog("Failed to initialized : ", String(errInApps.count), "Products");
            if (pids.count == successInApps.count + errInApps.count){
                weakSelf?.inAppInitialized = true;
            }
        }
    }
    
    func isInAppActive (inApp : String) -> Bool {
        return FirebaseManager.shared.isInAppActive(inApp);
    }
    
    func canMakePayments () -> Bool {
        return SwiftyStoreKit.canMakePayments;
    }
    
    func allInAppIds() -> [String] {
        let ids = self.inAppObjects.compactMap({$0.identifier});
        return ids;
    }
    
    func reteriveProductsInfo (_ pIds:Set<String>, infoCallback:@escaping InAppInitializeBlock) {
        
        weak var weakSelf = self;
        SwiftyStoreKit.retrieveProductsInfo(pIds) { (reteriveResult) in
            weakSelf?.retereiveProductInfo(callback: infoCallback, reteriveResult: reteriveResult);
        };
    }
    
    func restoreTransactions (completion:@escaping InAppPurchaseRestoreBlock){
        
        if (inAppInitialized == false){
            Logger.shared.inAppLog("InApps not initialized");
            return;
        }
        
        SwiftyStoreKit.restorePurchases(atomically: true, applicationUsername: BundleManager.applicationName()) { (restoreResult) in
            
            var successRestored = [SuccessString]()
            var failedRestored = [ErrorString]()

            for (skError, _) in restoreResult.restoreFailedPurchases {
                failedRestored.append(skError.localizedDescription);
            }
            
            for purchased in restoreResult.restoredPurchases {
                
                let downloads = purchased.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                } else if purchased.needsFinishTransaction {
                    // Deliver content from server, then:
                    SwiftyStoreKit.finishTransaction(purchased.transaction)
                }

                let iObj = self.getInAppObject(forId: purchased.productId);
                iObj?.purchased = true;
                successRestored.append(purchased.productId);
            }
            
            completion(successRestored, failedRestored)
        }
    }
    
    func purchaseProduct (_ id:String, completion:@escaping InAppPurchaseCompleteBlock) {
        
        if (inAppInitialized == false){
            completion(nil, "InApps not initialized");
            Logger.shared.inAppLog("InApps not initialized");
            return;
        }

        if (self.canMakePayments() == false){
            completion(nil, "Cannot make payments");
            return;
        }
        
        if (self.isProductPurchased(id)){
            completion(nil, "Product already purchased");
            return;
        }
        
        weak var weakSelf = self;
        
        SwiftyStoreKit.purchaseProduct(id, quantity: 1, atomically: true, applicationUsername: BundleManager.applicationName(), simulatesAskToBuyInSandbox: false) { (purchaseResult) in
            
            if case .success(let purchase) = purchaseResult {
                let downloads = purchase.transaction.downloads
                if !downloads.isEmpty {
                    SwiftyStoreKit.start(downloads)
                }
                // Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                let iObj = weakSelf?.getInAppObject(forId: id);
                iObj?.purchased = true;
                completion(id, nil);
            }
            else{
                completion(nil, "Failed to purchase products")
            }
        }
    }
    
    func isProductPurchased (_ id:String) -> Bool {
        
        if (inAppInitialized){
            // this is our main source to get values from in memory.
            let iObj = self.getInAppObject(forId: id);
            return iObj?.purchased ?? false;
        }else{
            //Untill the in apps are initized rely on old values from user default.
            return InAppObject.isProductPurchased(id);
        }
    }
    
    private func retereiveProductInfo(callback:InAppInitializeBlock, reteriveResult:RetrieveResults){
        
        var successInApps:[SuccessInAppObjects] = [SuccessInAppObjects]()
        var errorInApps:[ErrorInAppObjects] = [ErrorInAppObjects]()
        
        if (reteriveResult.error != nil){
            Logger.shared.inAppLog("Failed to retereive inapps : ", reteriveResult.error?.localizedDescription ?? "ERR")
        }else{
            
            for invalidProd in reteriveResult.invalidProductIDs {
                self.removeInAppObject(forId: invalidProd);
                let obj = InAppObject(product: nil, isValid: false);
                self.inAppObjects.append(obj);
                errorInApps.append(obj);
            }

            for retrProd in reteriveResult.retrievedProducts {
                self.removeInAppObject(forId: retrProd.productIdentifier);
                let obj = InAppObject(product: retrProd, isValid: true);
                self.inAppObjects.append(obj);
                successInApps.append(obj);
            }
        }
        callback(successInApps, errorInApps);
    }
    
    private func removeInAppObject(forId:String){
        self.inAppObjects.removeAll(where: {$0.identifier == forId});
    }

    private func getInAppObject(forId:String) -> InAppObject?{
        let iAp = self.inAppObjects.first(where: {$0.identifier == forId});
        return iAp;
    }

    private func setupIAP() {
        
        Logger.shared.inAppLog("Setting UP IAP", #function);
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            
            for purchase in purchases {
                
                Logger.shared.inAppLog("Transaction Completed for purchase \(purchase.productId)",
                    "Quantity :\(purchase.quantity)",
                    " Transaction State :\(purchase.transaction.transactionState)",
                    " Transaction Date :\(String(describing: purchase.transaction.transactionDate))")
                
                switch purchase.transaction.transactionState
                {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }

}

