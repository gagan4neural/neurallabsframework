//
//  InAppObject.swift
//  NeuralLabs
//
//  Created by Gagan on 02/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import StoreKit

fileprivate let kInAppsPurchased = "InAppsPurchased"

open class InAppObject {
    
    public var title: String? {
        get{
            return self.product?.localizedTitle;
        }
    }
    public var description : String? {
        get{
            return self.product?.localizedDescription;
        }
    }
    public var amount: String? {
        get{
            return self.product?.price.stringValue;
        }
    }
    public var localProductPrice: String? {
        get{
            return self.product?.localizedPrice;
//            return self.product?.price.stringValue.moneyString(self.product?.priceLocale ?? Locale(identifier: "en_US"));
        }
    }
    public var identifier : String? {
        get{
            return self.product?.productIdentifier;
        }
    }
    var icon = ""
    public var purchased : Bool {
        get{
            let inAppsPurchased:[String]? = NeuralDefaults.getInapp(kInAppsPurchased) as? [String];
            if let id =  identifier{
                return (inAppsPurchased?.contains(id) ?? false);
            }
            return false
        }
        set (newValue){
            var inAppsPurchased:[String]? = (NeuralDefaults.getInapp(kInAppsPurchased) as? [String]);
            if let id =  identifier{
                if (inAppsPurchased == nil){
                    inAppsPurchased = [String]();
                }
                inAppsPurchased?.append(id);
                NeuralDefaults.setInApp(kInAppsPurchased, value: inAppsPurchased as Any);
            }
        }
    }
    public var error : String?
    private var _isValid : Bool = false;
    public var isValid : Bool {
        get{
            return (self.error == nil)
        }
    };
    
    private var product: SKProduct?

    init (error:String?) {
        self.error = error;
    }
    
    init (product:SKProduct?, isValid:Bool) {
        self.product = product;
    }
    
    static func isProductPurchased (_ identifier:String?) -> Bool{
        let inAppsPurchased:[String]? = NeuralDefaults.getInapp(kInAppsPurchased) as? [String];
        if let id =  identifier{
            return (inAppsPurchased?.contains(id) ?? false);
        }
        return false
    }
}
