//
//  NeuralObjc.swift
//  NeuralLabs
//
//  Created by Gagan on 13/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

@objc open class NeuralObjc: NSObject {

    static let instance = NeuralObjc();
    
    @objc public static func shared() -> NeuralObjc {
        return NeuralObjc.instance;
    }
    
    @objc static public func launchMoreApps() {
        NeuralLabManager.launchVendor();
    }
    @objc static public func canShowBannerAds() -> Int {
        return (NeuralLabManager.canShowBannerAds() == true) ? 1 : 0;
    }
    
    //MARK: Fonts
    @objc public func iconFont () -> UIFont? {
        return FontManager.shared.iconFont();
    }
    @objc public func iconFont (size: CGFloat) -> UIFont? {
        return FontManager.shared.iconFont(size:size);
    }
    @objc public static func helveticaNeue(_ size: CGFloat) -> UIFont? {
        return FontManager.shared.helveticaNeue(size);
    }
    @objc public static func helveticaNeueLight(_ size: CGFloat) -> UIFont? {
        return FontManager.shared.helveticaNeueLight(size);
    }
    @objc public static func badgeFont() -> UIFont? {
        return FontManager.shared.badgeFont();
    }
    @objc public static func titleFontSize() -> CGFloat {
        return nlUtils.shared.titleFontSize();
    }
    @objc public let arrowLeft = IconFontCodes.arrowLeft;
    @objc public let more_vert = IconFontCodes.more_vert;
    @objc public let menu = IconFontCodes.menu;
    @objc public let favoriteOutline = IconFontCodes.favoriteOutline;
    @objc public let favoriteFill = IconFontCodes.favoriteFill;
    @objc public let share = IconFontCodes.share;
    @objc public let check = IconFontCodes.check;
    @objc public let edit = IconFontCodes.edit;
    @objc public let close = IconFontCodes.close;
    @objc public let folder = IconFontCodes.folder;
    @objc public let money = IconFontCodes.money;
    @objc public let time_restore_setting = IconFontCodes.time_restore_setting;
    @objc public let check_circle = IconFontCodes.check_circle;
    @objc public let mic = IconFontCodes.mic;
    @objc public let chevronDown = IconFontCodes.chevronDown;
    @objc public let youtube = IconFontCodes.youtube;
    @objc public let play = IconFontCodes.play;
    @objc public let playFill = IconFontCodes.playFill;
    @objc public let playOutline = IconFontCodes.playOutline;
    @objc public let playlistAudio = IconFontCodes.playlistAudio;
    @objc public let pause = IconFontCodes.pause;
    @objc public let pauseFill = IconFontCodes.pauseFill;
    @objc public let pauseOutline = IconFontCodes.pauseOutline;
    @objc public let timeInterval = IconFontCodes.timeInterval;
    @objc public let camera = IconFontCodes.camera;
    @objc public let camera_alt = IconFontCodes.camera_alt;
    @objc public let camera_add = IconFontCodes.camera_add;
    @objc public let camera_rear = IconFontCodes.camera_rear;
    @objc public let scissors = IconFontCodes.scissors;
    @objc public let pictureInPicture = IconFontCodes.pictureInPicture;
    @objc public let lockClose = IconFontCodes.lockClose;
    @objc public let lockOpen = IconFontCodes.lockOpen;
    @objc public let collection_item = IconFontCodes.collection_item;
    @objc public let collection_video = IconFontCodes.collection_video;
    @objc public let album = IconFontCodes.album;
    @objc public let skipNext = IconFontCodes.skipNext;
    @objc public let skipPrev = IconFontCodes.skipPrev;
    @objc public let shuffle = IconFontCodes.shuffle;
    @objc public let shopping_cart_plus = IconFontCodes.shopping_cart_plus;
    @objc public let mute = IconFontCodes.mute;
    @objc public let volumeUp = IconFontCodes.volumeUp;
    @objc public let volumeDown = IconFontCodes.volumeDown;
    @objc public let _repeat = IconFontCodes._repeat;
    @objc public let _8tracks = IconFontCodes._8tracks;
    @objc public let format_list_bulleted = IconFontCodes.format_list_bulleted;
    @objc public let rotateRight = IconFontCodes.rotateRight;
    @objc public let mic_off = IconFontCodes.mic_off;
    @objc public let flag = IconFontCodes.flag;
    @objc public let filledStar = IconFontCodes.filledStar;
    @objc public let mail_send = IconFontCodes.mail_send;
    @objc public let infoFill = IconFontCodes.infoFill;
    @objc public let refresh_sync = IconFontCodes.refresh_sync;
    @objc public let gridView = IconFontCodes.gridView;
    @objc public let listView = IconFontCodes.listView;
    @objc public let audio = IconFontCodes.audio;
    @objc public let block = IconFontCodes.block;
    @objc public let help_outline = IconFontCodes.help_outline;
//    @objc public let listView = IconFontCodes.listView;


    
    
    //MARK: nlUtils
    @objc public func titleBarHeight () -> CGFloat {
        return nlUtils.shared.titlebarHeight();
    }
    @objc public func isIpad() -> Int {
        return (nlUtils.shared.isIpad() == true) ? 1 : 0;
    }
    @objc public func isPortrait() -> Int {
        return (nlUtils.shared.isPortrait() == true) ? 1 : 0;
    }
    @objc public func showToast(onView view:UIView?, msg:String, duration:CGFloat){
        nlUtils.shared.showToast(msg, on: view, timeDuration: duration)
    }
    @objc public func showHud(onView view:UIView?, msg:String) {
        nlUtils.shared.showHud(msg, on: view);
    }
    @objc public func hideHud(fromView view:UIView?) {
        nlUtils.shared.hideHud(from: view)
    }
    @objc public func stringFromTime(ms:TimeInterval) -> NSString? {
        return String.timeFrom(interval: ms) as NSString?;
    }
    @objc public static func tableViewCellHeight() -> CGFloat{
        return nlUtils.shared.tableViewCellHeight();
    }
    @objc public func collectionViewSize() -> CGSize{
        return nlUtils.shared.collectionViewSize();
    }
    @objc public static func dismissAllPopOvers(_ nvc:UINavigationController?){
        if (nvc == nil){
            return nlUtils.shared.dismissAllPopOvers(fromNavigationController:nlUtils.shared.applicationNavigationController());
        }else{
            return nlUtils.shared.dismissAllPopOvers(fromNavigationController:nvc);
        }
    }
    @objc public static func presentOrDismissModalController(_ viewController: UIViewController?, on navigationController: UINavigationController?){
        if navigationController == nil{
            nlUtils.shared.presentOrDismissModalController(viewController, on: nlUtils.shared.applicationNavigationController());
        }else{
            nlUtils.shared.presentOrDismissModalController(viewController, on: navigationController);
        }
    }
    @objc public func defaultRect() -> CGRect {
        return nlUtils.shared.defaultRect();
    }
    @objc static public func rearrange(_ vc: UIViewController?, for nav: UINavigationController?, show: Bool, shouldPresent present: Bool, completion complete: @escaping flowCompletion) {
        return nlUtils.shared.rearrange(vc, for: nav, show: show, shouldPresent: present, completion: complete);
    }
    @objc static public func appNavigationController () -> UINavigationController? {
        return nlUtils.shared.applicationNavigationController()
    }
    @objc static public func feedbackMsg () -> String{
        return nlUtils.shared.feedbackMsg;
    }
    @objc static public func feedbackReciptant () -> String{
        return nlUtils.shared.feedbackReciptant;
    }
    @objc static public func feedbackSubject () -> String{
        return nlUtils.shared.feedbackSubject;
    }
    @objc static public func documentDirectoryContents() -> [Any]? {
        return nlUtils.shared.documentDirectoryContents();
    }
    @objc static public func clearDocumentDirectory() {
        return nlUtils.shared.clearDocumentDirectory();
    }
    @objc static public func shareContents(_ contents: [Any]?, on controller: UIViewController?, anchorView anchor: UIView?, completion: @escaping flowCompletion) {
        nlUtils.shared.shareContents(contents, on: controller, anchorView: anchor, completion: completion);
    }
    @objc static public func share(_ image: UIImage?, onViewController controller: UIViewController?, anchorView anchor: UIView?, completion: @escaping flowCompletion) {
        nlUtils.shared.share(image, on: controller, anchorView: anchor, completion: completion);
    }
    @objc static public func documentDirectory() -> String? {
        return nlUtils.shared.documentDirectory();
    }
    @objc static public func badgeSize() -> CGSize {
        return nlUtils.shared.badgeSize();
    }

    
    // Bundle
    @objc static public func bundleIdentifier() -> String? {
        return BundleManager.shared.bundleIdentifier;
    }
    @objc static public func bundleDisplayName() -> String? {
        return BundleManager.shared.bundleDisplayName();
    }

    
    //MARK: Ads
    @objc public static func isRemoveAdsPurchased() -> Int {
        return (AdManager.shared.isRemoveAdsPurchased == true) ? 1 : 0;
    }
    @objc public static func showInterstetialAd() {
        AdManager.shared.showInterstetialAd();
    }
    @objc public static func showVideoAd() {
        AdManager.shared.showVideoAd();
    }
    @objc public static func showBannerAd(view:UIView) {
        AdManager.shared.showBannerAd(view: view);
    }
    
    //MARK: InApps
    @objc public static func isInAppPurchased(identifier:String) -> Int {
        return (InAppManager.shared.isProductPurchased(identifier)) == true ? 1 : 0;
    }
    @objc public static func isInAppActive(identifier:String) -> Int {
        return (InAppManager.shared.isInAppActive(inApp: identifier)) == true ? 1 : 0;
    }
    @objc public static func purchaseProduct (_ id:String, completion:@escaping InAppPurchaseCompleteBlock) {
        InAppManager.shared.purchaseProduct(id, completion: completion);
    }
    @objc public static func isInAppsReady() -> Int{
        return InAppManager.shared.inAppInitialized ? 1 : 0
    }
    @objc public static func restoreTransactions (completion:@escaping InAppPurchaseRestoreBlock){
        InAppManager.shared.restoreTransactions(completion: completion);
    }
    @objc public static func allInAppIds () -> [String]{
        return InAppManager.shared.allInAppIds()
    }
    @objc public static func inAppTitle(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].title;
    }
    @objc public static func inAppPrice(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].localProductPrice;
    }
    @objc public static func inAppDescription(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].description;
    }
    @objc public static func inAppIcon(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].icon;
    }
    @objc public static func inAppPurchased(atIndex index:Int) -> Int{
        return (InAppManager.shared.inAppObjects[index].purchased == true) ? 1 : 0;
    }
    @objc public static func inAppAmount(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].amount;
    }
    @objc public static func inAppIdentifier(atIndex index:Int) -> String?{
        return InAppManager.shared.inAppObjects[index].identifier;
    }
}

