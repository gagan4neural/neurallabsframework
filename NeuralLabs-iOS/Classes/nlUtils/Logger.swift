//
//  Logger.swift
//  NeuralLabs
//
//  Created by Gagan on 01/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

open class Logger {
    
    public static let shared : Logger = Logger();
    
    public func log (_ msgs : String...) {
        print("Message \(Date())", msgs.joined(separator: " ---- "));
    }

    func firebaseLog (_ msgs : String...) {
        for msg in msgs {
            print(msg);
        }
    }

    func firebaseTagLog (_ tag:String, dictMsgs:[AnyHashable:Any]...) {
        print("-----> Firebase Message Tag \(tag) <-----")
        for msg in dictMsgs {
            print(msg);
        }
    }
    
    func inAppLog (_ msgs:String...){
        print("-----> InApp Logging <-----")
        for msg in msgs {
            print(msg);
        }
    }

    func adServiceLog (_ msgs:String...){
        print("-----> AdService Logging <-----")
        for msg in msgs {
            print(msg);
        }
    }
    
    func avFoundationLog (_ msgs: String...){
        print("-----> AVFoundation Logging <-----")
        for msg in msgs {
            print(msg);
        }
    }

    public func assertionLogs(_ msg: String){
        print("-----> Assertion Logg <-----")
        print(msg);
        assert(false, msg);
    }
}
