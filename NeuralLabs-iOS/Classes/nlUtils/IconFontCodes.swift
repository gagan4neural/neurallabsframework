//
//  IconFontCodes.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public struct IconFontCodes {
    
    public init() {
    }
    
    public static let _8tracks = "\u{f3ef}"
    
    public static let album = "\u{f228}"
    public static let arrowLeft = "\u{f2ea}"
    public static let arrowRight = "\u{f2ee}"
    public static let aspectRatio = "\u{f365}"
    public static let aspectRatioAlt = "\u{f364}"
    public static let audio = "\u{f10f}"
    
    public static let block = "\u{f119}"
    public static let brush = "\u{f11f}"
    public static let borderColor = "\u{f22e}"
    public static let brightness5 = "\u{f36d}"
    public static let brightness6 = "\u{f36e}"
    public static let brightness7 = "\u{f36f}"

    public static let camera = "\u{f28c}"
    public static let camera_add = "\u{f283}"
    public static let camera_alt = "\u{f284}"
    public static let camera_rear = "\u{f289}"
    public static let check = "\u{f26b}"
    public static let check_circle = "\u{f269}"
    public static let chevronDown = "\u{f2f9}"
    public static let chevronLeft = "\u{f2fa}"
    public static let chevronRight = "\u{f2fb}"
    public static let chevronUp = "\u{f2fc}"
    public static let close = "\u{f136}"
    public static let closeCircle = "\u{f135}"
    public static let collection_item = "\u{f14b}"
    public static let collection_video = "\u{f151}"
    public static let crop = "\u{f238}"
    
    public static let edit = "\u{f158}"

    public static let facebook = "\u{f343}"
    public static let favoriteFill = "\u{f15f}"
    public static let favoriteOutline = "\u{f15e}"
    public static let filledStar = "\u{f27d}"
    public static let filter_list = "\u{f160}"
    public static let flag = "\u{f162}"
    public static let folder = "\u{f228}"
    public static let font = "\u{f16a}"
    public static let format_color_fill = "\u{f240}"
    public static let format_list_bulleted = "\u{f247}"
    public static let formatSize = "\u{f24b}"
    public static let forward10 = "\u{f3a1}"

    public static let gridView = "\u{f315}"
    public static let gridViewOff = "\u{f314}"

    public static let help_outline = "\u{f1f5}"

    public static let image = "\u{f17f}"
    public static let infoFill = "\u{f039}"
    public static let input_SVideo = "\u{f2a2}"
    
    public static let listView = "\u{f320}"
    public static let lockClose = "\u{f191}"
    public static let lockCloseOutline = "\u{f190}"
    public static let lockOpen = "\u{f18f}"
    
    public static let mail_send = "\u{f194}"
    public static let mic = "\u{f2ab}"
    public static let mic_off = "\u{f2a8}"
    public static let menu = "\u{f197}"
    public static let more_horz = "\u{f19c}"
    public static let more_vert = "\u{f19b}"
    public static let money = "\u{f19a}"
    public static let mute = "\u{f3bb}"

    public static let panormaHorizontal = "\u{f391}"
    public static let panormaVertical = "\u{f392}"
    public static let pause = "\u{f3a7}"
    public static let pauseFill = "\u{f3a6}"
    public static let pauseOutline = "\u{f3a5}"
    public static let pin = "\u{f1a8}"
    public static let pictureInPicture = "\u{f396}"
    public static let play = "\u{f3aa}"
    public static let playFill = "\u{f3a9}"
    public static let playOutline = "\u{f3a8}"
    public static let playlistAudio = "\u{f3ab}"
    public static let playlistPlus = "\u{f3ac}"
    
    public static let receipt = "\u{f1b4}"
    public static let refresh = "\u{f1b9}"
    public static let refresh_sync = "\u{f1b8}"
    public static let _repeat = "\u{f3ae}"
    public static let replay10 = "\u{f3af}"
    public static let rotateRight = "\u{f307}"

    public static let time_restore_setting = "\u{f335}"
    
    public static let scanner = "\u{f2c7}"
    public static let scissors = "\u{f1bc}"
    public static let share = "\u{f35b}"
    public static let shopping_cart_plus = "\u{f1ca}"
    public static let search = "\u{f1c3}"
    public static let skipNext = "\u{f39f}"
    public static let skipPrev = "\u{f3a0}"
    public static let shuffle = "\u{f3b3}"
    public static let sync = "\u{f2c0}"
    
    public static let text = "\u{f256}"
    public static let timeInterval = "\u{f334}"

    public static let volumeDown = "\u{f3b9}"
    public static let volumeUp = "\u{f3bc}"
    public static let volumeOff = "\u{f3ba}"
    public static let volumeMute = "\u{f3bb}"
    public static let youtube = "\u{f409}"
    
    public static let _5StarRatting = "\u{f27d}\u{f27d}\u{f27d}\u{f27d}\u{f27d}"
}
