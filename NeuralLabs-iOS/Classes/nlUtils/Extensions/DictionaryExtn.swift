//
//  DictionaryExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

extension (NSDictionary) {
    
    public func dictionary () -> Dictionary <String, Any> {
        var dict : Dictionary<String, Any> = Dictionary<String, Any>();
        
        for key in self.allKeys {
            let stringKey : String = key as! String;
            dict[stringKey] = self.value(forKey: stringKey);
        }
        
        return dict;
    }

}

extension (Dictionary) {
    public func nsDictionary () -> NSDictionary {
        let nsDict : NSDictionary = NSDictionary();
        
        for (key, value) in self {
            nsDict.setValue(value, forKey: key as! String);
        }
        
        return nsDict;
    }
}
