//
//  UIViewControllerExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 19/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public extension (UIViewController){
    
    func isPresented () -> Bool{
        
        if (self.isBeingDismissed || self.isBeingPresented){
            return true;
        }
        
        var nav: UINavigationController? = self.navigationController;
        if (nav == nil){
            nav = nlUtils.shared.applicationNavigationController();
        }
        
        if (nav!.presentedViewController == self) {
            return true;
        }
        
        if nav?.presentedViewController == self{
            return true;
        }
        
        return false;
    }
}
