//
//  UISplitViewControllerExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 27/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public extension (UISplitViewController) {
    
    public func primaryViewController () -> UIViewController?{
        let vc = self.viewControllers.first;
        if (vc?.isKind(of: UINavigationController.classForCoder()) ?? false){
            let nvc = vc as? UINavigationController;
            return nvc?.viewControllers.first;
        }
        return vc;
    }
    
    public func secondaryViewController () -> UIViewController? {
        let vc = (self.viewControllers.count > 1) ? self.viewControllers[1] : nil
        if (vc?.isKind(of: UINavigationController.classForCoder()) ?? false){
            let nvc = vc as? UINavigationController;
            return nvc?.viewControllers.first;
        }
        return vc;
    }
    
}
