//
//  UINavigationExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

extension (UINavigationController) {
    
    func isPortrait() -> Bool{
        return self.view.isPortrait();
    }

    func isLandscape() -> Bool{
        return self.view.isLandscape();
    }
}
