//
//  URLExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation


extension (URL) {
    
    func open(completionCallback callback: @escaping completion) {
        let url : URL? = self;
        
        if let anUrl = url, let aNew = [AnyHashable : Any]() as? [UIApplication.OpenExternalURLOptionsKey : Any] {
            UIApplication.shared.open(anUrl, options: aNew, completionHandler: callback)
        }
    }
    
    public func pathString() -> String  {
        let str = self.absoluteString;
        let path = str.replacingOccurrences(of: "file:///", with: "/");
        return path;
    }
}
