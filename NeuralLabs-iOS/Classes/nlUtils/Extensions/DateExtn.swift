//
//  DateExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 27/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public extension (Date){
    
    public func dateString () -> String?{
        let df = DateFormatter()
        df.dateStyle = .short;
        return df.string(from: self);
    }
}
