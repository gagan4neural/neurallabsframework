//
//  StringExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 30/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

fileprivate let maxColor : String = "0xFF"

//MARK: Color
extension (String) {
    
    // Colors
    func rgbaColors () -> [String] {
        var retVal : [String] = [String]();
        if (self.count < 6){
            //A color string cannot be less then 6 characters.
            return retVal;
        }
        else if (self.count > 8){
            //A color string cannot be greater then 8 characters.
            return retVal;
        }
        
        let array = Array(self);
        
        for (index, _) in array.enumerated() {
            if ((index % 2) == 1){
                let str = String(array[(index-1)...index])
                retVal.append(str);
            }
        }
        
        return retVal;
    }
    
    func redColorHex () -> String {
        let rgb : [String] = self.rgbaColors();
        if (rgb.count > 0){
            return rgb[0];
        }
        return maxColor;
    }
    func redColorFloat () -> Float {
        return self.redColorHex().toFloat();
    }
    func redColorInt () -> Int {
        return self.redColorHex().toInt();
    }
    
    func greenColorHex () -> String {
        let rgb : [String] = self.rgbaColors();
        if (rgb.count > 1){
            return rgb[1];
        }
        return maxColor;
    }
    func greenColorFloat () -> Float {
        return self.greenColorHex().toFloat();
    }
    func greenColorInt () -> Int {
        return self.greenColorHex().toInt();
    }
    
    func blueColorHex () -> String {
        let rgb : [String] = self.rgbaColors();
        if (rgb.count > 2){
            return rgb[2];
        }
        return maxColor;
    }
    func blueColorFloat () -> Float {
        return self.blueColorHex().toFloat();
    }
    func blueColorInt () -> Int {
        return self.blueColorHex().toInt();
    }
    
    func alphaColorHex () -> String {
        let rgba : [String] = self.rgbaColors();
        if (rgba.count > 3){
            return rgba[3];
        }
        return maxColor;
    }
    func alphaColorFloat () -> Float {
        return self.alphaColorHex().toFloat();
    }
    func alphaColorInt () -> Int {
        return self.alphaColorHex().toInt();
    }
    
    func rgbaColor () -> UIColor {
        return UIColor(red: CGFloat(self.redColorFloat()),
                       green: CGFloat(self.greenColorFloat()),
                       blue: CGFloat(self.blueColorFloat()),
                       alpha: CGFloat(self.alphaColorFloat()));
    }
}


//MARK: Conversions
extension (String){
    
    func toFloat () -> Float {
        var str : String = self;
        if !((self.contains("x") || self.contains("X"))) {
            str = "0x" + str;
        }
        if let floatVal = Float(str) {
            return (floatVal/255.0);
        }
        return Float(1.0);
    }
    
    func toInt () -> Int {
        var str : String = self;
        if !((self.contains("x") || self.contains("X"))) {
            str = "0x" + str;
        }
        if let intVal = Int(str) {
            return intVal;
        }
        return Int(0);
    }
    
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "YES", "1":
            return true
        case "False", "false", "no", "NO", "0":
            return false
        default:
            return nil
        }
    }
    
    func appendPath(component: String) -> String {
        var retVal = self;
        if (self.last != "/") {
            retVal.append("/");
        }
        retVal.append(component)
        return retVal;
    }
}


//MARK: TIME
public extension (String){
    
    static func timeFrom(interval: TimeInterval) -> String? {
        return String(fromTimeInterval: interval);
    }
    static func timeFrom(float64 time: Float64) -> String? {
        return String(fromTimeInterval: TimeInterval(exactly: time as Double) ?? 0);
    }

    init?(fromTimeInterval time:TimeInterval){
        if (time.isNaN){
            return nil;
        }
        let ti = Int(time)
        let seconds: Int = ti % 60
        let minutes: Int = (ti / 60) % 60
        let hours: Int = ti / 3600
        
        if (minutes < 1) && (hours < 1) {
            self = String(format: "%02lds", seconds)
        } else if (hours < 1) {
            self = String(format: "%02ldm, %02lds", minutes, seconds)
        } else {
            self = String(format: "%02ldh:%02ldm, %02lds", hours, minutes, seconds)
        }
    }
}


//MARK: Price/Money
extension (String){
    func moneyString (_ locale: Locale) -> String? {
        let formatter = NumberFormatter()
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
        formatter.locale = locale;
        return formatter.string(for: self);
    }
}
