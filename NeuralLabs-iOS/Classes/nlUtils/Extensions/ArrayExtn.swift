//
//  ArrayExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 24/11/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public extension (Array){
    
    public static func randomIndexedArray(limit: Int) -> [Int]{
        var idx:[Int] = [];
        if (limit <= 0){
            return idx;
        }
        var uniArray = Array.uniformIndexedArray(limit: limit, assending: true);
        
        while (uniArray.count > 0){
            let select = Int(arc4random()) % uniArray.count;
            idx.append(uniArray[select]);
            uniArray.remove(at: select);
        }
        return idx;
    }
    
    public static func uniformIndexedArray(limit: Int, assending:Bool) -> [Int]{
        var idx:[Int] = [];
        if (limit <= 0){
            return idx;
        }
        if (assending == true){
            var lmt = Int(0);
            while(lmt < limit){
                idx.append(lmt);
                lmt += 1;
            }
        }else{
            var lmt = (limit - 1);
            while(lmt >= 0){
                idx.append(lmt);
                lmt -= 1;
            }
        }
        return idx;
    }
    
    mutating func relativeRotateLeft(offset: Int){
        // Movement will be towards StartIndex. Rotation will be relative.
        //TODO: Optimize this logical operation

        var pendingMoves = offset % self.endIndex; // this is cause of relative movement
        
        while(pendingMoves > 0){
            var index = self.startIndex;
            let firstItem = self.first;
            
            repeat {
                self[index] = self[index+1];
                index += 1;
            }while(index < (self.endIndex-1))
            
            self[index] = firstItem!;
            pendingMoves -= 1;
        }
    }

    mutating func relativeRotateRight(offset: Int){
        // Movement will be towards EndIndex. Rotation will be relative.
        //TODO: Optimize this logical operation
        
        var pendingMoves = offset % self.endIndex; // this is cause of relative movement
        
        while(pendingMoves > 0){
            var index = self.startIndex;
            let lastItem = self.last;
            
            repeat {
                self[index+1] = self[index];
                index += 1;
            }while(index < (self.endIndex-1))
            
            self[0] = lastItem!;
            pendingMoves -= 1;
        }
    }
    
    mutating func reverse(){
        // Reverse this array
        //TODO: Optimize this logical operation
        
        var pendingMoves = self.count / 2;
        var start = self.startIndex;
        var end = self.endIndex-1;
        
        repeat {
            let startValue = self[start]
            let endValue = self[end]
            self[start] = endValue;
            self[end] = startValue;
            start += 1;
            end -= 1;
            pendingMoves -= 1;
        }while(pendingMoves > 0)
    }
    
    mutating func popFirst() -> Element? {
        let first = self.first;
        self.removeFirst();
        return first;
    }
}
