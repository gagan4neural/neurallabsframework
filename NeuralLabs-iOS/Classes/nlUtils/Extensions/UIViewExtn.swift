//
//  UIViewExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

public extension (UIView){
    
    func isPortrait() -> Bool {
        return (self.frame.width < self.frame.height)
    }
    
    func isLandscape() -> Bool {
        return (self.frame.width > self.frame.height)
    }
    
    func pointerIdentifier() -> String {
        return "\(Unmanaged.passUnretained(self).toOpaque())";
    }
    
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        let rotation = self.transform.rotated(by: radians)
        self.transform = rotation
    }
}
