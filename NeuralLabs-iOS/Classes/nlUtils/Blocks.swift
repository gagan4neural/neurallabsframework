//
//  Blocks.swift
//  NeuralLabs
//
//  Created by Gagan on 26/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

public typealias ErrorString = String;
public typealias SuccessString = String;
public typealias ErrorInAppObjects = InAppObject;
public typealias SuccessInAppObjects = InAppObject;
public typealias ProgressInfo = String;

public typealias onViewAppearedBlock = (Any?) -> Void
public typealias CompletionBlock = (Any?) -> Void
public typealias onCompletionCallback = (Bool) -> Void
public typealias completion = (Bool) -> Void
public typealias flowCompletion = () -> Void
public typealias PopToAcceptorCompletion = () -> Void
public typealias completeWithObject = (Any?, Int) -> Void
public typealias errorBlock = (Any?, Any?, Int) -> Void
public typealias AnimationCompletion = (() -> Void)


// InApps
public typealias InAppInitializeBlock = ([SuccessInAppObjects], [ErrorInAppObjects]) -> Void
public typealias InAppPurchaseCompleteBlock = (SuccessString?, ErrorString?) -> Void
public typealias InAppPurchaseRestoreBlock = ([SuccessString], [ErrorString]) -> Void

//Ads
typealias InterstetialAdWillPresentBlock = () -> Void
typealias InterstetialAdDidPresentBlock = () -> Void
typealias InterstetialAdDismissBlock = () -> Void
typealias InterstetialAdFailedBlock = () -> Void
typealias BannerAdsAvaliableBlock = () -> Void
typealias BannerAdsFailedBlock = () -> Void
typealias RemoveAdsPurchase = (Bool) -> Void

// Designing
public typealias designLabelBlock = (UILabel) -> Void

// Assets
public typealias MediaCollectionAccessCallback = ([MediaCollectionProtocol]?, ProgressInfo?, ErrorString?) -> Void
public typealias ItemAssetAccessCallback = ([MediaItemsProtocol]?, ProgressInfo?, ErrorString?) -> Void
public typealias SelectedMediaCollection = (MediaCollectionProtocol) -> Void;
public typealias SelectedMediaItem = (MediaCollectionProtocol, MediaItemsProtocol) -> Void;
public typealias MediaSelectionsCallback = ([MediaItemsProtocol]) -> Void;

// TIME & Media
public typealias TotalMediaTime = TimeInterval
public typealias ElapsedMediaTime = TimeInterval
public typealias RemainingMediaTime = TimeInterval
public typealias MediaPlaybackTimeCallback = (TotalMediaTime, ElapsedMediaTime, RemainingMediaTime) -> Void
public typealias MediaTimeCallback = (TotalMediaTime) -> Void

// Gestures
public typealias NUMBER_OF_TAPS = UInt
public typealias NUMBER_OF_TOUCHES = UInt
public typealias TouchPoint = CGPoint
public typealias TouchPoints = [TouchPoint]
public typealias PressDuration = TimeInterval
public typealias PinchScale = CGFloat
public typealias ScalerVelocity = CGFloat
public typealias VectorVelocity = CGPoint
public typealias TapGestureCallback = (NUMBER_OF_TAPS, NUMBER_OF_TOUCHES, TouchPoints) -> Void
public typealias SwipeGestureCallback = (NUMBER_OF_TOUCHES, UISwipeGestureRecognizer.Direction, TouchPoints) -> Void
public typealias PressGestureCallback = (NUMBER_OF_TAPS, NUMBER_OF_TOUCHES, PressDuration, TouchPoints) -> Void
public typealias PinchGestureCallback = (PinchScale, ScalerVelocity, TouchPoints) -> Void
public typealias PanGestureCallback = (VectorVelocity, TouchPoints) -> Void


//Network Request/Response
public typealias NetworkResponseCallback = (NetworkResponse) -> Void
public typealias NetworkResponseImageCallback = (UIImage?, ErrorString?) -> Void


//MARK: Popover callback
public typealias NeuralPopoverCallback = (NeuralPopoverItem) -> Void



class PopToAcceptorCompletionWrapper{
    
    init(completionBlock: @escaping PopToAcceptorCompletion) {
        self.completionBlock = completionBlock;
    }
    
    static func popToAcceptorCompletionWrapperWith(completionBlock : @escaping PopToAcceptorCompletion) -> PopToAcceptorCompletionWrapper{
        return PopToAcceptorCompletionWrapper(completionBlock:completionBlock);
    }

    var completionBlock: PopToAcceptorCompletion?
}
