//
//  NeuralDefaults.swift
//  NeuralLabs
//
//  Created by Gagan on 02/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

class NeuralDefaults {

    static let shared : NeuralDefaults = NeuralDefaults();
    static let fireBaseKey = "FIREBASE"
    static let inAppKey = "INAPP"
    static let settingsKey = "SETTINGS"
    static let youtubeKey = "YOUTUBE"


    // MARK: - UserDefaults
    func setUserDefaultValue(_ value: Any?, forKey key: String) {
        if (value == nil) {
            return
        }
        UserDefaults.standard.setValue(value, forKey: key)
    }
    
    func userDefaultValue(forKey key: String) -> Any? {
        return UserDefaults.standard.value(forKey: key)
    }
}


//MARK: Settings
extension (NeuralDefaults){
    static func setSettings (_ key:AnyHashable, value:Any){
        var settingDict: [AnyHashable : Any] = NeuralDefaults.shared.getSettingsDict();
        settingDict[key] = value;
        NeuralDefaults.shared.setSettingsDict(settingDict);
    }
    
    static func getSettings (_ key:AnyHashable) -> Any? {
        var settingDict : [AnyHashable : Any] = NeuralDefaults.shared.getSettingsDict();
        return settingDict[key];
    }
    
    private func getSettingsDict () -> [AnyHashable:Any] {
        var settingDict: [AnyHashable : Any]? = self.userDefaultValue(forKey: NeuralDefaults.settingsKey) as? [AnyHashable : Any];
        if (settingDict == nil) {
            settingDict = [AnyHashable:Any]();
            self.setSettingsDict(settingDict!);
        }
        return settingDict!;
    }
    
    private func setSettingsDict (_ value: [AnyHashable:Any]) {
        self.setUserDefaultValue(value, forKey: NeuralDefaults.settingsKey);
    }
}


//MARK: FireBase
extension (NeuralDefaults){
    static func setFirebase (_ key:AnyHashable, value:Any){
        var firebaseDict : [AnyHashable : Any] = NeuralDefaults.shared.getFirebaseDict();
        firebaseDict[key] = value;
        NeuralDefaults.shared.setFirebaseDict(firebaseDict);
    }
    
    static func getFirebase (_ key:AnyHashable) -> Any? {
        var firebaseDict : [AnyHashable : Any] = NeuralDefaults.shared.getFirebaseDict();
        return firebaseDict[key];
    }
    
    private func getFirebaseDict () -> [AnyHashable:Any] {
        var firebaseDict : [AnyHashable : Any]? = self.userDefaultValue(forKey: NeuralDefaults.fireBaseKey) as? [AnyHashable : Any];
        if (firebaseDict == nil) {
            firebaseDict = [AnyHashable:Any]();
            self.setFirebaseDict(firebaseDict!);
        }
        return firebaseDict!;
    }
    
    private func setFirebaseDict (_ value : [AnyHashable:Any]) {
        self.setUserDefaultValue(value, forKey: NeuralDefaults.fireBaseKey);
    }
}


//MARK: InApp
extension (NeuralDefaults){
    static func setInApp (_ key:AnyHashable, value:Any){
        var inAppDict : [AnyHashable : Any] = NeuralDefaults.shared.getInAppDict();
        inAppDict[key] = value;
        NeuralDefaults.shared.setInAppDict(inAppDict);
    }
    
    static func getInapp (_ key:AnyHashable) -> Any? {
        var inAppDict : [AnyHashable : Any] = NeuralDefaults.shared.getInAppDict();
        return inAppDict[key];
    }
    
    private func getInAppDict () -> [AnyHashable:Any] {
        var inAppDict : [AnyHashable : Any]? = self.userDefaultValue(forKey: NeuralDefaults.inAppKey) as? [AnyHashable : Any];
        if (inAppDict == nil) {
            inAppDict = [AnyHashable:Any]();
            self.setInAppDict(inAppDict!);
        }
        return inAppDict!;
    }
    
    private func setInAppDict (_ value : [AnyHashable:Any]) {
        self.setUserDefaultValue(value, forKey: NeuralDefaults.inAppKey);
    }
}



//MARK: YouTube
extension (NeuralDefaults){
    static func setYouTube (_ key:AnyHashable, value:Any){
        var youTubeDict : [AnyHashable : Any] = NeuralDefaults.shared.getYouTubeDict();
        youTubeDict[key] = value;
        NeuralDefaults.shared.setYouTubeDict(youTubeDict);
    }
    
    static func getYouTube (_ key:AnyHashable) -> Any? {
        var youtubeDict : [AnyHashable : Any] = NeuralDefaults.shared.getYouTubeDict();
        return youtubeDict[key];
    }
    
    private func getYouTubeDict () -> [AnyHashable:Any] {
        var youtubeDict : [AnyHashable : Any]? = self.userDefaultValue(forKey: NeuralDefaults.youtubeKey) as? [AnyHashable : Any];
        if (youtubeDict == nil) {
            youtubeDict = [AnyHashable:Any]();
            self.setYouTubeDict(youtubeDict!);
        }
        return youtubeDict!;
    }
    
    private func setYouTubeDict (_ value : [AnyHashable:Any]) {
        self.setUserDefaultValue(value, forKey: NeuralDefaults.youtubeKey);
    }
}
