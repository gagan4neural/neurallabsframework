//
//  nlUtils.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

fileprivate let defaultTitleHeight:Double = 40;

open class nlUtils {
    
    public static let shared : nlUtils = nlUtils();

    //MARK: Device
    public func isIpad() -> Bool {
        return (UIDevice.current.userInterfaceIdiom == .pad);
    }
    public func isIphone() -> Bool {
        return (UIDevice.current.userInterfaceIdiom == .phone);
    }
    public func isTv() -> Bool {
        return (UIDevice.current.userInterfaceIdiom == .tv);
    }
    public func isCarplay() -> Bool {
        return (UIDevice.current.userInterfaceIdiom == .carPlay);
    }
    public func isPortrait () -> Bool {
        switch UIDevice.current.orientation {
        case .portrait:
            return true;
        case .portraitUpsideDown:
            return true;
        case .faceUp:
            return true;
        case .faceDown:
            return true;
        default:
            return false;
        }
    }
    public func isLandscape () -> Bool {
        switch UIDevice.current.orientation {
        case .landscapeRight:
            return true;
        case .landscapeLeft:
            return true;
        default:
            return false;
        }
    }
    
    public var systemBrightness : CGFloat {
        get{
            return UIScreen.main.brightness;
        }set{
            UIScreen.main.brightness = newValue;
        }
    }
    
    public var screenSize = UIScreen.main.bounds.size;
    public let searchViewStartCharactersCount: Int = 3;

    
    //MARK: App Navigation
    public func applicationDelegate () -> UIApplicationDelegate? {
        return UIApplication.shared.delegate;
    }
    
    public func applicationWindow () -> UIWindow? {
        
        if let appDel = applicationDelegate() {
            return (appDel.window)!;
        }
        return nil;
    }
    
    public func applicationNavigationController () -> UINavigationController? {

        if let appWindow = self.applicationWindow() {
            return (appWindow.rootViewController as? UINavigationController)
        }
        return nil;
    }
    
    public func topViewController () -> UIViewController? {
        
        if let appNav = self.applicationNavigationController() {
            return appNav.topViewController;
        }
        
        return nil;
    }
    
    public func dismissAllPopOvers(_ callbackObject: Any?) {
        if !Thread.isMainThread {
            /*
             * Ensure that this method executes on main thread. Otherwise indeterminate results will be obtained. If this method is executed on non UI thread in some versions of iOS it will crash , in Some versions of iOS there will be slow performance.
             */
            weak var weakSelf = self;
            DispatchQueue.main.async(execute: {
                weakSelf!.dismissAllPopOvers(callbackObject)
                return
            })
        }
        
        var completion: PopToAcceptorCompletion? = nil
        if (callbackObject is PopToAcceptorCompletionWrapper) {
            completion = (callbackObject as? PopToAcceptorCompletionWrapper)?.completionBlock
        }
        if completion == nil {
            completion = {
                print("\(#function) || Active view controller dismissed successfully.")
            }
        }
        
        dismissAllPopOvers(fromNavigationController: callbackObject)
    }

    public func dismissAllPopOvers(fromNavigationController callbackObject: Any?) {
        var completion: PopToAcceptorCompletion? = nil
        if (callbackObject is PopToAcceptorCompletionWrapper) {
            completion = (callbackObject as? PopToAcceptorCompletionWrapper)?.completionBlock
        }
        if completion == nil {
            // Local log printing
            completion = {
                print("\(#function) || Active view controller dismissed successfully.")
            }
        }
        
        let appNavController: UINavigationController? = self.applicationNavigationController()
        
        if appNavController?.presentedViewController != nil {
            appNavController?.dismiss(animated: true) {
                completion?()
            }
        }
    }

    public static func rearrange(_ controller: UINavigationController?, top view: UIViewController?, pushing: Bool, animated: Bool) -> Bool
    {
        return self.rearrange(controller, top: view, pushing: pushing, animated: animated, intermediateViews: nil)
    }

    public static func rearrange(_ controller: UINavigationController?, top view: UIViewController?, pushing: Bool, animated: Bool, intermediateViews intermediate: [UIViewController]?) -> Bool
    {
        
        if controller?.topViewController != view
        {
            if pushing
            {
                if intermediate == nil
                {
                    if let aView = view
                    {
                        controller?.pushViewController(aView, animated: animated)
                    }
                }
                else
                {
                    var vcs = controller?.viewControllers
                    for iview: UIViewController? in intermediate ?? []
                    {
                        if let anIview = iview
                        {
                            if !(vcs?.contains(anIview) ?? false)
                            {
                                vcs?.append(anIview)
                                iview?.viewDidLoad()
                            }
                        }
                    }
                    
                    if let aView = view
                    {
                        vcs?.append(aView)
                    }
                    if let aVcs = vcs
                    {
                        controller?.setViewControllers(aVcs, animated: animated)
                    }
                }
            } else {
                if view != nil {
                    if let aView = view {
                        controller?.popToViewController(aView, animated: animated)
                    }
                } else {
                    controller?.popViewController(animated: animated)
                }
            }
        }
        return false
    }

    public func presentOrDismissModalController(_ viewController: UIViewController?, on navigationController: UINavigationController?)
    {
        if (viewController == nil) || (navigationController == nil)
        {
            // No mean to execute is one of param is NULL.
            return
        }
        
        if !Thread.isMainThread
        {
            // Make Sure this piece of executes on Main thread. Requests may come from many service layers or business logic.
            weak var weakSelf = self;
            DispatchQueue.main.async(execute: {
                weakSelf!.presentOrDismissModalController(viewController, on: navigationController)
            })
            return
        }
        
        if self.isIpad()
        {
            // This can be clubbed in one method or split into 2 method. Just for better readability iPhone and iPad methods have been splitted.
            if let vc = viewController {
                iPad_presentOrDismissModalController(vc, on: navigationController)
            }
        }
        else
        {
            iPhone_presentOrDismissModalController(viewController, on: navigationController)
        }
    }
    

    public func iPad_presentOrDismissModalController(_ viewController: UIViewController, on navigationController: UINavigationController?)
    {
        if navigationController?.presentedViewController != nil
        {
            // There is already a presented view controller on top.
            if (navigationController?.presentedViewController is UINavigationController)
            {
                // Already presented view controller is of navigation controller type. we need to push the required view controller
                let nav = navigationController?.presentedViewController as? UINavigationController
                nav?.setViewControllers([viewController], animated: true)
            }
            else
            {
                navigationController?.dismiss(animated: false)
                {
                    navigationController?.present(viewController, animated: false)
                }
            }
        }
        else
        {
            var nav: UINavigationController? = nil
            if (viewController is UINavigationController)
            {
                /*
                 * Receipt dialog and some other dialogs are so much tickthly coupled with ticket View Controller. To allow MainUIController to handle dialogs this is done.
                 */
                nav = viewController as? UINavigationController
            }
            else
            {
                nav = UINavigationController(rootViewController: viewController)
            }
            
            nav?.modalPresentationStyle = .formSheet
            nav?.modalTransitionStyle = .coverVertical
            if let aNav = nav
            {
                navigationController?.present(aNav, animated: true)
            }
        }
    }

    public func iPhone_presentOrDismissModalController(_ viewController: UIViewController?, on navigationController: UINavigationController?)
    {
        let topView: UIViewController? = navigationController?.topViewController == viewController ? nil : navigationController?.topViewController
        
        let ignoreUsualPush = false
        if topView != nil
        {
            var vc = navigationController?.viewControllers
            
            if let aController = viewController
            {
                vc?.append(aController)
            }
            if let aVc = vc
            {
                navigationController?.setViewControllers(aVc, animated: true)
            }
        } else {
            if !ignoreUsualPush {
                let _ = nlUtils.rearrange(navigationController, top: viewController, pushing: true, animated: true)
            }
        }
    }

    
    
    
    // MARK: - Calculations
    public func min(_ val1: CGFloat, value2 val2: CGFloat) -> CGFloat {
        return Swift.min(val1, val2)
    }
    
    public func max(_ val1: CGFloat, value2 val2: CGFloat) -> CGFloat {
        return Swift.max(val1, val2)
    }
    
    public func scale10 (value: CGFloat) -> CGFloat{
        return self.scale(value, scale: 10);
    }
    
    public func scale100 (value: CGFloat) -> CGFloat{
        return self.scale(value, scale: 100);
    }
    
    public func scale1000 (value: CGFloat) -> CGFloat{
        return self.scale(value, scale: 1000);
    }
    
    private func scale(_ value:CGFloat, scale:CGFloat) -> CGFloat{
        return value * scale;
    }

    //MARK: Sizing
    public func titlebarHeight() -> CGFloat {
        
        var retVal: CGFloat = 0.0
        if self.isIpad() {
            retVal = iPad_titlebarHeight()
        } else {
            retVal = iPhone_titlebarHeight()
        }
        return retVal
    }
    
    public func iPhone_titlebarHeight() -> CGFloat {
        let retVal: CGFloat = (self.isPortrait()) ? 54.0 : 34.0;
        return retVal
    }

    public func iPad_titlebarHeight() -> CGFloat {
        let retVal: CGFloat = 64.0
        return retVal
    }
    
    public func titleFontSize() -> CGFloat {
        
        var retSize: CGFloat = 0.0
        if self.isIpad() {
            retSize = iPad_titleFontSize()
        } else {
            retSize = iPhone_titleFontSize()
        }
        return retSize
    }

    public func iPad_titleFontSize() -> CGFloat {
        let retSize: CGFloat = 20.0
        let scale: CGFloat = UIScreen.main.scale * 2.0
        return retSize + scale
    }
    
    public func iPhone_titleFontSize() -> CGFloat {
        let retSize: CGFloat = 18.0
        let scale: CGFloat = UIScreen.main.scale
        return retSize + scale
    }
    
    public func defaultFontSize () -> CGFloat {
        // default font size for all places in application
        var retSize: CGFloat = 0.0
        if self.isIpad() {
            retSize = 25.0
            let scale: CGFloat = UIScreen.main.scale * 2.5
            retSize = retSize + scale
        } else {
            retSize = 16.0
            let scale: CGFloat = UIScreen.main.scale
            retSize = retSize + scale
        }
        return retSize
    }
    
    public func badgeSize() -> CGSize {
        return self.isIpad() ? CGSize(width: 20, height: 20) : CGSize(width: 13, height: 13)
    }

    public func iconFontSize() -> CGFloat {
        let scale: CGFloat = UIScreen.main.scale
        let defSize : CGFloat = isIpad() ? 22 : 17;
        return defSize*scale
    }

    public let selectedCellBorderWidth: CGFloat = 2;
    
    public func tableViewCellHeight() -> CGFloat {
        var retVal: CGFloat = 0.0
        let scale: CGFloat = UIScreen.main.scale
        if self.isIpad() {
            retVal = 44 + (scale * 5)
        } else {
            retVal = 44 + (scale * 2.5)
        }
        return retVal
    }
    
    public func collectionViewSize() -> CGSize {
        
        var retVal = CGSize(width: 0, height: 0)
        if self.isIpad() {
            retVal = iPad_collectionViewSize()
        } else {
            retVal = iPhone_collectionViewSize()
        }
        return retVal
    }
    
    public func iPad_collectionViewSize() -> CGSize {
        var retVal = CGSize(width: 0, height: 0)
        let scale: CGFloat = UIScreen.main.scale
        retVal.width = 180 + (scale * 10)
        retVal.height = retVal.width
        return retVal
    }

    public func iPhone_collectionViewSize() -> CGSize {
        var retVal = CGSize(width: 0, height: 0)
        let scale: CGFloat = UIScreen.main.scale
        retVal.width = 120 + (scale * 10)
        retVal.height = retVal.width
        return retVal
    }

    public func assetImageSize() -> CGSize {
        return nlUtils.shared.applicationNavigationController()?.view.frame.size ?? self.assetThumbnailImageSize();
    }

    public func assetThumbnailImageSize() -> CGSize {
        return CGSize(width: 256, height: 256);
    }
    
    public func adjustGridCell(_ size:CGFloat, parentSize:CGFloat, numberOfCells: Int) -> CGFloat{
        var retVal = size;
        if (size < 0) ||
            (parentSize < size) ||
            (numberOfCells <= 0){
            Logger.shared.log("Cannot make cell adjustments with the given sizes");
            return retVal;
        }
        let errMargin: CGFloat = parentSize - (size * CGFloat(numberOfCells));
        let perCellErrMargin = errMargin / CGFloat(numberOfCells) - 5;
        
        retVal = retVal + perCellErrMargin;
        return retVal;
    }
    
    public func defaultSplitViewRatio() -> CGFloat {
        return 0.3;
    }
    
    public func defaultTabViewHeight() -> CGFloat{
        return 60;
    }
    
    public let statusBarHeight: CGFloat = 20;

    public func defaultBannerAdHeight() -> CGFloat {
        return bannerAdDefaultHeight;
    }
    
    //Logging
    public func callStackSymbols () {
        print("Stack symbols : \(Thread.callStackSymbols)")
    }
    
    
    //MARK: default setup methods
    public func defaultRect() -> CGRect {
        return CGRect(x: 0, y: 0, width: 320, height: 50)
    }
    
    public func estimatedKeyboardSize() -> CGSize {
        var retSize = CGSize.zero
        if isIpad() {
            if isPortrait() {
                retSize = CGSize(width: 768, height: 264)
            } else {
                retSize = CGSize(width: 1024, height: 352)
            }
        } else {
            if isPortrait() {
                retSize = CGSize(width: 320, height: 216)
            } else {
                retSize = CGSize(width: 480, height: 162)
            }
        }
        return retSize
    }

    
    //MARK: Sharing
    public func share(_ image: UIImage?, on controller: UIViewController?) {
        if (controller == nil) || (image == nil) {
            return
        }
        let ctrl = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        controller?.present(ctrl, animated: true)
    }

    public func share(_ image: UIImage?, on controller: UIViewController?, completion: @escaping () -> Void) {
        if (controller == nil) || (image == nil) {
            return
        }
        let ctrl = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        controller?.present(ctrl, animated: true, completion: completion)
    }
    
    public func share(_ image: UIImage?, on controller: UIViewController?, anchorView anchor: UIView?, completion: @escaping flowCompletion) {
        if (controller == nil) || (image == nil) {
            return
        }
        let ctrl = UIActivityViewController(activityItems: [image as Any], applicationActivities: nil)
        if ctrl.responds(to: #selector(getter: UIActivityViewController.popoverPresentationController)) {
            // iOS8
            ctrl.popoverPresentationController?.sourceView = anchor
        }
        controller?.present(ctrl, animated: true, completion: completion)
    }
    
    public func shareContents(_ contents: [Any]?, on controller: UIViewController?, anchorView anchor: UIView?, completion: @escaping flowCompletion) {
        if (controller == nil) || (contents == nil) {
            return
        }
        var ctrl: UIActivityViewController? = nil
        if let aContents = contents {
            ctrl = UIActivityViewController(activityItems: aContents, applicationActivities: nil)
        }
        if ctrl?.responds(to: #selector(getter: UIActivityViewController.popoverPresentationController)) ?? false {
            // iOS8
            ctrl?.popoverPresentationController?.sourceView = anchor
        }
        if let aCtrl = ctrl {
            controller?.present(aCtrl, animated: true, completion: completion)
        }
    }

    
    //MARK: Navigation Control Methods
    public func rearrange(_ vc: UIViewController?, for nav: UINavigationController?, show: Bool, shouldPresent present: Bool, completion complete: @escaping flowCompletion) {
        if (vc == nil) || (nav == nil) {
            return
        }
        if show {
            // Show this view controller
            if present
            {
                var preNav: UINavigationController? = nil
                if let aVc = vc
                {
                    preNav = UINavigationController(rootViewController: aVc)
                }
                if let aNav = preNav
                {
                    nav?.present(aNav, animated: true, completion: complete);
                }
            }
            else
            {
                if let aVc = vc
                {
                    nav?.pushViewController(aVc, animated: true)
                }
            }
        }
        else
        {
            let top = nav?.topViewController
            if (top is UINavigationController)
            {
                // If top is nav then it is always presented
                let topNav = top as? UINavigationController
                
                if (topNav?.isEqual(vc) ?? false) || (topNav?.viewControllers[0].isEqual(vc) ?? false) {
                    topNav?.dismiss(animated: true, completion: complete);
                }
                else
                {
                    let vcs = topNav?.viewControllers
                    var vcsAfterPop: [AnyHashable] = []
                    for v: UIViewController? in vcs ?? [] {
                        if v?.isEqual(vc) ?? false {
                            break
                        }
                        if let aV = v {
                            vcsAfterPop.append(aV)
                        }
                    }
                    if let aPop = vcsAfterPop as? [UIViewController] {
                        topNav?.setViewControllers(aPop, animated: false)
                    }
                }
            }
            else
            {
                let topVC = top
                
                if nav?.presentedViewController == topVC
                {
                    // Top view controller is presented one
                    topVC?.dismiss(animated: true, completion: complete);
                }
                else
                {
                    let vcs = nav?.viewControllers ?? []
                    var vcsAfterPop: [UIViewController] = vcs
                    for v in vcs
                    {
                        if v.isEqual(vc)
                        {
                            break
                        }
                        if let appendVc = vc
                        {
                            vcsAfterPop.append(appendVc)
                        }
                    }
                    nav?.setViewControllers(vcsAfterPop, animated: true)
                }
            }
        }
    }

    
    //MARK: Toast messages
    
    public func showHud(_ msg: String?, on view: UIView?) {
        HUD.show(.labeledProgress(title: nil, subtitle: msg), onView: view);
        HUD.dimsBackground = true;
    }

    public func showHud(_ title:String?, msg: String?, on view: UIView?) {
        HUD.show(.labeledProgress(title: title, subtitle: msg), onView: view);
        HUD.dimsBackground = true;
    }

    public func hideHud(from view: UIView?) {
        HUD.hide(animated: true);
    }
    
    public func showToast(_ msg: String?, on view: UIView?, timeDuration time: CGFloat) {
        HUD.flash(.labeledProgress(title: nil, subtitle: msg), onView: view);
    }

    
    //MARK: Directory
    public func documentDirectory() -> String? {
        var retVal: String? = nil
        retVal = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return retVal
    }
    
    public func libraryDirectory() -> String? {
        var retVal: String? = nil
        retVal = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        return retVal
    }
    
    public func applicationSupportDirectory() -> String? {
        var retVal: String? = nil
        retVal = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true)[0]
        return retVal
    }

    public func documentDirectoryContents() -> [Any]?
    {
        var retVal: [AnyHashable] = []
        let contents = try? FileManager.default.contentsOfDirectory(atPath: documentDirectory() ?? "")
        
        for file: String in contents ?? [] {
            let path = URL(fileURLWithPath: documentDirectory() ?? "").appendingPathComponent(file).absoluteString
            retVal.append(path)
        }
        
        return retVal
    }

    public func clearDocumentDirectory() {
        let contents = documentDirectoryContents()
        
        for file: String in contents as? [String] ?? [] {
            try? FileManager.default.removeItem(atPath: file)
        }
    }
    
    public func jsonContents(atPath path: String?) -> Any?
    {
        var retDict: Any? = nil
        if path == nil {
            return retDict
        }
        
        let contents: Data? = FileManager.default.contents(atPath: path ?? "")
        if contents == nil {
            return retDict
        }
        
        if let aContents = contents {
            retDict = try? JSONSerialization.jsonObject(with: aContents, options: [])
        }
        
        return retDict
    }
    
    public func copyDocumentDirectory (_ progressCallback:@escaping CompletionBlock, errorCallback:@escaping CompletionBlock, successCallback: @escaping CompletionBlock){
        let contents:[String]? = self.documentDirectoryContents() as? [String]? ?? [];
        
        if (contents?.count ?? 0 <= 0){
            successCallback("No items to copy");
            return;
        }
        
        AssetManager.shared.saveFilesToGallery(files: contents!) { (assetAccessResponse) in
            
            if assetAccessResponse.response == .Success{
                successCallback("Successfully fetched all assets");
            }else if (assetAccessResponse.response == .Progress){
                progressCallback(assetAccessResponse.info);
            }else if (assetAccessResponse.response == .Failure){
                errorCallback(assetAccessResponse.failureReason);
            }
        }
    }
    
    public func createDirectory(path: String) -> (SuccessString?, ErrorString?){
        do{
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil);
        }catch{
            return (nil, "Failed to create directory.");
        }
        return (path, nil);
    }
    
    public func createDirectory(name: String) -> (SuccessString?, ErrorString?){
        guard let dd = self.documentDirectory() else{
            return (nil, "Failed to get document directory");
        }
        let directoryPath = dd + "/" + name;
        return self.createDirectory(path: directoryPath);
    }
    
    public func doesDirectoryExist(name: String) -> Bool {
        var retVal : ObjCBool = false
        guard let dd = self.documentDirectory() else{
            return false
        }
        let directoryPath = dd + "/" + name;
        FileManager.default.fileExists(atPath: directoryPath, isDirectory: &retVal);
        return retVal.boolValue;
    }
    
    public func timeStampFile(name: String, extn: String) -> String{
        let interval = Date().timeIntervalSince1970

        let strT = "\(interval)";
        guard let fileTime = strT.components(separatedBy: ".").first else{
            return name+"."+extn;
        }
        return name+"_"+fileTime+"."+extn;
    }
    
    public func write(image: UIImage, atPath: String) -> Bool {
        var isSuccess = false;
        var data: Data? = nil;
        let attributes: [FileAttributeKey:Any] = [:]
        if (atPath.contains(".jpg")) {
            data = image.jpegData(compressionQuality: 0.6);
        }else if (atPath.contains(".png")){
            data = image.pngData();
        }
        if (data != nil){
            isSuccess = self.write(data: data!, atPath: atPath, attributes:attributes);
        }
        return isSuccess;
    }

    public func write(data: Data, atPath: String, attributes: [FileAttributeKey: Any]) -> Bool {
        let isSuccess = FileManager.default.createFile(atPath: atPath, contents: data, attributes: attributes);
        return isSuccess;
    }

    
    //MARK: Feedback handling

    public lazy var feedbackMsg : String = "Feedback for " + (BundleManager.shared.bundleDisplayName() ?? "");
    public lazy var feedbackReciptant : String = "feedbackneural@gmail.com"
    public lazy var feedbackSubject : String = "Feedback for " + (BundleManager.shared.bundleDisplayName() ?? "")

    public func scheduleReview() {
        //TODO: Need to fix this.
    }
    
    public func scheduleReview(_ appId: String?) {
    }

    public func askReview(_ appId: String?) {
    }
    
    public func askReview() {
    }

    
    //Animations:
    open var defaultLayoutAnimationDuration : Double = 0.2;
}
