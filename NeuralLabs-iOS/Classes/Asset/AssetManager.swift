//
//  AssetManager.swift
//  FileManager-iOS
//
//  Created by Gagan on 17/08/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import AssetsLibrary
import AVFoundation
import Photos
import MediaPlayer
import CoreAudioKit

//MARK: Callbacks
public typealias cb_AVAsset = (AVAsset?, AVAudioMix?, [AnyHashable: Any]?) -> Void;
public typealias cb_AVPlayerItem = (AVPlayerItem?, [AnyHashable: Any]?) -> Void;
public typealias cb_ThumbnailImage = (UIImage?, [AnyHashable: Any]?) -> Void;
public typealias cb_ImageInfo = (Data?, String?, UIImage.Orientation, [AnyHashable: Any]?) -> Void;
public typealias cb_Info = ([AnyHashable: Any]?) -> Void;
public typealias cb_Photos = (PHObjectPlaceholder) -> Void;
typealias cb_AllFolders = ([PHAssetCollection]) -> Void;
typealias cb_AllMedia = ([PHAsset]) -> Void
public typealias cb_AssetAccessResponse = (AssetAccessResponse) -> Void
typealias cb_ObjectPlaceHolder = (PHObjectPlaceholder?) -> Void
typealias cb_AddAssetSuccess = (String) -> Void
typealias cb_AddAssetError = (Error?) -> Void
public typealias cb_LivePhoto = (PHLivePhoto?, [AnyHashable : Any]?) -> Void
public typealias cb_AssetTitle = (String) -> Void
public typealias cb_CollectionImages = ([UIImage?]) -> Void

let kImageSavedStatus = "kImageSavedStatus"

fileprivate let assetsAccessBatchSize = 10000;


open class AssetManager {

    // shared instance
    public static let shared : AssetManager = AssetManager();

    private var isAudiosInitialized = false;
    private var isVideosInitialized = false;
    private var isImagesInitialized = false;
    
    private var audioCollection:[AudioCollection] = [AudioCollection]();
    private var videoCollection:[VideoCollection] = [VideoCollection]();
    private var imageCollection:[ImageCollection] = [ImageCollection]();
    
    private var audioCollectionCallback: MediaCollectionAccessCallback? = nil;
    private var videoCollectionCallback: MediaCollectionAccessCallback? = nil;
    private var imageCollectionCallback: MediaCollectionAccessCallback? = nil;

    private let kMPMediaPlaylistUUID = "AudioCollectionMPMediaPlaylistUUID";

    func initialize (){
        weak var weakSelf = self;
        DispatchQueue.global(qos: .background).async {
            weakSelf?.sel_initialize();
        };
    }
    
    private func sel_initialize () {
        
        self.fetchVideoCollections { (collections, progress, error) in
            if (error != nil){
                Logger.shared.log(#function, "Error in fetching video assets : ", error ?? "ERR");
            }else if (progress != nil){
                Logger.shared.log(#function, "Video asset progress : ", progress ?? "ERR");
            }else if (collections?.count ?? 0 > 0){
                self.printCollectionAssets(collections!);
            }else{
                Logger.shared.log(#function, "Misleading callback fired");
            }
        };
        
        self.fetchAudioCollections { (collections, progress, error) in
            if (error != nil){
                Logger.shared.log(#function, "Error in fetching audio assets : ", error ?? "ERR");
            }else if (progress != nil){
                Logger.shared.log(#function, "Audio asset progress : ", progress ?? "ERR");
            }else if (collections?.count ?? 0 > 0){
                self.printCollectionAssets(collections!);
            }else{
                Logger.shared.log(#function, "Misleading callback fired");
            }
        }
        
        self.fetchImageCollections { (collections, progress, error) in
            if (error != nil){
                Logger.shared.log(#function, "Error in fetching image assets : ", error ?? "ERR");
            }else if (progress != nil){
                Logger.shared.log(#function, "Image asset progress : ", progress ?? "ERR");
            }else if (collections?.count ?? 0 > 0){
                self.printCollectionAssets(collections!);
            }else{
                Logger.shared.log(#function, "Misleading callback fired");
            }
        }
    }
    
    private func printCollectionAssets(_ collections:[MediaCollectionProtocol]){
        
        for col in collections{
            Logger.shared.log(#function, "Collection Asset Identifier: ", col.identifier ?? "Nil Asset Identifier");
        }
    }
    
    public func accessAudioCollections (_ callback: MediaCollectionAccessCallback?) {
        self.audioCollectionCallback = callback;
        
        if (self.isAudiosInitialized){
            self.callbackAudioCollections(false, error: nil);
        }else{
            self.callbackAudioCollections(true, error: nil);
        }
    }

    public func accessVideoCollections (_ callback: MediaCollectionAccessCallback?) {
        self.videoCollectionCallback = callback;
        
        if (self.isVideosInitialized){
            self.callbackVideoCollections(false, error: nil);
        }else{
            self.callbackVideoCollections(true, error: nil);
        }
    }

    public func accessImageCollections (_ callback: MediaCollectionAccessCallback?) {
        self.imageCollectionCallback = callback;
        
        if (self.isImagesInitialized){
            self.callbackImageCollections(false, error: nil);
        }else{
            self.callbackImageCollections(true, error: nil);
        }
    }

    //MARK: Video Collection Access
    private func fetchVideoCollections (_ callback: MediaCollectionAccessCallback) {
        if (isVideosInitialized == true){
            callback(videoCollection, nil, nil);
            return;
        }
        weak var weakSelf = self;
        self.proceedWithVideoAccessAuthorisations { (val) in
            
            if (val == nil){
                // Video fetching is completed, now fire all notifications and delegates from here.
                Logger.shared.log(#function, "Succesfully fetched all video collections");
                // NO error
                weakSelf?.isVideosInitialized = true;
                weakSelf?.callbackVideoCollections(false, error: nil);
            }else{
                weakSelf?.isVideosInitialized = false;
                weakSelf?.callbackVideoCollections(false, error: val as? String);
                Logger.shared.log(#function, val as! String, "found \(self.videoCollection.count) collections");
            }
            NotificationController.noti_videoCollectionsInitialized();
        };
    }
    private func proceedWithVideoAccessAuthorisations (_ completion:@escaping CompletionBlock) {
        // checks that the application has permission to access photo library
        PHPhotoLibrary.requestAuthorization { (status) in
            switch (status){
            case .authorized:
                self.startFetchingVideoCollections(completion);
                break;
            case .denied:
                completion("Unable to access videos due to permission denied. Kindly go to settings and enable permission.");
                break;
            case .restricted:
                completion("Unable to access videos in restricted mode.");
                break;
            case .notDetermined:
                weak var weakSelf = self;
                PHPhotoLibrary.requestAuthorization({ (status) in
                    weakSelf?.proceedWithVideoAccessAuthorisations(completion);
                })
                break;
            }
        }
    }
    private func startFetchingVideoCollections (_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        // first start fetching all videos.
        self.fetchAllVideos { (val) in

            if (val == nil){
                Logger.shared.log(#function, "Successfully fetched all videos");
            }else{
                Logger.shared.log(#function, val as! String);
            }

            // first start fetching all collection videos.
            weakSelf?.fetchAllVideoCollections(completion);
        }
    }
    private func fetchAllVideos(_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        let videosCallback : cb_AllMedia = {(phAssetItems : [PHAsset]) in
            
            var videos : [VideoObject] = [VideoObject]();
            for phAsset in phAssetItems {
                if let assetObj : VideoObject = VideoObject(item: phAsset){
                    // only add it the item is a video.
                    videos.append(assetObj);
                }
            }
            if (videos.count > 0){
                weakSelf?.videoCollection.append(VideoCollection(videoObjects: videos));
                completion(nil);
            }else{
                completion("Failed to fetch videos");
            }
        }
        self.photoLibraryVideos(callback: videosCallback);
    }
    private func fetchAllVideoCollections(_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        let collectionCallback : cb_AllFolders = { (folders:[PHAssetCollection]) in
            
            let collectionObjects:[VideoCollection] = folders.compactMap({ (phCollection) -> VideoCollection in
                return VideoCollection(phCollection, collectionType: .Album);
            });
            weakSelf?.videoCollection += collectionObjects.filter({ (vCollection) -> Bool in
                //filter folders with no videos
                return (vCollection.mediaItems.count > 0)
            });
            completion(nil);
        }
        
        self.photoLibraryCollections(callback: collectionCallback);
    }
    private func photoLibraryVideos (callback : @escaping cb_AllMedia) {
        
        var retVal : Array<PHAsset> = Array<PHAsset>();
        let assetResult : PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: nil);
        var videosCount : Int = assetResult.count;
        
        if (videosCount <= 0){
            callback([]);
            return;
        }
        
        assetResult.enumerateObjects { (asset, index, stop) in
            
            retVal.append(asset);
            videosCount -= 1;
            if (videosCount <= 0){
                callback(retVal);
            }
        }
    }

    
    
    //MARK: Image Collection Access

    private func fetchImageCollections(_ callback: MediaCollectionAccessCallback) {
        if (isImagesInitialized == true){
            callback(imageCollection, nil, nil);
            return;
        }
        weak var weakSelf = self;
        self.proceedWithImageAccessAuthorisations { (val) in
            
            // Image fetching is completed, now fire all notifications and delegates from here.
            if (val == nil){
                // No Error
                Logger.shared.log(#function, "Successfully fetched all image collections");
                weakSelf?.isImagesInitialized = true;
                weakSelf?.callbackImageCollections(false, error: nil);
            }else{
                Logger.shared.log(#function, val as! String, "found \(self.imageCollection.count) collections");
                weakSelf?.isImagesInitialized = false;
                weakSelf?.callbackImageCollections(false, error: val as? String);
            }
            NotificationController.noti_imageCollectionsInitialized();
        };
    }
    private func proceedWithImageAccessAuthorisations (_ completion:@escaping CompletionBlock) {
        // checks that the application has permission to access photo library
        PHPhotoLibrary.requestAuthorization { (status) in
            switch (status){
            case .authorized:
                self.startFetchingImageCollections(completion);
                break;
            case .denied:
                completion("Unable to access images due to permission denied. Kindly go to settings and enable permission.");
                break;
            case .restricted:
                completion("Unable to access images in restricted mode.");
                break;
            case .notDetermined:
                weak var weakSelf = self;
                PHPhotoLibrary.requestAuthorization({ (status) in
                    weakSelf?.proceedWithImageAccessAuthorisations(completion);
                })
                break;
            }
        }
    }
    private func startFetchingImageCollections (_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        // first start fetching all images.
        self.fetchAllImages { (val) in

            if (val == nil){
                Logger.shared.log(#function, "Successfully fetched all images");
            }else{
                Logger.shared.log(#function, val as! String);
            }

            // first start fetching all collection images.
            weakSelf?.fetchAllImageCollections(completion);
        }
    }
    private func fetchAllImages(_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        let imagesCallback : cb_AllMedia = {(phAssetItems : [PHAsset]) in
            
            var images : [ImageObject] = [ImageObject]();
            for phAsset in phAssetItems {
                if let assetObj : ImageObject = ImageObject(item: phAsset){
                    // only add it the item is a video.
                    images.append(assetObj);
                }
            }
            if (images.count > 0){
                weakSelf?.imageCollection.append(ImageCollection(imageObjects: images));
                completion(nil);
            }else{
                completion("Failed to fetch Images");
            }
        }
        self.photoLibraryImages(callback: imagesCallback);
    }
    private func fetchAllImageCollections(_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        let collectionCallback : cb_AllFolders = { (folders:[PHAssetCollection]) in
            
            let collectionObjects:[ImageCollection] = folders.compactMap({ (phCollection) -> ImageCollection in
                return ImageCollection(phCollection, collectionType: .Album);
            });
            weakSelf?.imageCollection += collectionObjects.filter({ (iCollection) -> Bool in
                //filter folders with no images
                return (iCollection.mediaItems.count > 0)
            });
            completion(nil);
        }
        
        self.photoLibraryCollections(callback: collectionCallback);
    }
    private func photoLibraryImages (callback : @escaping cb_AllMedia) {
        
        var retVal : Array<PHAsset> = Array<PHAsset>();
        let assetResult : PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil);
        var imagesCount : Int = assetResult.count;
        
        if (imagesCount <= 0){
            callback([]);
            return;
        }
        
        assetResult.enumerateObjects { (asset, index, stop) in
            
            retVal.append(asset);
            imagesCount -= 1;
            if (imagesCount <= 0){
                callback(retVal);
            }
        }
    }
    
    public func fetchImagesFrom(path: String) -> [MediaItemsProtocol] {
        var retVal: [MediaItemsProtocol] = [];
        if (path.count <= 0){
            // invalid directory, no images found
            return retVal;
        }
        
        do {
            let contents: [String] = try FileManager.default.contentsOfDirectory(atPath: path)
            for c in contents{
                if c.contains(".jpg") || c.contains(".png") || c.contains(".gif"){
                    let filePath = path + "/" + c;
                    let io = ImageObject(path: filePath);
                    retVal.append(io);
                }
            }
        }catch {
            
        }
        return retVal;
    }
    
    public func videoMediaItemFrom(url: URL?) -> MediaItemsProtocol?{
        if url == nil{
            return nil;
        }
        let vo = VideoObject(url: url!);
        return vo;
    }
    
    
    //MARK: Audio Collection Access

    private func fetchAudioCollections (_ callback: MediaCollectionAccessCallback) {
        if (isAudiosInitialized == true){
            callback(audioCollection, nil, nil);
            return;
        }
        
        weak var weakSelf = self;
        
        self.startFetchingAudioCollections { (val) in
            // Audio fetching is completed, now fire all notifications and delegates from here.
            if (val == nil){
                Logger.shared.log(#function, "Successfully fetched all audio collections");
            }else{
                Logger.shared.log(#function, val as! String, "found \(self.audioCollection.count) collections");
            }
            
            weakSelf?.isAudiosInitialized = true;
            weakSelf?.callbackAudioCollections(false, error: nil);
            NotificationController.noti_audioCollectionsInitialized();
        };
    }
    private func startFetchingAudioCollections (_ completion:@escaping CompletionBlock) {
        weak var weakSelf = self;
        // first start fetching all images.
        self.fetchAllAudios { (val) in
            
            if (val == nil){
                Logger.shared.log(#function, "Successfully fetched all audios");
            }else{
                Logger.shared.log(#function, val as! String);
            }
            
            // first start fetching all collection images.
            weakSelf?.fetchAllAudioCollections(completion);
        }
    }
    private func fetchAllAudios(_ completion:@escaping CompletionBlock) {
        let allMediaItems: [MPMediaItem]? = MPMediaQuery.songs().items;
        
        let aObjects = allMediaItems?.map({ (mediaItem) -> AudioObject in
            return AudioObject(item: mediaItem);
        })
        self.audioCollection.append(AudioCollection(audioObjects: aObjects));
        completion(nil);
    }
    private func fetchAllAudioCollections(_ completion:@escaping CompletionBlock) {
        let mediaCollections:[MPMediaItemCollection]? =  MPMediaQuery.playlists().collections;
        for collection in mediaCollections ?? []{
            self.audioCollection.append(AudioCollection(collection, collectionType: .Playlist))
        }
        completion(nil);
    }
    
    public func createMediaPlaylist() {
        let appName = BundleManager.applicationName()
        var uuid:UUID? = NeuralDefaults.getSettings(kMPMediaPlaylistUUID) as? UUID;
        if (uuid == nil){
            uuid = UUID();
            NeuralDefaults.setSettings(kMPMediaPlaylistUUID, value: uuid!);
        }
        MPMediaLibrary.default().getPlaylist(with: uuid!, creationMetadata: MPMediaPlaylistCreationMetadata(name: appName), completionHandler: { playlist, error in
            if let error = error {
                print("\(error)")
            }
            
            if error == nil {
                print("All ok let's do some stuff with the playlist!")
            }
        });
    }

    public func saveURLToMediaCollections(url: URL) {
        
        self.createMediaPlaylist();
        let appName = BundleManager.applicationName()
        let uuid:UUID? = NeuralDefaults.getSettings(kMPMediaPlaylistUUID) as? UUID;
        
        if (uuid != nil){
            MPMediaLibrary.default().getPlaylist(with: uuid!, creationMetadata: MPMediaPlaylistCreationMetadata(name: appName), completionHandler: { playlist, error in
                
                if (error != nil){
                    Logger.shared.log("Cannot save item to playlist");
                }else{
                    playlist?.addItem(withProductID: "1234", completionHandler: { (e) in
                        Logger.shared.log(e?.localizedDescription ?? "");
                    })
                }
            });
        }
    }
    
    public func audioMediaItemFrom(url: URL?) -> MediaItemsProtocol?{
        if url == nil{
            return nil;
        }
        let ao = AudioObject(url: url!);
        return ao;
    }

    
    //MARK: Collections
    
    private func photoLibraryCollections (callback : @escaping cb_AllFolders) {
        var retVal : [PHAssetCollection] = [PHAssetCollection]();
        
        let options:PHFetchOptions = PHFetchOptions();
        options.includeHiddenAssets = true;
        options.includeAllBurstAssets = true;

        let albumAssetResults: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.album, subtype: PHAssetCollectionSubtype.any, options: options);
        let smartAlbumAssetResults: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.any, options: options);
        let momentAssetResults: PHFetchResult = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.moment, subtype: PHAssetCollectionSubtype.any, options: options);

        let albumAssetCount: Int = albumAssetResults.count;
        let smartAlbumAssetCount: Int = smartAlbumAssetResults.count;
        let momentAssetCount: Int = momentAssetResults.count;
        
        var totalAssetsCount = albumAssetCount + smartAlbumAssetCount + momentAssetCount;

        if (totalAssetsCount <= 0){
            callback(retVal);
        }
        
        albumAssetResults.enumerateObjects { (collection, index, stopPtr) in
            retVal.append(collection);
            totalAssetsCount -= 1;
            if (totalAssetsCount <= 0){
                callback(retVal);
            }
        }

        smartAlbumAssetResults.enumerateObjects { (collection, index, stopPtr) in
            retVal.append(collection);
            totalAssetsCount -= 1;
            if (totalAssetsCount <= 0){
                callback(retVal);
            }
        }

        momentAssetResults.enumerateObjects { (collection, index, stopPtr) in
            retVal.append(collection);
            totalAssetsCount -= 1;
            if (totalAssetsCount <= 0){
                callback(retVal);
            }
        }
    }
    
    

    
    //MARK: Asset write operations
    private var successCallback : cb_AssetAccessResponse? = nil;
    private var saveFilesProcessCounter : Int = 0;
    private var totalFilesCounter : Int = 0;
    private var remainingFiles : Int {
        get{
            return (totalFilesCounter - saveFilesProcessCounter);
        }
    }
    var image: UIImage!
    private var appAssetCollection: PHAssetCollection? // application asset folder in photos application
    private var appAssetCollectionPlaceholder: PHObjectPlaceholder?
    var photosAsset: PHFetchResult<AnyObject>!
    var assetThumbnailSize:CGSize!
    var collection: PHAssetCollection!

    private func createAlbum(callback:@escaping CompletionBlock) {
        
        //Get PHFetch Options
        let appName = BundleManager.applicationName();
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", appName)
        let collection : PHFetchResult = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        weak var weakSelf = self;
        
        //Check return value - If found, then get the first album out
        if let _: AnyObject = collection.firstObject {
            self.appAssetCollection = collection.firstObject
            callback(nil);
        } else {
            //If not found - Then create a new album
            PHPhotoLibrary.shared().performChanges({
                let createAlbumRequest : PHAssetCollectionChangeRequest = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: appName)
                self.appAssetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
            }, completionHandler: { success, error in

                if let identifier = weakSelf?.appAssetCollectionPlaceholder?.localIdentifier{
                    if (success == true){
                        let collectionFetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [identifier], options: nil);
                        weakSelf?.appAssetCollection = collectionFetchResult.firstObject;
                    }
                }
                callback(error?.localizedDescription);
            });
        }
    }
    public func saveURL(url: URL, callback:@escaping CompletionBlock){
        
        self.createAlbum { (error) in
            if (error != nil){
                Logger.shared.log("Error in creating album");
            }
        }
        
        PHPhotoLibrary.shared().performChanges({
            let assetRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url);
            let assetPlaceholder = assetRequest?.placeholderForCreatedAsset
            
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.appAssetCollection!)
            albumChangeRequest?.addAssets([assetPlaceholder] as NSFastEnumeration);
        }, completionHandler: { success, error in
            DispatchQueue.main.async {
                callback(error?.localizedDescription);
            }
        })
    }
    
    func saveFilesOnCurrentThread (files : Array<String>, callback : @escaping cb_AssetAccessResponse) {
        self.successCallback = callback;
        self.sel_saveFilesToGallery(files: files);
    }
    
    public func saveFilesToGallery (files : Array<String>, callback : @escaping cb_AssetAccessResponse) {
        self.successCallback = callback;
        weak var weakSelf = self;
        DispatchQueue.global(qos: .background).async {
            weakSelf?.sel_saveFilesToGallery(files: files);
        }
    }
    
    fileprivate func sel_saveFilesToGallery (files : Array<String>) {
        self.saveFilesProcessCounter = files.count;
        self.totalFilesCounter = files.count;
        
        for file in files {
            UISaveVideoAtPathToSavedPhotosAlbum(file, self, #selector(videoDidFinishSaving(path:error:context:)), nil);
        }
    }
    
    @objc fileprivate func videoDidFinishSaving(path : String, error : Error?, context : UnsafeMutableRawPointer){
        
        self.saveFilesProcessCounter -= 1;
        if (error != nil){
            print("Error in saving file : ", path);
            return;
        }
        
        if (self.saveFilesProcessCounter <= 0){
            if let response = AssetAccessResponse(response: .Success) {
                if (successCallback != nil){
                    successCallback!(response);
                }
            }
        }else{
            let info = String("Successfully saved \(self.remainingFiles)/\(self.totalFilesCounter)files") + String(self.remainingFiles);
            if let response = AssetAccessResponse(response: .Progress, failureReason: nil, warning: nil, info: info) {
                if (successCallback != nil){
                    successCallback!(response);
                }
            }
        }
    }
    
    func saveImage (images:Array<UIImage>, toAlbum:String, callback:@escaping cb_Info){
        
        weak var weakSelf = self;
        self.albumForName(name: toAlbum) { (albumPlaceHolder) in
            
            if (albumPlaceHolder != nil){
                let fetchResult : PHFetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [(albumPlaceHolder!.localIdentifier)], options: nil);
                if let assetCollection : PHAssetCollection = fetchResult.firstObject{
                    
                    var imageSaveCount = images.count;
                    
                    for image in images {
                        weakSelf?.addNewAssetForImage(image: image, toAlbum: assetCollection, cbSuccess: { (imageId) in
                            imageSaveCount -= 1;
                            if (imageSaveCount <= 0){
                                callback([kImageSavedStatus:true])
                            }
                        }, cbError: { (error) in
                            imageSaveCount -= 1;
                            if (imageSaveCount <= 0){
                                callback([kImageSavedStatus:false])
                            }
                        })
                    }
                }
            }
        }
    }
    
    func albumForName(name:String, callback:@escaping cb_ObjectPlaceHolder) {
        
        var albumPlaceHolder : PHObjectPlaceholder? = nil;
        PHPhotoLibrary.shared().performChanges({
            let changeRequest : PHAssetCollectionChangeRequest  = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name);
            albumPlaceHolder = changeRequest.placeholderForCreatedAssetCollection;
        }) { (success, error) in
            if ((success == true) && (albumPlaceHolder != nil)){
                callback(albumPlaceHolder);
            }else{
                callback(nil);
            }
        }
    }
    
    func addNewAssetForImage(image : UIImage, toAlbum:PHAssetCollection, cbSuccess:@escaping cb_AddAssetSuccess, cbError:@escaping cb_AddAssetError){
        PHPhotoLibrary.shared().performChanges({
            
            let createAssetRequest : PHAssetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            
            if let albumChangeRequest : PHAssetCollectionChangeRequest = PHAssetCollectionChangeRequest(for: toAlbum)
            {
                if let placeHolder : PHObjectPlaceholder = createAssetRequest.placeholderForCreatedAsset
                {
                    albumChangeRequest.addAssets([placeHolder] as NSFastEnumeration);
                    cbSuccess(placeHolder.localIdentifier);
                }else{
                    cbError(nil)
                }
            }else{
                cbError(nil)
            }
            
        }) { (success, error) in
            if (success == true){
                cbSuccess("")
            }else{
                cbError(error)
            }
        }
    }
}


//MARK: Callbacks
extension (AssetManager){
    
    private func callbackAssetCollection (_ callback:MediaCollectionAccessCallback?,
                                assets:[MediaCollectionProtocol],
                                throwProgress:Bool,
                                error:String?){
        DispatchQueue.main.async {
            if let cb = callback{
                if (throwProgress == true){
                    cb(assets, "\(assets.count) assets fetched", error);
                }else{
                    cb(assets, nil, error);
                }
            }
        }
    }
    
    private func callbackVideoCollections(_ throwProgress:Bool, error:String?){
        self.callbackAssetCollection(self.videoCollectionCallback, assets: self.videoCollection, throwProgress: throwProgress, error: error);
    }
    
    private func callbackImageCollections (_ throwProgress:Bool, error:String?){
        self.callbackAssetCollection(self.imageCollectionCallback, assets: self.imageCollection, throwProgress: throwProgress, error: error);
    }

    private func callbackAudioCollections (_ throwProgress:Bool, error:String?){
        self.callbackAssetCollection(self.audioCollectionCallback, assets: self.audioCollection, throwProgress: throwProgress, error: error);
    }
}
