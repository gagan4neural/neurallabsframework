//
//  AudioCollection.swift
//  NeuralLabs
//
//  Created by Gagan on 17/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import MediaPlayer
import CoreLocation

class AudioCollection: CollectionObject<AudioObject>, MediaCollectionProtocol {
    
    private var collection : MPMediaItemCollection? = nil;
    private var mediaCollectionType: MediaCollectionType = .Unknown;
    
    init (_ collection:MPMediaItemCollection?, collectionType:MediaCollectionType){
        super.init();
        self.collection = collection;
        self.mediaCollectionType = collectionType;
        self.prepareSubItemsFromCollection();
    }
    init (audioObjects:[AudioObject]?){
        super.init();
        self.mediaCollectionType = .AllMedia;
        self.mediaItems = audioObjects ?? [];
    }


    private func prepareSubItemsFromCollection (){
        // this is a Audio Playlist, needs to prepare sub-items
        if (self.collection == nil) || (self.collection!.items.count <= 0){
            return;
        }
        
        for mpMediaItem in self.collection!.items {
            let ao = AudioObject(item: mpMediaItem);
            self.mediaItems.append(ao);
            
            //Logger.shared.log("Converting audio collection : ");
            //ao.description();
        }
        //Logger.shared.log("Audio Collections : ", self.mediaItems.description);
    }

    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        let r = self.defaultCollectionName(self.mediaCollectionType);
        DispatchQueue.main.async {
            callback(r);
        }
    }
    
    var creationDate:Date?{
        get{
            var date: Date? = nil;
            if (self.collection?.representativeItem?.dateAdded != nil){
                date = self.collection?.representativeItem?.dateAdded;
            }else if (self.collection?.representativeItem?.releaseDate != nil){
                date = self.collection?.representativeItem?.releaseDate
            }else{
                date = self.mediaItems.first?.creationDate;
            }
            return date;
        }
    }
    var modifiedDate: Date?{
        get{
            var date: Date? = nil;
            if (self.collection?.representativeItem?.lastPlayedDate != nil){
                date = self.collection?.representativeItem?.lastPlayedDate;
            }else{
                date = self.mediaItems.first?.modifiedDate;
            }
            return date;
        }
    }
    var identifier: String?{
        get{
            return "Audio_Collection_Identifier";
        }
    }
    var isPlaying: Bool{
        get{
            return (self.mediaItems.filter {return $0.isPlaying}.count > 0);
        }
    }
    var showcaseImage: UIImage?{
        get{
            var img: UIImage? = nil;
            let imgSize = nlUtils.shared.assetImageSize();
            if (self.collection?.representativeItem?.artwork != nil){
                img = self.collection?.representativeItem?.artwork?.image(at: imgSize);
            }else{
                img = self.mediaItems.first?.showcaseImage;
            }
            return img;
        }
    }
    var url: URL?{
        get{
            return self.collection?.representativeItem?.assetURL;
        }
    }
    var type: MediaCollectionType{
        get{
            return self.mediaCollectionType
        }
    }
    
    func getMediaItems() -> [MediaItemsProtocol]{
        return self.mediaItems;
    }

    var location: CLLocation?{
        get{
            return nil
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }
    
    func getSequence(sequenceType:CollectionSequenceType, freshSequence:Bool) -> [Int]{
        if (freshSequence == true){
            self.itemSequence.removeAll();
        }
        if (self.itemSequence.count <= 0){
            switch sequenceType{
            case .Uniform:
                self.itemSequence = Array<Int>.uniformIndexedArray(limit: self.mediaItems.count, assending: true);
                break;
            case .Random:
                self.itemSequence = Array<Int>.randomIndexedArray(limit: self.mediaItems.count);
                break;
            }
        }
        return self.itemSequence;
    }
    
    func hasMoreData() -> Bool{
        // currently there is no pagination support in local collections. Needs to build on that.
        return false;
    }

    func requestCollectionThumbnailImages(_ callback:@escaping cb_CollectionImages){
        var itmCount = min(self.mediaItems.count, permissibleImagesDownload);
        var retImages:[UIImage?] = [];
        for itm in self.mediaItems{
            itm.requestImage { (image, info) in
                
                if (image != nil){
                    retImages.append(image);
                }
                itmCount -= 1;
                if (itmCount <= 0){
                    callback(retImages);
                }
            }
        }
    }
    
    func requestCollectionTime(_ callback: @escaping MediaTimeCallback){
        var itmCount = self.mediaItems.count;
        var retTime:TimeInterval = TimeInterval(0);
        for itm in self.mediaItems{
            itm.requestMediaTime { (mediaTime) in
                retTime = retTime.advanced(by: mediaTime);
            }
            itmCount -= 1;
            if (itmCount <= 0){
                callback(retTime);
            }
        }
    }

    func mediaItemsCount() -> String?{
        var retVal = "";
        var mediaType:[AnyHashable:Any] = [:];
        for item in self.mediaItems{
            
            let subType = item.mediaSubType();
            var count:Int? = 1;
            if mediaType.keys.contains(subType){
                count = mediaType[subType] as? Int;
                count = count! + 1;
            }
            mediaType[subType] = count;
        }
        
        for key in mediaType.keys{
            if let val = mediaType[key]{
                if (retVal.count <= 0){
                    retVal.append("\(val) \(key as! String)")
                }else{
                    retVal.append(", \(val) \(key as! String)")
                }
            }
        }

        return retVal
    }

    func getDictionary() -> [AnyHashable:Any]{
        var itmArr : [Any] = [];
        
        for i in self.mediaItems {
            itmArr.append(i.getDictionary());
        }
        return [kMediaItemDictionary:itmArr];
    }

    func description() {
        Logger.shared.log("Audio Collection identifier : ", self.identifier ?? "Nil Identifier");
        Logger.shared.log("Audio Collection creation Date : ", self.creationDate?.debugDescription ?? "Nil Creation Date");
        Logger.shared.log("Audio Collection modified Date : ", self.modifiedDate?.debugDescription ?? "Nil Modified Date");
        Logger.shared.log("Audio Collection assets : ", "\(self.mediaItems.count)");

        for itm in self.mediaItems{
            itm.description();
        }
    }
}
