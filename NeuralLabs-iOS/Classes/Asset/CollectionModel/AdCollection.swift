//
//  AdCollection.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 07/04/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import CoreLocation

class AdCollection: CollectionObject<AdObject>, MediaCollectionProtocol {
    
    func getMediaItems() -> [MediaItemsProtocol] {
        return [];
    }
    
    func getSequence(sequenceType: CollectionSequenceType, freshSequence: Bool) -> [Int] {
        return [];
    }
    
    func hasMoreData() -> Bool {
        return false;
    }
    
    func description() {
        
    }
    
    func getDictionary() -> [AnyHashable : Any] {
        return [:];
    }
    

    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        DispatchQueue.main.async {
            callback("Ad Object");
        }
    }
    var creationDate:Date?{
        get{
            return nil;
        }
    }
    var modifiedDate: Date?{
        get{
            return nil;
        }
    }
    var identifier: String?{
        get{
            return nil;
        }
    }
    var isPlaying: Bool{
        get{
            return false;
        }
    }
    var showcaseImage: UIImage?{
        get{
            return nil;
        }
    }
    var url: URL?{
        get{
            return nil;
        }
    }
    var type: MediaCollectionType{
        get{
            return .Unknown;
        }
    }
    
    var location: CLLocation? {
        get{
            return nil;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return true;
        }
    }
    
    func requestCollectionThumbnailImages(_ callback:@escaping cb_CollectionImages){
    }
    
    func requestCollectionTime(_ callback: @escaping MediaTimeCallback){
    }
    
    func mediaItemsCount() -> String?{
        return nil;
    }
}
