//
//  VideoCollection.swift
//  NeuralLabs
//
//  Created by Gagan on 17/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import Photos
import CoreLocation

class VideoCollection: CollectionObject<VideoObject>, MediaCollectionProtocol {
    
    
    private var collection : PHAssetCollection? = nil;
    private var mediaCollectionType: MediaCollectionType = .Unknown;

    init (_ collection:PHAssetCollection?, collectionType:MediaCollectionType){
        super.init();
        self.collection = collection;
        self.mediaCollectionType = collectionType;
        self.prepareSubItemsFromCollection();
    }
    init (videoObjects:[VideoObject]?){
        super.init();
        self.mediaCollectionType = .AllMedia;
        self.mediaItems = videoObjects ?? [];
    }

    private var photoLibraryCollectionType: PhotoLibraryCollectionType{
        if (self.collection?.assetCollectionType == PHAssetCollectionType.album){
            return PhotoLibraryCollectionType.Album;
        }
        else if (self.collection?.assetCollectionType == PHAssetCollectionType.smartAlbum){
            return PhotoLibraryCollectionType.SmartAlbum
        }
        else if (self.collection?.assetCollectionType == PHAssetCollectionType.moment){
            return PhotoLibraryCollectionType.Moment
        }
        return PhotoLibraryCollectionType.Unknown
    }

    func assetType () -> String?{
        switch self.collection?.assetCollectionType {
        case .album?:
            return "Album"
        case .smartAlbum?:
            return "Smart Album"
        case .moment?:
            return "Moment"
        default:
            break;
        }
        return nil;
    }
    
    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        var name:String = self.defaultCollectionName(self.mediaCollectionType);
        if let cName = self.collection?.localizedTitle{
            name = cName;
        }
        else if let aName = self.assetType(){
            name = aName;
        }
        DispatchQueue.main.async {
            callback(name);
        }
    }
    var creationDate:Date?{
        get{
            return self.collection?.startDate;
        }
    }
    var modifiedDate: Date?{
        get{
            return self.collection?.endDate;
        }
    }
    var identifier: String?{
        get{
            return self.collection?.localIdentifier;
        }
    }
    var isPlaying: Bool{
        get{
            return (self.mediaItems.filter {return $0.isPlaying}.count > 0);
        }
    }
    var showcaseImage: UIImage?{
        get{
            return nil;
        }
    }
    var url: URL?{
        get{
            return nil;
        }
    }
    var type: MediaCollectionType{
        get{
            return self.mediaCollectionType;
        }
    }
    
    var location: CLLocation? {
        get{
            return self.collection?.approximateLocation;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }

    func getSequence(sequenceType:CollectionSequenceType, freshSequence:Bool) -> [Int]{
        
        if (freshSequence == true){
            self.itemSequence.removeAll();
        }
        if (self.itemSequence.count <= 0){
            switch sequenceType{
            case .Uniform:
                self.itemSequence = Array<Int>.uniformIndexedArray(limit: self.mediaItems.count, assending: true);
                break;
            case .Random:
                self.itemSequence = Array<Int>.randomIndexedArray(limit: self.mediaItems.count);
                break;
            }
        }
        
        return self.itemSequence;
    }

    func hasMoreData() -> Bool{
        // currently there is no pagination support in local collections. Needs to build on that.
        return false;
    }

    func requestCollectionThumbnailImages(_ callback:@escaping cb_CollectionImages){
        var itmCount = min(self.mediaItems.count, permissibleImagesDownload);
        var retImages:[UIImage?] = [];
        for itm in self.mediaItems{
            itm.requestImage { (image, info) in
                
                if (image != nil){
                    retImages.append(image);
                }
                itmCount -= 1;
                if (itmCount <= 0){
                    callback(retImages);
                }
            }
        }
    }
    
    func requestCollectionTime(_ callback: @escaping MediaTimeCallback){
        var itmCount = self.mediaItems.count;
        var retTime:TimeInterval = TimeInterval(0);
        for itm in self.mediaItems{
            itm.requestMediaTime { (mediaTime) in
                retTime = retTime.advanced(by: mediaTime);
                
                itmCount -= 1;
                if (itmCount <= 0){
                    callback(retTime);
                }
            }
        }
    }
    
    func mediaItemsCount() -> String?{
        var retVal = "";
        var mediaType:[AnyHashable:Any] = [:];
        for item in self.mediaItems{
            
            let subType = item.mediaSubType();
            var count:Int? = 1;
            if mediaType.keys.contains(subType){
                count = mediaType[subType] as? Int;
                count = count! + 1;
            }
            mediaType[subType] = count;
        }
        
        for key in mediaType.keys{
            if let val = mediaType[key]{
                if (retVal.count <= 0){
                    retVal.append("\(val) \(key as! String)")
                }else{
                    retVal.append(", \(val) \(key as! String)")
                }
            }
        }
        
        return retVal
    }

    func getMediaItems() -> [MediaItemsProtocol]{
        return self.mediaItems;
    }
    
    private func prepareSubItemsFromCollection (){
        
        let result : PHFetchResult = PHAsset.fetchAssets(in: (self.collection)!, options: nil);
        weak var weakSelf = self;
        
        result .enumerateObjects { (phAsset, index, stopPtr) in
            
            if (phAsset.isKind(of: phAsset.classForCoder)){
                if let subAsset = VideoObject(item: phAsset){
                    weakSelf?.mediaItems.append(subAsset);
                }
            }
        }
    }
    
    func getDictionary() -> [AnyHashable:Any]{
        var itmArr : [Any] = [];
        
        for i in self.mediaItems {
            itmArr.append(i.getDictionary());
        }
        return [kMediaItemDictionary:itmArr];
    }

    func description() {
        Logger.shared.log("Video Collection identifier : ", self.identifier ?? "Nil Identifier");
        Logger.shared.log("Video Collection creation Date : ", self.creationDate?.debugDescription ?? "Nil Creation Date");
        Logger.shared.log("Video Collection modified Date : ", self.modifiedDate?.debugDescription ?? "Nil Modified Date");
        Logger.shared.log("Video Collection assets : ", "\(self.mediaItems.count)");

        for itm in self.mediaItems{
            itm.description();
        }
    }
    
    func getCollection() -> VideoCollection?{
        return self;
    }
}
