//
//  AssetCollection.swift
//  NeuralLabs
//
//  Created by Gagan on 17/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

enum PhotoLibraryCollectionType{
    case Unknown;
    case Album;
    case SmartAlbum;
    case Moment;
}

class CollectionObject<TYPE>{
    var mediaItems:Array<TYPE> = Array<TYPE>();
    let permissibleImagesDownload = 2;
    var itemSequence:[Int] = [];
    
    func defaultCollectionName(_ type: MediaCollectionType) -> String{
        switch (type) {
        case .AllMedia:
            return "Entire"
        case .Album:
            return "Albums"
        case .Folder:
            return "Folders"
        case .Gener:
            return "Geners"
        case .Playlist:
            return "Playlist"
        default:
            break;
        }
        return "Unknown";
    }
    
}
