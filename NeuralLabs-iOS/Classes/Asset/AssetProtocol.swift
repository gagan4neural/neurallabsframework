//
//  AssetProtocol.swift
//  NeuralLabs
//
//  Created by Gagan on 17/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

public let kMediaItemDictionary = "MediaItem";
public let kMediaCollectionDictionary = "MediaCollection";

public enum MediaItemType : UInt16{
    case Unknown = 1
    case Image = 2
    case Audio = 3
    case Video = 4
    case Document = 5
    case Youtube = 6
    case Spotify = 7
    case LiveStream = 8
}

public enum MediaCollectionType : UInt16{
    case Unknown = 0x0001
    case Playlist = 0x0002
    case Album = 0x0004
    case Folder = 0x0008
    case Gener = 0x0010
    case AllMedia = 0x0011
}

public enum CollectionSequenceType: UInt16{
    case Uniform;
    case Random;
}

public protocol AssetProtocol: AnyObject{
   
    func title(_ callback:@escaping cb_AssetTitle);
    var creationDate:Date? {get}
    var modifiedDate:Date? {get}
    var identifier:String? {get}
    var showcaseImage: UIImage? {get};
    var url: URL? {get};
    var location:CLLocation? {get}
    var isAdvertisement: Bool {get};
    
    func description();
    
    func getDictionary() -> [AnyHashable:Any];
}

public protocol MediaCollectionProtocol:AssetProtocol{
    var type:MediaCollectionType {get}
    var isPlaying:Bool {get}

    func getMediaItems() -> [MediaItemsProtocol];
    func requestCollectionThumbnailImages(_ callback:@escaping cb_CollectionImages);
    func requestCollectionTime(_ callback: @escaping MediaTimeCallback);
    func mediaItemsCount() -> String?;
    
    func getSequence(sequenceType:CollectionSequenceType, freshSequence:Bool) -> [Int];
    
    func hasMoreData() -> Bool;
}

public protocol MediaItemsProtocol:AssetProtocol{
    var type:MediaItemType {get}
    var favorite:Bool {get set}
    var isCloudItem: Bool {get}
    var numberOfTimesPlayed: Int {get set}
    var isPlaying:Bool {get set}
    var lyrics: String? {get}
    var genre: String? {get}
    var composer: String? {get}
    var beatsPerMinute: Int? {get}
    var customInfo: [AnyHashable: Any]? {get set};

    func requestAVAsset (_ callback: @escaping cb_AVAsset);
    func requestPlayerItem (_ callback: @escaping cb_AVPlayerItem);
    func requestImage (_ callback: @escaping cb_ThumbnailImage);
    func requestImageData (_ callback: @escaping cb_ImageInfo);
    func requestLivePhoto (_ callback: @escaping cb_LivePhoto);
    func requestMediaTime (_ callback: @escaping MediaTimeCallback);
    func mediaSubType() -> String?;
    
    func canSupportARMode() -> Bool;
    func canSupportSlowMotion() -> Bool;
    func canSeperateAudioVideo() -> Bool;
    func canSupportScreenShot() -> Bool;
    func canSupportPiP() -> Bool;
    func canSupportMediaClipping() -> Bool;
    
    func copy(deepCopy: Bool) -> MediaItemsProtocol?;
    func save(callback:@escaping CompletionBlock);
}
