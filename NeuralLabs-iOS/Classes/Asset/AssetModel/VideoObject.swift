//
//  VideoAssetObject.swift
//  NeuralLabs
//
//  Created by Gagan on 16/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import Photos
import AVKit

class VideoObject: AssetObject, MediaItemsProtocol {

    private var asset : PHAsset? = nil;
    private var assetResource: [PHAssetResource] = [];
    private var _isFavorite: Bool = false;
    private var _numberOfTimesPlayed: Int = 0;

    private var avAsset: AVAsset? = nil;
    private var avAudioMix: AVAudioMix? = nil;
    private var avAssetInfo: [AnyHashable:Any]? = nil;
    private var avAssetRequestId: PHImageRequestID = -1;

    private var avPlayerItem: AVPlayerItem? = nil;
    private var avPlayerItemFetchInProgress: Bool = false;
    private var avPlayerItemInfo: [AnyHashable:Any]? = nil;
    private var avPlayerItemRequestId: PHImageRequestID = -1;

    private var thumbnailImage: UIImage? = nil;
    private var thumbnailImageInfo: [AnyHashable:Any]? = nil;
    private var thumbnailImageRequestId: PHImageRequestID = -1;

    private var imageData: Data? = nil;
    private var imageUti: String? = nil;
    private var imageOrientation: UIImage.Orientation = .up
    private var imageInfo: [AnyHashable:Any]? = nil;
    private var imageRequestId: PHImageRequestID = -1;

    private var livePhoto: PHLivePhoto? = nil;
    private var livePhotoInfo: [AnyHashable:Any]? = nil;
    private var livePhotoRequestId: PHImageRequestID = -1;

    init ?(item:PHAsset?){
        // return nil if PHAsset is not and video.
        if (item?.mediaType != PHAssetMediaType.video){
            return nil;
        }
        super.init();
        asset = item;
        mediaItemType = .Video;
        
        assetResource = PHAssetResource.assetResources(for: asset!)
    }
    
    init? (url: URL){
        super.init();
        mediaItemType = .Video;
        self.avPlayerItem = AVPlayerItem(url: url)
    }
    
    func getAsset () -> PHAsset? {
        return self.asset;
    }
    
    
    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        var name = "Video-File"
        if let res = self.assetResource.first{
            name = res.originalFilename;
        }
        DispatchQueue.main.async {
            callback(name)
        }
    }
    var creationDate:Date?{
        get{
            return self.asset?.creationDate;
        }
    }
    var modifiedDate: Date?{
        get{
            return self.asset?.modificationDate;
        }
    }
    var identifier: String?{
        get{
            return self.asset?.localIdentifier;
        }
    }
    var isPlaying: Bool{
        get{
            return _isPlaying;
        }
        set (newValue){
            _isPlaying = newValue;
        }
    }
    var showcaseImage: UIImage?{
        get{
            return nil;
        }
    }
    var url: URL?{
        get{
            if let avItem = self.avPlayerItem {
                let asset = avItem.asset;
                if asset.isKind(of: AVURLAsset.classForCoder()){
                    return (asset as? AVURLAsset)?.url;
                }
            }
            return nil;
        }
    }
    var type:MediaItemType{
        get{
            return self.mediaItemType;
        }
    }

    var favorite: Bool {
        get{
            return _isFavorite;
        }
        set{
            //TODO: Do some magic here
        }
    }
    
    var isCloudItem: Bool {
        get{
            return false;
        }
    }
    
    var location: CLLocation? {
        get{
            return self.asset?.location;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }

    var numberOfTimesPlayed: Int {
        get{
            return _numberOfTimesPlayed
        }set{
            _numberOfTimesPlayed += 1;
        }
    }
    
    var lyrics: String?{
        get{
            return nil;
        }
    }
    var genre: String?{
        get{
            return nil;
        }
    }
    var composer: String?{
        get{
            return nil;
        }
    }
    var beatsPerMinute: Int?{
        get{
            return nil;
        }
    }
    var customInfo: [AnyHashable : Any]? {
        get{
            return _customInfo;
        }set{
            _customInfo = newValue;
        }
    }

    func canSupportARMode() -> Bool{
        return true;
    }
    
    func canSupportSlowMotion() -> Bool{
        return true;
    }
    
    func canSeperateAudioVideo() -> Bool{
        return true;
    }

    func canSupportScreenShot() -> Bool{
        return true;
    }
    
    func canSupportPiP() -> Bool{
        return AVPictureInPictureController.isPictureInPictureSupported()
    }
    
    func canSupportMediaClipping() -> Bool{
        return true;
    }

    func copy(deepCopy: Bool) -> MediaItemsProtocol? {
        let obj: VideoObject = VideoObject(item: ((self.asset?.copy())! as! PHAsset))!;
        obj._isFavorite = self._isFavorite;
        obj.numberOfTimesPlayed = self.numberOfTimesPlayed;
        return obj;
    }

    func save(callback:@escaping CompletionBlock){
        // nil means success; Failer will return failure reason...
        if let u = self.url{
            AssetManager.shared.saveURL(url: u, callback: callback);
        }
    }

    func requestAVAsset (_ callback: @escaping cb_AVAsset){
        if ((self.avAsset != nil) && (self.avAudioMix != nil)){
            callback(self.avAsset, self.avAudioMix, self.avAssetInfo);
            return;
        }
        self.getAVAsset(callback: callback)
    }
    
    func requestPlayerItem (_ callback: @escaping cb_AVPlayerItem){
        if (self.avPlayerItemFetchInProgress){
            // some avplayer item fetch request is in progress;
            return;
        }
        if (self.avPlayerItem != nil){
            callback(self.avPlayerItem, self.avAssetInfo);
            return;
        }
        self.getAVPlayerItem(callback: callback);
    }
    
    func requestImage (_ callback: @escaping cb_ThumbnailImage){
        if (self.thumbnailImage != nil){
            callback(self.thumbnailImage, self.thumbnailImageInfo);
            return;
        }
        self.getThumbnailImage(callback: callback);
    }
    
    func requestImageData (_ callback: @escaping cb_ImageInfo){
        if ((self.imageData != nil) && (self.imageUti != nil) && (self.imageInfo != nil)){
            callback(self.imageData, self.imageUti, self.imageOrientation, self.imageInfo)
            return;
        }
        self.getImageData(callback: callback);
    }
    
    func requestLivePhoto (_ callback: @escaping cb_LivePhoto){
        if ((self.livePhoto != nil) && (self.livePhotoInfo != nil)){
            callback(self.livePhoto, self.livePhotoInfo);
            return;
        }
        self.getLivePhoto(callback: callback);
    }

    func requestMediaTime (_ callback: @escaping MediaTimeCallback){
        self.requestAVAsset { (asset, audioMix, info) in
            if (asset != nil){
                let time:CMTime = asset!.duration;
                let seconds:Float64 = CMTimeGetSeconds(time);
                callback(TimeInterval(seconds));
            }else{
                callback(TimeInterval(0))
            }
        }
    }

    func mediaSubType() -> String?{
        return "Videos"
    }

    func getAVAsset (callback : @escaping cb_AVAsset) {
        if let asset = self.asset {
            weak var weakSelf = self;
            avAssetRequestId = PHImageManager.default().requestAVAsset(forVideo: asset, options: nil) { (avAsset, avAudioMix, info) in
                weakSelf?.avAsset = avAsset;
                weakSelf?.avAudioMix = avAudioMix;
                weakSelf?.avAssetInfo = info;
                callback(avAsset, avAudioMix, info);
            }
        }
    }
    
    func getAVPlayerItem (callback : @escaping cb_AVPlayerItem){
        
        if let asset = self.asset {
            weak var weakSelf = self;
            self.avPlayerItemFetchInProgress = true;
            avPlayerItemRequestId = PHImageManager.default().requestPlayerItem(forVideo: asset.copy() as! PHAsset, options: nil) { (playerItem, info) in
                weakSelf?.avPlayerItem = playerItem;
                weakSelf?.avPlayerItemInfo = info;
                DispatchQueue.main.async(execute: {
                    weakSelf?.avPlayerItemFetchInProgress = false;
                    callback(playerItem, info);
                })
            }
        }
    }
    
    func getThumbnailImage (callback : @escaping cb_ThumbnailImage) {
        if let asset = self.asset {
            weak var weakSelf = self;
            DispatchQueue.main.async {
                
                let imgSize = nlUtils.shared.assetImageSize();
                weakSelf?.thumbnailImageRequestId = PHImageManager.default().requestImage(for: asset, targetSize: imgSize , contentMode: .aspectFill, options: nil) { (image, info) in
                    weakSelf?.thumbnailImage = image;
                    weakSelf?.thumbnailImageInfo = info;
                    callback(image, info);
                }
            }
        }
    }
    
    func getImageData (callback : @escaping cb_ImageInfo) {
        if let asset = self.asset {
            weak var weakSelf = self;
            weakSelf?.imageRequestId = PHImageManager.default().requestImageData(for: asset, options: nil) { (data, dataUti, imageOrientation, info) in
                weakSelf?.imageUti = dataUti;
                weakSelf?.imageData = data;
                weakSelf?.imageOrientation = imageOrientation;
                weakSelf?.imageInfo = info;
                callback(data, dataUti, imageOrientation, info);
            }
        }
    }

    func getLivePhoto (callback : @escaping cb_LivePhoto) {
        if let asset = self.asset {
            weak var weakSelf = self;
            weakSelf?.livePhotoRequestId = PHImageManager.default().requestLivePhoto(for: asset, targetSize: assetSize, contentMode: .aspectFill, options: nil) { (livePhoto, info) in
                weakSelf?.livePhoto = livePhoto
                weakSelf?.livePhotoInfo = info
                callback(livePhoto, info);
            }
        }
    }

    func description() {
        Logger.shared.log("Video Asset identifier : ", self.identifier ?? "Nil Identifier");
        Logger.shared.log("Video Asset creation Date : ", self.creationDate?.debugDescription ?? "Nil Creation Date");
        Logger.shared.log("Video Asset modified Date : ", self.modifiedDate?.debugDescription ?? "Nil Modified Date");
    }


    func getDictionary() -> [AnyHashable:Any]{
        return [:];
    }
}
