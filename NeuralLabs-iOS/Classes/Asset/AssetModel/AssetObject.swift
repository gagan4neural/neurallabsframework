//
//  FileObject.swift
//  FileManager-iOS
//
//  Created by Gagan on 17/08/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import Foundation

class AssetObject{
    internal var _isPlaying : Bool = false;
    internal var mediaItemType: MediaItemType = .Unknown;
    internal var assetSize = CGSize(width: 1024, height: 1024);
    internal var _customInfo: [AnyHashable: Any]?
}
