//
//  AudioAssetObject.swift
//  NeuralLabs
//
//  Created by Gagan on 16/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import Photos
import MediaPlayer

class AudioObject: AssetObject, MediaItemsProtocol {
    
    
    private var mediaItem : MPMediaItem? = nil;
    private var _isFavorite: Bool = false;
    private var _numberOfTimesPlayed: Int = 0;
    private var avPlayerItem: AVPlayerItem? = nil;

    init (item:MPMediaItem?){
        super.init();
        mediaItem = item;
        mediaItemType = .Audio;
    }
    
    init? (url: URL){
        super.init();
        mediaItemType = .Audio;
        self.avPlayerItem = AVPlayerItem(url: url)
    }

    var lastPlayedDate: Date?{
        get{
            if (self.mediaItem != nil){
                return self.mediaItem?.lastPlayedDate;
            }
            return nil;
        }
    }
    var albumTitle: String? {
        get{
            if (self.mediaItem != nil){
                return self.mediaItem?.albumTitle;
            }
            return nil;
        }
    }
    var albumArtist: String? {
        get{
            if (self.mediaItem != nil){
                return self.mediaItem?.albumArtist;
            }
            return nil;
        }
    }

    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        let i = self.mediaItem?.title ?? "Audio";
        DispatchQueue.main.async {
            callback(i)
        }
    }
    var creationDate:Date?{
        get{
            return self.mediaItem?.dateAdded;
        }
    }
    var modifiedDate: Date?{
        get{
            return self.mediaItem?.lastPlayedDate;
        }
    }
    var identifier: String?{
        get{
            return self.mediaItem?.persistentID.description;
        }
    }
    var isPlaying: Bool{
        get{
            return _isPlaying;
        }
        set (newValue){
            _isPlaying = newValue;
        }
    }
    var showcaseImage: UIImage?{
        get{
            let imgSize = nlUtils.shared.assetImageSize();
            return self.mediaItem?.artwork?.image(at: imgSize);
        }
    }
    var url: URL?{
        get{
            if let u = self.mediaItem?.assetURL{
                return u;
            }
            if let avItem = self.avPlayerItem {
                let asset = avItem.asset;
                if asset.isKind(of: AVURLAsset.classForCoder()){
                    return (asset as? AVURLAsset)?.url;
                }
            }
            return nil;
        }
    }
    var type:MediaItemType{
        get{
            return self.mediaItemType;
        }
    }

    var favorite: Bool {
        get{
            return _isFavorite;
        }
        set{
            //TODO: Do some magic here
        }
    }
    
    var isCloudItem: Bool {
        get{
            return self.mediaItem?.isCloudItem ?? false;
        }
    }
    
    var location: CLLocation? {
        get{
            return nil;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }
    
    var numberOfTimesPlayed: Int {
        get{
            return _numberOfTimesPlayed
        }set{
            _numberOfTimesPlayed += 1;
        }
    }
    
    var lyrics: String?{
        get{
            return self.mediaItem?.lyrics;
        }
    }
    var genre: String?{
        get{
            return self.mediaItem?.genre;
        }
    }
    var composer: String?{
        get{
            return self.mediaItem?.composer;
        }
    }
    var beatsPerMinute: Int?{
        get{
            return self.mediaItem?.beatsPerMinute
        }
    }
    var customInfo: [AnyHashable : Any]? {
        get{
            return _customInfo;
        }set{
            _customInfo = newValue;
        }
    }

    func canSupportARMode() -> Bool{
        return false;
    }

    func canSupportSlowMotion() -> Bool{
        return true;
    }
    
    func canSeperateAudioVideo() -> Bool{
        return false;
    }

    func canSupportScreenShot() -> Bool{
        return false;
    }
    
    func canSupportPiP() -> Bool{
        return false;
    }
    
    func canSupportMediaClipping() -> Bool{
        return true;
    }

    func copy(deepCopy: Bool) -> MediaItemsProtocol? {
        let obj: AudioObject = AudioObject(item: self.mediaItem);
        obj._isFavorite = self._isFavorite;
        return obj;
    }
    
    func save(callback:@escaping CompletionBlock){
        // nil means success; Failer will return failure reason...
        if let u = self.url{
            AssetManager.shared.saveURL(url: u, callback: callback);
        }
    }

    func requestAVAsset (_ callback: @escaping cb_AVAsset){
        guard let url = (self.mediaItem?.assetURL) else{
            callback(nil, nil, nil);
            return;
        }
        let asset = AVAsset(url: url);
        callback(asset, nil, nil);
    }
    
    func requestPlayerItem (_ callback: @escaping cb_AVPlayerItem){
        if let u = self.mediaItem?.assetURL {
            let avPlayerItem = AVPlayerItem(url: u);
            callback(avPlayerItem, nil);
            return;
        }
        if let u = self.url {
            let avPlayerItem = AVPlayerItem(url: u);
            callback(avPlayerItem, nil);
            return;
        }
        callback(nil, nil);
    }
    
    func requestImage (_ callback: @escaping cb_ThumbnailImage){
        let img = self.mediaItem?.artwork?.image(at: nlUtils.shared.assetImageSize());
        callback(img, nil);
    }
    
    func requestImageData (_ callback: @escaping cb_ImageInfo){
        callback(nil, nil, .up, nil);
    }
    
    func requestLivePhoto (_ callback: @escaping cb_LivePhoto){
        callback(nil, nil);
    }

    func requestMediaTime (_ callback: @escaping MediaTimeCallback){
        callback(self.mediaItem?.playbackDuration ?? TimeInterval(0));
    }

    func mediaSubType() -> String?{
        return "Audios"
    }
    
    func description() {
        Logger.shared.log("Audio Asset creation Date : ", self.creationDate?.debugDescription ?? "Nil Creation Date");
        Logger.shared.log("Audio Asset modified Date : ", self.modifiedDate?.debugDescription ?? "Nil Modified Date");
    }

    func getDictionary() -> [AnyHashable:Any]{
        return [:];
    }
}
