//
//  AdObject.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 07/04/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import CoreLocation

class AdObject: AssetObject, MediaItemsProtocol {
    func requestAVAsset(_ callback: @escaping cb_AVAsset) {
        
    }
    
    func requestPlayerItem(_ callback: @escaping cb_AVPlayerItem) {
        
    }
    
    func requestImage(_ callback: @escaping cb_ThumbnailImage) {
        
    }
    
    func requestImageData(_ callback: @escaping cb_ImageInfo) {
        
    }
    
    func requestLivePhoto(_ callback: @escaping cb_LivePhoto) {
        
    }
    
    func requestMediaTime(_ callback: @escaping MediaTimeCallback) {
        
    }
    
    func mediaSubType() -> String? {
        return nil;
    }
    
    func copy(deepCopy: Bool) -> MediaItemsProtocol? {
        return nil;
    }
    
    func save(callback: @escaping CompletionBlock) {
    }
    
    func description() {
    }
    
    func getDictionary() -> [AnyHashable : Any] {
        return [:]
    }
    
    
    func title(_ callback:@escaping cb_AssetTitle){
        DispatchQueue.main.async {
            callback("Ad Object")
        }
    }
    var creationDate:Date?{
        get{
            return nil;
        }
    }
    var modifiedDate: Date?{
        get{
            return nil;
        }
    }
    var identifier: String?{
        get{
            return nil;
        }
    }
    var isPlaying: Bool{
        get{
            return false;
        }
        set (newValue){
        }
    }
    var showcaseImage: UIImage?{
        get{
            return nil;
        }
    }
    var url: URL?{
        get{
            return nil;
        }
    }
    var type:MediaItemType{
        get{
            return .Unknown;
        }
    }
    
    var favorite: Bool {
        get{
            return false;
        }
        set{
            //TODO: Do some magic here
        }
    }
    
    var isCloudItem: Bool {
        get{
            return false;
        }
    }
    
    var location: CLLocation? {
        get{
            return nil;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return true;
        }
    }
    
    var numberOfTimesPlayed: Int {
        get{
            return 0;
        }set{
        }
    }
    
    var lyrics: String?{
        get{
            return nil;
        }
    }
    var genre: String?{
        get{
            return nil;
        }
    }
    var composer: String?{
        get{
            return nil;
        }
    }
    var beatsPerMinute: Int?{
        get{
            return nil;
        }
    }
    var customInfo: [AnyHashable : Any]? {
        get{
            return _customInfo;
        }set{
            _customInfo = newValue;
        }
    }
    
    func canSupportARMode() -> Bool{
        return false;
    }
    
    func canSupportSlowMotion() -> Bool{
        return false;
    }
    
    func canSeperateAudioVideo() -> Bool{
        return false;
    }
    
    func canSupportScreenShot() -> Bool{
        return false;
    }
    
    func canSupportPiP() -> Bool{
        return false;
    }
    
    func canSupportMediaClipping() -> Bool{
        return false;
    }

}
