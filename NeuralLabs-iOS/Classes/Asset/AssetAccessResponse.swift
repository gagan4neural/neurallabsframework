//
//  FileAccessResponse.swift
//  FileManager-iOS
//
//  Created by Gagan on 17/08/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import Foundation

enum AssetAccessResponseEnum : Int {
    case Success
    case Unknown
    case Failure
    case Warning
    case Progress
    case Info
    case Other
}

public class AssetAccessResponse {
    
    init (response:AssetAccessResponseEnum,
          failureReason:String?,
          warning:String?,
          info:String?,
          otherInfo:Dictionary<String, String>?){
        self.response = response;
        self.failureReason = failureReason;
        self.warning = warning;
        self.info = info;
        self.otherInfo = otherInfo;
    }
    
    
    convenience init? (response:AssetAccessResponseEnum)
    {
        if (response != .Success){
            // failure reason must be specified for failure.
            return nil;
        }
        self.init(response: response,
                  failureReason:nil,
                  warning:nil,
                  info:nil,
                  otherInfo:nil);
    }

    convenience init? (response:AssetAccessResponseEnum,
                       failureReason:String?,
                       warning:String?,
                       info:String?)
    {
        if (response != .Success  &&  failureReason == nil){
            // failure reason must be specified for failure.
            return nil;
        }
        self.init(response:response,
                  failureReason:failureReason,
                  warning:warning,
                  info:info,
                  otherInfo:nil);
    }
    
    static let noReasons : String = "No Reasons"
    var response : AssetAccessResponseEnum = .Unknown;
    var failureReason : String? = "Unknown";
    var warning : String? = nil;
    var info : String? = nil;
    var otherInfo : Dictionary<String, String>? = nil;
    
    var isSuccess : Bool {
        get{
            return (self.response == .Success);
        }
    };
    
    
    func quickErrorTupple() -> (Bool, String) {
        return (self.isSuccess, (self.failureReason == nil) ? AssetAccessResponse.noReasons : (self.failureReason)!);
    }

    func quickWarningTupple() -> (Bool, String) {
        return (self.isSuccess, (self.warning == nil) ? AssetAccessResponse.noReasons : (self.warning)!);
    }

    func quickInfoTupple() -> (Bool, String) {
        return (self.isSuccess, (self.info == nil) ? AssetAccessResponse.noReasons : (self.info)!);
    }

}
