import UIKit

fileprivate let animatedTextInput_textSize: CGFloat = 18.0
fileprivate let animatedCounterLabel_textSize: CGFloat = 11.0

public protocol AnimatedTextInputStyle {
    var activeColor: UIColor { get }
    var placeholderInactiveColor: UIColor { get }
    var inactiveColor: UIColor { get }
    var lineInactiveColor: UIColor { get }
    var lineActiveColor: UIColor { get }
    var lineHeight: CGFloat { get }
    var errorColor: UIColor { get }
    var textInputFont: UIFont { get }
    var textInputFontColor: UIColor { get }
    var placeholderMinFontSize: CGFloat { get }
    var counterLabelFont: UIFont? { get }
    var leftMargin: CGFloat { get }
    var topMargin: CGFloat { get }
    var rightMargin: CGFloat { get }
    var bottomMargin: CGFloat { get }
    var yHintPositionOffset: CGFloat { get }
    var yPlaceholderPositionOffset: CGFloat { get }
    var textAttributes: [String: Any]? { get }
}

public struct AnimatedTextInputStyleBlue: AnimatedTextInputStyle {
    public var activeColor: UIColor {
        get{
            return ColorUtils.instance().textColor;
        }
    }
    public var lineActiveColor: UIColor {
        get{
            return ColorUtils.instance().textColor.withAlphaComponent(0.5);
        }
    }
    public var textInputFont: UIFont {
        get{
            return FontManager.shared.helveticaNeue(animatedTextInput_textSize) ?? UIFont.systemFont(ofSize: animatedTextInput_textSize);
        }
    }
    
    public var textInputFontColor: UIColor {
        get{
            return ColorUtils.instance().textColor;
        }
    }
    
    private static let customBlue = ColorUtils.instance().defaultColor;
    public let placeholderInactiveColor = UIColor.gray.withAlphaComponent(0.5)
    public let inactiveColor = UIColor.gray.withAlphaComponent(0.5)
    public let lineInactiveColor = UIColor.gray.withAlphaComponent(0.2)
    public let lineHeight: CGFloat = 1.0 / UIScreen.main.scale
    public let errorColor = ColorUtils.instance().warningColor(mode: .Normal)
    public let placeholderMinFontSize: CGFloat = 9
    public let counterLabelFont: UIFont? = FontManager.shared.helveticaNeue(animatedCounterLabel_textSize);
    public let leftMargin: CGFloat = 0
    public let topMargin: CGFloat = 20
    public let rightMargin: CGFloat = 0
    public let bottomMargin: CGFloat = 10
    public let yHintPositionOffset: CGFloat = 7
    public let yPlaceholderPositionOffset: CGFloat = 0
    //Text attributes will override properties like textInputFont, textInputFontColor...
    public let textAttributes: [String: Any]? = nil

    public init() { }
}
