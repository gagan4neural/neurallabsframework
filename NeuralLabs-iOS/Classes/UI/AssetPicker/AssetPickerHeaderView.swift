//
//  AssetPickerHeaderView.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 10/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

class AssetPickerHeaderView: NeuralBaseView {
    var btnClose: UIButton = UIButton(type: .custom);
    var btnAspectRatio: UIButton = UIButton(type: .custom);
    
    var closeCallback: onCompletionCallback? = nil;
    var aspectRationCallback: onCompletionCallback? = nil;
    
    func loadView () {
        self.addSubview(self.btnClose);
        self.addSubview(self.btnAspectRatio);
        self.backgroundColor = .clear;
        
        btnClose.backgroundColor = .clear;
        btnClose.titleLabel?.font = FontManager.shared.iconFont();
        btnClose.titleLabel?.textAlignment = .center;
        btnClose.setTitle(IconFontCodes.closeCircle, for: .normal);
        btnClose.setTitleColor(.darkGray, for: .normal);
        btnClose.setTitleColor(ColorUtils.instance().highlightedColor, for: .highlighted);
        btnClose.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        btnClose.addTarget(self, action: #selector(onBtnTap), for: .touchUpInside);
        
        btnAspectRatio.backgroundColor = .clear;
        btnAspectRatio.titleLabel?.font = FontManager.shared.iconFont();
        btnAspectRatio.titleLabel?.textAlignment = .center;
        btnAspectRatio.setTitle(IconFontCodes.aspectRatio, for: .normal);
        btnAspectRatio.setTitleColor(.darkGray, for: .normal);
        btnAspectRatio.setTitleColor(ColorUtils.instance().highlightedColor, for: .highlighted);
        btnAspectRatio.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        btnAspectRatio.addTarget(self, action: #selector(onBtnTap), for: .touchUpInside);
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        
        let offset: CGFloat = 5;
        let size = self.bounds.height - 2*offset;
        
        self.btnClose.frame = CGRect(x: offset, y: offset, width: size, height: size);
        self.btnAspectRatio.frame = CGRect(x: self.bounds.width - size - offset, y: offset, width: size, height: size);
    }
    
    @objc func onBtnTap(sender: UIButton){
        if (sender.isEqual(self.btnClose)){
            if (self.closeCallback != nil){
                self.closeCallback!(true);
            }
        }else if (sender.isEqual(self.btnAspectRatio)){
            if (self.aspectRationCallback != nil){
                self.aspectRationCallback!(true);
            }
        }
    }
}
