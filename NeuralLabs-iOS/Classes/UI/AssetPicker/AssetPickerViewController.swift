//
//  AssetPickerViewController.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 10/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

public class AssetPickerViewController: NeuralBaseViewController {

    let assetView = AssetPickerView();
    public var mediaSelectionCallback: MediaSelectionsCallback? = nil;

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        assetView.frame = self.view.bounds;
        assetView.autoresizingMask = [.flexibleWidth, .flexibleHeight];
        assetView.backgroundColor = .clear;
        self.view.addSubview(assetView);
        
        self.view.backgroundColor = .white;
        
        weak var weakSelf = self;
        assetView.loadView();
        assetView.mediaSelectionCallback = { (itms) -> Void in
            weakSelf?.safeDismiss(animated: true);
            if (weakSelf?.mediaSelectionCallback != nil){
                weakSelf?.mediaSelectionCallback!(itms);
            }
        }
    }

    func setMode(mode: AssetPickerMode) {
        self.assetView.setMode(mode: mode);
    }
    
    public func loadItems(items: [MediaItemsProtocol]){
        self.assetView.loadItems(items: items);
    }
}
