//
//  AssetPickerView.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 10/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

enum AssetPickerMode {
    case Viewer;
    case SingleSelection;
    case MultiSelection;
}

class AssetPickerView: NeuralBaseView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private var collectionView: NeuralBaseCollectionView? = nil;
    private var headerView: AssetPickerHeaderView? = nil;
    private var imageView: NeuralBaseImageView? = nil;
    private var mode: AssetPickerMode = .Viewer;

    var collectionViewReusage:[AnyHashable: Any] = [AnyHashable: Any]();
    private var dataSource: [MediaItemsProtocol] = [];
    private var selectedMediaItems: [MediaItemsProtocol] = [];
    private var showcaseItem: MediaItemsProtocol? = nil;
    

    var mediaSelectionCallback: MediaSelectionsCallback? = nil;
    
    func loadView() {
        self.loadHeaderView();
        self.loadCollectionView();
        self.loadImageView();
        
//        self.loadConstraints();
    }
    
    func loadItems(items: [MediaItemsProtocol]){
        self.dataSource = items;
        self.collectionView?.reloadData();
        self.showImage();
    }
    
    func setMode(mode: AssetPickerMode) {
        self.mode = mode;
        self.collectionView?.reloadData();
    }
    
    private func setUpCollectionView () {
        if (self.collectionView == nil){
            let layout = UICollectionViewFlowLayout();
            layout.scrollDirection = .vertical;
            self.collectionView = NeuralBaseCollectionView(frame: self.bounds, collectionViewLayout: layout);
            self.addSubview(self.collectionView!);
        }
        self.collectionView?.delegate = self;
        self.collectionView?.dataSource = self;
        self.collectionView?.backgroundColor = .clear;
        self.collectionView?.isPrefetchingEnabled = true;
        self.collectionView?.isScrollEnabled = true;
    }
    
    private func loadCollectionView(){
        self.setUpCollectionView();
        self.collectionView?.register(BaseImageCell.classForCoder(), forCellWithReuseIdentifier: BaseImageCell.reuseIdentifier);
        self.collectionView?.reloadData();
    }
    
    private func loadHeaderView () {
        if (self.headerView == nil){
            self.headerView = AssetPickerHeaderView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: nlUtils.shared.titlebarHeight()));
            self.headerView?.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin];
            self.addSubview(self.headerView!);
            
            weak var weakSelf = self;
            self.headerView?.aspectRationCallback = { (done) -> Void in
                if weakSelf?.imageView?.contentMode == .scaleAspectFit{
                    weakSelf?.imageView?.contentMode = .scaleAspectFill;
                }else if weakSelf?.imageView?.contentMode == .scaleAspectFill{
                    weakSelf?.imageView?.contentMode = .scaleAspectFit;
                }
            }
            
            self.headerView?.closeCallback = { (done) -> Void in
                if (weakSelf?.mediaSelectionCallback != nil){
                    weakSelf?.mediaSelectionCallback!(weakSelf?.selectedMediaItems ?? []);
                }
            }
        }
        self.headerView?.loadView();
    }
    
    private func loadImageView () {
        if (self.imageView == nil){
            self.imageView = NeuralBaseImageView(frame: self.bounds);
            self.imageView?.backgroundColor = .clear;
            self.imageView?.clipsToBounds = true;
            self.imageView?.contentMode = .scaleAspectFit;
            self.insertSubview(self.imageView!, belowSubview: self.headerView!);
        }
    }
    
    private func layoutViews() {
        var rect_header = self.headerView?.frame;
        var rect_imageView = self.imageView?.frame;
        var rect_collectionView = self.collectionView?.frame;

        let headerHeight = nlUtils.shared.titlebarHeight();
        let imageViewHeight = self.bounds.height * (nlUtils.shared.isPortrait() ? 0.4 : 0.15);
        let collectionViewHeight = self.bounds.height - headerHeight - imageViewHeight;

        var yOffset: CGFloat = 0;
        
        rect_header?.origin.x = 60;
        rect_header?.origin.y = yOffset;
        rect_header?.size.width = self.bounds.width - 120;
        rect_header?.size.height = headerHeight;

        rect_imageView?.origin.x = 0;
        rect_imageView?.origin.y = 0;
        rect_imageView?.size.width = self.bounds.width;
        rect_imageView?.size.height = imageViewHeight;

        yOffset += rect_imageView!.height;

        rect_collectionView?.origin.x = 0;
        rect_collectionView?.origin.y = yOffset;
        rect_collectionView?.size.width = self.bounds.width;
        rect_collectionView?.size.height = collectionViewHeight;
        
        self.headerView?.frame = rect_header!;
        self.imageView?.frame = rect_imageView!;
        self.collectionView?.frame = rect_collectionView!;
    }
    
    private func loadConstraints() {
        var dict:[String:UIView] = [:]
        dict["headerView"] = self.headerView;
        dict["imageView"] = self.imageView;
        dict["collectionView"] = self.collectionView;

        let headerHeight = nlUtils.shared.titlebarHeight();
        let imageViewHeight = self.bounds.height * 0.4;
        let collectionViewHeight = self.bounds.height - headerHeight - imageViewHeight;
        
        let hVFLHeaderView = "H:|-0-[headerView(==\(self.bounds.width))]-0-|";
        let vVFL = "V:|-0-[headerView(==\(headerHeight)@1000)]-1-[imageView(==\(imageViewHeight)@250)]-1-[collectionView(==\(collectionViewHeight)@1000)]-0-|";

        let hVFLImageView = "H:|-0-[imageView(==\(self.bounds.width))]-0-|";
        let hVFLCollectionView = "H:|-0-[collectionView(==\(self.bounds.width))]-0-|";

        let hConstHeaderView = NSLayoutConstraint.constraints(withVisualFormat: hVFLHeaderView, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: dict);
        let hConstImageView = NSLayoutConstraint.constraints(withVisualFormat: hVFLImageView, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: dict);
        let hConstCollectionView = NSLayoutConstraint.constraints(withVisualFormat: hVFLCollectionView, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: dict);
        
        let vConts = NSLayoutConstraint.constraints(withVisualFormat: vVFL, options: [], metrics: nil, views: dict);

        self.addConstraints(hConstHeaderView)
        self.addConstraints(hConstImageView)
        self.addConstraints(hConstCollectionView)
        self.addConstraints(vConts)
    }
    
    private func showImage() {
        weak var weakSelf = self;
        self.showcaseItem?.requestImage({ (img, info) in
            weakSelf?.imageView?.image = img;
        })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        
        self.layoutViews();
    }
}



//MARK: Collection View DataSource/Delegate Methods
extension (AssetPickerView) {
    //MARK: DataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: BaseImageCell = BaseImageCell.dequeue(collectionView, indexPath: indexPath, reuseIdentifier: BaseImageCell.reuseIdentifier, nibName: BaseImageCell.nibName, cellBundle: BundleManager.shared.frameworkBundle());
        collectionViewReusage[indexPath] = cell;
        cell.baseInit();
        let itm = self.dataSource[indexPath.row];
        
        itm.requestImage { (img, info) in
            cell.image = itm.showcaseImage;
        }
        if self.selectedMediaItems.contains(where: {return $0 === itm}){
            cell.isSelected = true;
        }else{
            cell.isSelected = false;
        }
        return cell
    }
    
    
    //MARK: Delegate Methods
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? BaseImageCell{
            cell.isSelected = true;
        }
        let itm = self.dataSource[indexPath.row];
        self.showcaseItem = itm;

        switch(self.mode){
        case .MultiSelection:
            self.selectedMediaItems.append(itm);
            break;
        case .SingleSelection:
            self.selectedMediaItems.removeAll();
            self.selectedMediaItems.append(itm);
            break;
        default:
            break;
        }
        self.collectionView?.reloadData();
        self.showImage();
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionViewReusage[indexPath] as? BaseImageCell{
            cell.isSelected = false;
        }
        let itm = self.dataSource[indexPath.row];
        self.showcaseItem = itm;

        switch(self.mode){
        case .MultiSelection:
            let itm = self.dataSource[indexPath.row];
            self.selectedMediaItems.removeAll(where: {return ($0 === itm)});
            break;
        case .SingleSelection:
            self.selectedMediaItems.removeAll();
            break;
        default:
            break;
        }
        self.collectionView?.reloadData();
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100);
    }
}
