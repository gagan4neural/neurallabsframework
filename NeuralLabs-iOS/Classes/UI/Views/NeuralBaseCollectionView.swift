//
//  NeuralBaseCollectionView.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

open class NeuralBaseCollectionView: UICollectionView {

    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout);
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
}
