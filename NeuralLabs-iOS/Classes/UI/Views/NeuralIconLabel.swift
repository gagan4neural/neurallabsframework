//
//  NeuralIconLabel.swift
//  NeuralLabs
//
//  Created by Gagan on 18/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

open class NeuralIconLabel: NeuralBaseLabel {

    override func baseInit () {
        self.textColor = ColorUtils.instance().textColor;
        self.font = FontManager.shared.iconFont();
        self.backgroundColor = .clear;
    }

}
