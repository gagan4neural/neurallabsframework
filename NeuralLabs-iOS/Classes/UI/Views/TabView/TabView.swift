//
//  TabView.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 24/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import UIKit

open class TabView: NeuralBaseView, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    private var delegate: TabViewDelegate? = nil;
    private var collectionView: NeuralBaseCollectionView?;
    
    public func loadTabsWithDelegate(delegate: TabViewDelegate){
        self.delegate = delegate;
        self.setUpCollectionView();
        self.refreshData();
    }
    
    public func refreshData() {
        self.collectionView?.reloadData();
    }
    
    func setUpCollectionView () {
        
        if (self.collectionView == nil){
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal;
            
            self.collectionView = NeuralBaseCollectionView(frame: self.bounds, collectionViewLayout: layout);
            
            self.collectionView?.delegate = self;
            self.collectionView?.dataSource = self;
            self.collectionView?.backgroundColor = .clear;
            self.collectionView?.isHidden = false;
            self.collectionView?.isPrefetchingEnabled = true;
            self.collectionView?.isScrollEnabled = true;
            
            self.collectionView?.register(TabViewCell.classForCoder(), forCellWithReuseIdentifier: TabViewCell.reuseIdentifier);
            
            self.addSubview(self.collectionView!);
        }
    }
    
    
    //MARK: Collection View Methods
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var retVal: Int = 0;
        if let del = self.delegate {
            retVal = del.numberOfTabs();
        }
        return retVal;
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell: TabViewCell = TabViewCell.dequeue(collectionView, indexPath: indexPath, reuseIdentifier: TabViewCell.reuseIdentifier, nibName: nil, cellBundle: Bundle.main)
        cell.dressCell();
        
        if let del = self.delegate {
            if let view = del.viewForTab(atIndex: indexPath.row){
                cell.neuralView?.addSubview(view);
                view.frame = cell.neuralView!.bounds;
            }
        }
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let minSize = min(self.frame.width, self.frame.height);
        var retVal = CGSize(width: minSize, height: minSize);
        if let del = self.delegate {
            retVal.width = del.tabWidth(atIndex: indexPath.row);
        }
        return retVal;
    }
    
    public func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            del.tabHighlighted(atIndex: indexPath.row);
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            del.tabUnHighlighted(atIndex: indexPath.row);
        }
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            del.tabSelected(atIndex: indexPath.row);
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let del = self.delegate {
            del.tabUnSelected(atIndex: indexPath.row);
        }
    }
    
    
    
    //MARK: Layout
    open override func layoutSubviews() {
        super.layoutSubviews();
        self.collectionView?.frame = self.bounds;
    }
}
