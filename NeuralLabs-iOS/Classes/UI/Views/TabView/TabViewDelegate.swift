//
//  TabViewDelegate.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 24/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation

public protocol TabViewDelegate {
    func numberOfTabs() -> Int;
    func viewForTab(atIndex index:Int) -> NeuralBaseView?;
    func tabWidth(atIndex index:Int) -> CGFloat;

    func tabSelected(atIndex index:Int);
    func tabUnSelected(atIndex index:Int);
    func tabHighlighted(atIndex index:Int);
    func tabUnHighlighted(atIndex index:Int);
}
