//
//  TabViewCell.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 24/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation

class TabViewCell: BaseCollectionViewCell, BaseCellProtocol {
    
    static let reuseIdentifier = "TabViewCell";
    
    var neuralView: NeuralBaseView? = nil;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func baseInit() {
        
    }
    
    func setTitle(_ title: String) {
        // Do nothing.
    }
    
    func dressCell() {
        if (neuralView == nil){
            neuralView = NeuralBaseView(frame: self.bounds);
            neuralView?.autoresizingMask = [.flexibleWidth, .flexibleHeight];
            neuralView?.backgroundColor = .white;
            self.contentView.addSubview(neuralView!);
        }
    }
}
