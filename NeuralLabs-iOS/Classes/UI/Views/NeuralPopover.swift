//
//  NeuralPopover.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 16/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


open class NeuralPopoverItem {
    public var title: String;
    public var imageName: String?;
    public var titleColor: UIColor = ColorUtils.init().textColor;
    public var indexPath: IndexPath? = IndexPath(item: 0, section: 0);
    
    var popoverHandler: ((PopoverItem) -> Void)? = nil
    public var callback: NeuralPopoverCallback? = nil;
    
    var image: UIImage? {
        get{
            if let i = self.imageName {
                return UIImage(named: i);
            }else{
                return nil;
            }
        }
    }
    
    public init(title: String, imageName: String?, indexPath: IndexPath, callback: NeuralPopoverCallback?){
        self.title = title;
        self.imageName = imageName;
        self.callback = callback;
        self.indexPath = indexPath;
        self.addHandler();
    }
    
    public convenience init(title: String, index: Int, callback: NeuralPopoverCallback?){
        self.init(title: title, imageName: nil, indexPath: IndexPath(item: index, section: 0), callback: callback);
    }
    
    private func addHandler() {
        
        weak var weakSelf = self;
        self.popoverHandler = { (popoverItem) -> Void in
            if (weakSelf?.callback != nil){
                weakSelf?.callback!(weakSelf!);
            }
        }
    }
}

open class NeuralPopoverManager{
    
    static public let shared: NeuralPopoverManager = NeuralPopoverManager();
    
    public func popoverWith(items: [NeuralPopoverItem], view: NeuralBaseView, viewController: NeuralBaseViewController){
        let pItems = self.popoverItemsFrom(neuralitems: items);
        let controller = PopoverController(items: pItems, fromView: view, style: .normal, initialIndex: 0)
        controller.coverColor = UIColor.gray
        controller.textColor = UIColor.white
        viewController.popover(controller);
    }
    
    private func popoverItemsFrom(neuralitems: [NeuralPopoverItem]) -> [PopoverItem]{
        var items: [PopoverItem] = [];
        for nItem in neuralitems {
            let i = PopoverItem(title: nItem.title, titleColor: nItem.titleColor, image: nItem.image, handler: nItem.popoverHandler);
            items.append(i);
        }
        return items
    }
}
