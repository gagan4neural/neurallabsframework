//
//  ProgressView.swift
//  NeuralLabs
//
//  Created by Gagan on 02/11/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

open class ProgressView : NeuralBaseView{
    
    private var activityIndicator:UIActivityIndicatorView? = nil;
    
    override init(frame: CGRect) {
        super.init(frame: frame);
        self.sharedInit();
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.sharedInit();
    }
    
    func sharedInit () {
        self.backgroundColor = .clear;
    }
    
    public func progressWithActivityIndicator (_ startAnimating:Bool) {
        
        if (activityIndicator == nil){
            activityIndicator =  UIActivityIndicatorView(style: .white);
            activityIndicator!.backgroundColor = .clear;
            activityIndicator!.frame = self.bounds;
            activityIndicator!.hidesWhenStopped = true;
            activityIndicator!.translatesAutoresizingMaskIntoConstraints = false;

            self.addSubview(activityIndicator!);
            
            var dict = [String: Any]();
            dict["activityIndicator"] = activityIndicator;
            let hVFL = "H:|-0-[activityIndicator(==\(self.bounds.width)@750)]-0-|";
            let vVFL = "V:|-0-[activityIndicator(==\(self.bounds.height)@750)]-0-|";

            let hConts = NSLayoutConstraint.constraints(withVisualFormat: hVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: dict);
            let vConts = NSLayoutConstraint.constraints(withVisualFormat: vVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: dict);

            self.addConstraints(hConts)
            self.addConstraints(vConts)
        }
        
        if (startAnimating){
            activityIndicator!.startAnimating()
        }else{
            activityIndicator!.stopAnimating()
        }
    }
}
