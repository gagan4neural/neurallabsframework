//
//  NeuralSplitViewController.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 19/03/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import UIKit

public protocol NeuralSplitViewProtocol: class{
    func categorySelection(indexPath: IndexPath);
    func itemSelection(indexPath: IndexPath);
}

@objc
open class NeuralSplitViewController: NeuralBaseViewController {

    private var categoryNVC: NeuralNavigationController?;
    private var detailNVC: NeuralNavigationController?;
    
    private var categoryVC: NeuralBaseViewController?;
    private var detailVC: NeuralBaseViewController?;

    private var categoryNView: UIView?;
    private var detailNView: UIView?;
    
    public var splitPercent: Float = 0.3; // should be in range from 0 to 1
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    public func load(CategoryViewController catVC: NeuralBaseViewController, DetailViewController detVC: NeuralBaseViewController)
    {
        self.categoryVC = catVC;
        self.detailVC = detVC;
        self.loadNavigations();
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
    }

    func loadNavigations()
    {
        guard let cvc = self.categoryVC else{
            let msg = "No category View Controller set";
            Logger.shared.log(msg);
            fatalError(msg);
        }
        if (categoryNVC == nil)
        {
            categoryNVC = NeuralNavigationController(rootViewController: cvc);
            categoryNVC?.isNavigationBarHidden = true;
            categoryNView = categoryNVC?.view;
            self.view.addSubview(categoryNView!);
        }

        if (nlUtils.shared.isIpad())
        {
            // in case of iPhone there is no detail view.
            guard let dvc = self.detailVC else{
                let msg = "No detail View Controller set";
                Logger.shared.log(msg);
                fatalError(msg);
            }
            
            if (detailNVC == nil)
            {
                detailNVC = NeuralNavigationController(rootViewController: dvc);
                detailNVC?.isNavigationBarHidden = true;
                detailNView = detailNVC?.view;
                self.view.addSubview(detailNView!);
            }
        }
    }
    
    public func pushDetail(animated: Bool) {
        if (nlUtils.shared.isIphone()){
            if let nvc = self.categoryNVC {
                self.detailVC?.safePush(nvc, animated: animated);
            }
        }
    }
    
    public func push(viewController: NeuralBaseViewController, animated: Bool){
        if (nlUtils.shared.isIpad()){
            if let nvc = self.detailNVC {
                nvc.pushViewController(viewController, animated: animated);
            }
        }else{
            if let nvc = self.categoryNVC {
                nvc.pushViewController(viewController, animated: animated);
            }
        }
    }

    public func popDetail(animated: Bool) {
        if (nlUtils.shared.isIpad()){
            if let nvc = self.detailNVC {
                nvc.popViewController(animated: animated);
            }
        }else{
            if let nvc = self.categoryNVC {
                nvc.popViewController(animated: animated);
            }
        }
    }

    public func currentViewControllers() -> Int {
        var retVal: Int = -1;
        if (nlUtils.shared.isIphone()){
            retVal = self.categoryNVC?.viewControllers.count ?? -1;
        }
        return retVal;
    }
    
    public func popToDetailRoot(animated: Bool) {
        if (nlUtils.shared.isIpad()){
            if let nvc = self.detailNVC{
                nvc.popToRootViewController(animated: animated);
            }
        }else{
            if let nvc = self.categoryNVC{
                nvc.popToRootViewController(animated: animated);
            }
        }
    }
    
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        
        let sPercent: CGFloat = ((self.splitPercent > 1)||(self.splitPercent < 0)) ? 0.3 : CGFloat(self.splitPercent);
        
        var x: CGFloat = 0.0;
        let y: CGFloat = 0.0;
        var w: CGFloat = self.view.frame.width * (nlUtils.shared.isIpad() ? sPercent : 1.0);
        var h: CGFloat = self.view.frame.height;
        if (NeuralLabManager.canShowBannerAds()){
            h = h - NeuralLabManager.bannerHeight(forView: self.view);
        }
        
        let cFrame: CGRect = CGRect(x: x, y: y, width: w, height: h);
        self.categoryNView?.frame = cFrame;
        
        if (nlUtils.shared.isIpad())
        {
            x = w;
            w = self.view.frame.width - w;
            let dFrame: CGRect = CGRect(x: x, y: y, width: w, height: h);
            self.detailNView?.frame = dFrame;
        }
    }

}
