//
//  NeuralBaseView.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

open class NeuralBaseView: UIView {

    open private(set) var blockNextAnimation: Bool = false;
    private var blockNextAnimation_semaphore = DispatchSemaphore(value: 1)
    
    open func setBlockNextAnimation(value:Bool){
        blockNextAnimation_semaphore.wait()
        self.blockNextAnimation = value;
        defer{blockNextAnimation_semaphore.signal()}
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    var width: CGFloat {
        get{
            return self.frame.width;
        }
    }
    var height: CGFloat {
        get{
            return self.frame.height;
        }
    }

    var halfWidth: CGFloat {
        get{
            return self.frame.width*0.5;
        }
    }
    var halfHeight: CGFloat {
        get{
            return self.frame.height*0.5;
        }
    }
}
