//
//  NeuralBaseImageView.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

open class NeuralBaseImageView: UIImageView {

    public override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    public override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage);
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }

}
