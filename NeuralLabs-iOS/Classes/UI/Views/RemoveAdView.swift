//
//  RemoveAdView.swift
//  NeuralLabs
//
//  Created by Gagan on 22/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import UIKit

fileprivate let TitleFontSize_iPad : CGFloat = 35
fileprivate let TitleFontSize_iPhone : CGFloat = 20
fileprivate let animationDuration : Double = nlUtils.shared.defaultLayoutAnimationDuration;
fileprivate let RemoveAdTitleText = "Tap here to Remove Ads"


class RemoveAdButton : UIButton {
    
    private lazy var lblText : NeuralBaseLabel = NeuralBaseLabel(frame: self.bounds);
    private lazy var lblIcon : NeuralBaseLabel = NeuralBaseLabel(frame: self.bounds);
    private lazy var aiView : UIActivityIndicatorView = UIActivityIndicatorView(style: .white);

    static func instance () -> RemoveAdButton {
        
        let removeAdButton : RemoveAdButton = RemoveAdButton(type: .custom);
        removeAdButton.isHidden = false;
        removeAdButton.backgroundColor = ColorUtils.instance().removeAdsBG
        
        weak var weakSelf = removeAdButton;
        removeAdButton.addTarget(weakSelf, action: #selector(onBtnTap), for: .touchUpInside);
        
        removeAdButton.addSubview(removeAdButton.lblText);
        removeAdButton.addSubview(removeAdButton.lblIcon);
        removeAdButton.addSubview(removeAdButton.aiView);
        
        removeAdButton.lblText.adjustsFontSizeToFitWidth = true;
        removeAdButton.lblIcon.adjustsFontSizeToFitWidth = true;

        removeAdButton.lblText.textColor = .white;
        removeAdButton.lblIcon.textColor = .white;

        removeAdButton.layoutLandscape();
        
        return removeAdButton;
    }
    
    static func titleFontSize () -> CGFloat{
        return ((nlUtils.shared.isIpad()) ? TitleFontSize_iPad : TitleFontSize_iPhone)
    }

    static func iconFontSize () -> CGFloat{
        return ((nlUtils.shared.isIpad()) ? TitleFontSize_iPad : TitleFontSize_iPhone)
    }


    override init(frame: CGRect) {
        super.init(frame: frame);
        self.sharedInit();
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.sharedInit();
    }
    
    private func sharedInit () {
        self.lblIcon.backgroundColor = UIColor.clear;
        self.lblIcon.textAlignment = .center;
        self.lblIcon.textColor = ColorUtils.instance().textColor;
        self.lblIcon.highlightedTextColor = ColorUtils.instance().highlightedColor
        self.lblIcon.font = FontManager.shared.iconFont(size: RemoveAdButton.iconFontSize());
        self.lblIcon.text = IconFontCodes.block;

        self.lblText.text = RemoveAdTitleText
        self.lblText.backgroundColor = UIColor.clear;
        self.lblText.textAlignment = .left;
        self.lblText.textColor = ColorUtils.instance().textColor;
        self.lblText.adjustsFontSizeToFitWidth = true;
        self.lblText.font = FontManager.shared.helveticaNeueFont(size: RemoveAdButton.titleFontSize());

        self.aiView.hidesWhenStopped = true;
        self.aiView.stopAnimating();
        
        self.listenNotifications();
    }
    
    func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationChanged), name: nlDeviceOrientationChanged, object: nil)
    }
    
    func removeNotifications () {
        NotificationCenter.default.removeObserver(self, name: nlDeviceOrientationChanged, object: nil);
    }
    
    deinit {
        self.removeNotifications();
    }
    
    override var isHighlighted: Bool {
        set (newValue) {
            super.isHighlighted = newValue;
            if (newValue == true) {
                self.backgroundColor = ColorUtils.instance().removeAdsBGHighlighted
            }else{
                self.backgroundColor = ColorUtils.instance().removeAdsBG
            }
        }
        get{
            return super.isHighlighted;
        }
    }

    func startProcessing () {
        self.lblText.text = "Processing..."
        self.aiView.startAnimating();
        self.lblIcon.isHidden = true;
    }
    
    func stopProcessing () {
        self.lblText.text = RemoveAdTitleText
        self.aiView.stopAnimating();
        self.lblIcon.isHidden = false;
    }
    
    @objc func deviceOrientationChanged (notification : NSNotification) {
        self.layoutSubviews();
    }
    
    @objc func onBtnTap (sender: RemoveAdButton) {
        NotificationController.noti_PurchaseRemoveAds();
    }
    
    override func layoutSubviews() {
        super.layoutSubviews();
        
        self.layoutPortrait();
//        if (nlUtils.shared.isLandscape()){
//            self.layoutLandscape();
//        }else if (nlUtils.shared.isPortrait()){
//            self.layoutPortrait();
//        }
    }
    
    func layoutPortrait () {
        
        self.lblText.isHidden = false;

        var rectIcon : CGRect = self.lblIcon.frame;
        var rectText : CGRect = self.lblText.frame;

        let wOffset = self.frame.height * 0.1;
        
        rectIcon.size.width = self.frame.height - (wOffset * 2);
        rectIcon.size.height = self.frame.height - (wOffset * 2);
        rectIcon.origin.x = wOffset;
        rectIcon.origin.y = wOffset;
        
        rectText.origin.x = rectIcon.origin.x + rectIcon.width + wOffset;
        rectText.origin.y = wOffset;
        rectText.size.width = self.frame.width - (rectText.origin.x + wOffset);
        rectText.size.height = self.frame.height - (wOffset * 2);

        self.lblIcon.frame = rectIcon;
        self.aiView.frame = rectIcon;
        self.lblText.frame = rectText;
    }

    func layoutLandscape () {
        
        self.lblText.isHidden = true;
        
        var rectIcon : CGRect = self.lblIcon.frame;
        
        let wOffset = self.frame.width * 0.1;
        let hOffset = self.frame.height * 0.1;

        rectIcon.origin.x = wOffset;
        rectIcon.origin.y = hOffset;
        rectIcon.size.width = self.frame.width - (wOffset * 2);
        rectIcon.size.height = self.frame.height - (hOffset * 2);
        
        self.lblIcon.frame = rectIcon;
        self.aiView.frame = rectIcon;
    }
    
}
