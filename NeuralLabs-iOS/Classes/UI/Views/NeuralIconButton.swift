//
//  NeuralIconButton.swift
//  NeuralLabs-iOS
//
//  Created by Gagandeep Madan on 06/09/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

@objc
open class NeuralIconButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame);
        self.initializeButton();
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeButton();
    }
    
    private func initializeButton () {
        self.titleLabel?.font = FontManager.shared.iconFont(size: nlUtils.shared.isIpad() ? 30 : 25);
        
        self.setTitleColor(ColorUtils.instance().defaultColor, for: .normal);
        self.setTitleColor(.black, for: .highlighted);
        
        self.backgroundColor = .clear;
    }
}
