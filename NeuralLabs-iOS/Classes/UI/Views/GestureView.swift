//
//  GestureView.swift
//  NeuralLabs
//
//  Created by Gagan on 04/11/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

fileprivate let minimumMoveYDiff: CGFloat = 5.0;
fileprivate let minimumMoveXDiff: CGFloat = 5.0;

public final class GestureView: NeuralBaseView, UIGestureRecognizerDelegate {

    private var tapGestures: [UITapGestureRecognizer] = [];
    private var longPress: [UILongPressGestureRecognizer] = [];
    private var swipes: [UISwipeGestureRecognizer] = [];
    private var pan: UIPanGestureRecognizer? = nil;
    private var pinch: UIPinchGestureRecognizer? = nil;
    
    private var prevPanTouchPoints: TouchPoints = [];

    public var tapCallback: TapGestureCallback? = nil;
    public var swipeCallback: SwipeGestureCallback? = nil;
    public var longPressCallback: PressGestureCallback? = nil;
    public var pinchCallback: PinchGestureCallback? = nil;
    public var panCallback: PanGestureCallback? = nil;
    public var moveYDiff: CGFloat = minimumMoveYDiff;
    public var moveXDiff: CGFloat = minimumMoveXDiff;

    
    override public init(frame: CGRect) {
        super.init(frame: frame);
        self.baseInit();
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.baseInit();
    }
    
    func baseInit() {
        self.backgroundColor = .clear;
    }
    
    public func enableTap(_ taps:Int, touches:Int){
        var tap = self.containsTapGesture(taps, touches: touches);
        if (tap == nil){
            tap = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(_:)))
            tap?.numberOfTouchesRequired = touches;
            tap?.numberOfTapsRequired = taps;
            
            for gest in self.tapGestures {
                gest.require(toFail: tap!)
            }
            self.tapGestures.append(tap!);
        }
        tap?.isEnabled = true;
        self.addGestureRecognizer(tap!);
    }
    
    public func enableLongPress(_ taps:Int, touches:Int, duration:TimeInterval){
        var longPress = self.containsLongPressGesture(taps, touches: touches, duration: duration);
        if (longPress == nil){
            longPress = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(_:)))
            longPress?.numberOfTouchesRequired = touches;
            longPress?.numberOfTapsRequired = taps;
            longPress?.minimumPressDuration = duration
            
            for gest in self.tapGestures {
                gest.require(toFail: longPress!)
            }

            self.longPress.append(longPress!);
        }
        longPress?.isEnabled = true;
        self.addGestureRecognizer(longPress!);
    }

    public func enableSwipe(_ touches:Int, direction:UISwipeGestureRecognizer.Direction){
        var swipe = self.containsSwipeGesture(touches, direction: direction);
        if (swipe == nil){
            swipe = UISwipeGestureRecognizer(target: self, action: #selector(onSwipe(_:)))
            swipe?.numberOfTouchesRequired = touches;
            swipe?.direction = direction
            
            for gest in self.tapGestures {
                gest.require(toFail: swipe!)
            }
            self.pan?.require(toFail: swipe!);
            self.swipes.append(swipe!);
        }
        swipe?.isEnabled = true;
        self.addGestureRecognizer(swipe!);
    }
    
    public func enablePinch(){
        if (self.pinch == nil){
            self.pinch = UIPinchGestureRecognizer(target: self, action: #selector(onPinch(_:)))
        }
        self.pinch!.isEnabled = true;
        self.addGestureRecognizer(self.pinch!);
    }
    
    public func enablePan() {
        if (self.pan == nil){
            self.pan = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        }
        self.pan!.isEnabled = true;
        self.addGestureRecognizer(self.pan!);
    }
    
    public func isTouchOnLeftHalf(_ point: TouchPoint) -> Bool {
        let retVal = (point.x > 0.0) && (point.x < self.halfWidth)
        return retVal;
    }

    public func isTouchOnRightHalf(_ point: TouchPoint) -> Bool {
        let retVal = (point.x > self.halfWidth) && (point.x < self.width);
        return retVal;
    }

    public func isTouchOnTopHalf(_ point: TouchPoint) -> Bool {
        let retVal = (point.y > 0.0) && (point.y < self.halfHeight);
        return retVal;
    }
    
    public func isTouchOnBottomHalf(_ point: TouchPoint) -> Bool {
        let retVal = (point.y > self.halfHeight) && (point.y < self.height);
        return retVal;
    }

    public func isCrossHorizontalMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = self.isTouchOnLeftHalf(prevPt) && self.isTouchOnRightHalf(newPt);
        return retVal;
    }

    public func isCrossVerticalMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = self.isTouchOnTopHalf(prevPt) && self.isTouchOnBottomHalf(newPt);
        return retVal;
    }
    
    public func isCrossHalfMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = self.isCrossHorizontalMovement(prevPt, newPt: newPt) || self.isCrossVerticalMovement(prevPt, newPt: newPt);
        return retVal;
    }
    
    public func isSufficientHorizontalMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = (abs(prevPt.x - newPt.x) > self.moveXDiff) && (self.isCrossHorizontalMovement(prevPt, newPt: newPt) == false);
        return retVal;
    }

    public func isSufficientVerticalMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = (abs(prevPt.y - newPt.y) > self.moveYDiff) && (self.isCrossVerticalMovement(prevPt, newPt: newPt) == false);
        return retVal;
    }
    
    public func isSufficientMovement(_ prevPt:TouchPoint, newPt:TouchPoint) -> Bool{
        let retVal = self.isSufficientHorizontalMovement(prevPt, newPt: newPt) || self.isSufficientVerticalMovement(prevPt, newPt: newPt);
        return retVal;
    }
    
    public func isValidPanVerticalMovement(_ point: TouchPoint) -> Bool{
        let retVal = self.isSufficientVerticalMovement(self.prevPanTouchPoints.last ?? point, newPt: point);
        return retVal;
    }

    public func isValidPanHorizontalMovement(_ point: TouchPoint) -> Bool{
        let retVal = self.isSufficientHorizontalMovement(self.prevPanTouchPoints.last ?? point, newPt: point);
        return retVal;
    }

    public func isValidPanMovement(_ point:TouchPoint) -> Bool{
        let retVal = self.isValidPanVerticalMovement(point) && self.isValidPanHorizontalMovement(point);
        return retVal;
    }
    
    public func endingDeviation(_ touchPoints:TouchPoints) -> TouchPoint{
        if (touchPoints.count < 2){
            return TouchPoint(x: 0, y: 0);
        }
        return self.deviationForPoints(touchPoints, startIdx: (touchPoints.count-1), endIdx: (touchPoints.count-2) );
    }
    
    public func startingDeviation(_ touchPoints:TouchPoints) -> TouchPoint{
        if (touchPoints.count < 2){
            return TouchPoint(x: 0, y: 0);
        }
        return self.deviationForPoints(touchPoints, startIdx: 0, endIdx: 1);
    }
    
    public func deviationForPoints(_ touchPoints:TouchPoints, startIdx:Int, endIdx:Int) -> TouchPoint {
        let pt1 = touchPoints[startIdx];
        let pt2 = touchPoints[endIdx];
        return TouchPoint(x: (pt2.x - pt1.x), y: (pt2.y - pt1.y));
    }
    
    private func containsTapGesture(_ taps:Int, touches:Int) -> UITapGestureRecognizer? {
        let gestures = self.tapGestures.filter({return (($0.numberOfTapsRequired == taps) && ($0.numberOfTouchesRequired == touches))});
        if (gestures.count > 0){
            return gestures.first
        }
        return nil;
    }

    private func containsLongPressGesture(_ taps:Int, touches:Int, duration:TimeInterval) -> UILongPressGestureRecognizer? {
        let gestures = self.longPress.filter({return (($0.numberOfTapsRequired == taps) && ($0.numberOfTouchesRequired == touches))});
        if (gestures.count > 0){
            return gestures.first
        }
        return nil;
    }

    private func containsSwipeGesture(_ touches:Int, direction: UISwipeGestureRecognizer.Direction) -> UISwipeGestureRecognizer? {
        let gestures = self.swipes.filter({return (($0.numberOfTouchesRequired == touches) && ($0.direction == direction))});
        if (gestures.count > 0){
            return gestures.first
        }
        return nil;
    }

    @objc func onTapGesture(_ sender:UITapGestureRecognizer){
        let gestures = self.tapGestures.filter { (gest) -> Bool in
            return sender == gest;
        }
        if ((gestures.count > 0) && (self.tapCallback != nil)){
            let gesture:UITapGestureRecognizer = gestures.first!;
            var locations:TouchPoints = [];
            locations.append(gesture.location(in: self));
            self.tapCallback!(UInt(gesture.numberOfTapsRequired), UInt(gesture.numberOfTouchesRequired), locations)
        }
    }

    @objc func onLongPress(_ sender:UILongPressGestureRecognizer){
        let gestures = self.longPress.filter({return $0 == sender});
        if ((gestures.count > 0) && (self.tapCallback != nil)){
            let gesture:UILongPressGestureRecognizer = gestures.first!;
            var locations:TouchPoints = [];
            locations.append(gesture.location(in: self));
            self.longPressCallback!(UInt(gesture.numberOfTapsRequired), UInt(gesture.numberOfTouchesRequired), gesture.minimumPressDuration, locations)
        }
    }
    
    @objc func onSwipe(_ sender:UISwipeGestureRecognizer){
        let gestures = self.swipes.filter({return $0 == sender});
        if ((gestures.count > 0) && (self.tapCallback != nil)){
            let gesture:UISwipeGestureRecognizer = gestures.first!;
            var locations:TouchPoints = [];
            locations.append(gesture.location(in: self));
            self.swipeCallback!(UInt(gesture.numberOfTouchesRequired), gesture.direction, locations)
        }
    }

    @objc func onPan(_ sender:UIPanGestureRecognizer){
        
        let location:TouchPoint = sender.location(in: self);
        var canAppendLocation = self.isSufficientMovement(self.prevPanTouchPoints.last ?? location, newPt: location);
        
        switch sender.state{
        case .began:
            self.prevPanTouchPoints.append(location)
            break;
        case .cancelled:
            canAppendLocation = false;
            break;
        case .changed:
            break;
        case .ended:
            canAppendLocation = false;
            self.prevPanTouchPoints.removeAll();
            break;
        case .failed:
            canAppendLocation = false;
            self.prevPanTouchPoints.removeAll();
            break;
        case .possible:
            break;
        }
        
        if (sender == self.pan){
            
            if (canAppendLocation){
                self.prevPanTouchPoints.append(location)
                if (self.panCallback != nil){
                    self.panCallback!(sender.velocity(in: self), self.prevPanTouchPoints)
                }else{
                    Logger.shared.log("Cannot send pan callback gesture due to nil callback")
                }
            }
        }
    }

    @objc func onPinch(_ sender:UIPinchGestureRecognizer){
        if (sender == self.pinch){
            var locations:TouchPoints = [];
            locations.append(sender.location(in: self));
            if (self.pinchCallback != nil){
                self.pinchCallback!(sender.scale, sender.velocity, locations)
            }else{
                Logger.shared.log("Cannot send pan callback gesture due to nil callback")
            }
        }
    }
}
