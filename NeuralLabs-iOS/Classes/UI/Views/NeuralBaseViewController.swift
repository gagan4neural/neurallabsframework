//
//  NeuralBaseViewController.swift
//  NeuralLabs
//
//  Created by Gagan on 23/09/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

@objc open class NeuralBaseViewController: UIViewController {

    open var viewAppeared: onViewAppearedBlock? = nil;
    open var allowBannerAds: Bool = true;

    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.connectServices();
        self.connectDeviceServices();
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        if (self.layoutAllowsBannerAds()){
            self.showBannerAds();
        }
        if (self.viewAppeared != nil){
            self.viewAppeared!("View Did Appeared");
        }
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        self.disconnectServices();
        self.disconnectDeviceServices();

        if (self.layoutAllowsBannerAds()){
            AdManager.shared.removeBannerAd(view: self.view);
        }
    }
    
    
    
    //MARK: Navigation Methods
    
    public func safePush(animated: Bool){
        if let nav = nlUtils.shared.applicationNavigationController(){
            self.safePush(nav, animated: animated);
        }
    }

    public func safePush(_ nav: UINavigationController, animated: Bool){
        let vcs = nav.viewControllers;
        let fvcs = vcs.filter({return $0 === self});
        if (fvcs.count > 0){
            Logger.shared.log("A similar view controller is already on navigation stack");
            return
        }
        nav.pushViewController(self, animated: animated)
    }
    
    public func safePop(animated: Bool){
        if let nav = nlUtils.shared.applicationNavigationController(){
            nav.popViewController(animated: animated);
        }
    }
    
    public func safePresent(animated: Bool, completion: AnimationCompletion? = nil){
        if let nav = nlUtils.shared.applicationNavigationController(){
            self.safePresent(nav, animated: animated);
        }
    }
    
    public func safePresent(_ nav: UINavigationController, animated: Bool, completion: AnimationCompletion? = nil){
        let vc = nav.presentedViewController
        if (vc === self){
            Logger.shared.log("A similar view controller is already on navigation stack");
            return
        }
        nav.present(self, animated: animated, completion: completion);
    }
    
    public func safeDismiss(animated: Bool, completion: AnimationCompletion? = nil){
        if let nav = nlUtils.shared.applicationNavigationController(){
            nav.dismiss(animated: animated, completion: completion)
        }
    }
    
    public func hideNavigationBar (){
        self.navigationController?.setNavigationBarHidden(true, animated: false);
    }
    

    //MARK: Toast Methods
    public func showToast (_ msg:String){
        nlUtils.shared.showToast(msg, on: self.view, timeDuration: 2);
    }
    
    public func showHud (_ msg:String){
        nlUtils.shared.showHud(msg, on: self.view);
    }
    
    public func hideHud () {
        nlUtils.shared.hideHud(from: self.view);
    }

    //MARK: AdService Methods
    public func showBannerAds () {
        AdManager.shared.showBannerAd(view: self.view);
    }
    
    public func showInterstetialAds () {
        AdManager.shared.showInterstetialAd()
    }

    public func showVideoAds () {
        AdManager.shared.showVideoAd()
    }

    open func layoutAllowsBannerAds () -> Bool {
        return allowBannerAds;
    }
    
    open func injectAdCellsDataSource(collections: inout [MediaCollectionProtocol]) {
        // method returns the indexes where the adcell needs to be injected. If not empty array will be returned.
        if NeuralLabManager.canShowBannerAds() == false {
            return;
        }
        let c1 = AdCollection();
        collections.insert(c1, at: 1);
    }

    open func injectAdCellsDataSource(items: inout [MediaItemsProtocol]) {
        // method returns the indexes where the adcell needs to be injected. If not empty array will be returned.
        if NeuralLabManager.canShowBannerAds() == false {
            return;
        }
        let c1 = AdObject();
        items.insert(c1, at: 1);
    }

    
    
    open func presentViewController(_ vc:UIViewController){
        nlUtils.shared.presentOrDismissModalController(vc, on: nlUtils.shared.applicationNavigationController())
    }
    
    //MARK: InApp Purchase Methods
    open func restoreInAppsProducts() {
        
        self.showHud("Restoring InApps")
        weak var weakSelf = self;
        
        NeuralLabManager.restoreInApps { (success, error) in
            nlUtils.shared.hideHud(from: self.view);
            DispatchQueue.main.async(execute: {
                if (success.count > 0){
                    weakSelf!.showToast("Successfully restored inapps");
                    if (success.filter({ (id) -> Bool in
                        return (id == NeuralLabManager.removeAdId);
                    }).count == 1){
                        AdManager.shared.showBannerAd(view: weakSelf!.view);
                        NotificationController.noti_RemoveAdsPurchased()
                    }
                }else{
                    weakSelf!.showToast("Failed to restore inapps");
                }
            })
        };
    }

    private func connectServices () {
        weak var weakSelf = self;
        AdManager.shared.interstetialAdWillPresent = {() in
            weakSelf?.interstetialAdsWillPresent();
        }
        AdManager.shared.interstetialAdDidPresent = {() in
            weakSelf?.interstetialAdsDidPresent();
        }
        AdManager.shared.interstetialAdDismiss = {() in
            weakSelf?.interstetialAdsDismiss();
        }
        AdManager.shared.interstetialAdFailed = {() in
            weakSelf?.interstetialAdsFailed();
        }
        AdManager.shared.bannerAdsAvaliable = {() in
            weakSelf?.bannerAdsAvaliable();
        }
        AdManager.shared.bannerAdsFailed = {() in
            weakSelf?.bannerAdsFailed();
        }
        AdManager.shared.removeAdsPurchased = {(isSuccess) in
            weakSelf?.removeAdsPurchase(success: isSuccess);
        }
        NeuralLabManager.shared.applicationInitialized = { (isSuccess) in
            weakSelf?.firebaseApplicationInitialized(isSuccess);
        }
        NeuralLabManager.shared.vendorInitialized = { (isSuccess) in
            weakSelf?.firebaseVendorInitialized(isSuccess);
        }
    }
    
    open func connectAssetServices(){
        NotificationCenter.default.addObserver(self, selector: #selector(onVideoCollectionsInitialzied(_ :)), name: nlVideoCollectionsInitialized, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onImageCollectionsInitialzied(_ :)), name: nlImageCollectionsInitialized, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onAudioCollectionsInitialzied(_ :)), name: nlAudioCollectionsInitialized, object: nil)
    }
    
    open func disconnectAssetServices(){
        NotificationCenter.default.removeObserver(self, name: nlVideoCollectionsInitialized, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlImageCollectionsInitialized, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlAudioCollectionsInitialized, object: nil);
    }

    open func connectDeviceServices(){
        NotificationCenter.default.addObserver(self, selector: #selector(onDeviceOrientationChange(_ :)), name: nlDeviceOrientationChanged, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onBatteryLevelDidChange(_ :)), name: nlBatteryLevelDidChange, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onBatteryStateDidChange(_ :)), name: nlBatteryStateDidChange, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onProximityStateChange(_ :)), name: nlProximityStateDidChange, object: nil);
    }
    
    open func connectSystemServices(){
        NotificationCenter.default.addObserver(self, selector: #selector(onSystemVolumeChanged(_ :)), name: nlSystemVolumeChanged, object: nil);
    }
    
    open func connectKeyboardServices() {
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardWillShow(_ :)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardWillHide(_ :)), name: UIResponder.keyboardWillHideNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDidShow(_ :)), name: UIResponder.keyboardDidShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDidHide(_ :)), name: UIResponder.keyboardDidHideNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardWillFrameChange(_ :)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDidFrameChange(_ :)), name: UIResponder.keyboardDidChangeFrameNotification, object: nil);
    }

    open func disconnectSystemServices(){
        NotificationCenter.default.removeObserver(self, name: nlSystemVolumeChanged, object: nil);
    }

    open func disconnectDeviceServices(){
        NotificationCenter.default.removeObserver(self, name: nlDeviceOrientationChanged, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlBatteryLevelDidChange, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlBatteryStateDidChange, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlProximityStateDidChange, object: nil);
    }

    open func disconnectKeyboardServices(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil);
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidChangeFrameNotification, object: nil);
    }

    private func disconnectServices() {
        AdManager.shared.interstetialAdWillPresent = nil
        AdManager.shared.interstetialAdDidPresent = nil
        AdManager.shared.interstetialAdDismiss = nil
        AdManager.shared.interstetialAdFailed = nil
        AdManager.shared.bannerAdsAvaliable = nil
        AdManager.shared.bannerAdsFailed = nil
        AdManager.shared.removeAdsPurchased = nil;
        NeuralLabManager.shared.applicationInitialized = nil
        NeuralLabManager.shared.vendorInitialized = nil
    }
    
    //MARK: Ads
    open func interstetialAdsWillPresent () {
        Logger.shared.log("Ads :", #function);
    }
    open func interstetialAdsDidPresent () {
        Logger.shared.log("Ads :", #function);
    }
    open func interstetialAdsDismiss () {
        Logger.shared.log("Ads :", #function);
    }
    open func interstetialAdsFailed () {
        Logger.shared.log("Ads :", #function);
    }
    open func bannerAdsAvaliable () {
        Logger.shared.log("Ads :", #function);
    }
    open func bannerAdsFailed () {
        Logger.shared.log("Ads :", #function);
    }
    open func removeAdsPurchase (success:Bool){
        Logger.shared.log(#function, "Remove ads purchase status : ", String(success));
    }
    
    //Firebase
    open func firebaseApplicationInitialized(_ success:Bool){
        Logger.shared.log("Firebase :", #function, "Firebase application DB initialized");
    }
    open func firebaseVendorInitialized(_ success:Bool){
        Logger.shared.log("Firebase :", #function, "Firebase vendor DB initialized");
    }
        
    
    //Assets
    open func videoCollections (_ collections: [MediaCollectionProtocol], err: String?){
        // Child class use this.
//        for coll in collections{
//            coll.description();
//        }
    }
    open func imageCollections (_ collections: [MediaCollectionProtocol], err: String?){
        // Child class use this.
//        for coll in collections{
//            coll.description();
//        }
    }
    open func audioCollections (_ collections: [MediaCollectionProtocol], err: String?){
        // Child class use this.
//        for coll in collections{
//            coll.description();
//        }
    }

    @objc private func onVideoCollectionsInitialzied(_ notification:Notification){
        weak var weakSelf = self;
        DispatchQueue.main.async {
            NeuralLabManager.getVideoCollection { (collections, info, err) in
                if (err != nil){
                    Logger.shared.log(#function, err ?? "ERR");
                }else if (info != nil){
                    Logger.shared.log(#function, info ?? "ERR");
                }else{
                    weakSelf?.videoCollections(collections ?? [], err: err)
                }
            }
        }
    }
    @objc private func onImageCollectionsInitialzied(_ notification:Notification){
        weak var weakSelf = self;
        DispatchQueue.main.async {
            NeuralLabManager.getImageCollection { (collections, info, err) in
                if (err != nil){
                    Logger.shared.log(#function, err ?? "ERR");
                }else if (info != nil){
                    Logger.shared.log(#function, info ?? "ERR");
                }else{
                    weakSelf?.imageCollections(collections ?? [], err: err)
                }
            }
        }
    }
    @objc private func onAudioCollectionsInitialzied(_ notification:Notification){
        weak var weakSelf = self;
        DispatchQueue.main.async {
            NeuralLabManager.getAudioCollection { (collections, info, err) in
                if (err != nil){
                    Logger.shared.log(#function, err ?? "ERR");
                }else if (info != nil){
                    Logger.shared.log(#function, info ?? "ERR");
                }else{
                    weakSelf?.audioCollections(collections ?? [], err: err)
                }
            }
        }
    }
    
    
    //MARK: UIDevice Services
    @objc private func onDeviceOrientationChange(_ notification:Notification){
        self.deviceOrientationChange(UIDevice.current.orientation);
    }
    open func deviceOrientationChange (_ orientation: UIDeviceOrientation) {
        // Child class override this.
    }
    @objc private func onBatteryLevelDidChange(_ notification:Notification){
        self.batteryLevelChange(UIDevice.current.batteryLevel);
    }
    open func batteryLevelChange (_ level: Float) {
        // Child class override this.
    }
    @objc private func onBatteryStateDidChange(_ notification:Notification){
        self.batteryStateChange(UIDevice.current.batteryState);
    }
    open func batteryStateChange (_ state: UIDevice.BatteryState) {
        // Child class override this.
    }
    @objc private func onProximityStateChange(_ notification:Notification){
        self.proximityStateChange(UIDevice.current.proximityState);
    }
    open func proximityStateChange (_ state: Bool) {
        // Child class override this.
    }
    
    
    //MARK: Keyboard Notifications
    
    @objc open func onKeyboardWillShow(_ sender: Notification) {
        //child class override this
    }

    @objc open func onKeyboardWillHide(_ sender: Notification) {
        //child class override this
    }

    @objc open func onKeyboardDidShow(_ sender: Notification) {
        //child class override this
    }

    @objc open func onKeyboardDidHide(_ sender: Notification) {
        //child class override this
    }

    @objc open func onKeyboardWillFrameChange(_ sender: Notification) {
        //child class override this
    }

    @objc open func onKeyboardDidFrameChange(_ sender: Notification) {
        //child class override this
    }

    
    //MARK: System Services
    open func systemVolumeChanged(_ newVolume: Float){
        // child class use this.
    }
    @objc private func onSystemVolumeChanged(_ sender: Notification){
        self.systemVolumeChanged(AVIManager.shared.systemVolume);
    }
}
