//
//  NeuralBaseLabel.swift
//  NeuralLabs
//
//  Created by Gagan on 12/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit

open class NeuralBaseLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame);
        self.baseInit();
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        self.baseInit();
    }
    
    func baseInit () {
        self.textColor = ColorUtils.instance().textColor;
        self.font = FontManager.shared.helveticaNeue(nlUtils.shared.defaultFontSize())
        self.backgroundColor = .white;
        self.textAlignment = .center;
    }

}
