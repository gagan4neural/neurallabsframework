//
//  FeedbackMailViewController.swift
//  AVPlayer
//
//  Created by Gagan on 19/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit
import MessageUI


open class FeedbackMailViewController: MFMailComposeViewController, MFMailComposeViewControllerDelegate {
    
    public static func mailComposer(withTarget target: Any?) -> FeedbackMailViewController? {
        if !MFMailComposeViewController.canSendMail() {
            return nil
        }
        let retVal = FeedbackMailViewController()
        if target == nil {
            retVal.mailComposeDelegate = retVal
        } else {
            retVal.mailComposeDelegate = target as? MFMailComposeViewControllerDelegate
        }
        return retVal
    }
    
    // MARK: - Property methods
    open func setSubject(_ subject: String?, body: String?, to: String?) {
        self.setMessageBody(body ?? "Message Content", isHTML: false);
        self.setSubject(subject ?? "Subject");
        self.setToRecipients([to!]);
    }
    
    // MARK: - delegate methods
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        nlUtils.shared.dismissAllPopOvers(nil)
    }
}
