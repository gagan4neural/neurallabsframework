//
//  BaseCellTableViewCell.swift
//  AVPlayer
//
//  Created by Gagan on 05/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit

open class BaseTableViewCell: UITableViewCell {

    public static func dequeue<T:BaseCellProtocol>(_ tableView: UITableView,
                           indexPath: IndexPath,
                           nibName:String?) -> T{
        return BaseTableViewCell.dequeue(tableView, indexPath: indexPath, reuseIdentifier: nil, nibName:nibName , cellBundle: nil);
    }
    
    public static func dequeue<T:BaseCellProtocol>(_ tableView: UITableView,
                           indexPath: IndexPath,
                           reuseIdentifier:String?,
                           nibName:String?,
                           cellBundle:Bundle?) -> T
    {
        
        var cell: T? = nil
        let identifier:String = reuseIdentifier ?? baseTableCellReuseIdentifier;
        let bundle:Bundle = cellBundle ?? Bundle.main;
        
        cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T
        
        if (cell == nil){
            let nibViews = bundle.loadNibNamed(nibName!, owner: self, options: nil)
            if let nvs:[UIView] = nibViews as? [UIView]{
                for view:UIView in nvs{
                    if (view is T) {
                        cell = view as? T
                        break;
                    }
                }
            }
        }
        
        return cell!
    }

    public static func dequeueBaseCell (_ tableView: UITableView,
                                                   indexPath: IndexPath,
                                                   reuseIdentifier:String?) -> BaseTableViewCell
    {
        
        var cell: BaseTableViewCell? = nil
        let identifier:String = reuseIdentifier ?? baseTableCellReuseIdentifier;
        
        cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? BaseTableViewCell
        
        if (cell == nil){
            cell = BaseTableViewCell(style: .default, reuseIdentifier: identifier);
        }
        
        if (cell != nil){
            cell?.indexPath = indexPath;
        }
        return cell!
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    public var indexPath: IndexPath? = nil;

}
