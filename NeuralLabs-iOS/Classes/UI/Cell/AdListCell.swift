//
//  AdListCell.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 07/04/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

public class AdListCell: BaseTableViewCell, BaseCellProtocol {

    static public let reuseIdentifier = "AdListCell";
    static public let nibName = "AdListCell";
    var adView: NeuralBaseView? = nil;
    

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    public func baseInit() {
        self.adView = NeuralBaseView(frame: self.bounds);
        self.adView?.backgroundColor = .clear;
        self.addSubview(self.adView!);
        
        var adViewDict:[String:UIView] = [:]
        adViewDict["adView"] = self.adView;
        
        let adViewHVFL = "H:|-0-[adView(==\(self.bounds.width))]-0-|";
        let adViewVVFL = "V:|-0-[adView(==\(self.bounds.height))]-0-|";
        
        let adViewHConts = NSLayoutConstraint.constraints(withVisualFormat: adViewHVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: adViewDict);
        let adViewVConts = NSLayoutConstraint.constraints(withVisualFormat: adViewVVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: adViewDict);
        
        self.addConstraints(adViewHConts)
        self.addConstraints(adViewVConts)
        
        let _ = NeuralLabManager.showBannerAd(withId: self.adView!.pointerIdentifier(), parentView: self.adView!, withRemoveAdsStrip: false);
    }
    
    public func setTitle(_ title: String) {
        
    }

}
