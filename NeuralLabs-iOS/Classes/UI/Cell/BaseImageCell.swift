//
//  CollectionImageCell.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 08/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import UIKit

class BaseImageCell: BaseCollectionViewCell, BaseCellProtocol {
    
    static let reuseIdentifier = "BaseImageCell";
    static let nibName = "BaseImageCell";
    var imageView: UIImageView? = nil;
    var dimmer: NeuralBaseView? = nil;
    var lblCheck: NeuralBaseLabel? = nil;
    var image: UIImage? {
        get{
            return self.imageView?.image;
        }set{
            self.imageView?.image = newValue;
        }
    }
    
    
    func baseInit() {
        self.imageView = UIImageView(frame: self.contentView.bounds);
        self.imageView?.backgroundColor = .red;
        self.imageView?.contentMode = .scaleAspectFit;
        self.addSubview(self.imageView!);

        self.dimmer = NeuralBaseView(frame: self.contentView.bounds);
        self.dimmer?.backgroundColor = UIColor.black.withAlphaComponent(0.75);
        self.dimmer?.isHidden = true;
        self.addSubview(self.dimmer!);

        self.lblCheck = NeuralBaseLabel(frame: self.contentView.bounds);
        self.lblCheck?.backgroundColor = .clear;
        self.lblCheck?.textAlignment = .center;
        self.lblCheck?.font = FontManager.shared.iconFont(size: 25);
        self.lblCheck?.textColor = .green;
        self.dimmer!.addSubview(self.lblCheck!);
        
        
        var imgvDict:[String:UIView] = [:]
        imgvDict["imageView"] = self.imageView;

        var dimmerDict:[String:UIView] = [:]
        dimmerDict["dimmer"] = self.dimmer;

        var lblDict:[String:UIView] = [:]
        lblDict["lblCheck"] = self.lblCheck;

        let imageHVFL = "H:|-0-[imageView(==\(self.bounds.width))]-0-|";
        let imageVVFL = "V:|-0-[imageView(==\(self.bounds.height))]-0-|";

        let dimmerHVFL = "H:|-0-[dimmer(==\(self.bounds.width))]-0-|";
        let dimmerVVFL = "V:|-0-[dimmer(==\(self.bounds.height))]-0-|";

        let lblCheckHVFL = "H:|-0-[lblCheck(==\(self.bounds.width))]-0-|";
        let lblCheckVVFL = "V:|-0-[lblCheck(==\(self.bounds.height))]-0-|";

        let imgvHConts = NSLayoutConstraint.constraints(withVisualFormat: imageHVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: imgvDict);
        let imgvVConts = NSLayoutConstraint.constraints(withVisualFormat: imageVVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: imgvDict);

        let dimmerHConts = NSLayoutConstraint.constraints(withVisualFormat: dimmerHVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: dimmerDict);
        let dimmerVConts = NSLayoutConstraint.constraints(withVisualFormat: dimmerVVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: dimmerDict);

        let lblHConts = NSLayoutConstraint.constraints(withVisualFormat: lblCheckHVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterX, metrics: nil, views: lblDict);
        let lblVConts = NSLayoutConstraint.constraints(withVisualFormat: lblCheckVVFL, options: NSLayoutConstraint.FormatOptions.alignAllCenterY, metrics: nil, views: lblDict);

        self.addConstraints(imgvHConts)
        self.addConstraints(imgvVConts)
        self.addConstraints(dimmerHConts)
        self.addConstraints(dimmerVConts)
        self.addConstraints(lblHConts)
        self.addConstraints(lblVConts)

    }
    
    func selectCell(){
        self.dimmer?.isHidden = false;
    }

    func deSelectCell(){
        self.dimmer?.isHidden = true;
    }

    override var isSelected: Bool{
        get{
            return super.isSelected;
        }
        set{
            super.isSelected = newValue;
            if (newValue == true){
                
            }else{
                
            }
        }
    }
    
    func setTitle(_ title: String) {
        
    }
}
