//
//  CellProtocol.swift
//  AVPlayer
//
//  Created by Gagan on 07/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import Foundation

let baseTableCellReuseIdentifier = "baseTableCellReuseIdentifier"
let baseCollectionCellReuseIdentifier = "baseCollectionCellReuseIdentifier"

public protocol BaseCellProtocol {
    func baseInit() -> Void;
    func setTitle(_ title:String) -> Void;
}
