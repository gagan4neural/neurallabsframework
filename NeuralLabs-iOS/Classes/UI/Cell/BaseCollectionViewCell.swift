//
//  BaseCollectionViewCell.swift
//  AVPlayer
//
//  Created by Gagan on 07/10/18.
//  Copyright © 2018 Gagan. All rights reserved.
//

import UIKit

open class BaseCollectionViewCell: UICollectionViewCell {
    
    public static func dequeue<T:BaseCellProtocol>(_ collectionView: UICollectionView,
                                                   indexPath: IndexPath,
                                                   nibName:String?) -> T{
        return BaseCollectionViewCell.dequeue(collectionView, indexPath: indexPath, reuseIdentifier: nil, nibName:nibName , cellBundle: nil);
    }
    
    public static func dequeue<T:BaseCellProtocol>(_ collectionView: UICollectionView,
                                                   indexPath: IndexPath,
                                                   reuseIdentifier:String?,
                                                   nibName:String?,
                                                   cellBundle:Bundle?) -> T
    {
        
        var cell: T? = nil
        let identifier:String = reuseIdentifier ?? baseCollectionCellReuseIdentifier;
        let bundle:Bundle = cellBundle ?? Bundle.main;
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? T
        
        if (cell == nil){
            let nibViews = bundle.loadNibNamed(nibName!, owner: self, options: nil)
            if let nvs:[UIView] = nibViews as? [UIView]{
                for view:UIView in nvs{
                    if (view is T) {
                        cell = view as? T
                        break;
                    }
                }
            }
        }
        
        return cell!
    }
    
    public static func dequeueBaseCell (_ collectionView: UICollectionView,
                                        indexPath: IndexPath,
                                        reuseIdentifier:String?) -> BaseCollectionViewCell
    {
        
        var cell: BaseCollectionViewCell? = nil
        let identifier:String = reuseIdentifier ?? baseCollectionCellReuseIdentifier;
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? BaseCollectionViewCell
        
        if (cell != nil){
            cell = BaseCollectionViewCell(frame: CGRect(x: 0, y: 0, width: 100, height: 100));
        }
        
        if (cell == nil){
            cell?.indexPath = indexPath;
        }
        return cell!
    }

    public var indexPath: IndexPath? = nil;
}
