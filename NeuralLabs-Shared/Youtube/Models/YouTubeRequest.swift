//
//  YouTubeSearcher.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 02/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


fileprivate let youTubeVideosPageSize: Int = 50;
fileprivate let maxYouTubeResults: Int = 200;


class YouTubeRequest: Equatable {
    
    private let baseSearch = "search?"
    private let baseVideos = "videos?"
    private let basePlaylistItems = "playlistItems?"

    // user dependent stuff. User has selected some params for search criterial
    var searchTitle: String? = nil;
    var linguisticParams: [String] = [];
    var countryCode: YtCountryCode? = nil;
    var categoryItem: YtCategoryItem? = nil;
    var playlistId: String? = nil;

    // system dependent search
    var part = "&part=snippet"
    var contentDetails = "&part=contentDetails"
    var maxResults: String = "&maxResults=\(youTubeVideosPageSize)";
    let chart: String = "&chart=mostPopular";
    var maxHeight: UInt = 8192;
    var maxWidth: UInt = 8192;
    
    //Pagination responses
    var responses: [YouTubeResponse] = [];
    
    init(searchTitle: String?, params: [String], categoryItem: YtCategoryItem?, countryCode: YtCountryCode?) {
        self.searchTitle = searchTitle;
        self.linguisticParams = params;
        self.categoryItem = categoryItem;
        self.countryCode = countryCode;
    }
    
    convenience init (searchTitle: String?){
        self.init(searchTitle: searchTitle,
                  params: [],
                  categoryItem: YouTubeManager.shared.selectedVideoCategory,
                  countryCode: YouTubeManager.shared.selectedYtCountryCode)
    }

    convenience init (playlistId: String?){
        self.init(searchTitle: nil,
                  params: [],
                  categoryItem: YouTubeManager.shared.selectedVideoCategory,
                  countryCode: YouTubeManager.shared.selectedYtCountryCode)
        self.playlistId = playlistId;
    }

    convenience init (searchTitle: String?, params:[String]){
        self.init(searchTitle: searchTitle,
                  params: params,
                  categoryItem: YouTubeManager.shared.selectedVideoCategory,
                  countryCode: YouTubeManager.shared.selectedYtCountryCode)
    }
    
    func isSearch() -> Bool{
        let retVal = (self.searchTitle != nil) && (self.searchTitle!.count > 0)
        return retVal;
    }
    func isPlaylist() -> Bool{
        let retVal = (self.playlistId != nil) && (self.playlistId!.count > 0)
        return retVal;
    }

    func getRequestURL() -> String {
        var retVal: String = YouTubeManager.baseURL;
        
        if self.isSearch(){
            retVal = retVal + baseSearch + "q=" + self.searchTitle!;
        }else if self.isPlaylist(){
            retVal = retVal + basePlaylistItems + "playlistId=" + self.playlistId!;
        } else{
            retVal = retVal + baseVideos;
        }

        retVal = retVal + part;
        if let cc = self.countryCode?.code{
            retVal = retVal + "&regionCode=" + cc
        }
        if let ci = self.categoryItem?.id {
            retVal = retVal + "&videoCategoryId=" + ci
        }
        retVal = retVal + chart;
        
        if let pToken = self.responses.last?.nextPageToken {
            retVal = retVal + "&pageToken=" + pToken
        }
        retVal = retVal + maxResults;

        maxWidth = UInt(max(nlUtils.shared.screenSize.width, nlUtils.shared.screenSize.height));
        maxHeight = maxWidth;
        
        retVal = retVal + "&maxHeight=" + String(maxHeight);
        retVal = retVal + "&maxWidth=" + String(maxWidth);
        retVal = retVal + "&key=" + YouTubeManager.apiKey

        return retVal;
    }
    
    func urlForContentDetails(_ details: [String]) -> String {
        var retVal: String = YouTubeManager.baseURL + baseVideos;
        let str = details.joined(separator: ",");
        
        retVal = retVal + "id=" + str + contentDetails;
        retVal = retVal + "&key=" + YouTubeManager.apiKey

        return retVal;
    }
    

    //MARK: Equatable protocol stubs
    static func == (lhs: YouTubeRequest, rhs: YouTubeRequest) -> Bool {
        if lhs.searchTitle != rhs.searchTitle{
            return false;
        }
        if lhs.playlistId != rhs.playlistId{
            return false;
        }
        if lhs.linguisticParams.count != rhs.linguisticParams.count{
            return false;
        }
        for l in lhs.linguisticParams {
            if (rhs.linguisticParams.contains(l) == false){
                return false;
            }
        }
        for r in rhs.linguisticParams {
            if (lhs.linguisticParams.contains(r) == false){
                return false;
            }
        }
        if (lhs.countryCode?.code != rhs.countryCode?.code) {
            return false;
        }
        if (lhs.categoryItem?.id != rhs.categoryItem?.id){
            return false;
        }
        return true;
    }
    
}

extension (YouTubeRequest){
    func assignResponse(response: YouTubeResponse){
        self.responses.append(response);
    }
    func canFetchMoreResults() -> Bool {
        let items = self.responses.flatMap({$0.items});
        
        if (self.responses.last?.nextPageToken == nil) {
            // there is no next page token in last page results. So there are no more results.
            return false;
        }
        
        if (items.count >= maxYouTubeResults){
            // max results fetch limit has been reached. As per understanding it is expected that is user interrested results are in top 200 after that user might get bored and change the search criteria.
            return false;
        }
        return true;
    }
}
