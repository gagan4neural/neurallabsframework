//
//  YouTubeResponse.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 03/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//



// This file holds all youtube video contracts.

import Foundation


class YouTubeResponse {
    
    private let kKind: String = "kind";
    private let kEtag: String = "etag";
    private let kNextPageToken: String = "nextPageToken";
    private let kPrevPageToken: String = "prevPageToken";
    private let kPageInfo: String = "pageInfo";
    private let kItems: String = "items";

    let kind: String?;
    let etag: String?;
    let nextPageToken: String?;
    let prevPageToken: String?;
    let pageInfo: YtVideoPageInfo?;
    private(set) var items: [YtVideoItem] = [];

    init(response : [String: Any]) {
        self.kind = response[kKind] as? String;
        self.etag = response[kEtag] as? String;
        self.nextPageToken = response[kNextPageToken] as? String;
        self.prevPageToken = response[kPrevPageToken] as? String;
        self.pageInfo = YtVideoPageInfo(data: response[kPageInfo] as! [String : Any]);
        
        if let arr = response[kItems] as? [[String: Any]]{
            for dItem in arr {
                let i = YtVideoItem(data: dItem);
                self.items.append(i);
            }
        }
    }
    
    init(neuralDefaultData: [AnyHashable: Any]){
        self.kind = nil;
        self.etag = nil;
        self.nextPageToken = nil;
        self.prevPageToken = nil;
        self.pageInfo = nil;

        if let arr = neuralDefaultData[kMediaItemDictionary] as? [[String: Any]]{
            for dItem in arr {
                let i = YtVideoItem(data: dItem);
                self.items.append(i);
            }
        }
    }
    
    func getAllVideoIds() -> [String] {
        let retVal: [String] = self.items.map({ (p) -> String in
            return p.getVideoId() ?? "";
        })
        return retVal;
    }
    
    func saveContentDetails(_ dict: [AnyHashable: Any]?) {
        
        let kId: String = "id";
        let kContentDetails: String = "contentDetails";
        
        
        if (dict == nil){
            Logger.shared.log("There is no dictionary to save youtube content details.");
            return;
        }
        if let items = dict![kItems] as? [[String: Any]] {
            for item in items {
                let id = item[kId] as? String;
                if let ytItem = self.items.first(where: {$0.getVideoId() == id}){
                    ytItem.contentDetail.loadContentDetails(item[kContentDetails] as? [String: Any]);
                }
            }
        }
    }
}

class YtVideoPageInfo{
    
    private let kTotalResults = "totalResults"
    private let kResultsPerPage = "resultsPerPage"
    
    let totalResults: Int;
    let resultsPerPage: Int;
    
    init(data: [String:Any]){
        self.totalResults = data[kTotalResults] as! Int;
        self.resultsPerPage = data[kResultsPerPage] as! Int;
    }
}

class YtVideoItem {

    private let kKind: String = "kind";
    private let kEtag: String = "etag";
    private let kId: String = "id";
    private let kSnippet: String = "snippet";

    let kind: String?;
    let etag: String?;
    let id: YtVideoItemId?;
    let videoId: String?;
    let snippet: YtVideoSnippet?;
    let dData: [String: Any];
    
    var contentDetail: YtContentDetail = YtContentDetail();

    init(data: [String:Any]){
        self.dData = data;
        self.kind = data[kKind] as? String;
        self.etag = data[kEtag] as? String;
        if let d = data[kId] as? [String: Any] {
            self.id = YtVideoItemId(data: d);
            self.videoId = nil;
        }else{
            self.id = nil;
            self.videoId = data[kId] as? String;
        }
        if let d = data[kSnippet] as? [String: Any] {
            self.snippet = YtVideoSnippet(data: d);
        }else{
            self.snippet = nil;
        }
    }
    
    func getVideoId() -> String?{
        if self.videoId != nil{
            return self.videoId;
        }else{
            return self.id?.videoId;
        }
    }
    
    func getDuration() -> TimeInterval {
        var retTm = TimeInterval(0);
        if let duration = self.contentDetail.duration {
            var fString = duration.replacingOccurrences(of: "P", with: "");
            fString = fString.replacingOccurrences(of: "T", with: "");
            
            let daysArray = fString.components(separatedBy: "D");
            fString = (daysArray.last)!;
            if (daysArray.count > 1){
                let days:Int = Int(daysArray.first!) ?? 0;
                retTm = retTm + TimeInterval(days * 24 * 60 * 60);
            }
            
            let hoursArray = fString.components(separatedBy: "H");
            fString = (hoursArray.last)!;
            if (hoursArray.count > 1){
                let hours:Int = Int(hoursArray.first!) ?? 0;
                retTm = retTm + TimeInterval(hours * 60 * 60);
            }

            let minsArray = fString.components(separatedBy: "M");
            fString = (minsArray.last)!;
            if (minsArray.count > 1){
                let mins:Int = Int(minsArray.first!) ?? 0;
                retTm = retTm + TimeInterval(mins * 60);
            }

            let secsArray = fString.components(separatedBy: "S");
            if (secsArray.count > 1){
                let secs:Int = Int(secsArray.first!) ?? 0;
                retTm = retTm + TimeInterval(secs);
            }
        }
        return retTm;
    }
}

class YtContentDetail {
    
    private let kDuration: String = "duration";
    private let kDimension: String = "dimension";
    private let kDefinition: String = "definition";
    private let kCaption: String = "caption";
    private let kLicensedContent: String = "licensedContent";
    private let kProjection: String = "projection";
    private let kRegionRestriction: String = "regionRestriction";

    fileprivate var duration: String?;
    fileprivate var dimension: String?;
    fileprivate var definition: String?;
    fileprivate var caption: String?;
    fileprivate var licensedContent: Bool?;
    fileprivate var projection: String?;
    fileprivate var regionRestriction: YtRegionRestriction?;
    
    func loadContentDetails(_ dict: [String: Any]?){
        self.duration = dict?[kDuration] as? String;
        self.dimension = dict?[kDimension] as? String;
        self.definition = dict?[kDefinition] as? String;
        self.caption = dict?[kLicensedContent] as? String;
        self.licensedContent = dict?[kProjection] as? Bool;
        self.projection = dict?[kProjection] as? String;
        
        if let d = dict?[kRegionRestriction] as? [String: Any]{
            regionRestriction = YtRegionRestriction(d);
        }
    }
}

class YtRegionRestriction{
    
    private let kBlocked: String = "blocked";
    private let kAllowed: String = "allowed";

    let blocked: [String]?;
    let allowed: [String]?;
    
    init(_ dict: [String: Any]){
        self.blocked = dict[kBlocked] as? [String];
        self.allowed = dict[kAllowed] as? [String];
    }

}

class YtVideoItemId {
    private let kKind = "kind";
    private let kPlaylistId = "playlistId";
    private let kVideoId = "videoId";
    
    var kind: String? = nil;
    var playlistId: String? = nil;
    var videoId: String? = nil;
    
    init(data: [String:Any]){
        self.kind = data[kKind] as? String;
        self.playlistId = data[kPlaylistId] as? String;
        self.videoId = data[kVideoId] as? String;
    }
}

class YtVideoSnippet {
    
    private let kPublishedAt:String = "publishedAt"
    private let kChannelId: String = "channelId"
    private let kTitle: String = "title"
    private let kDescription: String = "description"
    private let kChannelTitle: String = "channelTitle"
    private let kThumbnails: String = "thumbnails"
    private let kTags: String = "tags"
    private let kCategoryId: String = "categoryId"
    private let kLiveBroadcastContent: String = "liveBroadcastContent"
    private let kLocalized: String = "localized"
    private let kDefaultAudioLanguage: String = "defaultAudioLanguage"
    
    let publishedAt: String?;
    let channelId: String?;
    let title: String?;
    let description: String?;
    let thumbnails: YtVideoSnippetThumbnails?;
    let channelTitle: String?;
    let tags: [String]?;
    let categoryId: String?;
    let liveBroadcastContent: String?;
    let localized: YtVideoSnippetLocalized?;
    let defaultAudioLanguage: String?;
    
    init(data: [String:Any]){
        self.publishedAt = data[kPublishedAt] as? String;
        self.channelId = data[kChannelId] as? String;
        self.title = data[kTitle] as? String;
        self.description = data[kDescription] as? String;
        self.channelTitle = data[kChannelTitle] as? String;
        self.tags = data[kTags] as? [String];
        self.categoryId = data[kCategoryId] as? String;
        self.liveBroadcastContent = data[kLiveBroadcastContent] as? String;
        
        if let d = data[kThumbnails] as? [String:Any] {
            self.thumbnails = YtVideoSnippetThumbnails(data: d);
        }else{
            self.thumbnails = nil;
        }
        if let d = data[kLocalized] as? [String: Any] {
            self.localized = YtVideoSnippetLocalized(data: d);
        }else{
            self.localized = nil;
        }
        self.defaultAudioLanguage = data[kDefaultAudioLanguage] as? String;
    }
}

class YtVideoSnippetThumbnails{
    class Thumbnail{
        
        private let kUrl: String = "url";
        private let kWidth: String = "width";
        private let kHeight: String = "height";
        
        private let defWidth: Int = 100;
        private let defHeight: Int = 100;

        let url: String?
        private(set) var width: Int;
        private(set) var height: Int;
        
        init(data: [String:Any]?){

            self.width = defWidth
            self.height = defHeight

            if let d = data{
                self.url = d[kUrl] as? String;
                self.width = (d[kWidth] as? Int) ?? defWidth
                self.height = (d[kHeight] as? Int) ?? defHeight
            }else{
                self.url = nil;
            }
        }
    }
    
    private let kDefault: String = "default"
    private let kMedium: String = "medium"
    private let kHigh: String = "high"
    private let kStandard: String = "standard"
    private let kMaxres: String = "maxres"

    let _default: Thumbnail?;
    let medium: Thumbnail?;
    let high: Thumbnail?;
    let standard: Thumbnail?;
    let maxres: Thumbnail?;
    
    init(data: [String:Any]?){
        if let d = data {
            self._default = Thumbnail(data: d[kDefault] as? [String: Any]);
            self.medium = Thumbnail(data: d[kMedium] as? [String: Any]);
            self.high = Thumbnail(data: d[kHigh] as? [String: Any]);
            self.standard = Thumbnail(data: d[kStandard] as? [String: Any]);
            self.maxres = Thumbnail(data: d[kMaxres] as? [String: Any]);
        }else{
            self._default = nil;
            self.medium = nil;
            self.high = nil;
            self.standard = nil;
            self.maxres = nil;
        }
    }
    
    func preferedURL() -> String? {
        var pref: String? = nil;
        if (pref == nil){
            if let u = self.high?.url{
                pref = u;
            }
        }
        if let u = self._default?.url{
            pref = u;
        }
        if (pref == nil){
            if let u = self.standard?.url{
                pref = u;
            }
        }
        if (pref == nil){
            if let u = self.medium?.url{
                pref = u;
            }
        }
        if (pref == nil){
            if let u = self.maxres?.url{
                pref = u;
            }
        }
        return pref;
    }
}

class YtVideoSnippetLocalized{
    
    private let kTitle:String = "title"
    private let kDescription: String = "description"
    
    let title: String?;
    let description: String?;
    
    init(data: [String:Any]){
        self.title = data[kTitle] as? String;
        self.description = data[kDescription] as? String;
    }
}
