//
//  YouTubeVideoCategories.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 25/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation


class YouTubeCategories{

    static private let baseCategory = "videoCategories?"
    static private var part = "Snippet"
    private let neuralDefaultsKey = "YouTubeCategories"

    private var ytCategory: YtCategory? = nil;

    static func categoriesURL() -> String {
        let baseURl = YouTubeManager.baseURL + YouTubeCategories.baseCategory;
        let part = "part=" + YouTubeCategories.part;
        var region = "&regionCode=";
        if let code = YouTubeManager.shared.selectedYtCountryCode?.code {
            region = region + code;
        }
        let key = "&key=" + YouTubeManager.apiKey
        let retVal = baseURl + part + region + key;
        return retVal;
    }
    
    func loadCategories(_ callback:@escaping YouTubeCategoriesCallback){
        if (self.ytCategory != nil){
            callback(self.ytCategory);
            self.checkStaleDataFromHost();
        }
        else if (self.loadCategoriesFromUserDefault() == true){
            // there are no youtube categories stored in user defaults. Needs to refresh the categories
            callback(self.ytCategory);
            self.checkStaleDataFromHost();
        }else{
            self.loadCategoriesFromHost(callback);
        }
    }
    
    func selectCategoryItem(item:YtCategoryItem){
        for itm in self.ytCategory?.items ?? []{
            itm.snippet?.setSelected(value: false);
        }
        let selItm = self.ytCategory?.items?.first(where: {$0.snippet?.title == item.snippet?.title});
        selItm?.snippet?.setSelected(value: true);
        self.saveCategoriesTouserDefaults();
        let _ = self.loadCategoriesFromUserDefault();
    }

    private func loadCategoriesFromUserDefault() -> Bool{
        guard let dict: [String:Any] = NeuralDefaults.getYouTube(neuralDefaultsKey) as? [String: Any] else {
            Logger.shared.log("No Youtube Categories found in Neural defaults");
            return false;
        }
        if (dict.keys.count <= 0){
            Logger.shared.log("No Data in youtube Categories from Neural Defaults");
            return false;
        }
        self.ytCategory = YtCategory(dict: dict);
        return true;
    }
    
    private func saveCategoriesTouserDefaults() {
        if let dict = self.ytCategory?.getDictionary() {
            if (dict.keys.count > 0){
                NeuralDefaults.setYouTube(neuralDefaultsKey, value: dict);
            }
        }
    }
    
    private func loadCategoriesFromHost(_ callback:@escaping YouTubeCategoriesCallback){
        weak var weakSelf = self;
        let urlStr = YouTubeCategories.categoriesURL();
        let req = NetworkRequest.requestForURL(urlStr, responseCallback: { (networkResponse) -> Void in
            weakSelf?.loadCategoriesFromResponse(networkResponse: networkResponse);
            weakSelf?.saveCategoriesTouserDefaults();
            callback(weakSelf?.ytCategory);
        });
        NetworkManager.shared.executeNetworkRequest(req);
    }
    
    private func checkStaleDataFromHost () {
        // using eTags needs to check that the host data has been changed or still the same. If same then no changes are required otherwise needs to update neural defaults.
        
        weak var weakSelf = self;
        if (self.ytCategory == nil) || (self.ytCategory?.etag == nil){
            // No eTag information
            return;
        }
        let urlStr = YouTubeCategories.categoriesURL();
        let req = NetworkRequest.requestForURL(urlStr, responseCallback: { (networkResponse) -> Void in
            if (networkResponse.isDataModified()) {
                // the response is not HTTP_STATUS_CODE = 304 the new data is arrived.
                weakSelf?.loadCategoriesFromResponse(networkResponse: networkResponse);
                weakSelf?.saveCategoriesTouserDefaults();
            }
        });
        req.eTag = (self.ytCategory!.etag)!;
        NetworkManager.shared.executeNetworkRequest(req);
    }
    
    func selectedItem () -> YtCategoryItem? {
        let ytci = self.ytCategory?.items?.first(where: {$0.snippet?.isSelected == true});
        return ytci;
    }
}




//MARK: Contracts
private extension (YouTubeCategories){
    private func loadCategoriesFromResponse(networkResponse: NetworkResponse){
        guard let data = networkResponse.dictData else{
            Logger.shared.log("No Youtube Categories found");
            return;
        }
        
        if (networkResponse.isSuccess() == false){
            Logger.shared.log("Noework response was not sucessfull");
            return;
        }
        
        if (data.keys.count > 0){
            // build contracts from data
            ytCategory = YtCategory(dict: data);
        }
    }
}

public class YtCategory{
    private let kKind = "kind";
    private let keTag = "etag";
    private let kItems = "items";

    private let kind: String?;
    fileprivate let etag: String?;
    public fileprivate(set) var items: [YtCategoryItem]?;
    
    init(dict: [String: Any]){
        self.kind = dict[kKind] as? String;
        self.etag = dict[keTag] as? String;
        self.items = [];

        if let arr = dict[kItems] as? [[String: Any]]{
            for dict in arr {
                let i = YtCategoryItem(dict: dict);
                self.items?.append(i);
            }
        }
    }
    
    fileprivate func getDictionary() -> [String : Any]{
        var dict: [String: Any] = [:];
        dict[kKind] = self.kind;
        dict[keTag] = self.etag;
        let dItems = self.items?.compactMap({$0.getDictionary()});
        dict[kItems] = dItems;
        return dict;
    }
}

public class YtCategoryItem {
    private let kKind = "kind";
    private let keTag = "etag";
    private let kid = "id";
    private let kSnippet = "snippet"
    
    private let kind: String?;
    private let etag: String?;
    public let id: String?;
    public fileprivate(set) var snippet: YtCategorySnippet?;

    init(dict: [String: Any]) {
        self.kind = dict[kKind] as? String;
        self.etag = dict[keTag] as? String;
        self.id = dict[kid] as? String;
        
        if let d = dict[kSnippet] as? [String: Any]{
            self.snippet = YtCategorySnippet(dict: d)
        }else{
            self.snippet = nil;
        }
    }
    
    fileprivate func getDictionary() -> [String : Any]{
        var dict: [String: Any] = [:];
        dict[kKind] = self.kind;
        dict[keTag] = self.etag;
        dict[kid] = self.id;
        dict[kSnippet] = self.snippet?.getDictionary();
        return dict;
    }
}

public class YtCategorySnippet {
    private let kChannelId = "channelId";
    private let kTitle = "title";
    private let kAssignable = "assignable";
    private let kndIsSelected = "isSelected"; // this is neural defaults key

    private let channelId: String?;
    public let title: String?;
    private let assignable: Bool?;
    public fileprivate(set) var isSelected: Bool = false;
    
    init(dict: [String: Any]) {
        self.channelId = dict[kChannelId] as? String;
        self.title = dict[kTitle] as? String;
        self.assignable = dict[kAssignable] as? Bool;
        self.isSelected = (dict[kndIsSelected] as? Bool) ?? false
    }

    fileprivate func getDictionary() -> [String : Any]{
        var dict: [String: Any] = [:];
        dict[kChannelId] = self.channelId;
        dict[kTitle] = self.title;
        dict[kAssignable] = self.assignable;
        dict[kndIsSelected] = self.isSelected;
        return dict;
    }
    
    func setSelected(value: Bool) {
        self.isSelected = value
    }
}
