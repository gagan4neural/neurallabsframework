//
//  YouTubeSearches.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 04/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


// class supporting functionality for user defined searches. It will store all the texts which user have entered in the past and based on that it will show the trending results on first.
// this class is core and will help in reducing API usage per device.

class YouTubeSearches {
    
    private var userSearches: [YtUserSearch] = [];
    private let neuralDefaultsKey = "YouTubeSearches"

    func loadYouTubeSearches() {
        guard let data: [[String:Any]] = NeuralDefaults.getYouTube(neuralDefaultsKey) as? [[String: Any]] else {
            Logger.shared.log("No Youtube searches found in Neural defaults");
            return;
        }
        if (data.count <= 0){
            Logger.shared.log("No Youtube searches found in Neural defaults");
            return;
        }
        self.userSearches = data.compactMap({return YtUserSearch.instance(dict: $0)});
    }
    
    func saveYouTubeSearches() {
        let data: [[String: Any]] = self.userSearches.compactMap({return $0.getDictionary()});
        
        if (data.count > 0){
            NeuralDefaults.setYouTube(neuralDefaultsKey, value: data);
        }
    }
    
    func appendSearch(title: String) {
        var search: YtUserSearch? = nil;
        if let s = self.userSearches.first(where: {$0.searchTitle == title}) {
            if (s.isValid()){
                search = s;
            }
            // this is a dirty variable.
            //Remove this from the queue. Queue is precious only relevant data should be there which is helpful in search.
            if let index = self.userSearches.firstIndex(where: {return ($0 == s)}){
                self.userSearches.remove(at: index);
            }
        }
        
        if (search == nil){
            search = YtUserSearch();
            search?.searchTitle = title;
            search?.firstSearchDate = Date();
        }
        search?.lastSearchDate = Date();
        
        self.userSearches.append(search!);
    }
}

class YtUserSearch: Equatable {
    
    private static let kndTitle: String = "Title";
    private static let kndLastSearchDate: String = "LastSearchDate";
    private static let kndFirstSearchDate: String = "FirstSearchDate";
    private static let kndRequestedSearches: String = "RequestedSearches";
    private static let kndResultsArrived: String = "ResultsArrived";

    var searchTitle: String?;
    var lastSearchDate: Date? = nil;
    var firstSearchDate: Date? = nil;
    var requestedSearches: Int = 0; // num of times this title has been searched
    var resultsArrived: Int = 0; // las search results arrived for this title
    
    static func instance(dict: [String: Any]) -> YtUserSearch?{
        let retVal = YtUserSearch();
        
        retVal.searchTitle = dict[YtUserSearch.kndTitle] as? String;
        retVal.lastSearchDate = dict[YtUserSearch.kndLastSearchDate] as? Date;
        retVal.firstSearchDate = dict[YtUserSearch.kndFirstSearchDate] as? Date;
        retVal.requestedSearches = dict[YtUserSearch.kndRequestedSearches] as? Int ?? 0;
        retVal.resultsArrived = dict[YtUserSearch.kndResultsArrived] as? Int ?? 0;
        
        if (retVal.isValid() == false){
            return nil;
        }
        
        return retVal;
    }
    
    func getDictionary() -> [String:Any]{
        var retVal: [String: Any] = [:];
        retVal[YtUserSearch.kndTitle] = self.searchTitle;
        retVal[YtUserSearch.kndLastSearchDate] = self.lastSearchDate;
        retVal[YtUserSearch.kndFirstSearchDate] = self.firstSearchDate;
        retVal[YtUserSearch.kndRequestedSearches] = self.requestedSearches;
        retVal[YtUserSearch.kndResultsArrived] = self.resultsArrived;
        return retVal;
    }
    
    func isValid() -> Bool {
        if (self.searchTitle == nil){
            return false;
        }
        if (self.lastSearchDate == nil) && (self.firstSearchDate == nil){
            return false;
        }
        return true;
    }


    static func == (lhs: YtUserSearch, rhs: YtUserSearch) -> Bool {
        var retVal = true;
        
        if (lhs.searchTitle != rhs.searchTitle){
            retVal = false;
        }
        
        if (lhs.firstSearchDate != nil) && (rhs.firstSearchDate != nil){
            if (lhs.firstSearchDate?.compare(rhs.firstSearchDate!) != .orderedSame) {
                retVal = false;
            }
        }

        if (lhs.lastSearchDate != nil) && (rhs.lastSearchDate != nil){
            if (lhs.lastSearchDate?.compare(rhs.lastSearchDate!) != .orderedSame) {
                retVal = false;
            }
        }
        
        return retVal;
    }
}
