//
//  YouTubeCountryCodes.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 25/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation

public class YtCountryCode{
    public let code: String;
    public let countryName: String;
    public internal(set) var isSelected: Bool = false;
    public internal(set) var isCurrent: Bool = false;
    
    init(code: String, countryName: String) {
        self.code = code;
        self.countryName = countryName;
        if let currentLocaleCode = Locale.current.languageCode{
            self.isCurrent = currentLocaleCode == code;
        }
    }
}
