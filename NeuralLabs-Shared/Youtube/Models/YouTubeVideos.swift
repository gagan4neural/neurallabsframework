//
//  YouTubeVideos.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 25/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation


fileprivate typealias HostCollectionCallback = (YouTubeCollection?, ErrorString?) -> Void


public class YouTubeVideos {

    private let neuralDefaultsRecentKey = "YouTubeObject_Recent"
    private let neuralDefaultsFavoritesKey = "YouTubeObject_Favorites"

    
    //MARK: Collections
    private var collections: [YouTubeCollection] = [];
    private var recentCollections: YouTubeCollection? = nil;
    private var favoriteCollections: YouTubeCollection? = nil;
    
    func initializeLocalCollections() {
        self.loadLocalRecentCollections();
        self.loadLocalFavoriteCollections();
    }
    
    func fetchYouTubeCollections(title: String?, type: YouTubeCollectionType, callback:@escaping YouTubeCollectionCallback) {
        // assigning an empty search, this leads to set trending collections.
        self.fetchYouTubeCollections(type: type, request: YouTubeRequest(searchTitle: title), callback: callback);
    }

    func fetchYouTubeCollections(type: YouTubeCollectionType, title: String?, callback:@escaping YouTubeCollectionCallback){
        let request = YouTubeRequest(searchTitle: title);
        self.fetchYouTubeCollections(type: type, request: request, callback: callback);
    }

    func fetchYouTubeCollections(type: YouTubeCollectionType, title: String?, params:[String], callback:@escaping YouTubeCollectionCallback){
        let request = YouTubeRequest(searchTitle: title, params: params);
        self.fetchYouTubeCollections(type: type, request: request, callback: callback);
    }

    func fetchYouTubeVideos(fromCollection collectionss:[YouTubeCollection], callback:@escaping YouTubeCollectionCallback){
        //TODO: Currently will support only last collection to append more results.
        let coll = (collectionss.last)!;
        let req = (coll.youTubeRequest)!;
        if (coll.hasMoreData() == false) {
            callback(collectionss, "No more data avaliable");
            return;
        }
        self.fetchYouTubeCollections(type: coll.youtubeCollectionType, request: req, callback: callback);
    }
    
    func fetchYouTubeCollections(type: YouTubeCollectionType,
                                 request: YouTubeRequest,
                                 callback:@escaping YouTubeCollectionCallback){
        
        var errStr: String? = nil;

        if (type.isLocal) {
            self.callbackCollection(type: type, customColl:nil, errStr: errStr, callback: callback);
        }else{
            var useReq : YouTubeRequest? = request;
            if let findColl = self.searchCollection(forRequest: request){
                if (findColl.youTubeRequest?.canFetchMoreResults() == false) {
                    // the collection is full and cannot have more data.
                    self.callbackCollection(type: type, customColl: findColl, errStr: errStr, callback: callback);
                    return;
                }else{
                    useReq = findColl.youTubeRequest;
                }
            }
            
            weak var weakSelf = self;
            self.loadCollectionsFromHostWith(request: useReq!) { (collection, err) in
                
                if (err != nil){
                    // update the error string.
                    weakSelf?.callbackCollection(type: type, customColl:nil, errStr: err, callback: callback);
                    return;
                }
                
                if (collection == nil){
                    errStr = "No youtube collections found";
                    weakSelf?.callbackCollection(type: type, customColl:nil, errStr: errStr, callback: callback);
                    return;
                }
                
                var colls = weakSelf?.searchCollection(forRequest: useReq!);
                if (colls == nil){
                    // it's a new search
                    collection?.youTubeRequest = useReq;
                    weakSelf?.collections.append(collection!)
                    colls = collection;
                }else{
                    // it's a old search needs to merge the results.
                    colls?.mergeCollection(collection: collection!);
                }
                weakSelf?.callbackCollection(type: type, customColl:colls, errStr: errStr, callback: callback);
            };
        }
    }
    
    public func fetchYouTubePlaylist(item: MediaItemsProtocol, callback:@escaping YouTubeCollectionCallback){
        guard let ytObject = item as? YouTubeObject else{
            callback([], "Invalid playlist Identifier");
            return;
        }
        //TODO: a playlist collection is supposed to return only 50 results not more then that.
        let request = YouTubeRequest(playlistId: ytObject.playlistIdentifier());
        
        var foundCollection: YouTubeCollection? = nil;
        if (self.favoriteCollections?.youTubeRequest == request){
            foundCollection = self.favoriteCollections
        }else if (self.recentCollections?.youTubeRequest == request){
            foundCollection = self.recentCollections
        }else{
            for col in self.collections{
                if (col.youTubeRequest == request){
                    foundCollection = col;
                }
            }
        }
        
        if (foundCollection != nil){
            // we have found an existing collection with the same playlist ID
            // returning the found collection. currently there are no updates. same results would be retuned
            callback([foundCollection!], nil);
            return;
        }
        
        weak var weakSelf = self;

        self.loadCollectionsFromHostWith(request: request) { (collection, err) in
            
            if (err != nil){
                // update the error string.
                weakSelf?.callbackCollection(type: .CustomSearch, customColl:nil, errStr: err, callback: callback);
                return;
            }
            
            if (collection == nil){
                weakSelf?.callbackCollection(type: .CustomSearch, customColl:nil, errStr: err, callback: callback);
                return;
            }
            
            weakSelf?.callbackCollection(type: .CustomSearch, customColl:collection, errStr: err, callback: callback);
        }
    }
    
    private func callbackCollection(type: YouTubeCollectionType,
                                    customColl: YouTubeCollection?,
                                    errStr: ErrorString?,
                                    callback:@escaping YouTubeCollectionCallback){
        switch type {
        case .Recent:
            if let c = self.getRecentCollection(){
                callback([c], nil);
            }else{
                callback([], errStr);
            }
            break;
        case .Favorite:
            if let c = self.getFavoritesCollection(){
                callback([c], nil);
            }else{
                callback([], errStr);
            }
            break;
        case .CustomSearch:
            if (customColl != nil){
                callback([customColl!], nil);
            }else{
                callback([], errStr);
            }
            break;
        }
    }
    
    private func loadCollectionsFromHostWith(request:YouTubeRequest, callback:@escaping HostCollectionCallback){
        let urlStr = request.getRequestURL();
        weak var weakSelf = self;
        
        let contentDetail = {[request] (_ youtubeResponse: YouTubeResponse) -> Void in
            let urlContentDetail = request.urlForContentDetails(youtubeResponse.getAllVideoIds());
            
            let urlReq = NetworkRequest.requestForURL(urlContentDetail, responseCallback: { (networkResponse) in
                youtubeResponse.saveContentDetails(networkResponse.dictData);
                let col = weakSelf?.prepareCollectionFrom(response: youtubeResponse);
                callback(col, nil);
            });
            NetworkManager.shared.executeNetworkRequest(urlReq);
        }
        
        let req = NetworkRequest.requestForURL(urlStr, responseCallback: {[contentDetail] (networkResponse) -> Void in
            
            if (networkResponse.error != nil){
                callback(nil, networkResponse.error?.localizedDescription);
                return;
            }
            
            if let dict = networkResponse.dictData{
                let youtubeResponse = YouTubeResponse(response: dict);
                request.responses.append(youtubeResponse);
                contentDetail(youtubeResponse);
            }else{
                callback(nil, "No collections found.");
            }
        });
        NetworkManager.shared.executeNetworkRequest(req);
    }
}


//MARK: Local collection extraction
extension (YouTubeVideos) {
    func loadLocalRecentCollections() {
        if (self.recentCollections == nil){
            self.recentCollections = YouTubeCollection(objects: []);
        }
        // load from user defaults
        guard let dCollections: [AnyHashable:Any] = NeuralDefaults.getYouTube(neuralDefaultsRecentKey) as? [AnyHashable: Any] else {
            return;
        }
        let response: YouTubeResponse = YouTubeResponse(neuralDefaultData: dCollections);
        self.recentCollections = self.prepareCollectionFrom(response: response);
    }
    func addItemToRecents(_ item : YouTubeObject){
        if (self.recentCollections == nil){
            self.loadLocalRecentCollections();
        }
        let itemExist = self.recentCollections?.mediaItems.first(where: {return $0.identifier == item.identifier});
        if (itemExist != nil){
            self.recentCollections?.removeYouTubeObject(itemExist);
        }
        
        // if it is a re-add then it must be move to latest;
        self.recentCollections?.addYouTubeObject(item);
        let dCollections: [AnyHashable: Any] = self.recentCollections?.getDictionary() ?? [:];
        NeuralDefaults.setYouTube(neuralDefaultsRecentKey, value: dCollections);
    }
    func getRecentCollection() -> YouTubeCollection?{
        if (self.recentCollections == nil){
            self.loadLocalRecentCollections();
        }
        return self.recentCollections;
    }
    
    
    func loadLocalFavoriteCollections() {
        if (self.favoriteCollections == nil){
            self.favoriteCollections = YouTubeCollection(objects: []);
        }
        // load from user defaults
        guard let dCollections: [AnyHashable:Any] = NeuralDefaults.getYouTube(neuralDefaultsFavoritesKey) as? [AnyHashable: Any] else {
            return;
        }
        let response: YouTubeResponse = YouTubeResponse(neuralDefaultData: dCollections);
        self.favoriteCollections = self.prepareCollectionFrom(response: response);
    }
    func addItemToFavorites(_ item : YouTubeObject){
        if (self.favoriteCollections == nil){
            self.loadLocalFavoriteCollections();
        }
        let itemExist = self.favoriteCollections?.mediaItems.first(where: {return $0.identifier == item.identifier});
        if (itemExist != nil){
            self.favoriteCollections?.removeYouTubeObject(itemExist);
        }
        
        // if it is a re-add then it must be move to latest;
        self.favoriteCollections?.addYouTubeObject(item);
        let dCollections: [AnyHashable: Any] = self.favoriteCollections?.getDictionary() ?? [:];
        NeuralDefaults.setYouTube(neuralDefaultsFavoritesKey, value: dCollections);
    }
    func removeItemFromFavorites(_ item : YouTubeObject){
        if (self.favoriteCollections == nil){
            self.loadLocalFavoriteCollections();
        }
        let itemExist = self.favoriteCollections?.mediaItems.first(where: {return $0.identifier == item.identifier});
        if (itemExist != nil){
            self.favoriteCollections?.removeYouTubeObject(itemExist);
        }
        let dCollections: [AnyHashable: Any] = self.favoriteCollections?.getDictionary() ?? [:];
        NeuralDefaults.setYouTube(neuralDefaultsFavoritesKey, value: dCollections);
    }
    func getFavoritesCollection() -> YouTubeCollection?{
        if (self.favoriteCollections == nil){
            self.loadLocalFavoriteCollections();
        }
        return self.favoriteCollections;
    }
}


//MARK: Searcher functionality
extension (YouTubeVideos) {
    func searchCollection(forRequest request: YouTubeRequest) -> YouTubeCollection? {
        for c in self.collections{
            if (c.youTubeRequest == request) {
                return c;
            }
        }
        return nil;
    }
}


//MARK: Media Item/Collection preparation
extension (YouTubeVideos) {
    private func prepareCollectionFrom(response: YouTubeResponse) -> YouTubeCollection{
        var youtubeObjects:[YouTubeObject] = [];
        for item in response.items {
            let obj = YouTubeObject(item: item);
            youtubeObjects.append(obj);
        }
        let retVal = YouTubeCollection(objects: youtubeObjects);
        return retVal;
    }
}
