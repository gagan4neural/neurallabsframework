//
//  YouTubeVideoCollection.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 02/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import CoreLocation


public enum YouTubeCollectionType {
    case Recent;
    case Favorite;
    case CustomSearch
}

extension YouTubeCollectionType {
    var isLocal: Bool{
        get{
            return (self == .Recent) || (self == .Favorite);
        }
    }
}

class YouTubeCollection: CollectionObject<YouTubeObject>, MediaCollectionProtocol {
    
    private var mediaCollectionType: MediaCollectionType = .Unknown;
    var youtubeCollectionType: YouTubeCollectionType = .CustomSearch;
    var youTubeRequest: YouTubeRequest? = nil; // What search criteria was used to fetch this collection from host.

    init(objects: [YouTubeObject]) {
        super.init();
        self.mediaItems = objects;
        self.mediaCollectionType = .AllMedia;
    }

    func mergeCollection(collection: YouTubeCollection) {
        for youtubeObject in collection.mediaItems {
            self.mediaItems.append(youtubeObject);
        }
    }
    
    func addYouTubeObject(_ object: YouTubeObject){
        self.mediaItems.insert(object, at: 0)
    }

    func removeYouTubeObject(_ object: YouTubeObject?){
        if (object == nil){
            return;
        }
        self.mediaItems.removeAll(where: {return $0.identifier == object?.identifier})
    }
    

    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        callback(self.youTubeRequest?.searchTitle ?? "YouTube");
    }
    var creationDate:Date?{
        get{
            return nil;
        }
    }
    var modifiedDate: Date?{
        get{
            return nil;
        }
    }
    var identifier: String?{
        get{
            return nil;
        }
    }
    var isPlaying: Bool{
        get{
            return false;
        }
    }
    var showcaseImage: UIImage?{
        get{
            return self.mediaItems.first(where: {$0.showcaseImage != nil})?.showcaseImage;
        }
    }
    var url: URL?{
        get{
            return self.mediaItems.first(where: {$0.url != nil})?.url;
        }
    }
    var type: MediaCollectionType{
        get{
            return self.mediaCollectionType;
        }
    }
    
    var location: CLLocation? {
        get{
            return self.mediaItems.first(where: {$0.location != nil})?.location;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }

    func getSequence(sequenceType:CollectionSequenceType, freshSequence:Bool) -> [Int]{
        
        if (freshSequence == true){
            self.itemSequence.removeAll();
        }
        if (self.itemSequence.count <= 0){
            switch sequenceType{
            case .Uniform:
                self.itemSequence = Array<Int>.uniformIndexedArray(limit: self.mediaItems.count, assending: true);
                break;
            case .Random:
                self.itemSequence = Array<Int>.randomIndexedArray(limit: self.mediaItems.count);
                break;
            }
        }
        
        return self.itemSequence;
    }
    
    func hasMoreData() -> Bool {
        return self.youTubeRequest?.canFetchMoreResults() ?? false;
    }
    
    func requestCollectionThumbnailImages(_ callback:@escaping cb_CollectionImages){
        var itmCount = 2; // keeping 2 for youtube image download from URL
        var retImages:[UIImage?] = [];
        for itm in self.mediaItems{
            itm.requestImage { (image, info) in
                
                if (image != nil){
                    retImages.append(image);
                }
                itmCount -= 1;
                if (itmCount <= 0){
                    callback(retImages);
                }
            }
        }
    }
    
    func requestCollectionTime(_ callback: @escaping MediaTimeCallback){
        var itmCount = self.mediaItems.count;
        var retTime:TimeInterval = TimeInterval(0);
        for itm in self.mediaItems{
            itm.requestMediaTime { (mediaTime) in
                retTime = retTime.advanced(by: mediaTime);
                
                itmCount -= 1;
                if (itmCount <= 0){
                    callback(retTime);
                }
            }
        }
    }
    
    func mediaItemsCount() -> String?{
        var retVal = "";
        var mediaType:[AnyHashable:Any] = [:];
        for item in self.mediaItems{
            
            let subType = item.mediaSubType();
            var count:Int? = 1;
            if mediaType.keys.contains(subType){
                count = mediaType[subType] as? Int;
                count = count! + 1;
            }
            mediaType[subType] = count;
        }
        
        for key in mediaType.keys{
            if let val = mediaType[key]{
                if (retVal.count <= 0){
                    retVal.append("\(val) \(key as! String)")
                }else{
                    retVal.append(", \(val) \(key as! String)")
                }
            }
        }
        
        return retVal
    }
    
    func getMediaItems() -> [MediaItemsProtocol]{
        return self.mediaItems;
    }
    
    func getDictionary() -> [AnyHashable:Any]{
        var itmArr : [Any] = [];
        
        for i in self.mediaItems {
            itmArr.append(i.getDictionary());
        }
        return [kMediaItemDictionary:itmArr];
    }

    func description() {
        Logger.shared.log("Description of YouTube Collection");
    }
}
