//
//  YouTubeVideoObject.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 02/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import CoreLocation

// Single low level entity holding information about Video.
class YouTubeObject:AssetObject, MediaItemsProtocol {
    
    internal var ytVideoItem: YtVideoItem? = nil;
    private var _isFavorite: Bool = false;
    private var _numberOfTimesPlayed: Int = 0;

    private var thumbnailImage: UIImage? = nil;
    private var imageData: Data? = nil;
    private var imageUti: String? = nil;
    private var imageOrientation: UIImage.Orientation = .up
    private var imageInfo: [AnyHashable:Any]? = nil;

    
    init (item: YtVideoItem) {
        super.init();
        self.ytVideoItem = item;
        self.mediaItemType = .Youtube;
    }
    
    func playlistIdentifier() -> String?{
        return self.ytVideoItem?.id?.playlistId;
    }
    

    
    //MARK: AssetObjectProtocol
    func title(_ callback:@escaping cb_AssetTitle){
        let name = self.ytVideoItem?.snippet?.title ?? "Neuro tube";
        DispatchQueue.main.async {
            callback(name)
        }
    }
    var creationDate:Date?{
        get{
            return nil;
        }
    }
    var modifiedDate: Date?{
        get{
            return nil;
        }
    }
    var identifier: String?{
        get{
            return (self.ytVideoItem?.id?.videoId ?? self.ytVideoItem?.id?.playlistId) ?? self.ytVideoItem?.videoId;
        }
    }
    var isPlaying: Bool{
        get{
            return _isPlaying;
        }
        set (newValue){
            _isPlaying = newValue;
        }
    }
    var showcaseImage: UIImage?{
        get{
            if let d = self.imageData{
                return UIImage(data: d);
            }
            return nil;
        }
    }
    var url: URL?{
        get{
            if let u = self.ytVideoItem?.snippet?.thumbnails?.standard?.url{
                return URL(string: u);
            }
            return nil;
        }
    }
    var type:MediaItemType{
        get{
            return self.mediaItemType;
        }
    }
    
    var favorite: Bool {
        get{
            return _isFavorite;
        }
        set{
            //TODO: Do some magic here
        }
    }
    
    var isCloudItem: Bool {
        get{
            return false;
        }
    }
    
    var location: CLLocation? {
        get{
            return nil;
        }
    }
    
    var isAdvertisement: Bool {
        get{
            return false;
        }
    }

    var numberOfTimesPlayed: Int {
        get{
            return _numberOfTimesPlayed
        }set{
            _numberOfTimesPlayed += 1;
        }
    }
    
    var lyrics: String?{
        get{
            return nil;
        }
    }
    var genre: String?{
        get{
            return nil;
        }
    }
    var composer: String?{
        get{
            return nil;
        }
    }
    var beatsPerMinute: Int?{
        get{
            return nil;
        }
    }
    var customInfo: [AnyHashable : Any]? {
        get{
            return _customInfo;
        }set{
            _customInfo = newValue;
        }
    }

    func canSupportARMode() -> Bool{
        return true;
    }
    
    func canSupportSlowMotion() -> Bool{
        return false;
    }
    
    func canSeperateAudioVideo() -> Bool{
        return false;
    }
    
    func canSupportScreenShot() -> Bool{
        return true;
    }
    
    func canSupportPiP() -> Bool{
        return false;
    }
    
    func canSupportMediaClipping() -> Bool{
        return true;
    }
    
    func copy(deepCopy: Bool) -> MediaItemsProtocol? {
        let obj = YouTubeObject(item: self.ytVideoItem!);
        obj._isFavorite = self._isFavorite;
        obj._numberOfTimesPlayed = self.numberOfTimesPlayed;
        return obj;
    }
    
    func save(callback:@escaping CompletionBlock){
    }

    func requestAVAsset (_ callback: @escaping cb_AVAsset){
    }
    
    func requestPlayerItem (_ callback: @escaping cb_AVPlayerItem){
    }
    
    func requestImage (_ callback: @escaping cb_ThumbnailImage){
        if (self.thumbnailImage != nil){
            callback(self.thumbnailImage, nil);
            return;
        }
        self.getThumbnailImage(callback: callback);
    }
    
    func requestImageData (_ callback: @escaping cb_ImageInfo){
    }
    
    func requestLivePhoto (_ callback: @escaping cb_LivePhoto){
    }
    
    func requestMediaTime (_ callback: @escaping MediaTimeCallback){
        callback(self.ytVideoItem?.getDuration() ?? 0);
    }
    
    func mediaSubType() -> String?{
        return "YouTube Videos"
    }
    
    func getAVAsset (callback : @escaping cb_AVAsset) {
    }
    
    func getAVPlayerItem (callback : @escaping cb_AVPlayerItem){
    }
    
    func getThumbnailImage (callback : @escaping cb_ThumbnailImage) {
        if let url = self.ytVideoItem?.snippet?.thumbnails?.preferedURL() {
            NetworkManager.shared.downloadImage(url: url) { (img, errStr) in
                if errStr != nil{
                    callback(img, ["Error":errStr!])
                }else{
                    callback(img, nil);
                }
            };
        }else{
            // no valid url
            callback(nil, nil);
        }
    }
    
    func getImageData (callback : @escaping cb_ImageInfo) {
    }
    
    func getLivePhoto (callback : @escaping cb_LivePhoto) {
    }
    
    func getDictionary() -> [AnyHashable:Any]{
        return self.ytVideoItem?.dData ?? [:];
    }

    func description() {
        Logger.shared.log("YouTube Object Description");
    }
}
