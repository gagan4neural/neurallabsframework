//
//  YouTubePanelList.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 01/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation

public enum YouTubeNeuralPanelType: CaseIterable{
    case Trending
    //TODO: Currently there are based on local data. Will see that how to pick them from youtube.
    case History
//    case Favorite
    case Category
    case Country
}

public extension YouTubeNeuralPanelType {
    var title: String? {
        switch self {
        case .Trending:
            return "Trending";
        case .History:
            return "History"
//        case .Favorite:
//            return "Favorite"
        case .Category:
            return "Category"
        case .Country:
            return "Country"
        }
    }
    var icon: String? {
        switch self {
        case .Trending:
            return IconFontCodes.filledStar;
        case .History:
            return IconFontCodes.time_restore_setting;
//        case .Favorite:
//            return IconFontCodes.favoriteFill;
        case .Category:
            return IconFontCodes.receipt;
        case .Country:
            return IconFontCodes.flag;
        }
    }
    var index: Int {
        switch self {
        case .Trending:
            return 0;
        case .History:
            return 1;
        case .Category:
            return 2;
        case .Country:
            return 3;
        }
    }
}
