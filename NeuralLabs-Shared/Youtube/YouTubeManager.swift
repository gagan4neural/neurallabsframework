//
//  YouTubeManager.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 23/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation


//Youtube
public typealias YouTubeCollectionCallback = ([MediaCollectionProtocol], ErrorString?) -> Void
public typealias YouTubeCategoriesCallback = (YtCategory?) -> Void
public typealias YouTubeCountryCodesCallback = ([YtCountryCode]) -> Void

public class YouTubeManager{
    
    public static let shared = YouTubeManager();
    private let categories = YouTubeCategories();
    private let videos = YouTubeVideos();
    
    //TODO: Get this API key from firebase.
    static let apiKey = "AIzaSyC_Ji9IlLUKOMztylxr5bV8bljSYv9TRag"
    static let baseURL = "https://www.googleapis.com/youtube/v3/"
    private let ytVideosBaseURL = "https://www.googleapis.com/youtube/v3/videos?"
    private let ytVideosSearchBaseURL = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video"

    
    //MARK: Youtube Videos
    public func fetchYoutubeCollections(title: String?, type: YouTubeCollectionType, callback:@escaping YouTubeCollectionCallback) {
        self.videos.fetchYouTubeCollections(title: title, type: type, callback: callback);
    }
    
    public func fecthYouTubeCollections(collections: [MediaCollectionProtocol], callback:@escaping YouTubeCollectionCallback){
        self.videos.fetchYouTubeVideos(fromCollection: collections as! [YouTubeCollection], callback: callback);
    }
    
    public func fetchYouTubePlaylist(item: MediaItemsProtocol, callback:@escaping YouTubeCollectionCallback){
        // NOTE: think 10 times before making any changes in this flow. Every one is expecting that only 1 unique collection is returned every time for same result.
        if (YouTubeManager.shared.hasPlaylistIdentifier(item: item) != nil){
            self.videos.fetchYouTubePlaylist(item: item, callback: callback);
        }else{
            callback([], "Invalid playlist Identifier");
        }
    }
    
    public func getYouTubeCollectionFor(items: [MediaItemsProtocol]) -> MediaCollectionProtocol{
        //TODO: can we do something better for safe conversion?
        let coll = YouTubeCollection(objects: items as! [YouTubeObject]);
        return coll;
    }
    
    public func hasPlaylistIdentifier(item: MediaItemsProtocol) -> String? {
        var playlistId: String? = nil;
        if let yto = item as? YouTubeObject {
            playlistId = yto.playlistIdentifier();
        }
        return playlistId;
    }
    
    public func addRecentItem(_ item: MediaItemsProtocol) {
        if let yto = item as? YouTubeObject {
            self.videos.addItemToRecents(yto);
        }
    }

    public func addFavoriteItem(_ item: MediaItemsProtocol) {
        if let yto = item as? YouTubeObject {
            self.videos.addItemToFavorites(yto);
        }
    }

    public func removeFavoriteItem(_ item: MediaItemsProtocol) {
        if let yto = item as? YouTubeObject {
            self.videos.removeItemFromFavorites(yto);
        }
    }

    
    
    //MARK: Youtube Categories
    public func fetchYoutubeVideoCategories(_ callback:@escaping YouTubeCategoriesCallback) {
        self.categories.loadCategories(callback);
    }
    public var selectedVideoCategory: YtCategoryItem? {
        get{
            return self.categories.selectedItem();
        }set{
            if let i = newValue {
                self.categories.selectCategoryItem(item: i);
            }
        }
    }
    public func selectCategoryItem(item:YtCategoryItem){
        self.categories.selectCategoryItem(item: item);
    }

    

    //MARK: Youtube Country Codes
    private var _countryCodes: [YtCountryCode] = [];
    var countryCodes: [YtCountryCode] {
        get{
            if (_countryCodes.count <= 0){
                let sysLocal = NSLocale.system as NSLocale;
                let currentLocalCode: String? = (NSLocale.current as NSLocale).object(forKey: .countryCode) as? String
                
                for code in NSLocale.isoCountryCodes {
                    let countryName = sysLocal.displayName(forKey: .countryCode, value: code) ?? "Unknown Country";
                    let ytCC = YtCountryCode(code: code, countryName: countryName);
                    
                    if (code == currentLocalCode) {
                        ytCC.isCurrent = true
                        ytCC.isSelected = true;
                        _countryCodes.insert(ytCC, at: 0);
                    }else{
                        ytCC.isCurrent = false
                        ytCC.isSelected = false;
                        _countryCodes.append(ytCC);
                    }
                }
            }
            return _countryCodes;
        }
    }
    public func fetchCountryCodes(callback: YouTubeCountryCodesCallback){
        callback(self.countryCodes);
    }
    public var currentYtCountryCode: YtCountryCode? {
        get{
            if let cc = self.countryCodes.first(where: {return $0.isCurrent == true}) {
                return cc;
            }
            return nil;
        }
        // no setter: user cannot set its current country code.
    }
    public var selectedYtCountryCode: YtCountryCode? {
        get{
            if let cc = self.countryCodes.first(where: {return $0.isSelected == true}){
                return cc;
            }
            if let cc = self.currentYtCountryCode {
                // if no country is selected then return the current country as selected country.
                return cc;
            }
            return nil;
        }set{
            for cc in self.countryCodes {
                cc.isSelected = false;
            }
            newValue?.isSelected = true;
        }
    }
}


// YT Player settings
extension (YouTubeManager){
    public var youtubeSettings: [AnyHashable: Any] {
        get{
            var dParams: [AnyHashable: Any] = [:];
            dParams["playsinline"] = 1;
            dParams["controls"] = 0;
            dParams["autoplay"] = 1;
            dParams["showinfo"] = 1;
            dParams["origin"] = "http://www.youtube.com";
            dParams["rel"] = 1;
            dParams["modestbranding"] = 1;
            dParams["cc_load_policy"] = 1;
            return dParams
        }
    }
}
