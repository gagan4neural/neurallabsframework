//
//  AVIProtocol.swift
//  AVIProcessing
//
//  Created by Gagan on 20/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation

/*
 Maintaining a simple flow. Outside world will communicate with AVIManager and AVIManager will internally manage all players.
 NeuralPlayerDelegate is delegate for MediaPlayers to AVIManager.
 AVIDelegate is delegate for Outsideworld and AVIManager.
 */

/// @brief Maximum number of failures allowed in playing an item.
let maxPlayerFailuresAllowed = 5;


public protocol NeuralPlayerDelegate{
    /// @brief MediaPlayer has just started playing collection.
    func neuralPlayerStarted(withItem item:MediaItemsProtocol?);
    /// @brief MediaPlayer has just switched to a new item from the previous item. Caller can update there UI as like new playing item.
    func neuralPlayerUpdating(newItem: MediaItemsProtocol?, oldItem:MediaItemsProtocol?);
    /// @brief MediaPlayer will callback about the current timings situations in app. Currently this is dependent on the Polling based a 0.1 second timer is ticked off repeatedly to grab the timings.
    func neuralPlayerTimings(totalTime: TotalMediaTime, elapsedTime:ElapsedMediaTime, remainingTime:RemainingMediaTime);
    /// @brief MediaPlayer has finished playing all items in collection. Now asking for permission to re-play all items.
    func neuralPlayer(items:[MediaItemsProtocol]?, limitReached:Bool) -> Bool;
    /// @brief MediaPlayer has finished playing all items in collection. And now the MediaPlayer will shut down its connection. Caller must close its connection with this instance of media player.
    func neuralPlayerFinishedPlaying(withItems items:[MediaItemsProtocol]?);
    /// @brief There was an error in playing current item in media player. Since the media player is in queue mode the next item will be picked up.
    func neuralPlayerError(inItem item:MediaItemsProtocol?, error: String);
    /// @brief Needs to shuffle items in player or not. NeuralMediaPlayer relies more on this rather
    func neuralPlayerCanShuffleItems() -> Bool;
    
    // @brief Reverse methos. From outside world is someone is directly impacting on player then a callback to UI Layer or application. Players are directly connected to LockScreen/Headphones so calls can arrive from there also.
    func neuralPlayer(resumePause: Bool);
    func neuralPlayer(playStop: Bool);
    func neuralPlayer(muteUnMute: Bool);
    
    //MARK: Editor
    func neuralEditor(error: String, type: MediaEditingType);
    func neuralEdited(object: MediaItemsProtocol, url: URL, type: MediaEditingType);
}

public protocol AVIDelegate {
    func aviPlayerCreated(view:NeuralBaseView, itemType:MediaItemType);
    // These are the counter parts of the NeuralPlayerDelegate defined above.
    func aviPlayerStartedPlaying(withItem item:MediaItemsProtocol?, inCollection:MediaCollectionProtocol?);
    func aviPlayerFinishedPlaying(withCollection inCollection:MediaCollectionProtocol?);
    func aviPlayerCanRestart(forCollection collection:MediaCollectionProtocol?) -> Bool;
    func aviPlayer(error:String?, inPlayingItem:MediaItemsProtocol?, inCollection:MediaCollectionProtocol?);
    func aviPlayer(updatingItem: MediaItemsProtocol?, previousItem:MediaItemsProtocol?);
    func aviPlayer(totalTime:TotalMediaTime, elapsedTime:ElapsedMediaTime, remainingTime:RemainingMediaTime);
    func aviPlayerCanShuffleItems(inCollection: MediaCollectionProtocol?) -> Bool;
    func aviPlayer(resumePause: Bool);
    func aviPlayer(playStop: Bool);
    func aviPlayer(muteUnMute: Bool);
    
    func aviPlayerRequiresCustomerPlayerDelegate(type: MediaItemType) -> CustomThirdPartyPlayerDelegate?;

    
    //MARK: Editor
    func aviEditor(error: String, type: MediaEditingType);
    func aviEdited(object: MediaItemsProtocol, url: URL, type: MediaEditingType);
}


/// @brief Delegate will encapsulate all the media players under once common protocol;
public protocol NeuralMediaControlDelegate {
    var play:Bool {get set} // true = play, false = stop
    var resume:Bool {get set} // true = resume, false = pause
    var mute:Bool {get set} // true = mute, false = unmute
    var shuffleUnShuffle:Bool {get set} // true = shuffle, false = unShuffle
    var volume:Float {get set}
    var brightness:Float {get set}
    var totalTime:TimeInterval {get}
    var elapsedTime:TimeInterval {get}
    var remainingTime:TimeInterval {get}
    var screenShot:UIImage? {get}
    var rate: Float {get set}
    
    var seekPercent:Float {get set};

    func containsItem(_ item:MediaItemsProtocol) -> Bool;
    func playNextItem();
    func playPrevItem();
    func closePlayer();
    
    func jumpTo(item: MediaItemsProtocol);
    func jumpTo(index: Int);

    //MARK: Editor
    func edit(item: MediaItemsProtocol?, type: MediaEditingType, start: CGFloat, end: CGFloat, rate: CGFloat, cutMedia:Bool);
}


public protocol CustomThirdPartyPlayerDelegate : NeuralMediaControlDelegate{
    
    func loadView(withItems: [MediaItemsProtocol], startIndex: Int, delegate:NeuralPlayerDelegate?);
}
