//
//  AVQueuePlayerExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 05/12/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import AVFoundation


extension (AVQueuePlayer) {
    
    func lastItem() -> AVPlayerItem?{
        return self.items().last;
    }
    
    func currentTimeReachedTotalTime() -> Bool {
        return self.items().last?.currentTimeReachedTotalTime() ?? false;
    }
}
