//
//  AVPlayerItemExtn.swift
//  NeuralLabs
//
//  Created by Gagan on 05/12/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import AVFoundation

extension (AVPlayerItem) {
    
    func currentTimeReachedTotalTime() -> Bool {
        // as per apple docs -1 for time1<time2, 0 for time1==time2, +1 for time1>time2
        return (CMTimeCompare(self.currentTime(), self.duration) >= 0)
    }
}
