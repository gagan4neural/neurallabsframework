//
//  AVIProcessing.swift
//  AVIProcessing
//
//  Created by Gagan on 20/10/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import Foundation
import MediaPlayer

fileprivate let defaultVideoPlayerIdentifier = "NeuralVideoPlayer"
fileprivate let defaultAudioPlayerIdentifier = "NeuralAudioPlayer"
fileprivate let defaultYoutubePlayerIdentifier = "NeuralYoutubePlayer"

fileprivate let defaultVideoEditorIdentifier = "NeuralVideoEditor"
fileprivate let defaultAudioEditorIdentifier = "NeuralAudioEditor"
fileprivate let defaultYoutubeEditorIdentifier = "NeuralYoutubeEditor"

fileprivate typealias AbsoluteFilePath = String;
fileprivate typealias RelativeFilePath = String;


public enum MediaEditingType{
    case Unknown;
    case SlowMotion;
    case Clipping;
    case AudioFromVideo;
    case Filtering
}

enum VideoOutputType : String {
    case _MPEG4 = "mpeg"
    case _QuickTime = "quicktime"
    case _M4V = "m4v"
    case _3GPP = "3gp"
    case _3GPP2 = "3gpp"
}

enum AudioOutputType : String {
    case _M4A = "m4a"
    case _MP3 = "mp3"
    case _WAVE = "wave"
    case _AIFF = "aiff"
    case _AMR = "amr"
}

open class AVIManager: NeuralPlayerDelegate {
    
    public static let shared = AVIManager();
    public var aviDelegate: AVIDelegate? = nil;
    private let defaultPlayerSize = CGRect(x: 0, y: 0, width: nlUtils.shared.screenSize.width, height: nlUtils.shared.screenSize.height);
    
    private var collection: MediaCollectionProtocol? = nil;
    private var neuralMediaPlayers: [AnyHashable:NeuralMediaControlDelegate] = [:];
    private var _systemVolume: Float? = nil;
    
    
    public func load(collection:MediaCollectionProtocol,
                     withItem:MediaItemsProtocol,
                     delegate:AVIDelegate)
    {
        // Making sure that some previous players were closed properly.
        self.closePlayer();

        // Splitting the collection in there respective item types.
        self.collection = collection;
        
        var videoItems:[MediaItemsProtocol] = [];
        var audioItems:[MediaItemsProtocol] = [];
        var youtubeItems:[MediaItemsProtocol] = [];
        
        for itm in collection.getMediaItems(){
            switch itm.type{
            case .Audio:
                audioItems.append(itm);
                break;
            case .Video:
                videoItems.append(itm);
                break;
            case .Youtube:
                youtubeItems.append(itm);
                break;
            default:
                break;
            }
        }
        
        if (videoItems.count > 0){
            let videoPlayer:NeuralVideoPlayer = NeuralVideoPlayer(frame: defaultPlayerSize);
            let idx = collection.getMediaItems().firstIndex(where: {$0 === withItem}) ?? 0;
            
            videoPlayer.loadVideo(items: collection.getMediaItems(), atIndex: idx, delegate: self);
            self.neuralMediaPlayers[defaultVideoPlayerIdentifier] = videoPlayer;
            
            if let del = self.aviDelegate{
                del.aviPlayerCreated(view: videoPlayer, itemType:.Audio);
            }
        }
        if (audioItems.count > 0){
            let audioPlayer:NeuralAudioPlayer = NeuralAudioPlayer(frame: defaultPlayerSize);
            let idx = collection.getMediaItems().firstIndex(where: {$0 === withItem}) ?? 0;
            
            audioPlayer.loadAudio(items: collection.getMediaItems(), atIndex: idx, delegate: self);
            self.neuralMediaPlayers[defaultAudioPlayerIdentifier] = audioPlayer;
            
            if let del = self.aviDelegate{
                del.aviPlayerCreated(view: audioPlayer, itemType:.Video);
            }
        }
        if (youtubeItems.count > 0){
            let youtubePlayer:NeuralYouTubePlayer = NeuralYouTubePlayer(frame: defaultPlayerSize);
            let idx = collection.getMediaItems().firstIndex(where: {$0 === withItem}) ?? 0;
            
            var cDel: CustomThirdPartyPlayerDelegate? = nil;
            if let del = self.aviDelegate{
                cDel = del.aviPlayerRequiresCustomerPlayerDelegate(type: .Youtube);
            }

            youtubePlayer.loadVideo(items: collection.getMediaItems(), atIndex: idx, delegate: self, customPlayerDelegate: cDel);
            self.neuralMediaPlayers[defaultYoutubePlayerIdentifier] = youtubePlayer;
        }

        // Make sure to connect all servies for player.
        self.connectServices();
    }
    
    public func editFor(item: MediaItemsProtocol, type: MediaEditingType, delegate: AVIDelegate) {
        
        switch (item.type) {
        case .Audio:
            var audioPlayer: NeuralAudioPlayer? = nil;
            if let p = self.neuralMediaPlayers[defaultAudioEditorIdentifier] as? NeuralAudioPlayer{
                audioPlayer = p;
            }else{
                audioPlayer = NeuralAudioPlayer(frame: defaultPlayerSize);
                self.neuralMediaPlayers[defaultAudioEditorIdentifier] = audioPlayer;
            }
            audioPlayer!.editAudioFor(item: item, type: type, delegate: self);
            
            if let del = self.aviDelegate{
                del.aviPlayerCreated(view: audioPlayer!, itemType:.Audio);
            }
            break;
        case .Video:
            var videoPlayer: NeuralVideoPlayer? = nil;
            if let p = self.neuralMediaPlayers[defaultAudioEditorIdentifier] as? NeuralVideoPlayer{
                videoPlayer = p;
            }else{
                videoPlayer = NeuralVideoPlayer(frame: defaultPlayerSize);
                self.neuralMediaPlayers[defaultVideoEditorIdentifier] = videoPlayer;
            }
            videoPlayer!.editVideoFor(item: item, type: type, delegate: self);
            
            if let del = self.aviDelegate{
                del.aviPlayerCreated(view: videoPlayer!, itemType:.Video);
            }
            break;
        case .Youtube:
            // no editing for youtube
            break;
        default:
            break;
        }
    }
    
    public func closePlayer() {
        for player in self.neuralMediaPlayers.values{
            player.closePlayer();
        }
        self.neuralMediaPlayers.removeAll();
        self.disconnectServices();
    }
    
    public func closeEditor() {
        //TODO: Can we do anything better to improve code.
        if let v = self.neuralMediaPlayers[defaultVideoEditorIdentifier] as? NeuralVideoPlayer{
            v.closePlayer();
        }
        self.neuralMediaPlayers.removeValue(forKey: defaultVideoEditorIdentifier);

        if let v = self.neuralMediaPlayers[defaultAudioEditorIdentifier] as? NeuralAudioPlayer{
            v.closePlayer();
        }
        self.neuralMediaPlayers.removeValue(forKey: defaultAudioEditorIdentifier);
    }
    
    public func canPlayItem (item : MediaItemsProtocol?) -> Bool {
        if (item == nil) {
            return false;
        }
        //TODO: Needs to work on: On what condition basis an item can be decided to be playable.
        return true;
    }
    
    public func favoriteItem(_ mediaItem: MediaItemsProtocol?, isFav: Bool) {
        
        guard let item = mediaItem else{
            Logger.shared.log("Cannot parse nil item");
            return;
        }
        switch item.type {
        case .Youtube:
            if (isFav){
                YouTubeManager.shared.addFavoriteItem(item);
            }else{
                YouTubeManager.shared.removeFavoriteItem(item);
            }
            break;
        default:
            break;
        }
        item.favorite = isFav;
    }
    
    private func connectServices() {
        // start listening remote events.
        self.listenNotifications();
        self.listenRemoteCommandEvents();
    }
    
    private func disconnectServices() {
        self.removeNotifcations();
    }
}


//MARK: Player Controls
// These methods would be called from outside world to control the players.
extension (AVIManager){
    /// @brief: true = play , false = stop
    public func playStop(_ playStop:Bool, item:MediaItemsProtocol?) {
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.play = playStop;
            }
        }else{
            // For nil controlling all media players.
            for var view in self.neuralMediaPlayers.values{
                view.play = playStop;
            }
        }
    }
    /// @brief: Toggle the previous state.
    public func resumePause(toggle item:MediaItemsProtocol?) {
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.resume = !(view.resume);
                if let del = self.aviDelegate{
                    del.aviPlayer(resumePause: view.resume);
                }
            }
        }else{
            // For nil controlling all media players.
            for var view in self.neuralMediaPlayers.values{
                view.resume = !(view.resume);
                if let del = self.aviDelegate{
                    del.aviPlayer(resumePause: view.resume);
                }
            }
        }
    }
    /// @brief: true = resume , false = pause
    public func resumePause(_ resumePause:Bool, item:MediaItemsProtocol?) {
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.resume = resumePause;
            }
        }else{
            // For nil controlling all media players.
            for var view in self.neuralMediaPlayers.values{
                view.resume = resumePause;
            }
        }
    }
    /// @brief: true = mute , false = unmute
    public func muteUnmute(_ muteUnmute:Bool, item:MediaItemsProtocol?){
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.mute = muteUnmute;
            }
        }else{
            // For nil controlling all media players.
            for var view in self.neuralMediaPlayers.values{
                view.mute = muteUnmute;
            }
        }
    }
    public func setVolume(_ volume: Float, item:MediaItemsProtocol?){
        // update the volume of the player. Master volume is system volume.
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.volume = volume;
            }
        }
    }
    public func setBrightness(_ brightness: Float, item: MediaItemsProtocol?){
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.brightness = brightness;
            }
        }
    }
    public func totalTime(item: MediaItemsProtocol?) -> Float64 {
        if let i = item{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                return view.totalTime
            }
        }
        return 0;
    }
    public func elapsedTime(item: MediaItemsProtocol?) -> Float64 {
        if let i = item{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                return view.elapsedTime
            }
        }
        return 0;
    }
    public func remainingTime(item: MediaItemsProtocol?) -> Float64 {
        if let i = item{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                return view.remainingTime
            }
        }
        return 0;
    }
    public func screenShot(item: MediaItemsProtocol?) -> UIImage? {
        if let i = item{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1108), nil)
                return view.screenShot
            }
        }
        return nil;
    }
    public func advanceToNextItem(_ refItem: MediaItemsProtocol?) {
        // method will advance to next item. If there is any reference item provided then the next item to the reference item will be played
        if let i = refItem{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.playNextItem();
            }
        }else{
            //TODO: What to do here, caller has not supplied any reference item. Right now moving all players to next item.
            for view in self.neuralMediaPlayers.values {
                view.playNextItem();
            }
        }
    }
    public func advanceToPrevItem(_ refItem: MediaItemsProtocol?) {
        // method will move back to previous item from the reference item provided.
        if let i = refItem{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.playPrevItem();
            }
        }else{
            //TODO: What to do here, caller has not supplied any reference item. Right now moving all players to next item.
            for view in self.neuralMediaPlayers.values {
                view.playPrevItem();
            }
        }
    }
    public func jumpTo(item:MediaItemsProtocol?, index: Int){
        // jumps to specific item. Item should be in collection.
        if let i = item{
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                if let _ = (YouTubeManager.shared.hasPlaylistIdentifier(item: i)) {
                    view.jumpTo(index: index);
                }else{
                    view.jumpTo(item: i);
                }
                if let ad = self.aviDelegate {
                    ad.aviPlayer(updatingItem: item, previousItem: nil);
                }
            }
        }
    }
    public func shuffleUnShuffle(_ shuffleUnShuffle:Bool, item:MediaItemsProtocol?) {
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.shuffleUnShuffle = shuffleUnShuffle;
            }
        }
    }
    public func setSeek(percent value:Float, item:MediaItemsProtocol?){
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                view.seekPercent = value;
            }
        }
    }
    public func getSeek(percent item:MediaItemsProtocol?) -> Float{
        if let i = item{
            if var view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                return view.seekPercent;
            }
        }
        return 0;
    }
    public func edit(item: MediaItemsProtocol?, type: MediaEditingType, start: CGFloat, end: CGFloat, rate: CGFloat, cutMedia:Bool){
        if let i = item {
            if let view = self.neuralMediaPlayers.values.first(where: {$0.containsItem(i)}){
                return view.edit(item:i, type:type, start:start, end:end, rate:rate, cutMedia:cutMedia);
            }
        }
    }
    public func saveImage(image : UIImage, item: MediaItemsProtocol?) -> Bool{
        let dirPath = self.screenShotDirectoryPath(type: item?.type ?? .Video).0;
        
        if (dirPath.count <= 0){
            return false;
        }
        if (nlUtils.shared.createDirectory(name: dirPath).1 != nil){
            // there is some error in creating the directory. and the error is returned in 1st path.
            return false;
        }
        let path = dirPath + "/" + nlUtils.shared.timeStampFile(name: "ScreenShots", extn: ".jpg");
        if (nlUtils.shared.write(image: image, atPath: path)) {
            // unable to write image.
            return false;
        }
        return true;
    }
    
    public func getScreenShotImages() -> [MediaItemsProtocol]{
        var retVal: [MediaItemsProtocol] = [];
        var dirsToScan: [String] = [];
        dirsToScan.append(self.screenShotDirectoryPath(type: .Video).0);
        dirsToScan.append(self.screenShotDirectoryPath(type: .Youtube).0);

        for dir in dirsToScan {
            retVal += AssetManager.shared.fetchImagesFrom(path: dir);
        }
        
        return retVal;
    }
}

extension(AVIManager){
    // returns the releative path for screenshots directory;
    fileprivate func screenShotDirectoryPath(type: MediaItemType) -> (AbsoluteFilePath, RelativeFilePath) {
        var abs: AbsoluteFilePath = "";
        var rel: RelativeFilePath = "";
        switch(type) {
        case .Video:
            rel = "/ScreenShots/Videos";
            break;
        case .Youtube:
            rel = "/ScreenShots/YouTube";
            break;
        default:
            break;
        }
        if let dd = nlUtils.shared.documentDirectory(){
            abs = dd + rel;
        }
        return (abs, rel);
    }
    public func clipperDirectoryPath(type: MediaItemType) -> String {
        var retVal = "";
        switch(type) {
        case .Video:
            if let d = nlUtils.shared.documentDirectory() {
                retVal = d+"/Clipper/Videos";
            }
            break;
        case .Audio:
            if let d = nlUtils.shared.documentDirectory() {
                retVal = d+"/Clipper/Audios";
            }
            break;
        default:
            break;
        }
        return retVal;
    }
    public func slowMotionDirectoryPath(type: MediaItemType) -> String {
        var retVal = "";
        switch(type) {
        case .Video:
            if let d = nlUtils.shared.documentDirectory() {
                retVal = d+"/SlowMotion/Videos";
            }
            break;
        case .Audio:
            if let d = nlUtils.shared.documentDirectory() {
                retVal = d+"/SlowMotion/Audios";
            }
            break;
        default:
            break;
        }
        return retVal;
    }
    public func audioFromVideoDirectoryPath() -> String {
        var retVal = "";
        if let d = nlUtils.shared.documentDirectory() {
            retVal = d+"/AudioFromVideo/";
        }
        return retVal;
    }
}


//MARK: NeuralMediaPlayer Delegate Stubs
extension (AVIManager){
    public func neuralPlayerStarted(withItem item:MediaItemsProtocol?){
        if let del = self.aviDelegate{
            del.aviPlayerStartedPlaying(withItem: item, inCollection: self.collection);
        }
    }
    public func neuralPlayer(items:[MediaItemsProtocol]?, limitReached:Bool) -> Bool{
        var retVal = false;
        if let del = self.aviDelegate{
            retVal = del.aviPlayerCanRestart(forCollection: self.collection);
        }
        return retVal;
    }
    public func neuralPlayerFinishedPlaying(withItems items:[MediaItemsProtocol]?){
        if let del = self.aviDelegate{
            del.aviPlayerFinishedPlaying(withCollection: self.collection);
        }
    }
    public func neuralPlayerError(inItem item:MediaItemsProtocol?, error: String){
        if let del = self.aviDelegate{
            del.aviPlayer(error: error, inPlayingItem: item, inCollection: self.collection);
        }
    }
    public func neuralPlayerCanShuffleItems() -> Bool{
        if let del = self.aviDelegate{
            return del.aviPlayerCanShuffleItems(inCollection: self.collection);
        }
        return false;
    }
    public func neuralPlayerUpdating(newItem: MediaItemsProtocol?, oldItem: MediaItemsProtocol?) {
        if let del = self.aviDelegate{
            del.aviPlayer(updatingItem: newItem, previousItem: oldItem);
        }
    }
    public func neuralPlayerTimings(totalTime: TotalMediaTime, elapsedTime: ElapsedMediaTime, remainingTime: RemainingMediaTime) {
        if let del = self.aviDelegate{
            del.aviPlayer(totalTime: totalTime, elapsedTime: elapsedTime, remainingTime: remainingTime);
        }
    }
    public func neuralPlayer(resumePause: Bool){
        if let del = self.aviDelegate{
            del.aviPlayer(resumePause: resumePause);
        }
    }
    public func neuralPlayer(playStop: Bool){
        if let del = self.aviDelegate{
            del.aviPlayer(playStop: playStop);
        }
    }
    public func neuralPlayer(muteUnMute: Bool){
        if let del = self.aviDelegate{
            del.aviPlayer(muteUnMute: muteUnMute);
        }
    }
    public func neuralEditor(error: String, type: MediaEditingType){
        if let del = self.aviDelegate{
            del.aviEditor(error: error, type: type);
        }
    }
    public func neuralEdited(object: MediaItemsProtocol, url: URL, type: MediaEditingType){
        if let del = self.aviDelegate{
            del.aviEdited(object: object, url: url, type: type);
        }
    }
}

//MARK: System Volume extension
extension (AVIManager){
    public var systemVolume: Float {
        get{
            var retVal: Float = AVAudioSession.sharedInstance().outputVolume;
            if (_systemVolume != nil){
                retVal = _systemVolume!;
            }
            return retVal;
        }set{
            // iOS does not allows to change the system volume. This really makes sense that any app cannot change system volume. Otherwise it would be a big blunder.
            _systemVolume = newValue;
        }
    }
    
    public func hideSystemVolumeView(_ view:UIView) {
        // Keep native volume vide out of bounds.
        let volumeView = MPVolumeView(frame: CGRect(x: nlUtils.shared.screenSize.width, y: nlUtils.shared.screenSize.height, width: 100, height: 100))
        volumeView.isHidden = false
        // as per docs native volume view cannot be hidden so making its alpha to negligible.
        volumeView.alpha = 0.01
        view.addSubview(volumeView);
    }
}

//MARK: Notifications
extension (AVIManager){
    public func listenNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onVolumeDidChange(sender:)), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil);
    }
    
    public func removeNotifcations() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil);
    }
    
    @objc func onVolumeDidChange(sender: Notification){
        let vol = sender.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as? Float
        Logger.shared.log("System Volume changed \(String(describing: vol))");
        self.systemVolume = vol ?? 0;
        NotificationController.noti_systemVolumeChanged();
    }
}


//MARK: External Remote command
extension (AVIManager){

    public func remoteCommandTriggered(_ command: UIEvent.EventSubtype) {
        // Remote command from external source has been triggered.
        
        switch command {
        case .none:
            break;
        case .motionShake:
            break;
        case .remoteControlPlay:
            self.playStop(true, item: nil)
            if let del = self.aviDelegate{
                del.aviPlayer(playStop: true);
            }
            break;
        case .remoteControlPause:
            self.resumePause(false, item: nil)
            if let del = self.aviDelegate{
                del.aviPlayer(resumePause: false);
            }
            break;
        case .remoteControlStop:
            self.playStop(false, item: nil)
            if let del = self.aviDelegate{
                del.aviPlayer(playStop: false);
            }
            break;
        case .remoteControlTogglePlayPause:
            self.resumePause(toggle: nil);
            break;
        case .remoteControlNextTrack:
            self.advanceToNextItem(nil);
            break;
        case .remoteControlPreviousTrack:
            self.advanceToPrevItem(nil);
            break;
        case .remoteControlBeginSeekingBackward:
            break;
        case .remoteControlEndSeekingBackward:
            break;
        case .remoteControlBeginSeekingForward:
            break;
        case .remoteControlEndSeekingForward:
            break;
        }
    }
    
    func listenRemoteCommandEvents () {
        MPRemoteCommandCenter.shared().playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            print("Play Command Arrived");
            self.resumePause(toggle: nil);
            return .success;
        }
        MPRemoteCommandCenter.shared().pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            print("Pause Command Arrived");
            self.resumePause(toggle: nil);
            return .success;
        }
    }
}
