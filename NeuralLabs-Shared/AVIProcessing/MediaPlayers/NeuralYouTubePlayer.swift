//
//  NeuralYouTubePlayer.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 06/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import AVFoundation


final class NeuralYouTubePlayer: NeuralBaseView, NeuralMediaControlDelegate {
    
    
    // do not use this. this is only to keep the thing in current structure. custom player is going to do all stuff.
    private var videoItems: [MediaItemsProtocol] = [];
    private var neuralPlayerDelegate: NeuralPlayerDelegate? = nil;
    private var customPlayerDelegate: CustomThirdPartyPlayerDelegate? = nil;
    
    private var timer: Timer? = nil;


    //MARK: Video Loading
    func loadVideo(items: [MediaItemsProtocol], atIndex:Int, delegate: NeuralPlayerDelegate, customPlayerDelegate:CustomThirdPartyPlayerDelegate?) {
        
        self.neuralPlayerDelegate = delegate;
        self.customPlayerDelegate = customPlayerDelegate;
        self.videoItems = items;

        if let d = self.customPlayerDelegate {
            d.loadView(withItems: items, startIndex: atIndex, delegate: self.neuralPlayerDelegate);
        }
        
        weak var weakSelf = self;
        self.timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (t) in
            if let del = self.neuralPlayerDelegate {
                del.neuralPlayerTimings(totalTime: (weakSelf!.totalTime), elapsedTime: (weakSelf!.elapsedTime), remainingTime: (weakSelf!.remainingTime));
            }
        }
    }
    
    func editYouTubeFor(item: MediaItemsProtocol, type: MediaEditingType, delegate: NeuralPlayerDelegate) {
        // currently therer is no editing avaliable for youtube.
    }

}


//MARK: Player Cleanup
extension (NeuralYouTubePlayer) {
    func clearYouTubePlayer() {
        self.play = false;
        if let d = self.customPlayerDelegate {
            d.closePlayer();
        }
        if let del = self.neuralPlayerDelegate {
            del.neuralPlayerFinishedPlaying(withItems: self.videoItems)
        }
    }
}


//MARK: NeuralMediaControlDelegate Stub Methods
extension(NeuralYouTubePlayer){
    var play:Bool{
        get{
            if let d = self.customPlayerDelegate{
                return d.play;
            }
            return false;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.play = newValue;
            }
        }
        
    }
    var resume:Bool{
        get{
            if let d = self.customPlayerDelegate{
                return d.resume;
            }
            return false;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.resume = newValue;
            }
        }
    }
    var mute:Bool{
        get{
            if let d = self.customPlayerDelegate{
                return d.mute
            }
            return false;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.mute = newValue;
            }
        }
    }
    var shuffleUnShuffle:Bool{
        get{
            // shuffling setting is not a responsibility of player. Caller should let the play know at the time of usage throught delegate. A delegate is already there.
            return false;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.shuffleUnShuffle = newValue;
            }
        }
    }
    var volume:Float {
        get{
            if let d = self.customPlayerDelegate{
                return d.volume
            }
            return 0;
        }
        set (newValue){
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.volume = newValue;
            }
        }
    }
    var brightness:Float {
        get{
            if let d = self.customPlayerDelegate{
                return d.brightness
            }
            return 0;
        }
        set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.brightness = newValue;
            }
        }
    }
    var totalTime:TimeInterval {
        get{
            if let d = self.customPlayerDelegate{
                return d.totalTime
            }
            return 0;
        }
    }
    var elapsedTime:TimeInterval {
        get{
            if let d = self.customPlayerDelegate{
                return d.elapsedTime
            }
            return 0;
        }
    }
    var remainingTime:TimeInterval {
        if let d = self.customPlayerDelegate{
            return d.remainingTime
        }
        return 0;
    }
    var screenShot:UIImage? {
        get{
            if let d = self.customPlayerDelegate{
                return d.screenShot
            }
            return nil;
        }
    }
    var rate: Float {
        get{
            if let d = self.customPlayerDelegate{
                return d.rate
            }
            return 0;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.rate = newValue;
            }
        }
    }
    var seekPercent:Float {
        get{
            if let d = self.customPlayerDelegate{
                return d.seekPercent
            }
            return 0;
        }set{
            if (self.customPlayerDelegate != nil){
                (self.customPlayerDelegate)!.seekPercent = newValue;
            }
        }
    }
    
    func containsItem(_ item:MediaItemsProtocol) -> Bool{
        if let d = self.customPlayerDelegate{
            return d.containsItem(_:item);
        }
        return false;
    }
    
    func playNextItem() {
        if let d = self.customPlayerDelegate{
            return d.playNextItem();
        }
    }
    
    func playPrevItem() {
        if let d = self.customPlayerDelegate{
            return d.playPrevItem();
        }
    }
    
    func jumpTo(item: MediaItemsProtocol){
        if let d = self.customPlayerDelegate{
            return d.jumpTo(item:item);
        }
    }
    
    func jumpTo(index: Int){
        if let d = self.customPlayerDelegate{
            return d.jumpTo(index:index);
        }
    }
    
    func edit(item: MediaItemsProtocol?, type: MediaEditingType, start: CGFloat, end: CGFloat, rate: CGFloat, cutMedia: Bool) {
        
    }
    
    func closePlayer(){
        // A final shutdown of player
        self.clearYouTubePlayer();
        self.neuralPlayerDelegate = nil;
        self.timer?.invalidate();
        self.removeFromSuperview();
    }
}


//MARK: Notifications
extension (NeuralYouTubePlayer){
    func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationDidEnterBackground(_:)), name: nlApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationWillResignActive(_:)), name: nlApplicationWillResignActive, object: nil)
    }
    func removeNotifications () {
        NotificationCenter.default.removeObserver(self, name: nlApplicationDidEnterBackground, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlApplicationWillResignActive, object: nil);
    }
    @objc func onApplicationDidEnterBackground(_ sender: Notification){
        self.resume = false;
        guard let del = self.neuralPlayerDelegate else{
            return;
        }
        del.neuralPlayer(resumePause: self.resume);
    }
    @objc func onApplicationWillResignActive(_ sender: Notification){
    }
}

