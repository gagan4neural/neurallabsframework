//
//  NeuralVideoPlayer.swift
//  NeuralLabs
//
//  Created by Gagan on 10/11/18.
//  Copyright © 2018 Neural Labs. All rights reserved.
//

import UIKit
import AVFoundation

final class NeuralVideoPlayer: NeuralBaseView, NeuralMediaControlDelegate {

    private var videoItems:[MediaItemsProtocol]? = nil;
    private var editItem: MediaItemsProtocol? = nil;
    private var editingType: MediaEditingType = .Unknown;
    private var neuralPlayerDelegate: NeuralPlayerDelegate? = nil;
    
    
    var composition: AVMutableComposition? = nil;
    var videoTrack: AVMutableCompositionTrack? = nil;
    var audioTrack: AVMutableCompositionTrack? = nil;
    var compositionPrepared = false

    
    
    private var playingItemSequence: Int = 0;
    private var playingItem: MediaItemsProtocol?{
        get{
            if (self.videoItems != nil) && (self.videoItems!.count > 0) && (self.playingItemSequence >= 0) && (self.playingItemSequence < self.videoItems!.count){
                return self.videoItems![self.playingItemSequence];
            }
            return nil;
        }
    }
    // order in which items needs to be played.
    private var _itemSequenceOrder: [Int] = [];
    private var itemSequenceOrder:[Int] {
        get{
            if (_itemSequenceOrder.count <= 0){
                if let del = self.neuralPlayerDelegate {
                    if (del.neuralPlayerCanShuffleItems() == true){
                        _itemSequenceOrder = Array<Int>.randomIndexedArray(limit: self.videoItems!.count);
                    }else{
                        _itemSequenceOrder = Array<Int>.uniformIndexedArray(limit: self.videoItems!.count, assending: true);
                    }
                }
            }
            return _itemSequenceOrder;
        }
    }
    
    private var queuePlayer:AVQueuePlayer? = nil;
    private var playerLayer:AVPlayerLayer? = nil;
    private var timeObserver: Any? = nil;
    private var boundaryObserver: Any? = nil;
    private var lockQueuePlayer: DispatchSemaphore = DispatchSemaphore(value: 1)
    private var lockPlayerLayer: DispatchSemaphore = DispatchSemaphore(value: 1)
    private var avPlayerItems:[AVPlayerItem] = [];
    
    private var _playStop:Bool = false;
    private var _pauseResume:Bool = false;
    private var _playerUnknownStatusCount:Int = 0;
    

    //MARK: Video Loading
    func loadVideo(items: [MediaItemsProtocol], atIndex:Int, delegate: NeuralPlayerDelegate) {
        self.neuralPlayerDelegate = delegate;
        self.videoItems = items;
        
        if (atIndex >= 0) && (atIndex < items.count){
            self.playingItemSequence = atIndex;
        }else{
            self.playingItemSequence = 0;
        }
        self.prepareItemsForPlayer(canStartPlay: true);
    }
    
    func editVideoFor(item: MediaItemsProtocol, type: MediaEditingType, delegate: NeuralPlayerDelegate) {
        Logger.shared.log("------>>>>>>>> Editing for Video");
        self.editItem = item;
        self.neuralPlayerDelegate = delegate;
        self.editingType = type;
        self.videoItems = [item];
        self.playingItemSequence = 0;

        self.prepareItemsForPlayer(canStartPlay: true);
    }

    
    /// @brief extract avplayer items from MediaItemsProtocol
    private func prepareItemsForPlayer(canStartPlay: Bool){
        if (self.videoItems == nil) || (self.videoItems!.count <= 0){
            Logger.shared.log(#function, "Cannot prepare player with no video items");
            return;
        }
        if (self.playingItemSequence < 0) || (self.playingItemSequence > self.videoItems!.count){
            Logger.shared.log(#function, "Cannot request asset for out of bound index");
            return;
        }
        var itemSequence:[Int] = self.itemSequenceOrder;
        // for the array [1,0,2] if the playing sequence is 0 the idx would be 1
        let idx = itemSequence.firstIndex(of: self.playingItemSequence);
        if (idx != nil) && (idx != 0){
            // rotate the sequence matrix by idx.
            // for 0 there is no sense is making rotation. The playing sequence is at first location.
            itemSequence.relativeRotateLeft(offset: idx!);
        }
        
//        Logger.shared.log("Original Item sequence for playing : ", self.itemSequenceOrder.description)
//        Logger.shared.log("Decided Item sequence for playing : ", itemSequence.description)
        
        self.avPlayerItems.removeAll();
        self.play = false;
        weak var weakSelf = self;

        func extractAVPlayerItem(onCompletion: @escaping onCompletionCallback) {
            if (itemSequence.count <= 0){
                // condition to break recurrsion
                onCompletion(true);
                return;
            }
            
            let num = (itemSequence.popFirst())!
            let mediaItem = self.videoItems![num];
            
            mediaItem.requestPlayerItem { (avPlayerItem, info) in
                if let itm = avPlayerItem {
                    weakSelf?.avPlayerItems.append(itm.copy() as! AVPlayerItem);
                }
                if (itemSequence.count > 0){
                    DispatchQueue.main.async(execute: {
                        extractAVPlayerItem(onCompletion: onCompletion);
                    })
                }else{
                    onCompletion(true);
                }
            }
        }
        
        extractAVPlayerItem { (done) in
            if (done == true){
                weakSelf?.appendItemsInPlayer(items: (weakSelf?.avPlayerItems)!);
                if (canStartPlay){
                    weakSelf?.startPlayingVideo();
                }
            }
        }
    }
    
    private func updatePlayingIndexItem(){
        // since items are playing in queueplayer. the playing item index migth gets out of sync from the playing order. Needs to sync the playing index.
        if (self.videoItems == nil) || (self.videoItems!.count <= 0){
            Logger.shared.log(#function, "Cannot update playing index with no video items");
            return;
        }

        var index = 0;
        weak var weakSelf = self;
        var functionComplete = false;
        /*-------------------------------------------------*/
        func searchAVPlayerItem(onCompletion: @escaping onCompletionCallback){
            
            if (index >= self.videoItems!.count){
                // Reached till end of traversing. All items have been played
                index = self.videoItems!.count;
                onCompletion(true);
                return;
            }
            
            if (functionComplete == true){
                //TODO: Look at some better technique to break block based recurssion
                // this is block based recussion needs to break it like this. Will see if there is any other better approach
                return;
            }
            
            let item = self.videoItems![index]
            item.requestPlayerItem { (playerItem, info) in
                // when an item is changing the current item is still under the QueuePlayer
                index += 1;
                if (weakSelf?.queuePlayer?.currentItem == playerItem){
                    functionComplete = true;
                    // Found the next player item. Now its time to play that item.
                    onCompletion(true);
                }else{
                    searchAVPlayerItem(onCompletion: onCompletion);
                }
            }
        }
        /*-------------------------------------------------*/
        func playerItemUpdate(oldIndex: Int, newIndex: Int){
            
            Logger.shared.log("New Index : \(newIndex), OldIndex \(oldIndex)")
            
            if (oldIndex == newIndex){
                Logger.shared.log("No Change in player items detected.");
                return;
            }
            
            if (newIndex < self.videoItems!.count){
                if let del = self.neuralPlayerDelegate{
                    self.playingItemSequence = newIndex;
                    let oldItm = self.videoItems![oldIndex];
                    let newItm = self.videoItems![newIndex];
                    del.neuralPlayerUpdating(newItem: newItm, oldItem: oldItm);
                }
                self.queuePlayer?.advanceToNextItem();
            }else{
                self.playerItemsLimitReached();
            }
        }
        
        searchAVPlayerItem { (done) in
            if (done == true){
                playerItemUpdate(oldIndex: (weakSelf?.playingItemSequence)!, newIndex: index);
            }
        }
    }
    
    private func playerItemsLimitReached() {
        // All items in the AVQueuePlayer have been played and now its time to ask caller to re-play or to stop.
        // Caller can have some setting for this or can have a user interaction.
        if let del = self.neuralPlayerDelegate {
            let canRestart = del.neuralPlayer(items: self.videoItems, limitReached: true);
            if (canRestart == true){
                self.playingItemSequence = 0;
                self.prepareItemsForPlayer(canStartPlay: true);
            }else{
                del.neuralPlayerFinishedPlaying(withItems: self.videoItems);
            }
        }
    }
    
    //MARK: Layouting
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        if (self.playerLayer != nil){
            self.playerLayer?.frame = layer.bounds;
        }
    }

    
    //MARK: Core Functions
    private func initializePlayer(items: [AVPlayerItem]){
        if (self.videoItems!.count <= 0){
            Logger.shared.log("Cannot initilize AVQueuePlayer with 0 video items");
            return;
        }

        if (self.avPlayerItems.count < 1){
            Logger.shared.log("Player should have atleast 1 AvPlayer item to startwith.");
            return;
        }
        
        if (self.queuePlayer != nil){
            Logger.shared.log("Queue Player already initialized. NeuralVideoPlayer design does not allow 2 AVQueuePlayers")
            return;
        }
        
        defer{
            lockQueuePlayer.signal()
        }
        
        lockQueuePlayer.wait();

        weak var weakSelf = self;
        self.queuePlayer = AVQueuePlayer(items: self.avPlayerItems);
        self.queuePlayer?.actionAtItemEnd = AVPlayer.ActionAtItemEnd.advance;
        
        let interval = CMTime(seconds: 1, preferredTimescale: 10);
        self.timeObserver = self.queuePlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (cmTime) in
            if let del = weakSelf?.neuralPlayerDelegate{
                del.neuralPlayerTimings(totalTime: (weakSelf?.totalTime)!, elapsedTime: (weakSelf?.elapsedTime)!, remainingTime: (weakSelf?.remainingTime)!);
            }
        });
        // This is the core level dependency on video. If QueuePlayer is initialized then playerlayer must also be initialized.
        self.initializePlayerLayer();
        
        self.removeNotifications();
        self.listenNotifications();
    }
    
    private func appendItemsInPlayer(items: [AVPlayerItem]) {
        if (self.queuePlayer == nil){
            self.initializePlayer(items:items);
        }else{
            self.queuePlayer?.removeAllItems();
            for itm in items{
                if (self.queuePlayer?.canInsert(itm, after: self.queuePlayer?.items().last) == true){
                    itm.seek(to: CMTime.zero, completionHandler: nil);
                    self.queuePlayer?.insert(itm, after: self.queuePlayer?.items().last);
                }
            }
        }
    }
    
    private func initializePlayerLayer(){
        
        if (self.playerLayer != nil){
            Logger.shared.log("PlayerLayer already initialized. NeuralVideoPlayer design does not allow 2 AVPlayerLayer");
            return;
        }
        
        defer{
            lockPlayerLayer.signal();
        }
        lockPlayerLayer.wait();
        
        self.playerLayer = AVPlayerLayer(player: self.queuePlayer);
        self.layer.addSublayer(self.playerLayer!);

        self.playerLayer?.backgroundColor = UIColor.black.cgColor
        self.playerLayer?.videoGravity = .resizeAspect;
    }
    
    private func startPlayingVideo(){
        _playerUnknownStatusCount = 0;
        self.seekToZero();
        self.waitForPlayerToGetReady();
    }
    
    private func waitForPlayerToGetReady() {
        
        switch (self.queuePlayer?.status)!{
        case .unknown:
            self._playerUnknownStatusCount += 1;
            if (self._playerUnknownStatusCount >= maxPlayerFailuresAllowed){
                if let del = self.neuralPlayerDelegate {
                    del.neuralPlayerError(inItem: self.playingItem, error: "Player not responding to selected file.");
                }
            }else{
                // waiting for next 0.5 seconds to player to get ready
                weak var weakSelf = self;
                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in
                    weakSelf?.waitForPlayerToGetReady();
                    weakSelf?.seekToZero();
                }
            }
            break;
        case .readyToPlay:
            self.queuePlayer?.play();
            if let del = self.neuralPlayerDelegate {
                del.neuralPlayerStarted(withItem: self.playingItem);
            }
            break;
        case .failed:
            if let del = self.neuralPlayerDelegate {
                del.neuralPlayerError(inItem: self.playingItem, error: "An unknown exception occured while playing.");
            }
            break;
        }
    }
    
    private func seekToZero(){
        self.seekToTime(CMTime.zero);
    }
    
    private func seekToTime(_ time: CMTime){
        self.queuePlayer?.seek(to: time);
    }
}


//MARK: Player CleanUp
extension(NeuralVideoPlayer){
    private func clearAllItemsFromQueuePlayer() {
        if (self.queuePlayer != nil){
            self.queuePlayer?.removeAllItems();
        }
    }
    private func clearQueuePlayer() {
        if (self.queuePlayer != nil){
            if (self.timeObserver != nil){
                self.queuePlayer?.removeTimeObserver(self.timeObserver!);
            }
            if (self.boundaryObserver != nil){
                self.queuePlayer?.removeTimeObserver(self.boundaryObserver!);
            }
            self.queuePlayer?.removeAllItems();
        }
        self.avPlayerItems.removeAll();
        self.queuePlayer = nil;
    }
    
    private func clearPlayerLayer() {
        if (self.playerLayer != nil){
            self.playerLayer?.removeFromSuperlayer();
        }
        self.playerLayer = nil;
    }
    
    private func clearVideoPlayer () {
        // A complete video player clear...
        self.removeNotifications();
        self.clearQueuePlayer();
        self.clearPlayerLayer();
        
        if let del = self.neuralPlayerDelegate {
            del.neuralPlayerFinishedPlaying(withItems: self.videoItems)
        }
    }
}

//MARK: NeuralMediaControlDelegate Stub Methods
extension(NeuralVideoPlayer){
    var play:Bool{
        get{
            return _playStop;
        }set{
            _playStop = newValue;
            if (newValue){
                self.startPlayingVideo();
            }else{
                self.queuePlayer?.pause()
                self.queuePlayer?.rate = 0;
                self.clearAllItemsFromQueuePlayer();
            }
        }
    }
    var resume:Bool{
        get{
            return _pauseResume;
        }set{
            _pauseResume = newValue;
            if (newValue){
                self.queuePlayer?.play()
            }else{
                self.queuePlayer?.pause()
            }
        }
    }
    var mute:Bool{
        get{
            return self.queuePlayer?.isMuted ?? false
        }set{
            self.queuePlayer?.isMuted = newValue;
        }
    }
    var shuffleUnShuffle:Bool{
        get{
            // shuffling setting is not a responsibility of player. Caller should let the play know at the time of usage throught delegate. A delegate is already there.
            return false;
        }set{
            _itemSequenceOrder.removeAll();
            self.clearAllItemsFromQueuePlayer();
            self.prepareItemsForPlayer(canStartPlay: true);
        }
    }
    var volume:Float {
        get{
            return self.queuePlayer?.volume ?? 0.0;
        }
        set (newValue){
            var shouldMute:Bool = false;
            var vol: Float = 0.0;
            if (newValue < 0.0){
                shouldMute = true;
                vol = 0.0;
            }else if (newValue > 1.0){
                vol = 1.0;
            }
            self.queuePlayer?.volume = vol;
            self.mute = shouldMute;
        }
    }
    var brightness:Float {
        get{
            return Float(nlUtils.shared.systemBrightness);
        }
        set{
            nlUtils.shared.systemBrightness = CGFloat(newValue);
        }
    }
    var totalTime:TimeInterval {
        get{
            let seconds = CMTimeGetSeconds(self.queuePlayer?.currentItem?.duration ?? CMTime.zero);
            return seconds;
        }
    }
    var elapsedTime:TimeInterval {
        get{
            let seconds = CMTimeGetSeconds(self.queuePlayer?.currentItem?.currentTime() ?? CMTime.zero);
            return seconds;
        }
    }
    var remainingTime:TimeInterval {
        get{
            return self.totalTime - self.elapsedTime;
        }
    }
    var screenShot:UIImage? {
        get{
            if (self.playingItem == nil) ||
                (self.playingItem?.type != .Video) ||
                (self.playerLayer == nil) ||
                (self.queuePlayer == nil) ||
                (self.queuePlayer!.status != .readyToPlay){
                return nil;
            }
            var actualTime: CMTime = CMTime.zero;
            
            guard let asset = self.queuePlayer?.currentItem?.asset else {
                return nil;
            }
            
            do {
                let generator = AVAssetImageGenerator(asset: asset);
                let cgImg = try generator.copyCGImage(at: self.queuePlayer?.currentTime() ?? CMTime.zero, actualTime: &actualTime);
                let img = UIImage(cgImage: cgImg);
                return img;
            } catch let error {
                Logger.shared.log("Error in captiring screenshot \(error)")
            }
            return nil;
        }
    }
    var rate: Float {
        get{
            // setting the normal 1x rate is nil
            return self.queuePlayer?.rate ?? 1.0;
        }set{
            self.queuePlayer?.rate = newValue;
        }
    }
    var seekPercent:Float {
        get{
            var retVal: Float = -1.0
            if queuePlayer == nil {
                return retVal
            }
            let totalTime = Float(exactly: self.totalTime) ?? 1.0;
            let elapsedTime = Float(exactly: self.elapsedTime) ?? 0.0;
            retVal = Float(elapsedTime / totalTime);
            
            return retVal
        }set{
            if (queuePlayer == nil){
                return;
            }
            let percent = (newValue > 1.0) ? 1.0 : ((newValue < 0.0) ? 0.0 : newValue);
            let seekTime = CMTimeMultiplyByFloat64(self.queuePlayer?.currentItem?.duration ?? CMTime.zero, multiplier: Float64(percent));
            self.seekToTime(seekTime);
        }
    }
    
    func containsItem(_ item:MediaItemsProtocol) -> Bool{
        return self.videoItems?.first(where: {$0 === item}) != nil;
    }
    
    func playNextItem() {
        // going by the algorithmic way. queueplayer has the capability to (advanceToNext) but it does not have the capability of (advanceToPrevious) so providing both the capabilities by algorithmic way.
        if (self.videoItems == nil){
            Logger.shared.log("Queue does not contains any item");
            return;
        }
        if (self.videoItems!.count <= 1){
            Logger.shared.log("Queue only contains single item. So cannot make to next item");
            return;
        }

        self.playingItemSequence += 1;
        if (self.playingItemSequence >= self.itemSequenceOrder.count) {
            self.playingItemSequence = 0;
        }
        self.prepareItemsForPlayer(canStartPlay: true);
    }
    
    func playPrevItem() {
        if (self.videoItems == nil){
            Logger.shared.log("Queue does not contains any item");
            return;
        }
        if (self.videoItems!.count <= 1){
            Logger.shared.log("Queue only contains single item. So cannot make to previous item");
            return;
        }
        self.playingItemSequence -= 1;
        if (self.playingItemSequence < 0) {
            self.playingItemSequence = self.itemSequenceOrder.count - 1 ;
        }

        self.prepareItemsForPlayer(canStartPlay: true);
    }

    func jumpTo(item: MediaItemsProtocol){
        if (self.videoItems == nil) || (self.videoItems!.count <= 0){
            Logger.shared.log("Queue does not contains any item");
            return;
        }
        let index = self.videoItems?.firstIndex(where: { return ($0 === item) }) ?? 0
        if (index >= 0) && (index < self.videoItems!.count){
            self.playingItemSequence = index;
        }
        self.prepareItemsForPlayer(canStartPlay: true);
    }
    
    func jumpTo(index: Int){
        // TODO: implement this also.
    }
    
    func closePlayer(){
        // A final shutdown of player
        self.clearVideoPlayer();
        self.neuralPlayerDelegate = nil;
        self.removeFromSuperview();
    }
    
    func edit(item: MediaItemsProtocol?, type: MediaEditingType, start: CGFloat, end: CGFloat, rate: CGFloat, cutMedia:Bool){
        switch type {
        case .Clipping:
            self.cutVideo(item?.url, start: start, end: end);
            break;
        case .SlowMotion:
            self.slowMotionVideo(item?.url, start: start, end: end, rate: rate, cutVideo: cutMedia);
            break;
        case .AudioFromVideo:
            self.extractAudioFromVideo(item?.url, start: start, end: end);
            break;
        default: break;
        }
    }
}


//MARK: Editor Callbacks/Commons
extension (NeuralVideoPlayer){
    func editor(error: String, type: MediaEditingType){
        weak var weakSelf = self;
        DispatchQueue.main.async {
            if let d = weakSelf?.neuralPlayerDelegate{
                d.neuralEditor(error: error, type: type);
            }
        }
    }
    func edited(obj: VideoObject, url: URL, type: MediaEditingType){
        weak var weakSelf = self;
        DispatchQueue.main.async {
            if let d = weakSelf?.neuralPlayerDelegate{
                d.neuralEdited(object: obj, url: url, type: type);
            }
        }
    }
    func clearValues() {
        compositionPrepared = false
        composition = nil
        audioTrack = nil
        videoTrack = nil
    }
    func videoFileType() -> VideoOutputType {
        return ._M4V;
    }
    func audioFileType() -> AudioOutputType {
        return ._M4A;
    }
}

//MARK: SlowMotion / Clip
private extension (NeuralVideoPlayer){
    func slowMotionError(error: String){
        Logger.shared.log("Slow motion error", error);
        self.editor(error: error, type: .SlowMotion);
    }
    func slowMotionVideo(_ url: URL?, start: CGFloat, end: CGFloat, rate: CGFloat, cutVideo cut: Bool) {
        
        clearValues()
        if url == nil {
            self.slowMotionError(error: "Invalid input url.")
            return
        }

        let videoAsset = AVURLAsset(url: url!, options: nil)
        let currentAsset = AVAsset(url: url!)
        var tracks = currentAsset.tracks(withMediaType: .video)
        
        if (tracks.count <= 0) {
            self.slowMotionError(error: "No tracks found.")
            return
        }
        
        let vdoTrack: AVAssetTrack = tracks[0]

        self.composition = AVMutableComposition()
        self.videoTrack = self.composition!.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid);
        self.audioTrack = self.composition!.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid);

        let range = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration);

        do {
            try self.videoTrack?.insertTimeRange(range, of: videoAsset.tracks(withMediaType: .video)[0], at: .zero);
        }catch{
            self.slowMotionError(error: "Exception in inserting time range in video track")
            return;
        }
        
        do{
            try self.audioTrack?.insertTimeRange(range, of: currentAsset.tracks(withMediaType: .audio)[0], at: .zero);
        }catch{
            self.slowMotionError(error: "Exception in inserting time range in audio track")
            return;
        }
        
        let startTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(start))
        let endTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(end))
        
        let scaleFactor: Double = Double((1.0 - rate) + 1.0)
        let videoDuration: CMTime = videoAsset.duration
        
        if cut {
            videoTrack?.removeTimeRange(CMTimeRangeMake(start: endTime, duration: videoAsset.duration))
            audioTrack?.removeTimeRange(CMTimeRangeMake(start: endTime, duration: videoAsset.duration))
        }
        
        videoTrack?.scaleTimeRange(CMTimeRangeMake(start: startTime, duration: endTime), toDuration: CMTimeMake(value: Int64(Double(videoDuration.value) * scaleFactor), timescale: videoDuration.timescale))
        audioTrack?.scaleTimeRange(CMTimeRangeMake(start: startTime, duration: endTime), toDuration: CMTimeMake(value: Int64(Double(videoDuration.value) * scaleFactor), timescale: videoDuration.timescale))

        if cut {
            videoTrack?.removeTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: startTime))
            audioTrack?.removeTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: startTime))
        }
        
        videoTrack?.preferredTransform = vdoTrack.preferredTransform

        let sloMoPath = AVIManager.shared.slowMotionDirectoryPath(type: .Video);
        let sloMoName = nlUtils.shared.timeStampFile(name: "SloMo", extn: self.videoFileType().rawValue);
        let filePath = sloMoPath + sloMoName;
        self.saveSlowMotionComposition(filePath: filePath);
    }
    
    func saveSlowMotionComposition(filePath: String) {
        guard let assetExport = AVAssetExportSession(asset: self.composition!, presetName: AVAssetExportPresetHighestQuality) else{
            self.slowMotionError(error: "Failed to create export session.");
            return;
        }
        let outputFileType = AVFileType.mp4;
        
        assetExport.outputURL = URL(fileURLWithPath: filePath);
        assetExport.outputFileType = outputFileType
        assetExport.shouldOptimizeForNetworkUse = false

        assetExport.exportAsynchronously(completionHandler: {
            switch assetExport.status {
            case .failed:
                let err = "Export session faiied with error: \(String(describing: assetExport.error))"
                self.slowMotionError(error: err);
            case .completed:
                self.slowMotionVideoPrepared(url: assetExport.outputURL);
                break;
            case .cancelled:
                self.slowMotionError(error: "Export Session cancelled");
                break;
            default:
                break
            }
        })
    }
    
    func slowMotionVideoPrepared(url: URL?){
        if (url == nil){
            self.slowMotionError(error: "Cannot create asset with nil url");
            return;
        }
        guard let obj = VideoObject(url: url!) else{
            self.slowMotionError(error: "Failed to create object.");
            return;
        }
        self.edited(obj: obj, url: url!, type: .SlowMotion);
    }
}


//MARK: Video Cutter
extension (NeuralVideoPlayer){
    func cutterError(error: String){
        Logger.shared.log("Cutter error", error);
        self.editor(error: error, type: .Clipping);
    }
    func cutVideo(_ url: URL?, start: CGFloat, end: CGFloat) {
        clearValues()
        
        if url == nil {
            self.cutterError(error: "Invalid input url.")
            return
        }
        
        let videoAsset = AVURLAsset(url: url!, options: nil)
        let currentAsset = AVAsset(url: url!)
        var tracks = currentAsset.tracks(withMediaType: .video)

        if (tracks.count <= 0) {
            self.cutterError(error: "No tracks found.")
            return
        }
        
        let vdoTrack: AVAssetTrack = tracks[0]
        
        self.composition = AVMutableComposition()
        self.videoTrack = self.composition!.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid);
        self.audioTrack = self.composition!.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid);

        let range = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration);
        
        do {
            try self.videoTrack?.insertTimeRange(range, of: videoAsset.tracks(withMediaType: .video)[0], at: .zero);
        }catch{
            self.cutterError(error: "Exception in inserting time range in video track")
            return;
        }
        
        do{
            try self.audioTrack?.insertTimeRange(range, of: currentAsset.tracks(withMediaType: .audio)[0], at: .zero);
        }catch{
            self.cutterError(error: "Exception in inserting time range in audio track")
            return;
        }
        
        let startTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(start))
        let endTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(end))
        
        videoTrack?.removeTimeRange(CMTimeRangeMake(start: endTime, duration: videoAsset.duration))
        audioTrack?.removeTimeRange(CMTimeRangeMake(start: endTime, duration: videoAsset.duration))

        videoTrack?.removeTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: startTime))
        audioTrack?.removeTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: startTime))

        videoTrack?.preferredTransform = vdoTrack.preferredTransform
        
        let clipPath = AVIManager.shared.clipperDirectoryPath(type: .Video);
        let clipName = nlUtils.shared.timeStampFile(name: "Clip", extn: self.videoFileType().rawValue);
        let filePath = clipPath + clipName;
        self.saveClipperComposition(filePath: filePath);
    }
    func saveClipperComposition(filePath: String) {
        guard let assetExport = AVAssetExportSession(asset: self.composition!, presetName: AVAssetExportPresetHighestQuality) else{
            self.cutterError(error: "Failed to create export session.");
            return;
        }
        let outputFileType = AVFileType.mp4;
        
        assetExport.outputURL = URL(fileURLWithPath: filePath);
        assetExport.outputFileType = outputFileType
        assetExport.shouldOptimizeForNetworkUse = false
        
        assetExport.exportAsynchronously(completionHandler: {
            switch assetExport.status {
            case .failed:
                let err = "Export session faiied with error: \(String(describing: assetExport.error))"
                self.cutterError(error: err);
            case .completed:
                self.clipperVideoPrepared(url: assetExport.outputURL);
                break;
            case .cancelled:
                self.cutterError(error: "Export Session cancelled");
                break;
            default:
                break
            }
        })
    }
    func clipperVideoPrepared(url: URL?){
        if (url == nil){
            self.cutterError(error: "Cannot create asset with nil url");
            return;
        }
        guard let obj = VideoObject(url: url!) else{
            self.cutterError(error: "Failed to create object.");
            return;
        }
        self.edited(obj: obj, url: url!, type: .Clipping);
    }
}


extension (NeuralVideoPlayer){
    func audioFromVideoError(error: String){
        Logger.shared.log("Audio From Video error", error);
        self.editor(error: error, type: .AudioFromVideo);
    }
    func extractAudioFromVideo(_ url: URL?, start: CGFloat, end: CGFloat) {
        clearValues()
        
        if url == nil {
            self.audioFromVideoError(error: "Invalid input url.")
            return
        }
        
        let videoAsset = AVURLAsset(url: url!, options: nil)
        let currentAsset = AVAsset(url: url!)
        var tracks = currentAsset.tracks(withMediaType: .video)
        
        if (tracks.count <= 0) {
            self.audioFromVideoError(error: "No tracks found.")
            return
        }
        
        let vdoTrack: AVAssetTrack = tracks[0]
        
        self.composition = AVMutableComposition()
        self.videoTrack = self.composition!.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid);
        self.audioTrack = self.composition!.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid);
        
        let range = CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration);
        
        // We are only interested in Audio here.
        do{
            try self.audioTrack?.insertTimeRange(range, of: currentAsset.tracks(withMediaType: .audio)[0], at: .zero);
        }catch{
            self.audioFromVideoError(error: "Exception in inserting time range in audio track")
            return;
        }
        
        let startTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(start))
        let endTime: CMTime = CMTimeMultiplyByFloat64(videoAsset.duration, multiplier: Float64(end))
        
        audioTrack?.removeTimeRange(CMTimeRangeMake(start: endTime, duration: videoAsset.duration))
        audioTrack?.removeTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: startTime))
        
        audioTrack?.preferredTransform = vdoTrack.preferredTransform

        let afvPath = AVIManager.shared.audioFromVideoDirectoryPath();
        let afvName = nlUtils.shared.timeStampFile(name: "AudioFromVideo", extn: self.audioFileType().rawValue);
        let filePath = afvPath + afvName;
        self.saveAudioFromVideoComposition(filePath: filePath);
    }
    func saveAudioFromVideoComposition(filePath: String) {
        guard let assetExport = AVAssetExportSession(asset: self.composition!, presetName: AVAssetExportPresetHighestQuality) else{
            self.audioFromVideoError(error: "Failed to create export session.");
            return;
        }
        let outputFileType = AVFileType.caf;
        
        assetExport.outputURL = URL(fileURLWithPath: filePath);
        assetExport.outputFileType = outputFileType
        assetExport.shouldOptimizeForNetworkUse = false
        
        assetExport.exportAsynchronously(completionHandler: {
            switch assetExport.status {
            case .failed:
                let err = "Export session faiied with error: \(String(describing: assetExport.error))"
                self.audioFromVideoError(error: err);
            case .completed:
                self.audioFromVideoPrepared(url: assetExport.outputURL);
                break;
            case .cancelled:
                self.audioFromVideoError(error: "Export Session cancelled");
                break;
            default:
                break
            }
        })
    }
    func audioFromVideoPrepared(url: URL?){
        if (url == nil){
            self.audioFromVideoError(error: "Cannot create asset with nil url");
            return;
        }
        guard let obj = AudioObject(url: url!) else{
            self.audioFromVideoError(error: "Failed to create object.");
            return;
        }
        weak var weakSelf = self;
        DispatchQueue.main.async {
            if let d = weakSelf?.neuralPlayerDelegate{
                d.neuralEdited(object: obj, url: url!, type: .AudioFromVideo);
            }
        }
    }
}

//MARK: Notifications
extension (NeuralVideoPlayer){
    func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(playerItemTimeJumped(_:)), name: NSNotification.Name.AVPlayerItemTimeJumped, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemFailedToPlayTillEnd(_:)), name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemPlaybackStalled(_:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemNewAccessLogEntry(_:)), name: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemNewErrorLogEntry(_:)), name: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationDidEnterBackground(_:)), name: nlApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationWillResignActive(_:)), name: nlApplicationWillResignActive, object: nil)
    }
    func removeNotifications () {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil);

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemTimeJumped, object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: nil);

        NotificationCenter.default.removeObserver(self, name: nlApplicationDidEnterBackground, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlApplicationWillResignActive, object: nil);
    }
    @objc func playerItemDidEnd(_ sender: Notification){
        weak var weakSelf = self;
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (t) in
            /* Big NERD explanation
                Code works perfectly when queuePlayer is set to advance mode on item completion. For other cases the code will check else conditions in "updatePlayingIndexItem" Method
                Providing a 0.1s tick delay so that the queueplayer will settle down its queue. Because in advance mode the queue player removes item after playing.
                GOTCHA: While replaying queueplayer each individual item needs to set to zero time. Otherwise EndItem Notification will be fired back to back.
             */
            
            if ((weakSelf?.queuePlayer?.items().count)! <= 0){
                // player has reached to its end.
                weakSelf?.playerItemsLimitReached();
            }else{
                let senderItem = sender.object as? AVPlayerItem;
                if ((senderItem == weakSelf?.queuePlayer?.currentItem)){
                    weakSelf?.updatePlayingIndexItem();
                }
            }
        }
    }
    @objc func playerItemTimeJumped(_ sender: Notification){
    }
    @objc func playerItemFailedToPlayTillEnd(_ sender: Notification){
    }
    @objc func playerItemPlaybackStalled(_ sender: Notification){
    }
    @objc func playerItemNewAccessLogEntry(_ sender: Notification){
    }
    @objc func playerItemNewErrorLogEntry(_ sender: Notification){
    }
    @objc func onApplicationDidEnterBackground(_ sender: Notification){
        self.resume = false;
        guard let del = self.neuralPlayerDelegate else{
            return;
        }
        del.neuralPlayer(resumePause: self.resume);
    }
    @objc func onApplicationWillResignActive(_ sender: Notification){
    }
}
