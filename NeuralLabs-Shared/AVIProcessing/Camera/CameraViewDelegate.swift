//
//  CameraViewDelegate.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 28/05/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import UIKit

public protocol CameraViewDelegate {
    
    func capturedImage(_ image: UIImage);
    func droppedImage(_ image: UIImage);
    
}
