//
//  CameraView.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 28/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation
import AVFoundation
import CoreMotion
import CoreFoundation

public enum CameraType{
    case Rear;
    case Front;
}

public enum CameraStatus{
    case Unknown;
    case Initializing;
    case Capturing;
    case Halted;
    case Stopped;
}

public enum CameraReturnType: Int{
    case Unknown = 0x00;
    case Image = 0x01;
    case Audio = 0x02;
    case Video = 0x04
    
    func isImage() -> Bool{
        let i = self.rawValue & CameraReturnType.Image.rawValue;
        return (i>0);
    }

    func isVideo() -> Bool{
        let i = self.rawValue & CameraReturnType.Video.rawValue;
        return (i>0);
    }

    func isAudio() -> Bool{
        let i = self.rawValue & CameraReturnType.Audio.rawValue;
        return (i>0);
    }
}

open class CameraView: NeuralBaseView, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureMetadataOutputObjectsDelegate {

    private var session: AVCaptureSession? = nil;
    private var frontCameraInput: AVCaptureDeviceInput? = nil;
    private var rearCameraInput: AVCaptureDeviceInput? = nil;
    private var videoDataOutput: AVCaptureVideoDataOutput? = nil;
    private var previewLayer: AVCaptureVideoPreviewLayer? = nil;
    private var stillImageOutput: AVCapturePhotoOutput? = nil;
    private var metaDataOutput: AVCaptureMetadataOutput? = nil;
    private var dataQueue: DispatchQueue? = nil;
    private var imgvDetector: UIImageView? = nil;
    
    public var cameraViewDelegate: CameraViewDelegate? = nil;
    public var type: CameraType = .Rear;
    public var status: CameraStatus = .Unknown;
    public var cameraReturnType: CameraReturnType = .Unknown;
    
    func loadCamera() {
        self.initializeSession();
        self.loadPreviewLayer();
        self.listenNotifications();
    }
    
    func authorisationStatus() -> AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatus(for: .video)
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews();
        self.previewLayer?.frame = self.layer.bounds;
    }
    
}


//MARK: Core Camera Functionality
fileprivate extension (CameraView) {
    func initializeSession() {
        if (self.session != nil){
            // session already initialized
            return;
        }
        
        self.initializeDataQueue();
        self.initializeVideoDataOutput();
        self.initializeMetaDataOutput();
        self.initializePhotoCaptureOutput();
        self.initializeCameraInputDevices();
        
        if ((self.rearCameraInput == nil) && (self.type == .Rear)){
            Logger.shared.log("Failed to load Rear Camera");
            return;
        }
        if ((self.frontCameraInput == nil) && (self.type == .Front)){
            Logger.shared.log("Failed to load Front Camera");
            return;
        }
        if (self.videoDataOutput == nil){
            Logger.shared.log("Failed to load Video Data ourput");
            return;
        }
        
        self.createAVSession();
    }
    
    func cleanupSession() {
        self.session = nil;
        self.frontCameraInput = nil;
        self.rearCameraInput = nil;
        self.videoDataOutput = nil;
        if (self.previewLayer != nil){
            self.previewLayer?.removeFromSuperlayer();
        }
        self.previewLayer = nil;
        self.stillImageOutput = nil;
        self.metaDataOutput = nil;
        self.dataQueue = nil;
        self.imgvDetector = nil;
    }
    
    func createAVSession() {
        self.session = AVCaptureSession();
        
        if (((self.session?.canAddInput(self.frontCameraInput!)) ?? false) && (self.type == .Front)){
            self.session?.addInput(self.frontCameraInput!);
        }
        if (((self.session?.canAddInput(self.rearCameraInput!)) ?? false) && (self.type == .Rear)){
            self.session?.addInput(self.rearCameraInput!);
        }
        if (self.session?.canAddOutput(self.videoDataOutput!) ?? false) {
            self.session?.addOutput(self.videoDataOutput!)
        }
        if (self.session?.canAddOutput(self.stillImageOutput!) ?? false) {
            self.session?.addOutput(self.stillImageOutput!)
        }
        if (self.session?.canAddOutput(self.metaDataOutput!) ?? false) {
            self.session?.addOutput(self.metaDataOutput!)
        }
        self.session?.sessionPreset = .high
    }
    
    func loadPreviewLayer() {
        
        if (self.session == nil){
            Logger.shared.log("Cannot load preview layer with no session");
            return;
        }
        
        if (self.previewLayer != nil){
            self.previewLayer?.removeFromSuperlayer();
            self.previewLayer = nil;
        }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session!);
        self.previewLayer?.frame = self.bounds;
        self.updatePreviewGravity();
        
        self.layer.addSublayer(self.previewLayer!);
    }
    
    func initializeDataQueue() {
        if (self.dataQueue != nil) {
            // Data Queue already Initialized;
            return;
        }
        self.dataQueue = DispatchQueue(label: "com.neural.neuralLabs.CameraView");
    }
    
    func initializeVideoDataOutput() {
        if (self.videoDataOutput != nil) {
            // Video Data Output already Initialized
            return ;
        }
        self.videoDataOutput = AVCaptureVideoDataOutput();
        self.videoDataOutput?.setSampleBufferDelegate(self, queue: self.dataQueue);
    }
    
    func initializeMetaDataOutput() {
        if (self.metaDataOutput != nil){
            // MetaData already Initialized
            return ;
        }
        self.metaDataOutput = AVCaptureMetadataOutput();
        self.metaDataOutput?.setMetadataObjectsDelegate(self, queue: self.dataQueue);
    }

    func initializePhotoCaptureOutput() {
        if (self.stillImageOutput != nil){
            // stillImageOutput already Initialized
            return ;
        }
        self.stillImageOutput = AVCapturePhotoOutput();
    }
    
    func initializeCameraInputDevices() {
        
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera, .builtInTelephotoCamera], mediaType: .video, position: .unspecified);
        
        do {
            for device in deviceDescoverySession.devices {
                if device.position == .back {
                    self.rearCameraInput = try AVCaptureDeviceInput(device: device);
                }else if device.position == .front{
                    self.frontCameraInput = try AVCaptureDeviceInput(device: device);
                }
            }
        }catch {
            Logger.shared.log("Failed to initialize camera input devices")
        }
    }
    
    func updatePreviewGravity(){
        switch(UIDevice.current.orientation){
        case .portrait:
            self.previewLayer?.connection?.videoOrientation = .portrait
            break;
        case .portraitUpsideDown:
            self.previewLayer?.connection?.videoOrientation = .portrait
            break;
        case .landscapeLeft:
            self.previewLayer?.connection?.videoOrientation = .landscapeRight
            break;
        case .landscapeRight:
            self.previewLayer?.connection?.videoOrientation = .landscapeLeft
            break;
        default:
            break;
        }
    }
}

//MARK: Core Imaging functions
extension (CameraView){
    func image(from sampleBuffer: CMSampleBuffer) -> UIImage? {
        
        // Get a CMSampleBuffer's Core Video image buffer for the media data
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else{
            Logger.shared.avFoundationLog("Unable to get image buffer from sample buffer")
            return nil;
        }

        CVPixelBufferLockBaseAddress(imageBuffer, [])

        let ciImage = CIImage(cvPixelBuffer: imageBuffer);
        let i = UIImage(ciImage: ciImage);
        CVPixelBufferUnlockBaseAddress(imageBuffer, [])
        return i;
        
        
//        // Lock the base address of the pixel buffer
//        CVPixelBufferLockBaseAddress(imageBuffer, [])
//
//        // Get the number of bytes per row for the pixel buffer
//        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
//
//        // Get the number of bytes per row for the pixel buffer
//        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)
//        // Get the pixel buffer width and height
//        let width = CVPixelBufferGetWidth(imageBuffer)
//        let height = CVPixelBufferGetHeight(imageBuffer)
//
//        // Create a device-dependent RGB color space
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//
//        var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
//        bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
//
//        let bitsPerComp = 8//CGImageGetBitsPerComponent(imageBuffer);
//
//        // Create a bitmap graphics context with the sample buffer data
//        guard let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: bitsPerComp, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo) else {
//            Logger.shared.avFoundationLog("Unable to create context");
//            CVPixelBufferUnlockBaseAddress(imageBuffer, [])
//            return nil;
//        }
//
//        // Create a Quartz image from the pixel data in the bitmap graphics context
//        let quartzImage = context.makeImage()
//        // Unlock the pixel buffer
//        CVPixelBufferUnlockBaseAddress(imageBuffer, [])
//
//        // Create an image object from the Quartz image
//        let image = UIImage(cgImage: quartzImage!)
//
//        CVPixelBufferUnlockBaseAddress(imageBuffer, [])
//        return image
    }
}

//MARK: delegates
extension (CameraView) {
    public func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if (self.cameraReturnType.isImage() && (self.cameraViewDelegate != nil)) {
            // Only work on images
            weak var weakSelf = self;
//            DispatchQueue.global(qos: .background).async {
                if let image = weakSelf!.image(from: sampleBuffer){
                    DispatchQueue.main.async {
                        weakSelf!.cameraViewDelegate?.capturedImage(image);
                    }
                }
//            }
        }
    }
    public func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if (self.cameraReturnType.isImage() && (self.cameraViewDelegate != nil)) {
            // Only work on images
            weak var weakSelf = self;
            DispatchQueue.global(qos: .background).async {
                if let image = weakSelf!.image(from: sampleBuffer){
                    weakSelf!.cameraViewDelegate?.droppedImage(image);
                }
            }
        }
    }
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
    }
}


//MARK: public dealing functions
public extension (CameraView) {
    
    func startCamera(type: CameraType){
        
        switch self.authorisationStatus() {
        case .denied:
            // user has denied the permission to access camera
            break;
        case .notDetermined:
            // user permission is not determinined
            break;
        case .restricted:
            // system has restricted app to access camera
            break;
        default:
            break;
        }
        
        self.type = type;
        self.status = .Initializing;
        self.loadCamera();
        
        self.session?.startRunning();
    }
    
    func stopCamera(){
        self.removeNotifications();
        self.status = .Stopped;
    }
    
    func updateCameraType(type: CameraType) {
        self.removeNotifications();
        self.type = type;
    }
}



//MARK: Notifications
extension (CameraView) {
    
    func listenNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(onOrientationChanged(sender:)), name: nlDeviceOrientationChanged, object: nil);
    }
    
    func removeNotifications() {
        NotificationCenter.default.removeObserver(self, name: nlDeviceOrientationChanged, object: nil);
    }

    @objc func onOrientationChanged(sender: Notification){
        self.updatePreviewGravity();
    }
}
