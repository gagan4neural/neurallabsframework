//
//  RecorderView.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 17/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


public class RecorderObject {
    
    var audioData: Data?
    var dtRealStart: Date?
    var startLocation: TimeInterval = 0.0
    var endLocation: TimeInterval = 0.0
    var duration: TimeInterval = 0.0
    var audioSize: NSNumber?
    var strAddedDate: String?
    var selected = false
}

public final class RecorderView: NeuralBaseView {
    
    
    public func loadView() {
        
    }
}
