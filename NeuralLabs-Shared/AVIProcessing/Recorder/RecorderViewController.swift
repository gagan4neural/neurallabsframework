//
//  RecorderViewController.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 17/02/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation


public class RecorderViewController: NeuralBaseViewController {
    
    private let recorderView: RecorderView = RecorderView();
    private var imageView: NeuralBaseImageView? = nil;
    
    public override func viewDidLoad() {
        super.viewDidLoad();
        self.recorderView.loadView();
        
        let bgImage = BundleManager.applicationBackgroundFromBundle()
        self.imageView = NeuralBaseImageView(image: bgImage, highlightedImage: nil);
        self.imageView?.contentMode = .scaleAspectFill;
        self.imageView?.clipsToBounds = true;
        
        self.recorderView.backgroundColor = .clear;
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        self.recorderView.frame = self.view.bounds;
        self.recorderView.autoresizingMask = [.flexibleWidth, .flexibleHeight];
    }
}
