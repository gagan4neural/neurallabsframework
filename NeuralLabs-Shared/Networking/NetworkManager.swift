//
//  NetworkManager.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 23/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation

//TODO: Can we design a network optimizer kind of thing which will finetune the paging size and other things based on the network strength?
let defaultPagingSize: Int = 50;
let defaultRequestTimeout: TimeInterval = 120; // 120s for timeout


open class NetworkManager:NSObject, URLSessionDataDelegate, URLSessionDownloadDelegate, URLSessionStreamDelegate{

    public static let shared = NetworkManager();
    private var networkQueue: OperationQueue?
    
    // a queue maintaining pending network requests.
    private var networkRequestQueue: [NetworkRequest] = [];

    // Core of this class. Never try to make them as public.
    private var defaultSession: URLSession?
    private var defaultConfig: URLSessionConfiguration?
    private var ephemeralSession: URLSession?
    private var ephemeralConfig: URLSessionConfiguration?
    private var backgroundSession: URLSession?
    private var backgroundConfig: URLSessionConfiguration?

    public func initialize() {
        self.startNetworkQueue();
        self.startSessions();
        self.listenNotifications();
    }

    public func deInitialize() {
        self.removeNotifications();
        self.shutDownSessions();
        self.shutDownNetworkQueue();
    }
}


//MARK: Session Management
fileprivate extension (NetworkManager){
    // Core stuff for URLSession Management
    private func startSessions() {
        self.startDefaultSession()
        self.startEphemeralSession()
        self.startBackgroundSession()
    }
    
    private func shutDownSessions(){
        self.stopDefaultSession()
        self.stopEphemeralSession()
        self.stopBackgroundSession()
    }
    
    private func startDefaultSession() {
        defaultConfig = URLSessionConfiguration.default;
        defaultSession = URLSession(configuration: defaultConfig!, delegate: self, delegateQueue: self.networkQueue!)
    }

    private func stopDefaultSession() {
        defaultSession?.invalidateAndCancel();
        defaultSession = .none;
        defaultConfig = nil;
    }

    private func startEphemeralSession() {
        ephemeralConfig = URLSessionConfiguration.ephemeral;
        ephemeralSession = URLSession(configuration: ephemeralConfig!, delegate: self, delegateQueue: self.networkQueue!);
    }

    private func stopEphemeralSession() {
        ephemeralSession?.invalidateAndCancel();
        ephemeralSession = .none;
        ephemeralConfig = nil;
    }

    private func startBackgroundSession() {
        backgroundConfig = URLSessionConfiguration.background(withIdentifier: self.queueID());
        backgroundSession = URLSession(configuration: backgroundConfig!, delegate: self, delegateQueue: self.networkQueue!);
    }
    
    private func stopBackgroundSession() {
        backgroundSession?.invalidateAndCancel();
        backgroundSession = .none;
        backgroundConfig = nil;
    }
}


//MARK: Operation Queue
fileprivate  extension (NetworkManager){
    func startNetworkQueue() {
        self.networkQueue = OperationQueue();
        self.networkQueue?.name = self.queueID();
        self.networkQueue?.waitUntilAllOperationsAreFinished();
    }
    func shutDownNetworkQueue() {
        self.networkQueue?.cancelAllOperations();
        self.networkQueue = nil;
    }
    func queueID() -> String {
        let bundleID = BundleManager.shared.bundleIdentifier ?? "com.neural";
        let className = NSStringFromClass(NetworkManager.self);
        let id = bundleID + "." + className;
        return id;
    }
}


//MARK: Notifications
fileprivate extension (NetworkManager){
    func listenNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationDidEnterBackground(_:)), name: nlApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onApplicationWillResignActive(_:)), name: nlApplicationWillResignActive, object: nil)
    }
    func removeNotifications(){
        NotificationCenter.default.removeObserver(self, name: nlApplicationDidEnterBackground, object: nil);
        NotificationCenter.default.removeObserver(self, name: nlApplicationWillResignActive, object: nil);
    }
    @objc func onApplicationDidEnterBackground(_ sender: Notification){
    }
    @objc func onApplicationWillResignActive(_ sender: Notification){
    }
}


//MARK: Network Request Management
public extension (NetworkManager){
    func executeNetworkRequest(_ request:NetworkRequest){

        // check if there is a same request is processing
        let req = self.networkRequestQueue.first(where: {$0.url == request.url})
        if (req != nil) || (req?.isCompleted() == false){
            Logger.shared.log("Invalid Request: Another request with same URL is already in progress. Please wait untill it completes : ");
            return;
        }
        
        // check if request is valid or not.
        if (request.isValidRequest() == false){
            Logger.shared.log("Invalid Request ", request.debugDescription());
            return;
        }
        
        // create a valid task for request.
        guard let task = self.createDataTaskFor(request: request) else {
            Logger.shared.log("Failed to create task for request");
            return;
        }
        request.assignDataTask(task);
        
        self.networkRequestQueue.append(request);
        task.resume();
    }
    
    private func createDataTaskFor(request: NetworkRequest) -> URLSessionDataTask?{
        weak var weakSelf = self;
        let task = self.defaultSession?.dataTask(with: request.urlRequest!, completionHandler: { (data, urlResponse, error) in
            weakSelf?.newResponseArrived(data: data, urlResponse: urlResponse, error: error, forRequest: request);
        });
        return task;
    }
    
    private func clearCompletedRequests() {
        self.networkRequestQueue.removeAll(where: {$0.isCompleted() == true});
    }
}

//MARK: Network Response Management
public extension (NetworkManager){
    
    func newResponseArrived(data: Data?, urlResponse: URLResponse?, error: Error?, forRequest req:NetworkRequest) {
        let nResponse = NetworkResponse(data: data, urlResponse: urlResponse, error: error);
        weak var weakSelf = self;
        DispatchQueue.main.async {
            req.assignNetworkResponse(response: nResponse);
            weakSelf?.clearCompletedRequests();
        }
    }
}

//MARK: Data Task Delegates
extension (NetworkManager){

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
    }

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome downloadTask: URLSessionDownloadTask){
        
    }

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didBecome streamTask: URLSessionStreamTask){
        
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data){
        
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void){
        
    }
}

//MARK: Download Task Delegates
extension (NetworkManager){
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL){
        
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64){
        
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64){
        
    }
}

//MARK: Stream Task Delegates
extension (NetworkManager){

    public func urlSession(_ session: URLSession, readClosedFor streamTask: URLSessionStreamTask){
        
    }

    public func urlSession(_ session: URLSession, writeClosedFor streamTask: URLSessionStreamTask){
        
    }

    public func urlSession(_ session: URLSession, betterRouteDiscoveredFor streamTask: URLSessionStreamTask){
        
    }
    
    public func urlSession(_ session: URLSession, streamTask: URLSessionStreamTask, didBecome inputStream: InputStream, outputStream: OutputStream){
        
    }
}



//MARK: Image Download
extension (NetworkManager){
    func downloadImage(url: String, callback:@escaping NetworkResponseImageCallback){
        let req = NetworkRequest.requestForURL(url) { (response) in
            if (response.error != nil){
                callback(nil, response.error?.localizedDescription);
                return;
            }
            if (response.data == nil){
                callback(nil, "No Image data received from url");
                return;
            }
            
            let img = UIImage(data: response.data!)
            callback(img, nil);
        };
        self.executeNetworkRequest(req);
    }
}
