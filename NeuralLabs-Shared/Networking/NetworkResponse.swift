//
//  NetworkResponse.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 23/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation

open class NetworkResponse{
    
    public var error: Error? = nil;
    private var urlResonse: URLResponse? = nil;
    public var data: Data? = nil;
    private var httpUrlResponse: HTTPURLResponse? {
        get{
            return self.urlResonse as? HTTPURLResponse;
        }
    }
    weak public var request: NetworkRequest? = nil;
    
    
    convenience init(data d:Data?, urlResponse ur:URLResponse?, error e:Error?){
        self.init();
        self.urlResonse = ur;
        self.data = d;
        self.error = e;
    }
    
    var url: URL? {
        get{
            return self.urlResonse?.url;
        }
    }
    
    var strData: String? {
        get{
            if (self.data == nil){
                return nil;
            }
            return String(data: self.data!, encoding: .ascii);
        }
    }
    
    var dictData: [String: Any]?{
        get{
            if (self.data == nil){
                return nil;
            }
            let json = try? JSONSerialization.jsonObject(with: self.data!, options: .mutableContainers);
            return json as? [String: Any];
        }
    }
    var arrData: [Any]?{
        get{
            if (self.data == nil){
                return nil;
            }
            let json = try? JSONSerialization.jsonObject(with: self.data!, options: .mutableContainers);
            return json as? [Any];
        }
    }
    var statusCode: Int? {
        get{
            return self.httpUrlResponse?.statusCode;
        }
    }
}



// HTTP Status Code
public extension (NetworkResponse) {
    func isSuccess() -> Bool{
        return (self.statusCode == API_REQUEST_SUCCESS)
    }
    func isDataModified() -> Bool{
        return (self.statusCode == API_REQUEST_NOT_MODIFIED)
    }
}
