//
//  NetworkRequest.swift
//  NeuralLabsFramework
//
//  Created by Gagan on 23/12/18.
//  Copyright © 2018 NeuralLabs. All rights reserved.
//

import Foundation


let API_HEADER_FIELD_NONE_MATCH :String = "If-None-Match"
let API_HEADER_FIELD_ETAG :String = "Etag"
let API_REQUEST_SUCCESS :Int = 200
let API_REQUEST_NOT_MODIFIED :Int = 304


public enum RequestType{
    case GET,POST,DELETE;
}

public enum RequestMode{
    case SYNC, ASYNC;
}

public enum RequestExecutionType{
    case Default, Private, Background;
}

open class NetworkRequest{
    
    //MARK: Request URLs
    var strURL: String?;
    private var _url: URL? = nil;
    var url: URL? {
        get{
            if (_url != nil){
                return _url
            }else{
                let urlString = self.strURL?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                if let u = URL(string: urlString ?? ""){
                    return u;
                }
            }
            return nil;
        }set{
            _url = newValue;
        }
    }
    var urlRequest: URLRequest?{
        get{
            var urlRequest: URLRequest? = nil;
            if let u = self.url{
                urlRequest = URLRequest(url: u, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: self.requestTimeout);
                if (self.eTag != nil){
                    urlRequest?.addValue(self.eTag!, forHTTPHeaderField:API_HEADER_FIELD_NONE_MATCH);
                }
            }
            return urlRequest;
        }
    }
    
    var eTag: String? = nil; // to be set in headers
    var requestType: RequestType = .GET;
    var requestMode: RequestMode = .ASYNC // currently all request are supposed to asynchronous.
    var priority:Int = 1 // as the number increases the prioirty decreases
    var requestTimeout: TimeInterval = defaultRequestTimeout;
    var executionType: RequestExecutionType = .Default;
    var pageSize: Int = defaultPagingSize;
    var hasMoreData: Bool = false; // more pages needs to some
    
    var paramaters: [AnyHashable:Any] = [:];
    
    
    //MARK: Response
    var responseCallback: NetworkResponseCallback? = nil;
    
    
    //MARK: Data Task
    private var _dataTask: URLSessionDataTask? = nil;
    func assignDataTask(_ task:URLSessionDataTask){
        _dataTask = task;
    }
    
    
    //MARK: Response
    private var _networkResponse: NetworkResponse? = nil;
    func assignNetworkResponse(response: NetworkResponse){
        _networkResponse = response;
        _networkResponse!.request = self;
        if (self.responseCallback != nil){
            self.responseCallback!(_networkResponse!);
        }
    }
    


    func debugDescription() -> String{
        let msg = "NetworkRequest Msg: {URL: \(String(describing: strURL))}, {RequestType:\(requestType)}, {RequestMode:\(requestMode)}, {Priority:\(priority)}"
        return msg;
    }

}

//MARK: User Interaction Methods
public extension (NetworkRequest){
    func cancelRequest() {
        self._dataTask?.cancel();
        self._dataTask = nil;
    }
    func isCompleted() -> Bool{
        return (self._dataTask?.state == .completed)
    }
    func isRunning() -> Bool{
        return (self._dataTask?.state == .running)
    }
    func isSuspended() -> Bool{
        return (self._dataTask?.state == .suspended)
    }
}

//MARK: Request Factory
public extension (NetworkRequest){
    static func requestForURL(_ url: String, responseCallback callback: @escaping NetworkResponseCallback) -> NetworkRequest{
        let request = NetworkRequest();
        request.strURL = url;
        request.responseCallback = callback;
        return request;
    }
}

//MARK: Validations
public extension (NetworkRequest){
    func isValidRequest() -> Bool{
        if (self.strURL == nil){
            return false;
        }
        if (self.url == nil){
            return false;
        }
        if (self.urlRequest == nil){
            return false;
        }
        return true;
    }
}
