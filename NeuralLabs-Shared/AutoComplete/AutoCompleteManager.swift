//
//  AutoCompleteManager.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 14/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation

public typealias AutoCompleteResultsCallback = ([AutoCompleteResult], ErrorString?) -> Void

open class AutoCompleteManager{
    
    public static let shared = AutoCompleteManager();
    private var autoCompleteResults: [AutoCompleteResult] = [];
    private let baseURL: String = "https://suggestqueries.google.com/complete/search?client=youtube&ds=yt&alt=json&q="
    
    public func search(text: String, callback:@escaping AutoCompleteResultsCallback){
        
        var numberOfResultsToFetch: Int = 5;
        let localResults = self.fetchLocalResults();
        numberOfResultsToFetch = numberOfResultsToFetch - localResults.count;
        
        self.fetchHostResults(text, completionHandler: callback);
    }
    
    private func fetchLocalResults() -> [AutoCompleteResult]{
        return [];
    }
    
    private func fetchHostResults(_ term: String, completionHandler: @escaping AutoCompleteResultsCallback) -> Void {
        
        weak var weakSelf = self;
        
        DispatchQueue.global().async {
            let URLString = (weakSelf?.baseURL)! + term
            
            guard let url = URL(string: URLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "") else {
                completionHandler([], "Unable to parse auto complete URL.")
                return
            }
            
            guard let data = try? Data(contentsOf: url) else {
                completionHandler([], "No AutoComplete data.")
                return
            }
            
            guard let response = String(data: data, encoding: String.Encoding.ascii) else {
                completionHandler([], "No AutoComplete response")
                return
            }
            
            var JSON: NSString?
            let scanner = Scanner(string: response)
            
            scanner.scanUpTo("[[", into: nil) // Scan to where the JSON begins
            scanner.scanUpTo(",{", into:  &JSON)
            
            guard JSON != nil else {
                completionHandler([], "Auto Complete JSON Parsing failed")
                return
            }
            
            //The idea is to identify where the "real" JSON begins and ends.
            JSON = NSString(format: "%@", JSON!)
            
            do {
                let array = try JSONSerialization.jsonObject(with: JSON!.data(using: String.Encoding.utf8.rawValue) ?? Data(), options: .allowFragments)
                var result:[AutoCompleteResult] = []
                
                for i in 0 ..< (array as AnyObject).count {
                    for j in 0 ..< 1 {
                        let suggestion = ((array as AnyObject).object(at: i) as AnyObject).object(at: j)
                        if let str = suggestion as? String {
                            let res = AutoCompleteResult(title: str, description: nil);
                            weakSelf?.autoCompleteResults.append(res)
                            result.append(res)
                        }
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    completionHandler(result,nil)
                })
            }
            catch {
                DispatchQueue.main.async(execute: {
                    completionHandler([], "Error in fetching google autocomplete results.")
                })
            }
        }
        
    }
}
