//
//  AutoCompleteResponse.swift
//  NeuralLabs-iOS
//
//  Created by Gagan on 14/01/19.
//  Copyright © 2019 NeuralLabs. All rights reserved.
//

import Foundation

public class AutoCompleteResult{
    
    public var title: String?;
    public var description: String?;
    
    init(title : String?, description: String?){
        self.title = title;
        self.description = description;
    }
    
}
